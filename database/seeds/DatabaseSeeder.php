<?php

use Illuminate\Database\Seeder;
use App\Models\Actualites;
use App\Models\Communiques;
use App\Models\Evenements;
use App\Models\References;
use App\Models\Presse;
use App\Models\Product;
use App\Models\Subject;
use App\Models\Pays;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//         $this->call(AllSeeder::class);
         $this->call(FeedbackSeeder::class);
    }
}
class AllSeeder extends Seeder {

    public function run()
    {
        // DB::table('Product')->delete();
        // DB::table('Presse')->delete();
        // DB::table('References')->delete();
        // DB::table('Communiques')->delete();
        // DB::table('Evenements')->delete();
        // DB::table('Actualites')->delete();
        User::create([
            'name'      => 'admin',
            'email'     => 'admin@admin.com',
            'password'  => bcrypt('admin'),
        ]);
        Product::create([
            'title'             =>  'Ansible',
            'image'             =>  '/img/produits/Ansible_reference_logo.png',
            'quote'             =>  'Ansible, société basée en Caroline du Nord, Etats-Unis, a développé une solution innovante et open source dautomatisation.',
            'notre_offre'       =>  '<p>La solution Ansible est très puissante, permettant le management des configurations complexes et industrialisant les déploiements, pour un résultat &quot;DevOps made simple&quot;.</p><p>Ansible a déjà conquis de nombreuses organisations, y compris les plus exigeantes, grâce à ses atouts : &quot;AgentLess&quot;, &quot;Human readable automation&quot;, &quot;workflow orchestration&quot;, &quot;security&quot;, &quot;control delegation&quot; ... et est compatible avec tout l\'écosystème Cloud, dont Docker, OpenStack ou Amazon...</p>	',
            'fiche_detaillee'   =>  '<h2>Ansible en détail</h2><p>					                <strong>Version étudiée : </strong>xx</p><p>		                            <strong>Site web : </strong>		                            <a class="site" href="https://www.ansible.com/" title="https://www.ansible.com/" target="_blank">https://www.ansible.com/</a></p><p>Ansible, société basée en Caroline du Nord, Etats-Unis, a développé une solution innovante et open source d\'automatisation.</p><p><a href=\'http://www.open-source-guide.com/Solutions/Infrastructure/Deploiement-et-sauvegarde/Ansible\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '1'
        ]);
        Product::create([
            'title'             =>  'Elastic',
            'image'             =>  '/img/produits/Elasticsearch_produit_full.png',
            'quote'             =>  'Elastic est une technologie innovante basée sur Apache Lucene, complètement intégrée à l\'écosysteme big data / Hadoop. Elastic propose de nombreux services à forte valeur ajoutée au travers des produits elasticsearch, logstach, kibana, et plus récemment beats, watcher ou shield',
            'notre_offre'       =>  '  <p>Elasticsearch a été créé en 2012 et dispose de deux maisons mères : à Amsterdam (Hollande) et à Los Altos (USA, Californie). Elasticsearch est composé aujourd’hui d’environ 100 salariés à travers le monde.</p><p>Elasticsearch,</p><p>Inc. Corporate est représenté à travers 3 projets Open Source :</p>
<ul>

<li><b>Elasticsearch</b> : moteur de recherche distribué, intégrant une base de données NoSQL, et RESTful, basé sur le moteur Apache Lucene.</li>

<li><b>Logstash</b> : outil de collecte, analyse et stockage de logs</li>

<li><b>Kibana</b> : interface web permettant de rechercher des infos stockées par Logstash dans ElasticSearch</li>

</ul>
<p>Quelques chiffres traduisent le formidable succès de la technologie Elastic : Plus de 8 millions de téléchargements depuis sa sortie / Moyenne &gt;500 000 téléchargements par mois /Croissance multipliée par 4 sur les 6 derniers mois</p><p>Cette croissance importante a permis à Elasticsearch de lever $70 millions de fonds ce qui porte les investissements à plus de 100 millions de $.</p><p>Smile possède depuis quelques années une <b>expertise reconnue sur l’analyse, la conception, l’implémentation et la maintenance de projets avec Elastic</b>, que ce soit au travers de projets autonomes sur de la recherche/indexation pure, ou pour des projets ecommerce.</p><p>Smile a notamment réalisé <b>Magento Elastic Suite</b>, une suite de modules Magento basés sur Elastic, avec de nombreuses fonctionnalités dédiées à booster la performance ecommerce.</p><table class="renderedtable" border="0" cellpadding="2" cellspacing="0" width="100%">
<tr>
<td class=" text-center" valign="top">  <h4>Pertinence de la recherche full-text (avec Elastic)</h4><p class=" text-left">Si vous suivez notre actualité Magento et eCommerce, il ne vous aura pas échappé que nous avons récemment mis en ligne un nouveau moteur de recherche basé sur ElasticSearch. <a href="http://blog.smile.fr/Pertinence-de-la-recherche-fulltext-et-ecommerce">Lire la suite sur notre blog</a>
</p>
  </td>
</tr>

</table>',
            'fiche_detaillee'   =>  '<h2>Elastic en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	0.90.5				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.elasticsearch.org/download/" title="http://www.elasticsearch.org/download/" target="_blank">http://www.elasticsearch.org/download/</a>
		                        </p><p>	<strong>Distribuée par : </strong> Editeur 		(Elasticsearch)		             						                					           		</p>   <p><strong>Licence : </strong>
			                						                						                						                							                			Apache
				                							                							                					                					            	</p>			            				            					                <p>	 					                					                		<strong>Technologie : </strong>
			                						                					            							            Java  						        </p>				        					        						       <p><strong>Année de création : </strong>2010 </p> <p>Elasticsearch est un moteur de recherche distribué basé sur Lucene.</p><p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Moteurs-de-recherche/Elasticsearch\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '1'
        ]);

        Product::create([
            'title'             =>  'Hadoop',
            'image'             =>  '/img/produits/Hadoop_produit_full.png',
            'quote'             =>  'Hadoop est un ensemble de projets et d’outils Open source de la fondation Apache permettant de stocker et traiter massivement des données. Hadoop a été développé à l’origine par Facebook et Yahoo, et est maintenant au coeur de l\'innovation et de l\'écosysteme bigdata.',
            'notre_offre'       =>  ' <p>Hadoop est un ensemble de projets et d’outils Open source de la fondation Apache permettant de stocker et traiter massivement des données. Hadoop a été développé à l’origine par Facebook et Yahoo, et est maintenant au coeur de l\'innovation et de l\'écosysteme bigdata.</p><p>
Il existe plusieurs distributions d’Hadoop, parmi lesquelles on distinguera les principales : Hortonworks, Cloudera et MapR.<br />L’ensemble Hadoop fournit plusieurs briques puissantes pour le Big Data :</p>
<ul>

<li>l’entreposage de données opérationnelles / ODS (HDFS ou Hbase) ou en entrepôt de données (Hbase et Hive).</li>

<li>l’intégration et le traitement parallélisé de données (YARN/Map-Reduce, Pig)</li>

<li>le requêtage et l’analyse de masses de données (Hive+YARN/Map-Reduce, Pig)</li>

<li>le datamining (Mahout)</li>

</ul>
<p>Notons que les principaux portails décisionnels Open Source intègrent directement un connecteur Hive pour une exploitation des données traitées dans un cluster Hadoop.</p><table class="renderedtable" border="0" cellpadding="2" cellspacing="0" width="100%">
<tr>
<td valign="top">  <h4>A lire sur le blog des experts de Smile</h4><p>
<a href="http://blog.smile.fr/Comparatif-des-interfaces-sql-d-exploitation-d-entrepots-de-donnees-big-data-nosql">&gt;&gt; Comparatif des interfaces SQL d\'exploitation d\'entrepôts de données Big Data/NoSQL</a>
<br />A l\'heure où de plus en plus d\'entreprises déploient ou migrent leur entrepôt de données sur Hadoop, il nous a paru intéressant de partager un comparatif des outils d\'accès aux données en langage SQL, qui reste à ce jour une référence. Nous présentons ici un comparatif synthétique des briques Big Data Apache Hive et Apache Drill face à MySQL sur les fonctionnalités de lecture des données.</p><p>
<a href="http://blog.smile.fr/Analyser-l-usage-de-sites-web-a-fortes-volumetries-en-sql-avec-mongodb-et-hadoop-hive">&gt;&gt; Analyser l\'usage de sites web à fortes volumétries en SQL avec MongoDB et Hadoop HIVE</a>
<br />Les logs de sites web constituent une matière première, facilement disponible et riche en informations sur l\'usage des plate-formes web. Mais cette matière est régulièrement gaspillée. </p><p>
<a href="http://blog.smile.fr/Hadoop-2-0-mapreduce-devient-yarn-et-propose-de-nouvelles-fonctionnalites">&gt;&gt; Hadoop 2.0 : MapReduce devient YARN et propose de nouvelles fonctionnalités</a>
<br />La récente release d\'Hadoop 2.0 amène son lot de nouveautés, parmi lesquelles on notera la présence de YARN (Yet Another Resource Negotiator). </p>
  </td>
</tr>

</table>',
            'fiche_detaillee'   =>  '<h2>Hadoop en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	1.1.01.0				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://hadoop.apache.org" title="http://hadoop.apache.org" target="_blank">http://hadoop.apache.org</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Fondation
				                						                			(Apache )
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			Apache
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>

<p>Hadoop est aujourd’hui la plateforme de référence permettant l’écriture d’application de stockage et de traitement de données distribuées en mode batch.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Big-data/Hadoop\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>
                        					',
            'lines'             =>  '2'
        ]);

        Product::create([
            'title'             =>  'Drupal Commerce',
            'image'             =>  '/img/produits/Drupal-Commerce_produit_full.jpg',
            'quote'             =>  '<p>Smile et Commerce Guys sont partenaires depuis la création de cette dernière. Nous disposons de plusieurs experts pour vous aider dans la création de votre site sous Drupal Commerce :</p>
<ul>

<li>Equipe de Direction Technique pour la validation de la perennité et qualité de</li>

<li>Drupal Commerce (audit technique + fonctionnel)</li>

<li>Equipes de dev : plateaux Drupal (150 personnes dans le groupe)</li>

<li>Equipes E-Business &amp; E-Commerce : 10 experts métier</li>

</ul>',
            'notre_offre'       =>  ' <p>Sortie en août 2011, Drupal Commerce est la branche e-commerce du CMS mondialement connu, Drupal. Successeur de l\'extension Ubercart, Drupal Commerce bénéficie du support de son éditeur Commerce Guys pour la gestion de la roadmap, l\'animation de la communauté et les offres de support.</p><p>Si à l\'heure actuelle le périmètre fonctionnel de Drupal Commerce reste un peu moins riche que ses aînés, c\'est la seule solution avec RBSChange à offrir de vraies fonctionnalités de gestion de contenus en s\'appuyant sur l\'efficacité reconnue de Drupal. A l\'image du CMS, Drupal Commerce est par ailleurs une solution extrêmement modulaire qui bénéficie d\'une communauté trés active.</p><p>Drupal Commerce est distribuée sous licence GPL v3.</p><p>Drupal Commerce est développé intégralement en PHP/MySQL. Il est fourni sous forme de module pour Drupal 7.</p>
					</div>
				</div>
				<div class="block_with_tabs--content with-border" data-id="fiche" style="display: none;">
					<div class="rte_ctnt_block">
													<h2>Drupal Commerce en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	1.8				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.drupalcommerce.org    " title="http://www.drupalcommerce.org    " target="_blank">http://www.drupalcommerce.org    </a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(Commerce Guys)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			GPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2011					            </p>

<p>Sortie en août 2011, Drupal Commerce est la branche e-commerce du CMS mondialement connu, Drupal. Successeur de l\'extension Ubercart, Drupal Commerce bénéficie du support de son éditeur Commerce Guys pour la gestion de la roadmap, l\'animation de la communauté et les offres de support.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/E-commerce/Drupal-commerce\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'fiche_detaillee'   =>  '	<h2>Drupal Commerce en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	1.8				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.drupalcommerce.org    " title="http://www.drupalcommerce.org    " target="_blank">http://www.drupalcommerce.org    </a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(Commerce Guys)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			GPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2011					            </p>

<p>Sortie en août 2011, Drupal Commerce est la branche e-commerce du CMS mondialement connu, Drupal. Successeur de l\'extension Ubercart, Drupal Commerce bénéficie du support de son éditeur Commerce Guys pour la gestion de la roadmap, l\'animation de la communauté et les offres de support.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/E-commerce/Drupal-commerce\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '2'
        ]);

        Product::create([
            'title'             =>  'Akeneo',
            'image'             =>  '/img/produits/Akeneo_produit_full.png',
            'quote'             =>  '<p>En à peine 4 ans, <b>Akeneo</b> s\'est hissé en tête des solutions de <b>Product Information Management</b> (PIM).&nbsp;</p>',
            'notre_offre'       =>  ' <p>Créée&nbsp; par une équipe franco-américaine (dont Yoav Kutner - co-fondateur et ancien CTO de Magento), Akeneo se dote en 2016 de nouvelles fonctionnalités et d’une interface plus collaborative, plus facile d’utilisation et mieux adaptée aux besoins des entreprises.&nbsp;&nbsp;De plus en plus d’acteurs du E-commerce mondial profitent de cette solution open source pour&nbsp;<b>collecter</b>,&nbsp;<b>enrichir</b>&nbsp;et&nbsp;<b>diffuser</b>&nbsp;les informations de leurs catalogues produits. Qu\'importe le profil des contributeurs pourvu que la complétude soit assurée par le PIM ! Ainsi, pour les utilisateurs aux profils Marketing, financiers, techniques, commerciaux, ... la simplicité d\'utilisation se conjugue avec le contrôle des données diffusées.</p><p>De la création (ou de l’import) des données jusqu’à l’export des informations produits vers un site marchand, une application mobile, un support imprimé ou encore un point de vente, les utilisateurs d’une même instance travaillent facilement à la production de&nbsp;<b>contenus de qualité</b>. Et l\'excellence du contenu étant un pilier du SEO, les utilisateurs trouveront en Akeneo un allié solide pour arriver en pole position des moteurs de recherche.</p><p><b>Smile</b>,<b>&nbsp;partenaire Gold d’Akeneo</b>, accompagne ses clients dans&nbsp; l’intégration du PIM et dans la réussite de projets comme&nbsp;<a href="http://eram.fr/" target="_blank">Eram.fr</a>
,&nbsp;<a href="http://camif.fr/" target="_blank">Camif.fr</a>
,&nbsp;<a href="http://store.lequipe.fr/" target="_blank">Store.lequipe.fr</a>
, etc.</p>			      ',
            'fiche_detaillee'   =>  '<h2>Akeneo en détail</h2>																		                <p>					                <strong>Version étudiée : </strong>
				                	1.4				            	</p>          				            					                <p>
		                            <strong>Site web : </strong>		                            <a class="site" href="http://www.akeneo.com/" title="http://www.akeneo.com/" target="_blank">http://www.akeneo.com/</a>
		                        </p>
			                			                			                	<p>				                	<strong>Distribuée par : </strong>				                					                		Editeur				                						                			(Akeneo)			                						                					           		</p>			       		 				       		 					                <p>				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			Autre open source
				                							                							                					                					            	</p>
			            				            					            <p>				                					                					                		<strong>Technologie : </strong>
			                						                							            							            	PHP							            						            						        </p>
					        					        						        <p>				                	<strong>Année de création : </strong>2013					            </p><p>Crée en 2012 par une équipe franco-américaine (dont Yoav Kutner - co-fondateur et ancien CTO de Magento), Akeneo est un outil de PIM (Product Information Management) open source destiné aux marchands souhaitant centraliser l\'ensemble de leurs informations produits en un seul et même point.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/Pim/Akeneo\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '1'
        ]);

        Product::create([
            'title'             =>  'GLPI',
            'image'             =>  '/img/produits/GLPI_produit_full.jpg',
            'quote'             =>  '<p>GLPI est une<b>&nbsp;solution d\'assistance et de gestion d\'inventaire de parc informatique open source. </b>Initialement GLPI était axé sur la gestion du parc informatique d\'une entreprise mais au fil des versions celui-ci a évolué vers la<b>&nbsp;gestion des contrats associées aux équipements</b>, la <b>gestion des licences</b>, l\'<b>assistance aux utilisateurs</b> associée à une <b>base de connaissances</b> et d\'autres fonctions annexes.</p><h3><b>Smile est partenaire Gold de GLPI.</b></h3>	 ',
            'notre_offre'       =>  '<h3>Gestion du parc informatique</h3><p>GLPI est capable de répertorier&nbsp;:</p><ul><li>les postes utilisateurs (ordinateurs portables, fixes, serveurs)</li><li>les moniteurs (lié aux ordinateurs de la catégorie précédente).</li><li>les logiciels installés (lié aux ordinateurs) avec une gestion des licences.</li><li>les matériels réseau (switchs, routeurs)</li><li>les périphériques (clavier, souris, etc).</li><li>les imprimantes</li><li>les cartouches/toner</li><li>les consommables&nbsp;: ceux-ci permettent d\'effectuer des attributions aux utilisateurs avec une gestion des stocks avancée.</li><li>les téléphones</li>
</ul><p>Chacun de ces éléments dispose de nombreux attributs&nbsp;: des attributs propres au type de matériel et des attributs plus génériques comme la localisation, le statut (en stock, attribué) ou encore l\'utilisateur associé.</p><p>GLPI peut également être associé aux logiciels<b>&nbsp;OCSng</b> et <b>FusionInventory</b> pour effectuer des remontées d\'inventaire automatiques sur les postes utilisateurs par l\'intermédiaire d\'agents installés en local.</p><h3>Assistance - Helpdesk</h3><p>Depuis de nombreuses versions, la composante Helpdesk de GLPI a fortement évoluée et dispose désormais de très nombreuses fonctions&nbsp;: <b>ouverture de tickets, suivi, définition de la typologie, priorité, statut, notifications par e-mail aux utilisateurs entièrement paramétrables , gestion des SLA avec escalade, etc</b>. GLPI se rapproche des bonnes pratiques ITIL&nbsp;: enquête de satisfaction, validation des solutions aux incidents/problèmes remontés, définition de la matrice de priorité, etc.</p><p>Grâce à sa composante inventaire, GLPI permet également de lier les tickets aux éléments de l\'inventaire et donc de connaître au moment de la saisie d\'un ticket quels matériels ou logiciels dispose l\'utilisateur.</p><p>GLPI dispose d\'une gestion des droits extrêmement souple permettant de laisser <b>la possibilité aux utilisateurs de saisir directement un ticket</b> (avec une interface simplifiée), ou de n\'accorder qu\'un droit de visualisation aux utilisateurs et de garder le centre d\'appel en tant que point d\'entrée pour la création de tickets.</p><h3>Intégration au SI et gestion des droits</h3><p><b>Une des grandes forces de GLPI est sa capacité à s\'intégrer au SI.</b> GLPI offre par défaut la possibilité de récupérer et synchroniser les comptes utilisateurs depuis un LDAP ou un Active Directory. Ceci permet de récupérer facilement la plupart des informations des utilisateurs&nbsp;: email, téléphone, fonction, etc.</p><p>Les profils et droits des utilisateurs peuvent ensuite découler des groupes d\'appartenance des utilisateurs dans l\'annuaire grâce à un moteur de règle flexible. Chaque utilisateur peut disposer de plusieurs profils sur différentes «&nbsp;entités&nbsp;» / composants de GLPI.</p><h3>Extensions GLPI</h3><p>GLPI offre un mécanisme de plugins lui permettant d\'étendre ses fonctions. On peut ainsi par exemple&nbsp;:</p>
<ul><li>ajouter des fonctions de gestion de projet</li><li>offrir une intégration à l\'outil de supervision Nagios</li><li>ajouter la gestion de nouveaux éléments d\'inventaire</li><li>ajouter une API webservices pour permettre à d\'autres outils d\'interagir avec GLPI (inventaire/helpdesk).</li></ul><p>Tous ces plugins sont regroupés sur une section dédiée du site officiel de GLPI&nbsp;: <a href="http://plugins.glpi-project.org/" target="_blank">http://plugins.glpi-project.org/</a>
</p><h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur <a href="http://www.open-source-guide.com/Solutions/Infrastructure/Gestion-de-parc-et-d-inventaires/Glpi" target="_blank">la solution&nbsp;GLPI</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>			          	                    ',
            'fiche_detaillee'   =>  '<h2>GLPI en détail</h2>																		                <p>
					                <strong>Version étudiée : </strong>				                	0.83.7				            	</p>
			            				            					                <p>		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.glpi-project.org" title="http://www.glpi-project.org" target="_blank">http://www.glpi-project.org</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Communauté
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			GPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2003					            </p>

<p>GLPI est un outil d\'inventaire de parc informatique et de Helpdesk, lancé en 2003 et porté par Julien Dombre, Jean-Mathieu Doléans et Bazile Lebeau.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Infrastructure/Gestion-de-parc-et-d-inventaires/Glpi\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '3'
        ]);

        Product::create([
            'title'             =>  'Solr',
            'image'             =>  '/img/produits/Solr_produit_full.png',
            'quote'             =>  '<p>Solr, la plateforme logicielle de recherche s\'appuyant sur le moteur de recherche Lucene.</p>	 ',
            'notre_offre'       =>  '<p>Solr est une surcouche de Lucene qui ajoute des fonctionnalités et facilite le déploiement de certaines fonctions de Lucene reconnues comme trop techniques. Son développement a été initié par CNET Networks lesquels ont décidé en 2006 de publier leur travail.</p><p>Solr est un serveur de recherche d\'entreprise permettant de centraliser les opérations d\'indexation et de services de résultats. Solr est capable de communiquer avec les autres applications via de nombreux protocoles basés sur des standards ouverts, il dispose également d’une interface d’administration en mode Web. L’une des caractéristiques majeures de Lucene est la capacité à indexer les contenus par champ, ou par attribut, c’est-à-dire qu’un document n’est pas analysé comme un simple ensemble de mots, il est constitué de champs, chaque champ étant une suite de mots (terms). Solr permet de tirer pleinement parti de cette fonctionnalité. Ce fonctionnement permet une gestion beaucoup plus fine de la pertinence et de la recherche avancée.&nbsp;</p><p>A noter également l\'amélioration de la distribution des traitements et des données à travers les fonctionnalités SolR Cloud apparues en version 4.0.</p><p>Solr est disponible sous la licence Apache.</p><h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur <a href="http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Moteurs-de-recherche/Solr" target="_blank">la solution&nbsp;Solr</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>	',
            'fiche_detaillee'   =>  '<h2>Solr en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	4.6				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://lucene.apache.org/solr" title="http://lucene.apache.org/solr" target="_blank">http://lucene.apache.org/solr</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Fondation
				                						                			(Apache)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			Apache
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2006					            </p>

<p>Solr est une surcouche de Lucene qui ajoute des fonctionnalités et facilite le déploiement de certaines fonctions de Lucene reconnues comme trop techniques. Son développement a été initié par CNET Networks lesquels ont décidé en 2006 de publier leur travail.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Moteurs-de-recherche/Solr\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '5'
        ]);

        Product::create([
            'title'             =>  'XWiki',
            'image'             =>  '/img/produits/XWiki_produit_full.png',
            'quote'             =>  'XWiki, le wiki applicatif de seconde génération.',
            'notre_offre'       =>  '<p>La solution XWiki a été créée en 2004 par Ludovic Dubost. Elle est aujourd’hui essentiellement supportée par la société <a href="http://www.xwiki.com" target="_blank">XWiki SAS</a>
.</p><p><a href="http://www.xwiki.com/lang/fr/Product/WebHome" target="_blank">Wiki applicatif de seconde génération</a>
, XWiki est utilisé pour le travail collaboratif, le partage d\'informations ou encore la gestion de contenus structurés ou non. En plus des fonctionnalités wiki traditionnelles (mise en forme facilitée, gestion des droits d\'accès, édition collaborative, historique des modifications, rollback…), il offre la possibilité de programmer au sein même des pages du wiki. Autrement dit, il est capable de s\'adapter et d\'évoluer en fonction des besoins de ses utilisateurs. Cette capacité est un véritable élément différenciateur par rapport à d\'autres outils collaboratifs.</p><p>XWiki est idéal pour bâtir une base de connaissance, un intranet collaboratif ou espace de veille sectorielle voire concurrentielle.</p><p>XWiki est développé en Java sur une base Hibernate. Les langages de programmation au sein du wiki sont Velocity et Groovy. Il dispose de plusieurs API et d’un système de plugins et de portlets.</p><p>Pour en savoir plus :</p>
<ul>

<li>Le site XWiki SAS : <a href="http://www.xwiki.com/lang/en/Home/WebHome" target="_blank">www.xwiki.com</a>
</li>

<li>Le site de la Communauté XWiki : <a href="http://www.xwiki.org" target="_blank">www.xwiki.org</a>
</li>

</ul>
<h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur la solution&nbsp;<a href="http://www.open-source-guide.com/Solutions/Applications/Blogs-wiki-et-forums/Xwiki" target="_blank">XWiki</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>		',
            'fiche_detaillee'   =>  '<h2>XWiki en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	4.3				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.xwiki.org" title="http://www.xwiki.org" target="_blank">http://www.xwiki.org</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(XWiki SAS)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			LGPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2004					            </p>

<p>La solution XWiki a été créée en 2004 par Ludovic Dubost. Elle est aujourd’hui essentiellement supportée par la société XWiki.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/Blogs-wiki-et-forums/Xwiki\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '4'
        ]);

        Product::create([
            'title'             =>  'ESIgate',
            'image'             =>  '/img/produits/ESIGate_produit_full.png',
            'quote'             =>  '<p>L\'agrégateur de contenus web&nbsp;open source, développé par Smile.</p>	  ',
            'notre_offre'       =>  '<p>ESI-Gate est un <b>agrégateur de contenus web</b> open source, développé par Smile. Il peut s\'interfacer à des serveurs existants, dont il récupère les pages Html à la volée. Les différentes bribes de contenus ainsi obtenues de différents serveurs sont ensuite assemblées en une page unique, servie à l\'internaute.</p><p>La grande force de cette approche est qu\'elle est totalement agnostique technologiquement, capable d\'intégrer n\'importe quelles applications web, sans demander la moindre modification de l\'existant.&nbsp;</p><p><b>ESI-Gate permet de construire un site en assemblant des pages, des blocs de contenu et des ressources provenant de différentes applications web</b>.&nbsp;ESI-Gate peut aussi être configuré pour extraire des bribes de contenus au sein des pages existantes. Dans ce cas, il est possible d\'insérer des commentaires Html, afin de parfaitement délimiter les blocs à extraire.</p>   <p>C\'est une technique qui a pour elle la simplicité et la non-ingérance. A certains égards, <b>ESI-Gate se comporte comme un portail web</b>, réunissant des informations de différentes origines. Mais c\'est un outil infiniment plus simple, plus performant et surtout moins structurant qu\'une solution portail.</p><p><b>Smile est à l\'initiative du projet ESI-Gate depuis le début et l\'a délivré en open source.</b> Depuis, Smile l\'a mis en œuvre avec succès sur une diversité de plateformes opérationnelles.</p><p>Pour en savoir plus : <a href="http://www.esigate.org/" target="_blank">www.esigate.org</a>
</p><h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur la solution&nbsp;<a href="http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Autres/Esigate" target="_blank">ESIgate</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>	',
            'fiche_detaillee'   =>  '	<h2>ESIgate en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	4.1				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.esigate.org    " title="http://www.esigate.org    " target="_blank">http://www.esigate.org    </a>
		                        </p>
			                			                			                	<p>				                	<strong>Distribuée par : </strong>
				                					                		Communauté				                						                					           		</p>			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						               						                							                			Apache
				                							                								                					                					            	</p>
			            				            					                <p>				                					                					                		<strong>Technologie : </strong>
			                						                							            							            	Java							            						            						        </p>
					        					        						        <p>				                	<strong>Année de création : </strong>

2008					            </p>
<p>ESIGate est un outil d’assemblage web. Il a été créé par plusieurs ingénieurs de Smile, premier intégrateur européen de solutions open source.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Autres/Esigate\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '3'
        ]);

        Product::create([
            'title'             =>  'Pentaho Data Integration',
            'image'             =>  '/img/produits/Pentaho-Data-Integration_produit_full.png',
            'quote'             =>  '<p>Pentaho Data Integration (PDI) est un ETL open source qui permet de concevoir et exécuter des opérations de manipulation et de transformation de données.</p>	',
            'notre_offre'       =>  '<p>Grâce à un modèle graphique à base d’étapes, il est possible de créer sans programmation des processus composés d’imports et d’exports de données, et de différentes opérations de transformation, telles que des conversions, des jointures, l’application de filtres, ou même l’exécution de fonctions Javascript.</p> <p>PDI permet de créer deux types de processus :</p>
<ul>

<li>Les transformations : traitements effectués au niveau d\'une ou plusieurs bases de données comprenant des opérations de lecture, de manipulation et d\'écriture. C’est à ce niveau que sont manipulées les données.</li>

<li>Les tâches : traitements de plus haut niveau, combinant des actions telles que l\'exécution d\'une transformation PDI, l\'envoi d\'un mail, le téléchargement d\'un fichier ou le lancement d\'une application. Il est possible d\'exécuter des actions différentes en fonction de la réussite ou de l\'échec de chaque étape. Le rôle d’une tâche est donc d’orchestrer les différents traitements.</li>

</ul>
<p>PDI est notamment utilisable avec un référentiel partagé. Ainsi, plusieurs développeurs peuvent utiliser des objets communs. Ce référentiel est stocké au sein d’une base de données relationnelle. Le développeur peut donc facilement s\'y connecter et changer de référentiel à sa guise.</p><p>PDI peut se connecter sur un grand nombre de bases de données, dont Oracle, Sybase, MySQL, PostgreSQL, Informix, SQLServer et bien d’autres. Ils disposent aussi d’une palette de connecteurs spécifiques pour certains logiciels du marché.</p>',
            'fiche_detaillee'   =>  '<h2>Pentaho Data Integration en détail</h2>													                <p>					                <strong>Version étudiée : </strong>				                	5.0				            	</p>			            				            					                <p>		                            <strong>Site web : </strong>		                            <a class="site" href="http://www.pentaho.fr/explore/pentaho-data-integration/" title="http://www.pentaho.fr/explore/pentaho-data-integration/" target="_blank">http://www.pentaho.fr/explore/pentaho-data-integration/</a>
		                        </p><p>			                	<strong>Distribuée par : </strong> 				                					                		Editeur 				                						                			(Pentaho)				                						                					           		</p>			       		 				       		 					                <p>
				                					                				                			<strong>Licences : </strong>
			            							                						                						                							                			Autre commerciale
				                							                			; 			                					                						                						                							                			LGPL
				                							                								                					                					            	</p>
			            				            					                <p>				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>

<p>Pentaho Data Integration (PDI), longtemps connu sous le nom de Kettle, est un ETL open source qui permet de concevoir et d’exécuter des opérations de manipulation et de transformation de données. Au moment où nous écrivons ces lignes, Pentaho Data Integration est disponible dans sa version 5.0.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Etl/Pentaho-data-integration\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '2'
        ]);
        Product::create([
            'title'             =>  'Talend ESB',
            'image'             =>  '/img/produits/Talend-ESB_produit_full.jpg',
            'quote'             =>  '<p>Talend fournit une plateforme unifiée permettant la gestion complète de l’intégration de données&nbsp;: ETL + ESB intégrés dans le même outil.</p>',
            'notre_offre'       =>  '<p>Talend ESB est bâti sur des standards Open Source et est composé de plusieurs briques logicielles.</p><h5>Talend ESB Studio</h5><p>Ce designer, basé sur une interface Eclipse, permet le développement des différents objets de la plateforme ESB :</p>
<ul>

<li>Intégration de données</li>

<li>Qualité de données</li>

<li>Gestion de données</li>

<li>Data Services</li>

<li>Routage de Messages</li>

</ul>
<p>Ce studio permet donc de créer des route Camel et des jobs classique d’intégration de données publiables également sous forme de service.</p><h5>La plateforme d’exécution</h5>
<ul>

<li>Apache CXF&nbsp;pour la gestion des webservices</li>

<li>Apache Camel pour la médiation</li>

<li>Apache ActiveMQ pour la gestion des messages</li>

<li>Apache Karaf comme runtime</li>

</ul>
<h5>Talend Administration Center</h5><p>La console d’administration de Talend un une interface web de gestion. Elle permet en outre&nbsp;:</p>
<ul>

<li>Le monitoring</li>

<li>Le déploiement de services à partie d’une interface web ou / et d’un API</li>

<li>La gestion des versions</li>

<li>C’est fonctionnalité sont pour la plupart disponible dans le studio</li>

<li>Etc.</li>

</ul>',
            'fiche_detaillee'   =>  '<h2>Talend ESB en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	5.4.1				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.talend.com/products/esb" title="http://www.talend.com/products/esb" target="_blank">http://www.talend.com/products/esb</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(Talend)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                				                			<strong>Licences : </strong>
			            							                						                						                							                			Apache
				                							                			; 					                					                						                						                							                			Autre commerciale
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2011					            </p>

<p>Talend, leader de la gestion de données avec son ETL et sa suite d\'intégration open source a ajouté à son catalogue un ESB pleinement intégré à son écosystème en acquérant l\'éditeur Sopera et ses clients en 2010.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Esb-eai/Talend-esb\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '1'
        ]);

        Product::create([
            'title'             =>  'Mule ESB',
            'image'             =>  '/img/produits/Mule-ESB_produit_full.png',
            'quote'             =>  '<p>Mule est une plateforme favorisant les échanges multiples entre différentes applications utilisant des protocoles de communication hétérogènes.</p>',
            'notre_offre'       =>  '<p>Edité par MuleSoft, Mule ESB dispose de trois briques principales&nbsp;:</p><h5>Mule ESB</h5><p>C’est la plateforme de gestion des services. Le ou les serveurs permettent de recevoir, de traiter, de transformer et de diffuser les messages définis dans les flux.</p><h5>Mule Studio</h5><p>MuleSoft fournit un designer (Eclipse) de flux. Il reste possible d’utiliser un autre éditeur XML mais ce studio facilite tout de même la visualisation et la création des flux.</p><h5>Mule Management Console</h5><p>Cette application Web simplifie le déploiement des flux et le monitoring de la plateforme en centralisant l’administration. Il offre également une interface d’audit des flux en production et un mécanisme d’alerte.</p><p>Mule supporte plus de 50 standards, protocoles et technologies, comme JMS, JDBC, TCP, UDP, Multicast, HTTP, servlet, SMTP, POP3, XMPP. Mule c’est également une plateforme d\'échanges qui possèdent de nombreuses capacités de routage. L’architecture de Mule est conçue pour être extensible.</p>',
            'fiche_detaillee'   =>  '<h2>Mule ESB en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	3.4				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.mulesoft.org " title="http://www.mulesoft.org " target="_blank">http://www.mulesoft.org </a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(MuleSoft Inc)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                				                			<strong>Licences : </strong>
			            							                						                						                							                			Autre open source
				                							                			; 					                					                						                						                							                			Autre commerciale
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2005					            </p>

<p>Mule ESB est un projet open source développé depuis 2005 par la société MuleSoft et son fondateur Ross Mason sur l\'idée que les connexions inter-applicatives doivent être faciles et rapides à mettre en oeuvre.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Esb-eai/Mule-esb\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '5'
        ]);

        Product::create([
            'title'             =>  'WSO2 ESB',
            'image'             =>  '/img/produits/WSO2-ESB_produit_full.jpg',
            'quote'             =>  '<p>WSO2 ESB est un projet open source permettant de mettre en relation des environnements hétérogènes en utilisant un modèle orienté service.</p>',
            'notre_offre'       =>  '<p>WSO2 est développé depuis 2005 par le Dr. Sanjiva Weerawarana et Paul Fremantle, anciens de chez IBM, créateurs notamment de Apache SOAP et WSIF (Web Services Invocation Framework).</p><p>WSO2 ESB&nbsp; repose sur la plateforme Carbon, implémentant les spécifications OSGi, commune à tous les produits de WSO2, modulaire, extensible et pouvant nativement être mise à l\'échelle. Il utilise les projets Apache Synsapse, pour la composante de médiation, et Apache Axis2, pour les web services, dont l\'éditeur est le principal contributeur.</p><p>Rapide et ayant une empreinte mémoire réduite, le bus de service d\'entreprise de WSO2 est hautement interopérable et supporte de nombreux transports, formats et protocoles comme POP3, JMS, AMQP, FIX, CIFS, MLLP, SMS, SOAP, REST, EDI, HL7, OAGIS, Hessian, CORBA/IIOP.</p><p>Contrairement à ses principaux concurrents, WSO2 ESB est 100% Open Source et ne possède pas de version premium payante. Son code source est librement accessible sur un dépôt SVN. La société se rémunère sur le support et les services de développement et production.</p>',
            'fiche_detaillee'   =>  '<h2>WSO2 ESB en détail</h2>																		                <p>
					                <strong>Version étudiée : </strong>				                	4.8.0				            	</p>
			            				            					                <p>		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.wso2.com" title="http://www.wso2.com" target="_blank">http://www.wso2.com</a>
		                        </p><p>				                	<strong>Distribuée par : </strong> 				                					                		Editeur
				                						                			(WSO2)				                						                					           		</p>
			       		 				       		 					                <p>				                					                					                		<strong>Licence : </strong>
			                						                						               						                							                			Apache
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2007					            </p>

<p>WSO2 ESB est un projet open source permettant de mettre en relation des environnements hétérogènes en utilisant un modèle orienté service. Il est développé depuis 2005 par le Dr. Sanjiva Weerawarana et Paul Fremantle, anciens de chez IBM, créateurs notamment de Apache SOAP et WSIF (Web Services Invocation Framework).</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Esb-eai/Wso2-esb\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '3'
        ]);

        Product::create([
            'title'             =>  'BlueMind',
            'image'             =>  '/img/produits/Blue-Mind_produit_full.png',
            'quote'             =>  '<p>BlueMind représente la nouvelle génération de messagerie collaborative Open Source.</p>',
            'notre_offre'       =>  '<p>Développée en France, BlueMind est <b>une solution complète de messagerie d’entreprise</b>,&nbsp;d’agendas et de travail collaboratif adaptée aux Entreprises, collectivités, administrations et&nbsp;universités de toutes tailles.</p><p>Son objectif est de proposer <b>une solution moderne, riche et agréable</b> pour les utilisateurs et administrateurs avec une ouverture maximale pour l\'intégration avec des systèmes ou&nbsp;applications externes (portail, gestion de&nbsp;congés, CRM,..). Les fonctionnalités proposées sont larges et gérées graphiquement (filtres, signatures, répondeur, agendas d\'utilisateurs, de groupes, de &nbsp;domaine ou de ressources, recherche de disponibilités, impressions PDF, multi-carnets d\'adresses...).</p><p>Dans un second temps l’utilisateur et les administrateurs ont le choix <b>d’accéder à des fonctions plus élaborées</b> et au besoin plus ponctuel. A partir de la v2 les fonctionnalités d\'infrastructure offertes par la console d\'administration graphique sont riches : distribution des différents services, haute disponibilité (à l\'installation), archivage de mails, sauvegarde incrémentale et historisée, restaurations globales ou unitaires, migration graphique de boîtes aux lettres entre backend, tâches planifiées, synchronisation LDAP et ActiveDirectory..</p><p>Pour les logiciels&nbsp; Thunderbird et Outlook, BlueMind fourni en plus des connecteurs permettant de synchroniser les contacts et calendrier (ou un affichage direct des calendriers pour Thunderbird). BlueMind supporte la synchronisation avec les mobiles de type iPhone, iPad, Android…</p><p>La solution repose sur une base de données principale et les fondations techniques employées par BlueMind sont toutes <b>au jour des dernières technologies</b>. Ainsi la solution propose nativement la gestion du mode web déconnecté, des périphériques mobiles, supporte les plugins serveur et propose une API SOAP complète et accessible permettant d’envisager le développement d’interfaces métier. Du reste, elle s’appuie sur les logiciels libres préexistants : Postfix, cyrus IMAP, Roundcube, Apache, Tomcat, nginx, HornetQ, PostgreSQL. Globalement, BlueMind propose aujourd’hui un produit ergonomique, esthétique, intelligent, <b>simple dans son utilisation</b> et son administration est sans fioritures. Il répondra à la plupart des besoins collaboratifs pour une entreprise.</p><p>Le produit est distribué sous licence Affero GPL, à l\'exception des connecteurs Outlook et ActiveDirectory fournis dans le cadre d\'une souscription à l\'éditeur BlueMind.</p><h4>Périmètre fonctionnel :</h4><p>
- Messagerie avancée : alias, répondeurs, redirections, groupes, partages et délégations, filtres,identités,..<br />- Agendas partagés : utilisateurs, groupes, ressources, agendas de domaine, recherche de disponibilités, délégations et partages, impressions PDF, vues personalisées</p><h4>Avantages de la solution Blue Mind :</h4><p>
- Offre de souscription unique comprenant l\'ensemble des fonctionnalités de la solution<br />
- Richesse fonctionnelle et évolutivité<br />
- <b>Simplicité</b> : installation, administration et mises à jour graphiques<br />
- <b>Ergonomie</b>: interfaces modernes, simples, intuitives et très réactives plébiscitées par les utilisateurs<br />
- <b>Innovation</b> : web de 3ème génération gérant le mode déconnecté directement dans le navigateur (sans pluggins)<br />
- <b>Infrastructure</b> : multitenants, multiserveurs, multidomaines, stockage hiérarchique, haute disponibilité, sauvegarde incrémentale<br />- <b>Ouverture</b> : plugin serveur, API webservices couvrant 100% du périmètre, un Message-oriented middleware (Mom) intégré, le tout exploitable via un SDK</p><h4>Points Clés</h4><p>● Messagerie, messagerie instantanée</p><p>● Messagerie instantanée</p><p>● Recherche full text</p><p>● Agendas, contacts, tâches partagées</p><p>● Espaces collaboratifs (documents, agendas, forums,..)</p><p>● Mobilité (smartphone, tablettes)</p><p>● La gestion des catégories (tags) dans l\'agenda et les contacts</p><p>● Clients Outlook et Thunderbird</p><p>● Mode Web Déconnecté</p><p>● RIA Full Ajax</p><p>● Web Services</p><p>● Plugins et SDK</p><p>● SSO et kerberos</p><p>● Support de CalDAV (synchronisation application MAC, certains mobiles...)</p>	',
            'fiche_detaillee'   =>  '<h2>BlueMind en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	2.0				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.blue-mind.net" title="http://www.blue-mind.net" target="_blank">http://www.blue-mind.net</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(Blue Mind)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			AGPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2012					            </p>

<p class=" text-left">Blue Mind est une solution complète de messagerie d’entreprise, &nbsp;d’agendas et de travail collaboratif, elle est la plus jeune des &nbsp;solutions de messagerie collaborative décrites dans ce guide. Disponible &nbsp;depuis 2012, cette solution est avant tout pensée pour ne proposer à &nbsp;l’écran que les fonctions essentielles tout en gardant un aspect épuré et moderne.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Infrastructure/Messagerie-emailing-groupware/Blue-mind\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '5'
        ]);

        Product::create([
            'title'             =>  'Symfony_produit_full.png',
            'image'             =>  '/img/produits/Symfony_produit_full.png',
            'quote'             =>  '<h3>Présentation avancée de Symfony2  </h3><p>Symfony2 est un framework PHP open source sous licence MIT, crée par l’éditeur français SensioLabs dont <b>Smile est partenaire Gold</b>.</p>',
            'notre_offre'       =>  '<p>Symfony 1.x ayant connu un succès notable, une refonte complète a donné naissance à la version 2.x qui tire partie des évolutions de PHP 5.3 : namespace, closure, etc.</p><p>Symfony2 est un ensemble de composants faiblement couplés réunis sous la forme d’une distribution.</p><p>
La standard édition offre donc un framework full-stack incluant entre autres un ORM (Doctrine 2), le moteur de templating Twig, la gestion des emails avec SwiftMailer, ou encore un composant de sécurité pour la gestion de l’authentification utilisateur et des permissions.<br />Le respect du protocole HTTP et du paradigme MVC sont au coeur du framework, qui fournit un contexte de développement complet avec un outil complet de débug. Le développeur a un accès uniformisé à la base de données, aux contrôleurs, aux vues, etc. Le framework fournit également une large bibliothèque de fonctions utilitaires. Du côté de la sécurité, Symfony fournit des protections contre les attaques classiques sur les applications Web (SQL injection, XSS, CSRF, ...).</p><p>C’est <b>un cadre de travail</b> pour les développements :</p>
<ul>

<li>Bonnes pratiques : test, isolation fonctionnelle, structure</li>

<li>Normes applicatives : PSR-0 ou 4, 1,2,3…</li>

<li>Supportant les dernières évolutions de PHP 5.5: namespace/closure/traits, Zend Opcode/APC</li>

<li>Gestion du cache&nbsp;: ESI, cache applicatif…</li>

</ul>
<p>Les dernières nouveautés de la solution : le composant Expression Language permettant de gérer un moteur de règles, profiling des formulaires, gestion des fragments (sub-request, ESI, HInclude).</p><p>Un ensemble de Bundles viennent compléter les usages possibles. Les composants Symfony2 servent de base à de nombreuses applications : Drupal 8, eZ Publish 5, Composer, phpBB, PIM Akeneo, OroCrm...</p> ',
            'fiche_detaillee'   =>  '<h2>Symfony en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	2.4				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http:// www.symfony.com" title="http:// www.symfony.com" target="_blank">http:// www.symfony.com</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(Sensio Labs)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			MIT
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>

<p>Symfony est un framework MVC écrit en PHP, supporté par l’éditeur SensioLabs et distribué sous la licence MIT. Symfony 1.x ayant connu un succès notable, une refonte complète a donné naissance à la version 2.x qui tire partie des évolutions de PHP 5.3 : namespace, closure, etc.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Frameworks-et-bibliotheques-pour-le-developpement-web/Symfony\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '5'
        ]);

        Product::create([
            'title'             =>  'Rubedo',
            'image'             =>  '/img/produits/Rubedo_produit_full.png',
            'quote'             =>  '<p><b>Rubedo</b>&nbsp;est une&nbsp;<b>solution innovante de gestion de contenus web dont le Back office est extrêmement ergonomique.</b></p>	  ',
            'notre_offre'       =>  '<p>Sa particularité est de proposer une&nbsp;<b>solution intégrée et évolutive</b>&nbsp;qui permet de&nbsp;<b>définir et bâtir progressivement des stratégies web à 360°</b>&nbsp;(sites web et mobiles, applications métiers, extranet clients, e-commerce…).</p><p>Rubedo se compose de plusieurs briques open-source matures et de qualité&nbsp;: Zend, MongoDB, Elasticsearch, Bootstrap, Twig…</p><p>&nbsp;</p><h3>Rubedo en quelques points</h3><p><b>La facilité d’utilisation de Rubedo représente sa principal qualité, il est, par exemple, possible de définir les blocs qui s’afficheront ou non sur les différents devices (PC, tablette, mobile)</b>.</p><p><b>Créer de nouveaux sites en quelques minutes&nbsp;: </b>Des&nbsp;<b>wizards conviviaux et intuitifs</b>&nbsp;permettent de créer de nouveaux sites web et mobiles en quelques minutes. Les capacités de paramétrage avancées offrent une grande liberté aux administrateurs/webmasters et permettent dans de nombreux cas de s’affranchir de toute ligne de code.</p><p><b>Une galaxie de sites qui ne nécessite pas de démultiplier vos efforts&nbsp;: </b>La force de Rubedo est de proposer des référentiels des contenus et médias afin de publier les contenus sans les dupliquer. Créer, publier sur les différentes pages des différents sites, modifier une seule fois et le contenu est actualisé en temps réel sur l\'ensemble des sites.</p><p>Du côté des développeurs, le socle technique de Rubedo (Zend Framework, MongoDB et Elasticsearch) permet également de réduire le temps de développement de nouvelles fonctionnalités.</p><p>Le back-office de Rubedo est conçu comme un&nbsp;bureau virtuel&nbsp;qui rassemble l’ensemble des fonctions d’administration. Il permet d’administrer au sein d’une&nbsp;interface d’administration unifiée&nbsp;:</p>
<ul>

<li>un&nbsp;référentiel&nbsp;de&nbsp;contenus&nbsp;éditoriaux (articles, actualités, …)</li>

<li>un&nbsp;référentiel&nbsp;de&nbsp;documents&nbsp;(images, sons, vidéos, fichiers PDF, fichiers bureautiques, …)</li>

<li>un&nbsp;référentiel&nbsp;de&nbsp;fonctionnalités&nbsp;(blocs) qui sont agencées dans les pages</li>

<li>une&nbsp;galaxie de sites&nbsp;internet/intranet/extranet</li>

<li>Un référentiel d’utilisateurs</li>

</ul>',
            'fiche_detaillee'   =>  '<h2>Rubedo en détail</h2>
																	            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.rubedo-project.org/" title="http://www.rubedo-project.org/" target="_blank">http://www.rubedo-project.org/</a>
		                        </p>
			                			                			       		 				            						        	                        					    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/Cms/Rubedo\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '4'
        ]);

        Product::create([
            'title'             =>  'Centreon',
            'image'             =>  '/img/produits/Centreon_produit_full.png',
            'quote'             =>  '<p>La complexité des Systèmes d\'Informations ne permet pas de garder un oeil en permanence sur l\'état de fonctionnement de chacun des éléments d\'une infrastructure. L\'objectif de la supervision est donc<b>&nbsp;d\'automatiser la surveillance</b> de l\'ensemble de ces composants, de<b>&nbsp;détecter les anomalies </b>et de<b>&nbsp;déclencher des alertes </b>lorsqu\'un changement d\'état ou une défaillance est avéré. La finalité est donc d\'<b>anticiper au maximum les problèmes</b> et de <b>minimiser les interruptions de services</b> en améliorant la réactivité des équipes d\'exploitation.</p>',
            'notre_offre'       =>  '<h3>Pourquoi utiliser Centreon ?</h3><p>Créé en 2003 sous le nom d\'Oreon, Centreon est un front-end Web basé sur le célèbre moteur de supervision Nagios. Développé et supporté par la société française Merethis depuis 2005, il apporte au travers d\'une interface Web intuitive la possibilité de réaliser le paramétrage et le reporting de la supervision.</p><p>Centreon est <b>diffusé sous licence GPL</b> et est disponible en plusieurs versions. Une version communautaire librement accessible sur le site de l\'éditeur et une version Entreprise déclinée en 4 offres apportant des fonctionnalités complémentaires (nouveaux plugins, auto-découverte, module d\'import massif, génération de rapports, agents pré-compilés, etc.) et des niveaux de supports différents, permettant de s\'adapter aux besoins de chacun.</p><p>La <b>compatibilité de Centreon avec Nagios</b> lui permet de bénéficier des nombreux apports de la communauté et donne à l\'administrateur une panoplie de plugins permettant de superviser pratiquement n\'importe quel service de n\'importe quel système connecté à un réseau IP. De nombreux systèmes sont supportés : toutes distributions Linux et versions de Windows, AIX, *BSD, Solaris, HP-UX, AS400, etc.</p><p>Voici les<b>&nbsp;principales fonctionnalités</b> proposées par Centreon :</p>
<ul>

<li>Consultation de la métrologie et des informations d\'état des services/hôtes supervisés</li>

<li>Gestion avancée des templates, héritages multiples</li>

<li>Possibilité de définir la hiérarchie du réseau (relation parent/enfant) pour déterminer les hôtes arrêtés des hôtes non joignables</li>

<li>Support des surveillances de type active et passive</li>

<li>Gestion avancée des notifications (définition des périodes de surveillance et de remontés d\'alertes, escalades, périodes de maintenances, prise en compte des dépendances de services pour limiter le nombre d\'alertes)</li>

<li>Contrôle des droits d\'accès utilisateurs via une gestion avancée des ACL</li>

<li>Déclenchement d\'actions correctrices suivant les changements d\'état des services/hôtes</li>

<li>Support des architectures distribuées pour les parcs importants</li>

</ul>
<p>Un des avantages de cette solution de supervision est <b>son interopérabilité et sa capacité à s\'intégrer dans une infrastructure existante</b> de par sa conception modulaire et ses interfaces simples qui facilitent l\'interaction avec des applications externes.</p><p>Centreon offre également <b>une API (module CLAPI)</b> qui permet d\'interagir en ligne de commande sur la toute la configuration de Centreon. Le principal avantage est de pouvoir industrialiser le déploiement à grande échelle et automatiser la gestion de la supervision pour les parcs disposant de plusieurs milliers d\'équipements et de services à superviser</p><h3>Modules complémentaires</h3><p>La société Merethis a développé de nombreux modules complémentaires permettant d\'élargir les possibilités de la supervision :</p><p>
<b>Centreon BI :&nbsp;</b>Centreon Business Intelligence (BI) s\'appuie sur le moteur de reporting BIRT et donne la possibilité de générer des rapports personnalisables sur les disponibilités et les performances du Système d\'Information supervisés. Les rapports se basent sur les journaux et les données collectées par Centreon et peuvent être générés sous différents format PDF, XLS, CSV, PPT, DOC, etc. de manière automatique pour une période d\'activité définie par l\'utilisateur (mois en courant, dernière 24h, etc.) .<br />
<b>Centreon BAM :</b> Utiliser pour les SLA en mesurant en temps réel l\'activité de la production (services métiers) en agrégeant les états des différents points de contrôle supervisés avec Centreon. Il permet de donner une vue globale de l\'état de santé du système d\'information suivant l\'état des indicateurs et leur niveau d\'impact .<br />
<b>Centreon MAP :</b> Permet la représentation en temps réel sous forme cartographique des indicateurs supervisés par Centreon dans des vues personnalisées. Les vues sont configurables et peuvent être personnalisées pour correspondre aux besoins de chaque utilisateur : métiers, fonctionnels, techniques, topologiques etc. Centreon MAP offre également des fonctionnalités de Weather MAP permettant, par exemple, de représenter visuellement le taux d\'utilisation des liens réseaux suivant une échelle colorimétrique.<br /><b>Auto-découverte :</b> Module permettant de réaliser de manière automatique la découverte des services à superviser sur un hôtes (filesystem, interfaces réseaux, etc.) et de les ajouter à la supervision.</p><h3>En bref</h3>
<ul>

<li>Référence des solutions de supervision open source</li>

<li>Communauté forte de 250 000 utilisateurs</li>

<li>Diffusé sous licence GPL</li>

<li>Nombreux modules et plugins permettant d\'élargir le périmètre de la supervision</li>

<li>Support de la plupart des systèmes connus</li>

<li>Module de reporting avancé</li>

<li>Support Entreprise disponible</li>

</ul>
<h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur la solution <a href="http://www.open-source-guide.com/Solutions/Infrastructure/Supervision-et-la-metrologie/Centreon" target="_blank">Centreon</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>',
            'fiche_detaillee'   =>  '<h2>Centreon en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	2.4.5				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.centreon.com    " title="http://www.centreon.com    " target="_blank">http://www.centreon.com    </a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(Merethis)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			GPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>

<p>Centreon est un logiciel libre de supervision, édité par la société française Merethis. Il mesure la disponibilité et la performance des couches applicatives, du service utilisateur jusqu’aux ressources matérielles.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Infrastructure/Supervision-et-la-metrologie/Centreon\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '5'
        ]);

        Product::create([
            'title'             =>  'Hippo',
            'image'             =>  '/img/produits/HippoCMS_produit_full.png',
            'quote'             =>  '<p>HippoCMS est un CMS open source qui fait partie de la famille des CMS écrits en JAVA. Il a été créé en 1999 par l’éditeur néerlandais OneHippo. Il est fortement utilisé dans les pays nordiques et tout particulièrement aux Pays-Bas.</p>',
            'notre_offre'       =>  '<p>Il possède l\'essentiel des fonctionnalités que l\'on peut attendre d\'un CMS : édition de modèle de document, création de contenu, multilinguisme, templating, workflow de publication, multi-sites, import/export des contenus, édition de contenu en direct, etc.</p><p>L\'interface utilisateur est moins riche que dans certains autres produits mais elle reste ergonomique et fonctionnelle et inclut l\'essentiel des fonctionnalités courantes attendues d\'un bon CMS. La roadmap du produit est par ailleurs particulièrement dynamique et promet une belle suite.</p><p>HippoCMS est distribuée sous licence Apache 2.0. Deux éditions sont distribuées : une version Community et une version Enterprise apportant stabilité, support et garantie.</p><p>Bien conçu techniquement, Hippo CMS offre de réels avantages pour l\'intégration du besoin fonctionnel par les développeurs. Les concepteurs de HippoCMS ont eu à coeur de respecter les standards (JEE), d\'utiliser des outils éprouvés tels que Spring, JackRabbit et Maven, et de définir une architecture claire et extensible. Ces efforts facilitent une prise en main rapide par les développeurs.</p><h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur la solution <a href="http://www.open-source-guide.com/Solutions/Applications/Cms/Hippocms" target="_blank">Hippo</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>	',
            'fiche_detaillee'   =>  '<h2>Hippo en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	7.8				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.onehippo.com" title="http://www.onehippo.com" target="_blank">http://www.onehippo.com</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(OneHippo)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                				                			<strong>Licences : </strong>
			            							                						                						                							                			Apache
				                							                			; 					                					                						                						                							                			Autre commerciale
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Java							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

1999					            </p>

<p>HippoCMS est un CMS open source qui fait partie de la famille des CMS écrits en JAVA. Il a été créé en 1999 par l’éditeur néerlandais OneHippo. Il est fortement utilisé dans les pays nordiques et tout particulièrement aux Pays-Bas&nbsp;; il commence à pénétrer le marché français.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/Cms/Hippocms\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '1'
        ]);
        Product::create([
            'title'             =>  'Odoo',
            'image'             =>  '/img/produits/Odoo_produit_full.png',
            'quote'             =>  '<p>&nbsp;(ex-OpenERP) représente un idéal de logiciel agile, apte à répondre à n\'importe quel besoin d\'<b>application métier standard ou spécifique</b> avec également des verticalisations natives très adaptées au négoce, à l\'industrie ou au service.</p>	',
            'notre_offre'       =>  '<p><b>Odoo (ex-OpenERP) combine à la fois la force d\'un éditeur et celle d\'une large communauté,</b> comprenant ses intégrateurs présents dans le monde entier, qui balise l’ensemble des cas d\'usages et fournit de précieux retours, notamment sous forme de modules réutilisables. Tout ceci est rendu possible par une réelle innovation technologique qui s\'appuie sur des standards reconnus en termes de base de données et de webservices.</p><p><b>Odoo couvre tous les besoins, tels que ventes, achats, rh, projets, comptabilité, logistique, stock, production, facturation, ... et son framework permet de l\'adapter rapidement aux contextes spécifiques, que ce soit par le paramétrage de nouveaux workflows, de nouvelles informations, ou de tableaux de bord pour une toujours plus grande efficacité de l\'ERP en entreprise.</b></p><h3>Odoo pour une application de gestion spécifique</h3><p>Odoo, avec son framework orienté objet, est particulièrement adapté pour la réalisation d\'applications de gestion répondant à des enjeux spécifiques. Avec son framework, il sera facile de modéliser les objets de gestion, d\'en générer et adapter les formulaires de saisie ou de liste, avec les capacités natives de recherche, d\'auto-complétion, de gestion de droits, ... que ce soit via l\'interface d\'administration ou via les fichiers de configuration.</p><p>Les objets sont par ailleurs automatiquement accessibles en web services, ce qui simplifie la problématique d\'intégration au SI existant. Les données stockées en base restant dans un modèle relationnel classique et requetable en sql, cela permet une lisibilité, un reporting et une reprise de données simplifiés, complètement ouverts.&nbsp;</p><h3><a href="/Offres/Erp-et-business-apps/Odoo-pour-les-esn">Odoo pour les sociétés de services / ESN / SSII</a>
</h3>',
            'fiche_detaillee'   =>  '<h2>Odoo en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	8.0				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.odoo.com" title="http://www.odoo.com" target="_blank">http://www.odoo.com</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(OpenERP)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			AGPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	Python							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2005					            </p>

<p>Odoo (ex-OpenERP), fondé en 2005 par Fabien Pinckaers, combine à la fois la force d\'un éditeur et celle d\'une large communauté, comprenant des intégrateurs dans le monde entier, qui balisent l’ensemble des cas d\'usages et fournit de précieux retours, notamment sous forme de modules réutilisables.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/Erp-pgi/Odoo\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '2'
        ]);

        Product::create([
            'title'             =>  'Magento',
            'image'             =>  '/img/produits/Magento_produit_full.png',
            'quote'             =>  '	  <p>Magento est le dernier né dans le monde des outils e-commerce open source. Développé par la SSII américaine Varien, Magento fait partie de cette nouvelle génération de solutions open source professionnelle portées par un éditeur.</p>	  ',
            'notre_offre'       =>  '<p>Magento Commerce est la solution e-commerce open source de reference, par laquelle transitent annuellement plus de 50 milliards de dollars de transactions. Depuis 2008, plus de 200 000 boutiques en ligne ont vu le jour grâce à cet outil, parmi lesquelles celles de grandes marques telles que Nike, Christian Dior, Volcom, Rebecca Minkoff, The North Face et bien d’autres.</p><p>Magento a été reconnu comme la première solution e-commerce par le “2015 Internet Retailer Top 1000,” le “B2B 300”et le “Hot 100 lists.” L’éditeur travaille étroitement avec des commerçants, des marques ainsi que des fabricants B2B comme B2C afin de faire converger les expériences d’achat digitales et physiques.</p><p>En plus de sa plateforme phare e-commerce, Magento Commerce a aussi dans son portefeuille des solutions omnicanales basées sur le cloud (Order Management, In-Store).</p><p>
Magento Commerce s’appuie sur un réseau de plus de 300 partenaires à travers le monde ainsi qu’une communauté mondiale de plus de 66 000 développeurs.<br />Magento 2</p><h3>Magento 2</h3><p>Depuis la fin novembre 2015, Magento 2 est officiellement disponible en version stable. Suite au succès de la version 1, cette nouvelle mouture a été conçue afin de donner le pouvoir aux marques, aux commerçants, et aux entreprises B2C et B2B de délivrer des expériences d’achat omnicanales rapidement et efficacement à moindre coût.</p><p>Magento 2 est disponible en version “Entreprise” (payante) ainsi qu’en version “Communauté” (gratuite). L’éditeur est appuyé par Permira Funds, un fonds d’investissement international dont lees investissements totalisent environ $25 milliards. &nbsp;</p><p>Cette nouvelle version offre des performances et une extensibilité améliorées, des fonctionnalités pensées pour booster les taux de conversion, et des améliorations concernant l’agilité métier (“business agility” en anglais) et la productivité.</p><p>
Bâti sur des briques open source, Magento 2 offre une flexibilité et des opportunités d’innovation incomparables par rapport aux autres solutions sur le marché e-commerce. Smile est le premier expert européen de Magento, découvrez nos expertises et nos références.<br />
Plus d\'infos sur le Guide Open Source<br />Retrouvez plus d\'informations sur la solution Magento sur le site www.open-source-guide.com édité par Smile.</p><p><a href="http://magento.smile.eu" target="_blank"><b>Smile est le premier expert européen de Magento, découvrez nos expertises et nos références.</b></a>
</p><h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur la solution&nbsp;<a href="http://www.open-source-guide.com/Solutions/Applications/E-commerce/Magento" target="_blank">Magento</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>',
            'fiche_detaillee'   =>  '<h2>Magento en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	1.8				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.magentocommerce.com " title="http://www.magentocommerce.com " target="_blank">http://www.magentocommerce.com </a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(Magento Inc)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                				                			<strong>Licences : </strong>
			            							                						                						                							                			Autre open source
				                							                			; 					                					                						                						                							                			Autre commerciale
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2007					            </p>

<p>Dès les premières versions bêta parues en 2007, cet outil - porté par l\'éditeur américain Magento Inc. (anciennement Varien) - a généré un buzz phénoménal, au niveau mondial, permettant de fédérer en un temps record une communauté extrêmement active.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/E-commerce/Magento\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '1'
        ]);
        Product::create([
            'title'             =>  'Drupal',
            'image'             =>  '/img/produits/Drupal_produit_full.png',
            'quote'             =>  '<p>Conçu pour être un blog collectif, il trouve aujourd’hui des applications très variées :du portail communautaire au site corporate, en passant par l’intranet ou encore le site e-commerce.</p><p><a href="http://drupal.smile.eu" target="_blank"><b>Smile est le premier expert européen de Drupal, découvrez nos expertises et nos références.</b></a>
</p>',
            'notre_offre'       =>  '<h3>Introduction</h3><p>Drupal est un système de <b>gestion de contenu</b> à la fois professionnel et communautaire. <b>Professionnel </b>car il regroupe l’ensemble des fonctionnalités majeures que l’on peut attendre d’un CMS. Mais aussi parce que son développement s’est véritablement professionnalisé avec une équipe dédiée assurant une réelle dynamique.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p><b>Communautaire </b>car Drupal s’appuie sur une communauté ouverte, composée de milliers d’utilisateurs (intégrateurs comme Smile et indépendants, par exemple), qui contribuent sans cesse à son évolution.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>Ces deux aspects font de Drupal un acteur majeur de la gestion de contenu (sites et applications Web).</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>Mais la pertinence de cette solution vient surtout de <b>l’alliance entre sa simplicité d’utilisation, sa rapide implémentation et sa faculté à être étendue</b> par des milliers d’extensions déjà disponibles, ou par des développements spécifiques métiers. A l’instar des solutions open source, vous ne serez donc jamais bloqué pour faire évoluer votre système.</p><h3>Pourquoi utiliser Drupal ?</h3><p>Drupal repose sur une architecture modulaire qui couvre parfaitement les besoins de gestion de contenu. Cela passe par l\'utilisation des fonctionnalités du noyau, épaulées par quelques uns des modules les plus populaires. <b>Les forces de Drupal en tant que CMS sont : une grande flexibilité en termes d\'organisation du contenu</b> (possibilité de définir les mises en page, présentation, remontées automatiques), <b>des possibilités de mises à jour rapides</b> depuis le front-office (frontend editing), <b>ainsi qu\'un système de taxonomie hiérarchique extrêmement puissant.</b> Il a, de plus, une orientation web 2.0 (sur les aspects Social Publishing) transverse à l\'ensemble des contenus mis en ligne.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>Cependant, l\'atout majeur de Drupal reste <b>son extensibilité.</b> De par sa conception, il est tourné vers l\'évolution et de nombreux modules sont mis à disposition par la communauté et les intégrateurs, comme Smile. Les modules les plus appréciés <b>permettent de donner à Drupal une dimension réseau social très convaincante</b>, ou de lui conférer des possibilités de gestion documentaire.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>En ce qui concerne l\'ergonomie des outils mis à la disposition des administrateurs fonctionnels, l\'organisation modulaire de Drupal permet, en désactivant les modules qui ne sont pas nécessaires pour un site donné, <b>de proposer une interface sur mesure</b>, ne proposant que les fonctionnalités utiles pour les administrateurs. Ceci en fait un des CMS <b>les plus simples d’utilisation du marché</b>. Il réduit considérablement les coûts liés à la conduite du changement.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>Basé sur une architecture PHP, Drupal vous assure de trouver rapidement les compétences nécessaires au développement de nouveaux modules (vous n’êtes pas dépendant). Si les modules existants répondent parfaitement à vos besoins, alors <b>l\'essentiel de la mise oeuvre d\'un site motorisé par Drupal passe par un processus de configuration</b> ne réclamant pas ou peu de compétences techniques. Ces aspects sont bien entendu des arguments forts en faveur de la solution, en ce qui concerne le transfert de compétences notamment.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>L\'angle budgétaire plaide également en faveur de Drupal. En effet, l\'utilisation de l\'outil n\'est soumis à <b>aucun coût de licence</b>, quel que soit le nombre de serveurs ou d\'utilisateurs. Cependant, même si l’outil est 100% communautaire, il existe (et nous la recommandons) <b>une offre de support</b> (« garantie ») proposée par Acquia, société de conseil/ingénierie fondée par Dries Buytaert, créateur de Drupal.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>Enfin, Drupal jouit d\'une <b>notoriété indéniable</b>. De nombreuses sociétés de renommée internationale ont franchi le pas et font désormais confiance à Drupal, on peut citer : Procter &amp; Gamble, Mattel, Virgin, Universal Music, Intel, General Motors, Fujifilm, Reuters, Sony Ericsson, Nokia, Disney, Nike, Sony, etc.</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>La licence open source de Drupal a également permis <b>la constitution d\'une communauté d\'utilisateurs très active</b>, composée d\'experts compétents et fidèles (plusieurs milliers de modules développés et maintenus à ce jour). Des conférences sont régulièrement organisées à travers le monde (2010 : San Francisco et Copenhague, 2009 : Boston et Paris, 2008 : Washington DC et Hongrie...).</p><div class="object-right">
   <div class="factbox">
       <div class="factbox-design">
       <h2></h2>
       <div class="factbox-content">

       </div>
       </div>
   </div>
</div><p>Smile propose un <b>cycle de formation complet</b> sur Drupal, basé sur l’expérience de nombreux projets. Plus d\'information sur <a href="http://training.smile.fr/cms-gestion-de-contenu/formation-drupal-developpeur" target="_blank">Smile Training.</a>
</p><h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur la solution <a href="http://www.open-source-guide.com/Solutions/Applications/Cms/Drupal" target="_blank">Drupal</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>',
            'fiche_detaillee'   =>  '<h2>Drupal en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	7.25				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.drupal.org  " title="http://www.drupal.org  " target="_blank">http://www.drupal.org  </a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Communauté
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                					                		<strong>Licence : </strong>
			                						                						                						                							                			GPL
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

2000					            </p>

<p>Drupal est un CMS aux multiples facettes. Conçu à l’origine pour être un blog collectif, il trouve aujourd’hui des applications très variées : du site corporate au portail communautaire, il sait tout faire ! Il a été conçu dans les années 2000 par Dries Buytaert et connaît depuis un succès mondial. Une communauté énorme s’est créée autour du produit.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/Cms/Drupal\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '4'
        ]);

        Product::create([
            'title'             =>  'eZ publish',
            'image'             =>  '/img/produits/eZ-Publish_produit_full.png',
            'quote'             =>  '<p>D\'origine norvégienne, eZ publish est un <b>logiciel de gestion de contenus (ou CMS, Content Management System) haut-de-gamme</b>.</p>     ',
            'notre_offre'       =>  '<h4>Typologie de contenus</h4><p>eZ publish se distingue des autres CMS open source par ses possibilités de configuration et d’extension : eZ publish permet de créer des types de contenus en ligne ! En quelques clics et formulaires, vous créez le type de contenu &quot;Compte-rendu de réunion&quot; ou &quot;Actualité&quot; ou encore &quot;Fiche bibliographique&quot;. Le premier contenu sera composé d’un titre, du nom du projet associé, d’une date, d’une liste de participants, d’un corps de texte … Le second contiendra uniquement un titre et un petit texte. Le troisième sera plus complexe.</p><p>Avec eZ publish, c’est possible et l’information des contenus prend enfin du sens.</p><p>Il faut bien mesurer à quel point cette possibilité, apparemment élémentaire peut être déterminante dans certains contextes. C\'est le moyen, le seul moyen, de pousser jusqu\'au bout le principe de base de la gestion de contenu, celui de la séparation du contenu et de la mise en forme. En l\'absence de types d\'articles spécifiques, il est toujours possible de tout faire entrer dans le champ &quot;corps d\'article&quot;, mais aussitôt que le corps d\'article se structure, on commence à mêler de la mise en forme aux contenus. Avec les inconvénients que l\'on connait : impossibilité de réviser la mise en page ou les attributs, impossibilité de viser un autre média de restitution, etc...</p><p>Mais ce n\'est pas tout, les contenus parfaitement structurés, c\'est aussi le moyen de promouvoir une information de qualité dès la saisie : tel champ est une date, tel autre champ est obligatoire, etc.</p><p>C\'est donc bien une fonctionnalité qui a un caractère différenciant, et peut être un critère de choix fort. Lorsqu\'on en a besoin, eZ publish est la solution.</p><h4>eZpublish est hautement configurable</h4><p>En termes de fonctionnalités, eZ publish intègre des workflows paramétrables, une gestion des utilisateurs et des droits, un front-end sécurisé, une gestion des versions, etc.</p><h4>L\'interface de eZ publish est très simple d\'accès</h4><p>Une des grandes forces de eZ publish est que l\'interface d\'administration a su rester simple d\'accès pour les contributeurs, et ce malgré une richesse fonctionnelle importante.</p><h4>Environnement technique</h4><p>Sur le plan technique, eZ publish est tout aussi performant. eZ publish est en Php, fonctionne sur Apache et Mysql (PostGreSql est supporté), met en œuvre le protocole webdav, gère trois caches différents en fichiers, sépare très distinctement tous vos sites (données et templates), etc. eZ publish fonctionne indifféremment sur Windows ou Linux.</p><h4>Formation</h4><p>Smile propose un <b>cycle de formation complet</b> sur eZ Publish, basé sur l’expérience de nombreux projets. Plus d\'information sur <a href="http://training.smile.fr/cms-gestion-de-contenu" target="_blank">Smile Training</a>
.</p><h4>Plus d\'infos sur le Guide Open Source</h4><p>Retrouvez plus d\'informations sur la solution&nbsp;<a href="http://www.open-source-guide.com/Solutions/Applications/Cms/Ez-publish" target="_blank">eZ publish</a>
 sur le site <a href="http://www.open-source-guide.com" target="_blank">www.open-source-guide.com</a>
 édité par Smile.</p>	',
            'fiche_detaillee'   =>  '<h2>eZ publish en détail</h2>
																		                <p>
					                <strong>Version étudiée : </strong>
				                	5.2				            	</p>
			            				            					                <p>
		                            <strong>Site web : </strong>
		                            <a class="site" href="http://www.ez.no" title="http://www.ez.no" target="_blank">http://www.ez.no</a>
		                        </p>
			                			                			                	<p>
				                	<strong>Distribuée par : </strong>
				                					                		Editeur
				                						                			(eZ Systems)
				                						                					           		</p>
			       		 				       		 					                <p>
				                					                				                			<strong>Licences : </strong>
			            							                						                						                							                			GPL
				                							                			; 					                					                						                						                							                			Autre commerciale
				                							                								                					                					            	</p>
			            				            					                <p>
				                					                					                		<strong>Technologie : </strong>
			                						                								            							            	PHP							            						            						        </p>
					        					        						        <p>
				                	<strong>Année de création : </strong>

1999					            </p>

<p>La solution eZ Publish a été créée en 1999 par la société eZ Systems. eZ Publish se distingue des autres CMS open source par ses possibilités de configuration et d’extensions : il se présente comme un outil prêt à l’emploi mais aussi comme un framework de développement permettant de réaliser des applicatifs métiers légers.</p>					    						    		                        <p><a href=\'http://www.open-source-guide.com/Solutions/Applications/Cms/Ez-publish\' target="_blank">&gt; Voir la fiche complète sur le guide de l\'open source</a></p>',
            'lines'             =>  '3'
        ]);



        Presse::create([
            'title'     => 'Les nouveaux outils pour « Linux embarqué »  Pierre Ficheux, Smile',
            'link'      => 'http://www.informatiquenews.fr/nouveaux-outils-linux-embarque-pierre-ficheux-smile-47383',
            'source'    => 'informatiquenews.fr - Guy Hervier',
            'text'      => '<p>Autrefois présents mais inconnus du grand public, les systèmes embarqués ont aujourd’hui envahi notre quotidien et les logiciels libres y ont une place prépondérante, en particulier le système d’exploitation Linux. Le noyau Linux est présent dans le système Android de Google utilisé dans plus d’un milliard de téléphones mobiles. Le système d’exploitation Linux (dans sa version « embarquée ») est omniprésent sur les boîtiers d’accès à Internet et à la télévision numérique (la fameuse « set-top box »).</p><p>Dernièrement, ce même Linux a envahi l’industrie automobile et anime les systèmes dénommés IVI (In Vehicle Infotainment) incluant entre-autres la navigation, la diffusion multimédia dans l’habitacle ou l’assistance à la conduite.</p><p>La construction de tels systèmes est très éloignée de l’utilisation habituelle de Linux dans l’industrie qui est basée sur des « distributions » fournies par des éditeurs commerciaux (Red Hat, SuSE, etc.) ou des communautés (Debian, Ubuntu, etc.). Sur un PC classique la mémoire – et le disque dur – ont des capacités quasiment illimitées et même si l’on a la volonté de maîtriser les consommations d’espace et d’énergie, les contraintes sont bien moindres que dans le domaine des systèmes embarqués.</p>',
            'date'      => '2016-06-31',
        ]);
        Presse::create([
            'title'     => 'Nantes : une école du logiciel libre ouvrira à la rentrée',
            'link'      => 'http://www.ouest-france.fr/pays-de-la-loire/nantes-44000/nantes-une-ecole-du-logiciel-libre-ouvrira-la-rentree-4265057',
            'source'    => 'ouest-france.fr ',
            'text'      => '<p><b>Elle a été lancée ce mardi matin dans les locaux du start-up palace. Une école du logiciel libre et des solutions open source ouvrira à la rentrée à Nantes.</b></p><p>L\'école du logiciel libre a été lancée ce mardi matin à Nantes par Smile (expert en solutions open source) et l’Epsi (école d’ingénierie informatique). Soutenue financièrement par l’État, l’OSS (école du logiciel libre et des solutions open source) est l’un des six établissements d’enseignement supérieur de ce type à ouvrir en France.</p>',
            'date'      => '2016-05-31',
        ]);
        Presse::create([
            'title'     => 'L’Open Source School ouvre à Lille pour une informatique libérée',
            'link'      => 'http://www.lavoixdunord.fr/region/l-open-source-school-ouvre-a-lille-pour-une-informatique-ia19b0n3529716',
            'source'    => 'lavoixdunord.fr - Jean-Marc Petit',
            'text'      => '<p>La première école française du logiciel libre (Open Source School) va ouvrir à Lille à la rentrée. Une formation répondant aux besoins croissant d’un marché où travaillent déjà 50 000 personnes en France.</p><p>Que vous soyez un pro ou un amateur d’informatique, vous les avez sans doute déjà utilisés. lls s’appellent Linux (système d’information), Mozilla (moteur de recherche) ou Open Office (suite bureautique). On les appelle des logiciels libres, ou open source. Des logiciels qui ont la particularité d’avoir leur programme source ouvert au plus grand nombre, pour pouvoir les utiliser, les modifier et les redistribuer librement. Contrairement aux logiciels « fermés », comme Windows, uniquement accessibles sous licence.</p><h3>4 000 postes non pourvus</h3><p>Rien qu’en France, 50 000 personnes travaillent dans l’univers du « logiciel libre », un marché estimé à 4 milliards d’euros dans notre pays, en croissance régulière de plus de 10 %. Et où les besoins en emplois sont constants (4 000 postes sont non pourvus chaque année).</p><p>C’est dans ce contexte de forte demande qu’ouvre en septembre à Lille, la première école du logiciel libre, Open Source School. Portée par l’ESPI (école privée de sciences informatiques) et Smile, leader européen de l’intégration open source, l’OSS va accueillir pour sa première promotion une quinzaine d’étudiants, en formation par alternance pendant trois ans, mais également des personnes en formation continue ou en processus de reconversion. L’objectif étant d’atteindre la centaine d’étudiants au bout de trois ans.</p>',
            'date'      => '2016-05-25',
        ]);
        Presse::create([
            'title'     => 'Smile renforce son Comité Exécutif',
            'source'    => 'capital.fr',
            'link'      => 'http://www.capital.fr/bourse/communiques/smile-renforce-son-comite-executif',
            'text'      => '<p>Smile, leader européen de l\'intégration et de l\'infogérance de solutions open source, annonce la nomination de <b>Luc Fayet</b> au poste de Directeur des Ressources humaines.</p><p><b>Luc Fayet</b>, DRH expérimenté, intègre Smile en qualité de DRH, membre du Comité Exécutif, pour accompagner le groupe dans sa transformation et poser les bases de la nouvelle gouvernance RH. Il a notamment pour missions de coordonner les fonctions RH et le recrutement, d\'accompagner la société dans ses projets de croissance externes, d\'entretenir un dialogue social constructif et de développer une politique motivante de gestion des carrières.</p>',

            'date'      => '2016-05-03',
        ]);
        Presse::create([
            'title'     => 'Comment l\'Open Source School prépare sa première rentrée',
            'link'      => 'http://www.journaldunet.com/solutions/reseau-social-d-entreprise/1177400-comment-l-open-source-school-prepare-sa-premiere-rentree/',
            'source'    => 'journaldunet.com - Virgile Juhan',
            'text'      => '<p>Lancée à l’initiative de Smile et l\'Epsi, notamment, la première école dédiée à l\'open source est sur de bons rails. Mais le projet est vaste, et il reste encore du pain sur la planche.</p><p>Dans quelques mois, en septembre,&nbsp;<a href="http://www.opensourceschool.fr/" target="_blank">l\'Open Source School</a>
&nbsp;va ouvrir ses portes, dans plusieurs villes en France. S\'il existe de nombreuses écoles d\'informatique, celle-ci est d\'un nouveau genre puisqu\'elle va concentrer ses enseignements sur une partie bien particulière de l\'informatique&nbsp;: l\'open source. Un secteur en pleine croissance, qui a du mal à embaucher. Le potentiel est donc là&nbsp;: d\'après une&nbsp;récente étude&nbsp;réalisée par PAC, le secteur de l\'open source représente déjà 50&nbsp;000&nbsp;emplois en France, et va générer 4&nbsp;à 5&nbsp;000&nbsp;créations nettes d\'emplois chaque année d\'ici 2020.</p><p>En vue de répondre à ce besoin, l\'initiative a été imaginée, puis portée par un acteur bien connu du secteur, l\'intégrateur de solutions&nbsp;open source&nbsp;Smile. Le projet a pu obtenir un financement conséquent de l\'Etat (1,4&nbsp;million d\'euros) dans le cadre du programme d\'investissements d\'avenir. Smile s\'est aussi associée à l\'école informatique Epsi.</p><p>&quot;<i>Nous n\'aurions jamais été capables de lancer seuls cette école. Nous avons peut-être joué le rôle d\'étincelle, mais nos compétences n\'auraient pas suffi, et ce sont les synergies avec l\'Epsi qui nous ont permis de faire avancer le projet et de le concrétiser</i>&quot;, tient à préciser <b>Grégory Becue</b>, directeur associé de Smile, et co-fondateur de l\'école. L\'Open Source School, également appelée &quot;OSS&quot;, va pouvoir profiter des locaux et campus de l\'Epsi, et ainsi pouvoir se lancer, dès la rentrée prochaine, dans pas moins de 6&nbsp;villes&nbsp;: Bordeaux, Lille, Lyon, Montpellier, Nantes, et Paris.</p>',
            'date'      => '2016-04-27',
        ]);
        Presse::create([
            'title'     => 'Smile recherche 300 personnes à Paris et en région',
            'link'      => 'http://www.lemondeinformatique.fr/actualites/lire-smile-recherche-300-personnes-a-paris-et-en-region-64016.html',
            'source'    => 'lemondeinformatique.fr - Véronique Arène',
            'text'      => '<p>Pour se renforcer à l\'échelle nationale, l\'intégrateur français de solutions open source Smile entend recruter plusieurs centaines d\'ingénieurs et de consultants en Ile-de-France et en région d\'ici la fin de l\'année. Des postes sont également ouverts aux stages de pré-embauche</p><p>Smile poursuit le rythme de ses recrutements pour accompagner son développement national. Après avoir réalisé 260 embauches l\'an dernier, l’intégrateur français de solutions open source recherche 300 ingénieurs dans l\'Hexagone, principalement en Ile-de France. Les offres sont à pourvoir dans les 5 pôles d’activités du groupe et concernent des profils variés. Il s’agit notamment de chefs et de directeurs de projets, de développeurs, de consultants (ebusiness, big data, DevOps, ESB/ETL), ainsi que de spécialistes en mobilité. Sont également recherchés des ingénieurs (systèmes embarqués, qualité logiciel) ainsi que des avant-vente, et des commerciaux juniors et expérimentés. Le groupe ouvre également des stages de pré-embauche, ainsi que des contrats d\'apprentissage, avec proposition d’embauche à la clé.</p>',
            'date'      => '2016-02-24',
        ]);
        Presse::create([
            'title'     => 'Contribuer à l’open-source, cela s’apprend…',
            'link'      => 'http://business.lesechos.fr/directions-numeriques/technologie/open-source/021703447990-contribuer-a-l-open-source-cela-s-apprend-207454.php',
            'source'    => 'lesechos.fr - Florian Debes',
            'text'      => '<p>Les grands groupes participent de plus en plus à des travaux open-source, constate Grégory Bécue, directeur associé de Smile, une société d’intégration de logiciels libres. Il ouvre une école d’informatique spécialisée, en association avec l’EPSI.</p><p>Il n’y a pas qu’Apple,&nbsp;Facebook&nbsp;ou Google pour partager une partie de leurs développements informatiques avec la communauté du logiciel libre. «&nbsp;<i>L’inflexion est notable ces derniers mois, les grands groupes français contribuent de plus en plus à des travaux open source&nbsp;</i>», constate Grégory Bécue. Le directeur associé de Smile, une entreprise de services numériques spécialisée dans l’informatique ouverte perçoit dans le même temps une tension sur le recrutement de professionnels formés sur les outils de l’open source. «&nbsp;<i>Etre contributeur open-source, cela s’apprend, il y a des règles à comprendre</i>&nbsp;», affirme-t-il. C’est la raison d’être de l’Open Source School. Cette école d’informatique, créée en octobre dernier par l’EPSI (un établissement privé d’enseignement supérieur) et Smile, devrait accueillir 180 étudiants à la rentrée prochaine, sur six campus en France.&nbsp;Sa naissance vient combler un manque de formation qui rendait peu attractif le domaine&nbsp;.</p><p>Accessibles aux Bac +2 cette formation est dédiée à l’apprentissage (en alternance) des technologies libres et surtout de la philosophie de partage propre à l’open-source. Les cours seront donnés par des professionnels. Des modules de formation continue seront également proposés aux entreprises, sur un à cinq jours.</p>',
            'date'      => '2016-02-17',
        ]);
        Presse::create([
            'title'     => 'Open source : Open Wide est désormais niché dans le giron de Smile',
            'link'      => 'http://www.lembarque.com/open-source-open-wide-est-desormais-niche-dans-le-giron-de-smile_004475',
            'source'    => 'lembarque.com - François Gauthier',
            'text'      => '<p>Suite aux discussions exclusives entamées entre les deux entreprises en novembre dernier, les sociétés de services officiant dans le domaine de l’open source Smile et Open Wide ont annoncé début février la finalisation de leur négociation, et donc le rapprochement opérationnel des deux structures. L’ensemble va représenter un effectif global de 1 000 personnes (dont 850 en France), incluant les 150 collaborateurs d’Open Wide. La nouvelle société devrait afficher 70 millions d’euros de chiffre d’affaires annuel.</p><p>L’acquisition d’Open Wide va permettre à Smile de proposer des offres dans le domaine dynamique de l’embarqué, et dans l’industrie en général, avec notamment des applications métier écrites en Java, un savoir-faire issu d’Open Wide. Cinq lignes de métiers ont été définies pour le nouvel ensemble : Digital, E-business, Système d’information, Infrastructure et Ingénierie. Parallèlement, Smile prévoit une croissance à deux chiffres de son CA en 2015 (hors acquisitions externes), chiffre d’affaires qui a quasiment triplé en trois ans.</p>',
            'date'      => '2016-02-08',
        ]);
        Presse::create([
            'title'     => 'Smile finalise l’acquisition d’Open Wide',
            'link'      => 'http://www.channelnews.fr/smile-finalise-lacquisition-dopen-wide-59833',
            'source'    => 'channelnews.fr - Johann Armand',
            'text'      => '<p>La société de services open source Smile annonce dans un communiqué <i>« avoir finalisé les négociations avec la société Open Wide</i> » ouvrant la voie au «<i>&nbsp;rapprochement opérationnel</i> » des deux sociétés. Avec les 150 collaborateurs d’Open Wide, Smile compte désormais près de 1000 collaborateurs répartis sur 9 pays (850 en France) et 18 agences. Le nouvel ensemble devrait peser de l’ordre de 70 millions d’euros de chiffre d’affaires annuel et intègre au passage le Top 50 des entreprises de services numériques françaises.</p><p>« <i>Ce rapprochement [nous] permet de nous renforcer sur des offres open source stratégiques telles que l’embarqué pour l’industrie, les applications métiers Java, l’infogérance et le mobile natif</i> », explique Marc Palazon, son PDG. « <i>Les clients des deux sociétés, grands comptes français et internationaux, [vont] bénéficier de synergies et d’un accompagnement plus large sur l’ensemble des cinq lignes métiers du nouvel ensemble : Digital, E-business, Système d’information, Infrastructure et Ingénierie</i> », poursuit la société dans son communiqué.</p>',
            'date'      => '2016-02-04',
        ]);
        Presse::create([
            'title'     => 'Open Source School : la 1re école spécialisée en open source',
            'link'      => 'http://www.studyrama.com/actualite/open-source-school-la-1re-ecole-specialisee-en-open-101855',
            'source'    => 'studyrama.com ',
            'text'      => '<p>
OSS - Open Source School est la première école informatique spécialisée dans l’enseignement open source. Elle forme des professionnels capables d’assurer la conception, la réalisation et la direction de grands projets open source dans un environnement international exigeant.<br />Open Source School est implantée sur 6 campus en France à Bordeaux, Montpellier, Nantes, Lyon, Lille, Paris.</p><h3>Open Source School</h3><p>
L’open source est présent dans tous les secteurs d’activités et dans les grands secteurs d’innovation informatique : cloud, big data, mobilité et web. <br />Des milliards d’utilisateurs ont recours à l’open source tous les jours partout dans le monde. L’open source est un domaine en forte croissance pour les années à venir.</p><h3>Une école soutenue par l’écosystème open source</h3><p>
OSS - Open Source School est portée par deux acteurs clés de l’open source et de l’enseignement : le Groupe SMILE, leader européen et expert de l’open source, et l’école EPSI, établissement pionnier de l’enseignement supérieur en informatique.<br />La formation Open Source School est un programme en 3 ans, accessible post-Bac+2. La formation est sanctionnée par un diplôme reconnu par l’État, délivré par le Ministère chargé de l’emploi. La formation Open Source School est 100% en alternance : l’étudiant alterne des périodes de formation à l’école et des périodes de mise en pratique en entreprise.</p>',
            'date'      => '2016-02-03',
        ]);
        Presse::create([
            'title'     => 'L\'open source a désormais son école : l\'Open Source School',
            'link'      => 'http://www.journaldunet.com/solutions/reseau-social-d-entreprise/1172377-l-open-source-a-desormais-son-ecole-l-open-source-school/',
            'source'    => 'journaldunet.com - Virgile Juha',
            'text'      => '<p>Lancée par la SSLL Smile et l\'école EPSI, l\'Open Source School va proposer une formation initiale en alternance, de niveau Bac+5, mais aussi un catalogue de formations continues.</p><p>Une nouvelle école dédiée à l\'open source est née. Lancée par l\'intégrateur Smile et l\'école d\'informatique EPSI, l\'Open Source School se présente comme &quot;<i>la première école dédiée aux technologies open source</i>&quot;. Elle bénéfice de l\'aide du gouvernement français, qui lui a attribué une première aide de 1,4 million d\'euros dans le cadre de ses &quot;investissements d\'avenir&quot;.</p><p>L\'Open Source School (OSS) proposera des formations initiales et continues. La formation initiale sera en alternance, accessible aux Bac+2, et sanctionnée par un diplôme de niveau bac+5 reconnu par l\'Etat (titre RNCP niveau I). Elle sera proposée à Bordeaux, Lille, Lyon, Montpellier, Nantes, et Paris. &quot;<i>OSS sera LE vivier opérationnel français de l\'open source</i>&quot;, a promis Laurent Espine, directeur national du réseau EPSI. &quot;<i>75% de l\'enseignement d\'OSS est porté par les professionnels du secteur, ce qui garantit une formation en phase avec les attentes du marché</i>&quot;, a ajouté Grégory Bécue, directeur associé du groupe Smile et porteur du projet. L\'admission à la formation se fait sur dépôt de dossier de candidature, examen écrit et entretien individuel. L\'école a précisé à nos confrères de Zdnet que la formation a déjà commencé en septembre 2015 sous forme d\'une option du socle de formation de l\'EPSI.</p>',
            'date'      => '2016-02-03',
        ]);
        Presse::create([
            'title'     => 'Eram booste ses ventes en ligne avec Smile',
            'link'      => 'http://internet.dsisionnel.com/2016/06/eram-booste-ses-ventes-en-ligne-avec-smile/',
            'source'    => 'internet.dsisionnel.com - La Rédaction',
            'text'      => '<p>Smile,&nbsp;leader européen de l’intégration et de l’infogérance de solutions open source, accompagne l’enseigne Eram,&nbsp;leader de la chaussure en centre-ville et centres commerciaux,&nbsp; dans la mise en œuvre d’un site responsive design de nouvelle génération lui permettant de déployer à grande échelle sa stratégie de vente en ligne.</p><p>Dès 2009,&nbsp;Eram&nbsp;a pris le virage du digital pour lancer ses activités e-commerce. Fort du succès de cette première expérimentation et au regard de l’évolution de ses ventes, l’enseigne a souhaité en 2015 donner une nouvelle impulsion à son projet et déployer un nouveau site reposant sur un socle technique performant et évolutif.</p><p>Dans ce contexte, après avoir lancé un appel d’offres,&nbsp;Eram&nbsp;a confié ce développement au groupe Smile. La sélection de Smile s’explique par son expertise unique autour des technologies Magento, mais également par ses fortes compétences en matière de design et d’ergonomie. Au-delà de ces éléments, la proximité des équipes de Smile a également représenté un facteur déterminant.</p>',
            'date'      => '2016-02-17',
        ]);
        Presse::create([
            'title'     => 'Open Source en France : Smile complète son portefeuille avec Open Wide',
            'link'      => 'http://www.lemagit.fr/actualites/4500256916/Open-Source-en-France-Smile-complete-son-portefeuille-avec-Open-Wide',
            'source'    => 'http://www.lemagit.fr - Cyrille Chausson',
            'text'      => '<h3>Dans un contexte de concentration, Smile annonce un rapprochement avec Open Wide, deux intégrateurs clé de l’Open Source en France.</h3><p>&quot;Alors que se tiendra les 17 et 18 septembre le Paris Open Source Summit, qui réunira le gratin du logiciel ouvert, la marché français des prestataires continue sa consolidation. Smile, l’un des piliers de l’intégration Open Source en France, annonce être entré en négociation exclusive avec Open Wide, un autre acteur de l’Open Source en France.</p><p>Ce rapprochement, comme Marc Palazon, président de Smile, préfère le qualifier, s’inscrit dans une stratégie mise en place il y a deux ans par la société. Une stratégie qui doit rompre avec les objectifs de croissance organique propres à la société, mais vise à l’installe dans une orientation de développement par croissance externe, «&nbsp;en vue de prendre des parts de marché mais aussi de compléter le portefeuille d’offres de Smile », explique encore Marc Palazon. Smile est ainsi positionné sur les secteurs du Web, du eCommerce, de la collaboration, du numérique en général et plus récemment sur l’infrastructure et l’infogérance. Mais la société a pour vocation de renforcer et de compléter le portefeuille de compétences et de technologies pour devenir au final une sorte de guichet unique de l’Open Source, lance-t-il (une «&nbsp;one-stop-shop&nbsp;», selon ses dires)...&quot;</p>',
            'date'      => '2015-11-05',
        ]);

        Presse::create([
            'title'     => 'Open Source : Smile et Open Wide discutent d\'un rapprochement',
            'link'      => 'http://www.lemondeinformatique.fr/actualites/lire-open-source-smile-et-open-wide-discutent-d-un-rapprochement-62894.html',
            'source'    => 'lemondeinformatique.fr - Oscar Barthe',
            'text'      => '<p>&quot;Les deux acteurs majeurs de l\'intégration Open Source en France que sont Smile et Open Wide pourraient bientôt n\'en former plus qu\'un. Les deux SSLL sont entrées en négociations exclusives en vue d\'un rapprochement.</p><p>Smile est entré en discussion exclusive avec Open Wide. La société de services en logiciels libres (SSLL) souhaite faire l\'acquisition de&nbsp;son concurrent&nbsp;dans le cadre de sa stratégie de développement. Avec ses 150 collaborateurs et ses 15 M€ de chiffre d\'affaires annuel, le groupe Open Wide compte déjà plusieurs clients grands comptes prestigieux à l\'instar de Thales, Airbus ou Zodiac. Son expertise dans les systèmes embarqués et en temps réel est en outre complémentaire à&nbsp;celle de Smile qui compte aujourd\'hui 800 collaborateurs pour 52,8 M€ de CA. En janvier 2104, ce dernier a&nbsp;<a href="http://www.lemondeinformatique.fr/actualites/lire-l-integrateur-open-source-smile-change-d-actionnaire-majoritaire-56230.html" target="_blank">changé d\'actionnaires majoritaire</a>
, ce dernier étant désormais Keensight Capital...&quot;</p>',
            'date'      => '2015-11-06',
        ]);

        Presse::create([
            'title'     => 'Smile va embaucher 25 personnes à Montpellier',
            'link'      => 'http://ftp.smile.fr/client/Communication/Presse/Article-LaLettreM.pdf',
            'source'    => 'La lettre M - Ysis Percq',
            'text'      => '<p>
Créée en 2000, l’agence Smile de Montpellier, spécialisée dans les solutions en open source et dans l’hébergement de sites, emploie<br />120 personnes, et prévoit d\'en embaucher 25 cette année. Postes à pourvoir : développeurs et chefs de projet...</p>',
            'date'      => '2015-06-11',
        ]);



        Evenements::create([
            'title'         => 'Smile partenaire du BlendWebMix 2016',
            'text_small'    => 'Evénement | LYON',
            'img_small'     => 'evenement',
            'text'          => '<p>Smile participera à la conférence BlendWebMix cette année en tant que partenaire Silver. Venez nombreux les 2 et 3 novembre !</p><p>Pour la 4e édition de la conférence BlendWebMix, Smile aura l\'honneur d\'y participer en tant que partenaire Silver.</p><p>BlendWebmix est un lieu de rencontre pour tous ceux qui &quot;font&quot; le web d\'aujourd\'hui, à savoir développeurs, designers, business developers, marketeux, chercheurs... et créé des liens entre toutes ces communautés. Le but pour cette conférence est simple: faire en sorte que chaque participant progresse et apprenne grâce aux partages. Cette conférence constitue donc une opportunité de rencontrer les acteurs de ce marché et de savoir de quoi sera fait le web d\'ici quelques années.&nbsp;</p><p>Si vous aussi vous souhaitez en apprendre plus sur les évolutions du web et préparer votre transition digitale, rejoignez nous au BlendWebMix !</p><p>Notre équipe sera présente tout au long de l\'événement.</p><p>À cette occasion, un workshop Smile sera également organisé dont le thème vous sera prochainement dévoilé.</p><p>Pour en savoir plus, rendez-vous sur :&nbsp;<a href="http://www.blendwebmix.com/" target="_blank">www.blendwebmix.com</a>
&nbsp;</p>  ',
            'date'          => '2016-11-02',
            'adresse'       => 'Cité internationale de Lyon',
        ]);
        Evenements::create([
            'title'         => 'Evènement e-commerce à Genève : boostez vos ventes en ligne avec Magento 2 et Akeneo ! ',
            'text_small'    => '<p>Smile Suisse vous invite à une après-midi complète dédiée au e-commerce le jeudi 13 octobre prochain dès 13h30 à Genève ! Inscrivez-vous !</p>


<p>2 événements en 1 animés par Smile, 1er intégrateur et infogéreur de solutions open source en Europe :</p><h3>Boostez vos ventes en ligne avec Magento 2</h3><p>Quelques mois après le lancement du nouveau Magento, découvrez les nouveautés de la solution e-commerce de référence.</p>
<ul>

<li>Quelles sont les différences fonctionnelles avec Magento 1 ?</li>

<li>Quelles sont les nouvelles fonctionnalités ?</li>

<li>Comment améliorer l’expérience utilisateur ?</li>

<li>Faut-il migrer ?</li>

</ul>
<h3>Gérez efficacement votre catalogue avec Akeneo</h3><p>Si vous aussi vous avez des milliers de produits diffusés sur votre site e-commerce, sur mobile ou en magasin avec des médias à mettre régulièrement à jour par différentes personnes, alors il vous faut un PIM !</p><p>Et pour cela, Smile et son partenaire Akeneo vous expliquent tout :</p>
<ul>

<li>Pourquoi un PIM ?</li>

<li>Cas d\'écoles / Retours d\'expérience</li>

<li>Démonstration</li>

</ul>
<p><b>Événement réservé aux professionnels. Inscription gratuite. Attention, places limitées.</b></p>',
            'img_small'     => 'seminaire',
            'text'          => 'Séminaire | GENÈVE',
            'date'          => '2016-11-13',
            'adresse'       => 'Hotel Royal Geneva',
        ]);
        Evenements::create([
            'title'         => 'Smile est Gold sponsor de la DrupalCon 2016 à Dublin',
            'text_small'    => 'Salon | DUBLIN',
            'img_small'     => 'salon',
            'text'          => '<p>Smile participe à la DrupalCon 2016 en tant que Gold sponsor. La conférence se déroulera à Dublin du 26 au 30 septembre.</p>


<p>Cette année, l\'Irlande accueillera la DrupalCon 2016 à Dublin. Cet événement réunira les utilisateurs de Drupal, le fameux CMS open source, autour de conférences, de formations, de discussions... et bien d\'autres ! Smile en tant que partenaire Gold de l\'événement aura droit à son propre stand.</p><p>La DrupalCon, c\'est l\'occasion pour échanger sur les bonnes pratiques d\'un métier et peut-être même de trouver de nouveaux partenaires avec qui collaborer. La DrupalCon aura lieu au Convention Centre de Dublin du 26 au 30 septembre.</p><p>Pour plus d\'informations, rendez-vous sur le site de l\'événement:&nbsp;<a href="https://events.drupal.org/dublin2016">https://events.drupal.org/dublin2016</a>
</p>',
            'date'          => '2016-09-26',
            'adresse'       => 'Convention Centre Dublin, Spencer Dock, N Wall Quay, Dublin',
        ]);
        Evenements::create([
            'title'         => 'Smile participe aux Rencontres Régionales du Logiciel Libre 2016',
            'text_small'    => 'Salon | NANTES',
            'img_small'     => 'salon',
            'text'          => '<p>Smile est une fois de plus sponsor des Rencontres Régionales du Logiciel Libre. Cette année, l\'événement a lieu à Nantes à l\'Euro Meeting Center. Rejoignez-nous le 20 septembre !</p>


<p>Dans le cadre de la Digital Week de Nantes, Smile sera présent au Rencontres régionales du Logiciel Libre comme chaque année avec un stand. A cette occasion, Smile interviendra sur le panorama des CMS Open Source. Rejoignez-nous le 20 septembre pour découvrir les solutions Open Source qui vous correspondent.</p><p>Pour en savoir plus sur l\'événement, vous pouvez vous rendre sur le site internet dédié:&nbsp;<a href="http://rrll.alliance-libre.org/" target="_blank">http://rrll.alliance-libre.org/</a>
</p> ',
            'date'          => '2016-09-20',
            'adresse'       => 'Nantes, Euro Meeting Center, 21 Rue de Cornulier',
        ]);
        Evenements::create([
            'title'         => 'Conférence « Big Data et Open Source » avec Smile et l\'EPSI',
            'text_small'    => 'Evénement | LYON',
            'img_small'     => 'evenement',
            'text'          => '<p>Smile et l\'EPSI vous invitent à la conférence &quot;Big Data et Open Source&quot; le 26 mai à Lyon.</p>


<p>Pour tout savoir sur le Big Data, le concept, l\'approche du phénomène, ses enjeux, ou encore les risques... Rendez-vous au Campus de l\'EPSI Lyon.</p><p><b>Notre expert Smile Rachid El Abouti</b>&nbsp;et Richard Reynier de HT Consult interviendront afin d\'établir un constat de la situation actuelle avant de dessiner des anticipations sur l\'avenir du Big Data.</p><p>L\'intervention présentera également des exemples mis en œuvre, les usages métiers, le ROI de cet outil et la place de l\'open source.</p><p>La soirée débutera à 18h30 et sera suivie d\'un cocktail dînatoire.</p><p>Pour s\'inscrire et obtenir plus d\'informations :&nbsp;<a href="http://bit.ly/246NWwZ" target="_blank">http://bit.ly/246NWwZ</a>
&nbsp;</p>    ',
            'date'          => '2015-05-26',
            'adresse'       => 'LYON',
        ]);
        Evenements::create([
            'title'         => 'Webinar Smile : L\'Open Source, un accélérateur d\'innovation pour les objets connectés',
            'text_small'    => 'webinar',
            'img_small'     => 'Webinar | FRANCE',
            'text'          => '<p>Notre expert&nbsp;Olivier Viné présentera la thématique&nbsp;&quot;L\'Open Source, un accélérateur d\'innovation pour les objets connectés&quot; le mardi 31 mai de 11h à 11h45.</p>


<p>Dans le cadre du&nbsp;<b>Forum Open Source</b>&nbsp;organisé sur la plateforme Webikeo, venez échanger avec notre expert.</p><p>Utiliser des logiciels Open Source pour créer des objets connectés, c\'est faire le choix de maîtriser sa technologie et de bénéficier de la mutualisation des efforts de R&amp;D d\'un nombre croissant d\'industriels et d\'experts.</p><p>Notre expert fera un petit tour d\'horizon des solutions disponibles pour accélérer votre Time to Market avec l\'Open Source !</p><h3>Principaux points abordés :</h3><p>- Panorama des technologies libres pour les objets connectés</p><p>- Les pièges à éviter</p><p>- Exemples concrets de mise en œuvre.</p><p><b>La participation est gratuite, <a href="https://webikeo.fr/webinar/l-open-source-un-accelerateur-d-innovation-pour-les-objets-connectes" target="_blank">inscrivez-vous !</a>',
            'date'          => '2016-05-31',
            'adresse'       => 'FRANCE',
        ]);
        Evenements::create([
            'title'         => 'Webinar Smile : L\'Open Source, un accélérateur d\'innovation pour les objets connectés',
            'text_small'    => 'webinar',
            'img_small'     => 'Webinar | FRANCE',
            'text'          => '<p>Notre expert&nbsp;Olivier Viné présentera la thématique&nbsp;&quot;L\'Open Source, un accélérateur d\'innovation pour les objets connectés&quot; le mardi 31 mai de 11h à 11h45.</p>


<p>Dans le cadre du&nbsp;<b>Forum Open Source</b>&nbsp;organisé sur la plateforme Webikeo, venez échanger avec notre expert.</p><p>Utiliser des logiciels Open Source pour créer des objets connectés, c\'est faire le choix de maîtriser sa technologie et de bénéficier de la mutualisation des efforts de R&amp;D d\'un nombre croissant d\'industriels et d\'experts.</p><p>Notre expert fera un petit tour d\'horizon des solutions disponibles pour accélérer votre Time to Market avec l\'Open Source !</p><h3>Principaux points abordés :</h3><p>- Panorama des technologies libres pour les objets connectés</p><p>- Les pièges à éviter</p><p>- Exemples concrets de mise en œuvre.</p><p><b>La participation est gratuite, <a href="https://webikeo.fr/webinar/l-open-source-un-accelerateur-d-innovation-pour-les-objets-connectes" target="_blank">inscrivez-vous !</a>',
            'date'          => '2016-05-11',
            'adresse'       => 'FRANCE',
        ]);
        Evenements::create([
            'title'         => 'Smile sera présent à la 11e édition de la Convention Systematic',
            'text_small'    => 'Evénement | PARIS',
            'img_small'     => 'evenement',
            'text'          => '<p>Smile et l’équipe du Pôle Systematic vous invitent à la 11e édition de la convention annuelle Systematic&nbsp;le mercredi 8 juin 2016 à la Maison de la Chimie à Paris.</p>


<p>La convention annuelle de Systematic Paris-Région&nbsp;rassemble chaque année plus de 600 acteurs industriels, académiques et institutionnels. Sa priorité : Favoriser et accélérer l\'Open Innovation en contribuant à des projets numériques et au développement des PME dans des secteurs d\'avenir tels que l\'énergie, les télécoms, les systèmes d\'information, etc.&nbsp;</p><p>Cette année, le thème de l’événement sera : &quot;Partageons l\'innovation, créons notre avenir&quot;.</p><p>Ce thème s\'inscrit dans la démarche constructive du pôle de compétitivité mondial et sera articulé autour de conférences de prestige, d\'ateliers thématiques et d\'espaces d\'échanges.</p><p>Véritable vitrine d\'innovations technologiques, la 11ème édition promet encore une fois d\'être une véritable réussite !&nbsp;</p><p><b>Smile sera présent sur l\'espace d\'exposition de 12h30 à 17h00, venez nous rencontrer !</b></p><p>Pour plus d\'informations et vous inscrire à l’événement :&nbsp;<a href="http://www.events-systematic-paris-region.org/" target="_blank">http://www.events-systematic-paris-region.org/</a>
&nbsp;</p> ',
            'date'          => '2016-07-08',
            'adresse'       => 'PARIS',
        ]);
        Evenements::create([
            'title'         => 'Smile sera présent à la 11e édition de la Convention Systematic',
            'text_small'    => 'Evénement | PARIS',
            'img_small'     => 'evenement',
            'text'          => '<p>Smile et l’équipe du Pôle Systematic vous invitent à la 11e édition de la convention annuelle Systematic&nbsp;le mercredi 8 juin 2016 à la Maison de la Chimie à Paris.</p>


<p>La convention annuelle de Systematic Paris-Région&nbsp;rassemble chaque année plus de 600 acteurs industriels, académiques et institutionnels. Sa priorité : Favoriser et accélérer l\'Open Innovation en contribuant à des projets numériques et au développement des PME dans des secteurs d\'avenir tels que l\'énergie, les télécoms, les systèmes d\'information, etc.&nbsp;</p><p>Cette année, le thème de l’événement sera : &quot;Partageons l\'innovation, créons notre avenir&quot;.</p><p>Ce thème s\'inscrit dans la démarche constructive du pôle de compétitivité mondial et sera articulé autour de conférences de prestige, d\'ateliers thématiques et d\'espaces d\'échanges.</p><p>Véritable vitrine d\'innovations technologiques, la 11ème édition promet encore une fois d\'être une véritable réussite !&nbsp;</p><p><b>Smile sera présent sur l\'espace d\'exposition de 12h30 à 17h00, venez nous rencontrer !</b></p><p>Pour plus d\'informations et vous inscrire à l’événement :&nbsp;<a href="http://www.events-systematic-paris-region.org/" target="_blank">http://www.events-systematic-paris-region.org/</a>
&nbsp;</p> ',
            'date'          => '2016-05-18',
            'adresse'       => 'PARIS',
        ]);
        Evenements::create([
            'title'         => 'IT Forum 2016',
            'text_small'    => 'Evénement | ABIDJAN',
            'img_small'     => 'evenement',
            'text'          => '<p>IT Forum 2016 Maison de l\'entreprise Abidjan du 07 au 08 Juin 2016 de 08 heures à 18 heures.</p>


<p>IT Forum 2016 réunira sur un même espace tous les DSI et Responsables Informatiques, qu’ils soient issus des milieux scientifiques, économiques, industriels, gouvernementaux, d’affaires ou d’exploitation. IT Forum 2016, c’est le lieu idéal pour faire des rencontres intéressantes et productives avec des :</p>
<ul>

<li>Dirigeants de compagnies des secteurs concernés ;</li>

<li>Représentants de compagnies offrant des services IT ;</li>

<li>Spécialistes, Experts et autres scientifiques à la fine pointe de la recherche dans les</li>

<li>domaines concernés ;</li>

<li>Spécialistes du Gouvernement en matière de statistiques, de législation, d’accompagnement, d’environnement des affaires, etc.</li>

</ul>
<p>En tant que professionnel et/ou spécialiste des systèmes d’information, IT Forum 2016 c’est profiter des partages d’expériences et renforcer ses connaissances sur des thématiques d’intérêt général et d’actualité dans le domaine des systèmes d’information. En tant qu’entreprise, IT Forum 2016, c’est :</p>
<ul>

<li>associer l’image de votre entreprise à un évènement de référence en Côte d’Ivoire ;</li>

<li>bénéficier de la visibilité assurée par la grande diffusion des supports de communication média, hors média et multimédia de l’évènement ;</li>

<li>réaffirmer l’intérêt de votre entreprise pour l’accompagnement des entreprises à la transition au numérique ;</li>

<li>la meilleure occasion de présenter votre entreprise ou institution à un public privilégié de professionnels et vous permettra d’ouvrir de nouvelles perspectives et opportunités réelles de partenariat.</li>

</ul>',
            'date'          => '2016-07-07',
            'adresse'       => 'ABIDJAN',
        ]);
        Evenements::create([
            'title'         => 'IT Forum 2016',
            'text_small'    => 'Evénement | ABIDJAN',
            'img_small'     => 'evenement',
            'text'          => '<p>IT Forum 2016 Maison de l\'entreprise Abidjan du 07 au 08 Juin 2016 de 08 heures à 18 heures.</p>


<p>IT Forum 2016 réunira sur un même espace tous les DSI et Responsables Informatiques, qu’ils soient issus des milieux scientifiques, économiques, industriels, gouvernementaux, d’affaires ou d’exploitation. IT Forum 2016, c’est le lieu idéal pour faire des rencontres intéressantes et productives avec des :</p>
<ul>

<li>Dirigeants de compagnies des secteurs concernés ;</li>

<li>Représentants de compagnies offrant des services IT ;</li>

<li>Spécialistes, Experts et autres scientifiques à la fine pointe de la recherche dans les</li>

<li>domaines concernés ;</li>

<li>Spécialistes du Gouvernement en matière de statistiques, de législation, d’accompagnement, d’environnement des affaires, etc.</li>

</ul>
<p>En tant que professionnel et/ou spécialiste des systèmes d’information, IT Forum 2016 c’est profiter des partages d’expériences et renforcer ses connaissances sur des thématiques d’intérêt général et d’actualité dans le domaine des systèmes d’information. En tant qu’entreprise, IT Forum 2016, c’est :</p>
<ul>

<li>associer l’image de votre entreprise à un évènement de référence en Côte d’Ivoire ;</li>

<li>bénéficier de la visibilité assurée par la grande diffusion des supports de communication média, hors média et multimédia de l’évènement ;</li>

<li>réaffirmer l’intérêt de votre entreprise pour l’accompagnement des entreprises à la transition au numérique ;</li>

<li>la meilleure occasion de présenter votre entreprise ou institution à un public privilégié de professionnels et vous permettra d’ouvrir de nouvelles perspectives et opportunités réelles de partenariat.</li>

</ul>',
            'date'          => '2016-06-09',
            'adresse'       => 'ABIDJAN',
        ]);
        Evenements::create([
            'title'         => 'Webinar Tout savoir sur Magento 2',
            'text_small'    => 'Webinar | FRANCE & BELGIQUE',
            'img_small'     => 'webinar',
            'text'          => '<p>Quelques mois après le lancement du nouveau Magento 2, vous êtes très nombreux à réfléchir à la pertinence de créer ou de migrer votre site vers la nouvelle plateforme.</p>


<p><b>**********************************************************************************************************************************************</b></p><p><b>Nous sommes désolés mais les inscriptions à ce webinar sont closes. D\'autres séminaires&nbsp;et&nbsp;webinars&nbsp;Smile sur différentes thématiques sont à venir très prochainement. Vous les retrouverez dans la&nbsp;<a href="/Evenements">rubrique Événements</a>
 du site Smile.</b></p><p><b>**********************************************************************************************************************************************</b></p><p>Pour répondre à vos interrogations, Smile a le plaisir de vous faire découvrir les nouveautés de la solution e-commerce de référence le mardi 21 Juin à 11h00.</p>                </div>
				<div class="breaker"></div>
									<div class="actualites-detail__content__programme">

<h3>Au programme :</h3>
<ul>

<li>Magento 2 : les différences fonctionnelles avec Magento 1</li>

<li>Expérience utilisateur : Zoom sur le nouveau template Luma, sur le tunnel de commande, sur les pages &quot;catégorie&quot;,...</li>

<li>Gestion Back Office : Interface repensée, Gestion des onglets</li>

<li>Nouvelles fonctionnalités : Bannière automatique, intégration de nouveau moyen de paiement, amélioration de la page de création/edition de produit ...</li>

<li>Magento 2 : road map de l\'éditeur (fréquences) + Features à venir</li>

<li>Magento 1 : que va-t-il devenir ?</li>

</ul>',
            'date'          => '2015-06-11',
            'adresse'       => 'FRANCE & BELGIQUE',
        ]);

        Evenements::create([
            'title'         => 'Webinar Tout savoir sur Magento 2',
            'text_small'    => 'Webinar | FRANCE & BELGIQUE',
            'img_small'     => 'webinar',
            'text'          => '<p>Quelques mois après le lancement du nouveau Magento 2, vous êtes très nombreux à réfléchir à la pertinence de créer ou de migrer votre site vers la nouvelle plateforme.</p>


<p><b>**********************************************************************************************************************************************</b></p><p><b>Nous sommes désolés mais les inscriptions à ce webinar sont closes. D\'autres séminaires&nbsp;et&nbsp;webinars&nbsp;Smile sur différentes thématiques sont à venir très prochainement. Vous les retrouverez dans la&nbsp;<a href="/Evenements">rubrique Événements</a>
 du site Smile.</b></p><p><b>**********************************************************************************************************************************************</b></p><p>Pour répondre à vos interrogations, Smile a le plaisir de vous faire découvrir les nouveautés de la solution e-commerce de référence le mardi 21 Juin à 11h00.</p>                </div>
				<div class="breaker"></div>
									<div class="actualites-detail__content__programme">

<h3>Au programme :</h3>
<ul>

<li>Magento 2 : les différences fonctionnelles avec Magento 1</li>

<li>Expérience utilisateur : Zoom sur le nouveau template Luma, sur le tunnel de commande, sur les pages &quot;catégorie&quot;,...</li>

<li>Gestion Back Office : Interface repensée, Gestion des onglets</li>

<li>Nouvelles fonctionnalités : Bannière automatique, intégration de nouveau moyen de paiement, amélioration de la page de création/edition de produit ...</li>

<li>Magento 2 : road map de l\'éditeur (fréquences) + Features à venir</li>

<li>Magento 1 : que va-t-il devenir ?</li>

</ul>',
            'date'          => '2015-06-11',
            'adresse'       => 'FRANCE & BELGIQUE',
        ]);






        Actualites::create([
            'title'         => 'Eram booste ses ventes en ligne avec Smile',
            'text'          => '<p>Smile accompagne l’enseigne Eram (leader de la chaussure en centre-ville et centres commerciaux) dans la mise en œuvre d’un site responsive design de nouvelle génération lui permettant de déployer à grande échelle sa stratégie de vente en ligne. <b>Découvrez le témoignage !</b></p>				</div>

<p>Dès 2009, Eram a pris le virage du digital pour lancer ses activités e-commerce. Fort du succès de cette première expérimentation et au regard de l’évolution de ses ventes, l’enseigne a souhaité en 2015 donner une nouvelle impulsion à son projet et déployer un<b> <a href="http://www.eram.fr/" target="_blank">nouveau site</a>
</b> reposant sur un socle technique performant et évolutif.</p><p>Dans ce contexte, après avoir lancé un appel d’offres, Eram a confié ce développement au groupe Smile. <b>La sélection de Smile s’explique par son expertise unique autour des technologies Magento, mais également par ses fortes compétences en matière de design et d’ergonomie</b>. Au-delà de ces éléments, la proximité des équipes de Smile a également représenté un facteur déterminant.</p><p>Très rapidement, les équipes d’Eram et de Smile (Paris et Nantes) ont défini une méthodologie de travail agile pour valider pas à pas les développements réalisés. Cela a permis de faire travailler en parallèle les départements Intégration et Design, de tester les développements et de mettre en ligne le nouveau site en moins de huit mois.</p><p>Grâce à ce dispositif, le parcours client a gagné en fluidité ce qui a notamment permis d’enrichir considérablement l’expérience utilisateur. Cela s’est traduit par une amélioration globale des performances : amélioration du taux de rebond de 40 %, temps moyen passé sur le site en augmentation de 80 %, croissance de 35 % du trafic&nbsp; entrant depuis les mobiles…</p><p><b>Renaud MONTIN, Directeur Marketing et digital d’Eram</b> « <i>Smile nous a permis d’accélérer la digitalisation de notre marque. L’expertise, la proximité et la réactivité de leurs équipes sont des points clés qui expliquent le succès de notre projet. Nous avons bénéficié d’un accompagnement et pouvons désormais nous appuyer sur une plateforme spécialement paramétrée pour répondre à nos attentes. Forts de ces éléments, nous sommes en mesure de renforcer notre avantage concurrentiel et de nous imposer comme le leader de la vente en ligne sur les click &amp; mortar chaussures.</i> »</p><p>Grâce à son nouveau site, Eram a également pu offrir des services cross-canal à forte valeur ajoutée tels que la visibilité des stocks magasin et un store locator. Au final, cela se traduit par une expérience de shopping en ligne adaptée aux nouveaux parcours d’achat. Smile a su également concevoir une architecture ouverte à d’autres environnements. Ainsi, à titre d’exemple, les équipes de l\'enseigne enrichissent les nombreuses références produits de la marque via la solution de PIM Akeneo qui est étroitement connectée au site. Le service de réservation en boutique est également intégré au site et se fait par l\'intermédiaire de la solution packagée Socloz.</p><p>Au-delà du développement et de l’intégration du site-commerce, Smile a enfin réalisé le<b> <a href="http://www.eram.fr/blog/" target="_blank">blog d’Eram</a>
</b> sous Wordpress. Engageant et convivial, il fait partie de la stratégie digitale d’Eram et contribue à développer l\'image de l’enseigne sur Internet. A noter également l\'intégration d\'Olapic, outil de curation de contenu sur Instagram, qui permet d\'enrichir les fiches produit du contenu généré par la communauté Eram autour du hashtag #myeramtouch.</p><p>Une nouvelle fois, <b>Smile a donc démontré sa capacité à accompagner les plus grandes marques dans leurs projets e-commerce</b> : du conseil à l’intégration en passant par l’ergonomie, le design et la formation des utilisateurs.</p>	',
            'date'          => '2016-07-09',
        ]);
        Actualites::create([
            'title'         => 'Nouveau livre blanc Smile : Linux pour l’embarqué',
            'text'          => '<p>Smile inaugure une nouvelle collection de livre blanc sur le thème de l\'embarqué. Le premier de la série est consacré à l\'utilisation de Linux embarqué dans les systèmes industriels.</p>				</div>

<p><b><a href="/Ressources/Livres-blancs/Ingenierie/Linux-pour-l-embarque">Téléchargez le livre blanc &gt;&gt;</a>
</b></p><p>Après une introduction et un historique sur les systèmes embarqués, l\'ouvrage propose de tester directement les meilleures solutions du marché sous la forme de cas pratiques.</p><p>Ainsi notre expert Pierre Ficheux, Directeur Technique, vous indique pas à pas la marche à suivre pour créer un système Linux minimal, puis de prendre en main les deux principales solutions utilisables de manière industrielle : Yocto et Builrooot.</p><p><i>&quot;Le livre blanc vous permettra par une approche technique, pragmatique et indépendante d\'un éditeur commercial d\'évaluer ces deux outils en réalisant des tests réels sur la célèbre plate-forme Raspberry Pi. Il sera une aide précieuse pour choisir l\'outil et l\'assistance technique qui conviennent le mieux à vos projets&quot;,</i> indique Pierre Ficheux.</p><p>Le livre blanc est <a href="/Ressources/Livres-blancs/Ingenierie/Linux-pour-l-embarque">téléchargeable&nbsp; gratuitement sur le site smile.fr &gt;&gt; </a>
</p><p>Le prochain opus de la collection sera consacré à &quot;<i>Linux et le temps réel</i>&quot;. A suivre !</p>',
            'date'          => '2016-07-02',
        ]);
        Actualites::create([
            'title'         => 'Webinars Smile : Notre expertise au coeur de votre succès',
            'text'          => '<p>Le printemps de Smile c\'est aussi le temps des webinars. Pour tout savoir sur les solutions Open Source, retrouvez notre programme complet en mai &amp; juin 2016.</p>				</div>

<p>Nos experts vous donnent rendez-vous à l\'occasion de quatre webinars :</p>
<ul>

<li><a href="http://www.smile.fr/Evenements/L-open-source-un-accelerateur-d-innovation-pour-les-objets-connectes" target="_blank">L\'Open Source, un accélérateur d\'innovation pour les objets connectés</a>
 par <b>Olivier Viné</b> le mardi 31 mai de 11h à 11h45</li>

<li><a href="http://www.smile.fr/Evenements/Decouvrez-alfresco-5-1-la-solution-pour-une-gestion-documentaire-efficace" target="_blank">Découvrez Alfresco 5.1, la solution pour une gestion documentaire efficace</a>
 par <b>Olivier Favreau</b> le mercredi 1er juin de 10h à 10h45</li>

<li><a href="http://www.smile.fr/Evenements/Webinar-drupal-8-2016" target="_blank">Drupal 8 : Nouveautés et retours d\'expérience</a>
 par <b>Vincent Maucorps</b> et <b>Vincent Bourbon</b> le jeudi 9 juin de 13h30 à 14h30</li>

<li><a href="http://www.smile.fr/Evenements/Webinar-magento-2-juin-2016" target="_blank">Tout savoir sur Magento 2</a>
 par <b>Florent Guilbard</b> le mardi 21 juin à partir de 11h</li>

</ul>
<p>Inscrivez-vous et retrouvez tous nos événements dans <a href="http://www.smile.fr/Evenements" target="_blank">notre rubrique dédiée</a>
.</p>',
            'date'          => '2016-05-31',
        ]);
        Actualites::create([
            'title'         => 'Lancement d\'Open Source School à Bordeaux !',
            'text'          => '<p>L\'Open Source School continue sa route et s\'arrête à Bordeaux le 7 juin !</p>				</div>

<p><b>Emmanuelle RIPERT-CHOLLET</b>, Directrice du développement économique, de l\'emploi et de l\'enseignement supérieur à la Mairie de Bordeaux sera présente pour l’événement.</p><p><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</p><p>La table-ronde du jour portera sur «L’open source, facteur de croissance et d’emploi» et sera animée par <b>Nicolas CESAR</b>, Journaliste à Sud-Ouest Eco, avec la participation de:</p>
<ul>

<li>Fabien CAUCHY, Délégué régional Syntec numérique Aquitaine</li>

<li>Jean-Paul CHIRON, Expert logiciels libres</li>

<li>Jean-Christophe ELINEAU, Créateur d’Aquinetic, CoPrésident du Paris Open Source Summit</li>

<li>Thierry ROUBY, Chargé de mission au SYRPIN</li>

<li>Vincent RIBES, Directeur adjoint chez DIGITAL AQUITAINE</li>

<li>Cécilia GALTIER, Directrice de projet chez Smile Bordeaux</li>

</ul>
<p>Retrouvez-nous sur le campus d\'OSS Bordeaux,&nbsp;114 rue Lucien Faure&nbsp;33000 Bordeaux.</p><p>La table ronde sera suivie d\'un cocktail&nbsp;dînatoire.</p><p><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</p><p>Inscriptions et infos pratiques sur :&nbsp;<a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">www.opensourceschool.fr</a>
&nbsp;&nbsp;</p>				',
            'date'          => '2016-05-26',
        ]);
        Actualites::create([
            'title'         => 'Open Source School à Nantes : Lancement le 31 mai 2016 !',
            'text'          => '<p>Le tour de France du lancement de l\'Open Source School continue. Prochaine étape : Nantes le 31 mai !</p>				</div>

<p>L\'évènement se tiendra de 11h à 13h en présence de <b>Bassem ASSEH</b>, Adjoint du maire de Nantes, Chargé du dialogue citoyen.</p><p><b><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</b></p><p>Une table ronde « L’open source facteur de croissance et d’emploi » sera animée par Jérôme Cohonner, Fondateur de la société LisActiv\', avec la participation de :</p>
<ul>

<li>Jean Paul CHAPRON, Délégué régional Ouest, Syntec Numérique</li>

<li>Marc SABOUREAU, Président Alliance Libre et Vice-président du Conseil National du Logiciel Libre</li>

<li>Guillaume Thomassian, Référent Open Source GIE IRIS, Système U</li>

<li>Nicolas DUPONT, Co-fondateur et lead developper d\'Akeneo</li>

</ul>
<p>Retrouvez-nous au Sartup Palace, 18 rue Scribe, 44000 Nantes.</p><p>La table ronde sera suivie d\'un cocktail dinatoire.&nbsp;</p><p><b><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</b></p><p>Inscriptions et infos pratiques sur :&nbsp;<a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">www.opensourceschool.fr</a>
&nbsp;</p>',
            'date'          => '2016-05-20',
        ]);
        Actualites::create([
            'title'         => 'Ne manquez pas les événements de lancement de l\'Open Source School à Montpellier et Lille',
            'text'          => '<p>Après Lyon le 12 mai, ce sera au tour de Montpellier le 19 mai et Lille le 25 mai d\'accueillir la conférence de l\'Open Source School !</p>				</div>

<p>Ne manquez pas la conférence de lancement de l\'Open Source School en tournée dans toute la France.</p><h5><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">Inscrivez-vous !</a>
</h5><p><b>Au programme</b> :</p>
<ul>

<li>de 11h00-12h30 : présentation de l’école Open Source School, puis table-ronde sur le thème « l’Open source facteur de croissance et d’emploi », animée par Patrick BENICHOU, Fondateur d\'OPEN WIDE et avec la participation de:</li>

<li>12h30-13h30 : cocktail déjeunatoire.</li>

</ul>
<p><b>A Montpellier : le Jeudi 19 Mai 2016.</b></p><p>En présence de Philippe SAUREL, Maire de Montpellier et Président de Montpellier Méditerranée Métropole, la table ronde donnera la parole à des experts de l\'open source : </p>
<ul>

<li>Sébastien DUBOIS, Co-fondateur de Evolix, Vice-president de Medinsoft en charge de Libertis</li>

<li>Pierre DENISET, Président de la French South Digital</li>

<li>Bérengère POYET, Responsable des ressources humaines, CAPGEMINI MONTPELLIER</li>

<li>Sébastien DI CEGLIE, Etudiant en alternance OSS.</li>

</ul>
<p><b>A Lille : le Mercredi 25 Mai 2016.</b></p><h5><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">Inscrivez-vous !</a>
</h5><p>Inscriptions et infos pratiques sur :&nbsp;<a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">www.opensourceschool.fr</a>
</p>				',
            'date'          => '2016-05-19',
        ]);
        Actualites::create([
            'title'         => 'Smile décroche le label HappyAtWork for Starters de l’année 2016 !',
            'text'          => '<p>HappyAtWork for Starters est le premier label des entreprises où il fait bon débuter sa carrière. Il récompense l’excellence dans le management et la motivation des salariés avec la réalisation d’une enquête participative.</p>				</div>

<p>Décerné par Les Echos Start &amp; meilleures-entreprises.com, HappyAtWork for Starters s’appuie sur les critères suivants&nbsp;:</p>
<ul>

<li>6 dimensions prises en compte : développement professionnel - environnement stimulant - management &amp; motivation - salaire et reconnaissance - fierté - plaisir</li>

<li>Collaborateurs de moins de 28 ans.</li>

</ul>
<p>Plus de 100 collaborateurs de Smile ont participé à l’enquête !</p><p>&quot;<i>Nous sommes fiers d\'avoir obtenu ce label qui résulte des avis positifs à l’égard de Smile confirmés par les collaborateurs qui nous ont rejoints depuis moins d\'1 an, car il est le reflet de leur épanouissement au sein de notre ESN et c\'est notre ambition au quotidien d\'y contribuer.</i></p><p><i>Avec plus de 300 recrutements prévus cette année, nous espérons que d\'autres talents auront envie de prendre part à cette belle aventure !</i>&quot;, confie Luc Fayet, Directeur des Ressources Humaines chez Smile.</p><p>Merci à tous nos collaborateurs&nbsp;!</p>  ',
            'date'          => '2016-05-12',
        ]);
        Actualites::create([
            'title'         => 'Webinars Smile : Notre expertise au coeur de votre succès',
            'text'          => '<p>Le printemps de Smile c\'est aussi le temps des webinars. Pour tout savoir sur les solutions Open Source, retrouvez notre programme complet en mai &amp; juin 2016.</p>				</div>

<p>Nos experts vous donnent rendez-vous à l\'occasion de quatre webinars :</p>
<ul>

<li><a href="http://www.smile.fr/Evenements/L-open-source-un-accelerateur-d-innovation-pour-les-objets-connectes" target="_blank">L\'Open Source, un accélérateur d\'innovation pour les objets connectés</a>
 par <b>Olivier Viné</b> le mardi 31 mai de 11h à 11h45</li>

<li><a href="http://www.smile.fr/Evenements/Decouvrez-alfresco-5-1-la-solution-pour-une-gestion-documentaire-efficace" target="_blank">Découvrez Alfresco 5.1, la solution pour une gestion documentaire efficace</a>
 par <b>Olivier Favreau</b> le mercredi 1er juin de 10h à 10h45</li>

<li><a href="http://www.smile.fr/Evenements/Webinar-drupal-8-2016" target="_blank">Drupal 8 : Nouveautés et retours d\'expérience</a>
 par <b>Vincent Maucorps</b> et <b>Vincent Bourbon</b> le jeudi 9 juin de 13h30 à 14h30</li>

<li><a href="http://www.smile.fr/Evenements/Webinar-magento-2-juin-2016" target="_blank">Tout savoir sur Magento 2</a>
 par <b>Florent Guilbard</b> le mardi 21 juin à partir de 11h</li>

</ul>
<p>Inscrivez-vous et retrouvez tous nos événements dans <a href="http://www.smile.fr/Evenements" target="_blank">notre rubrique dédiée</a>
.</p>',
            'date'          => '2016-01-31',
        ]);
        Actualites::create([
            'title'         => 'Lancement d\'Open Source School à Bordeaux !',
            'text'          => '<p>L\'Open Source School continue sa route et s\'arrête à Bordeaux le 7 juin !</p>				</div>

<p><b>Emmanuelle RIPERT-CHOLLET</b>, Directrice du développement économique, de l\'emploi et de l\'enseignement supérieur à la Mairie de Bordeaux sera présente pour l’événement.</p><p><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</p><p>La table-ronde du jour portera sur «L’open source, facteur de croissance et d’emploi» et sera animée par <b>Nicolas CESAR</b>, Journaliste à Sud-Ouest Eco, avec la participation de:</p>
<ul>

<li>Fabien CAUCHY, Délégué régional Syntec numérique Aquitaine</li>

<li>Jean-Paul CHIRON, Expert logiciels libres</li>

<li>Jean-Christophe ELINEAU, Créateur d’Aquinetic, CoPrésident du Paris Open Source Summit</li>

<li>Thierry ROUBY, Chargé de mission au SYRPIN</li>

<li>Vincent RIBES, Directeur adjoint chez DIGITAL AQUITAINE</li>

<li>Cécilia GALTIER, Directrice de projet chez Smile Bordeaux</li>

</ul>
<p>Retrouvez-nous sur le campus d\'OSS Bordeaux,&nbsp;114 rue Lucien Faure&nbsp;33000 Bordeaux.</p><p>La table ronde sera suivie d\'un cocktail&nbsp;dînatoire.</p><p><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</p><p>Inscriptions et infos pratiques sur :&nbsp;<a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">www.opensourceschool.fr</a>
&nbsp;&nbsp;</p>				',
            'date'          => '2016-02-18',
        ]);
        Actualites::create([
            'title'         => 'Open Source School à Nantes : Lancement le 31 mai 2016 !',
            'text'          => '<p>Le tour de France du lancement de l\'Open Source School continue. Prochaine étape : Nantes le 31 mai !</p>				</div>

<p>L\'évènement se tiendra de 11h à 13h en présence de <b>Bassem ASSEH</b>, Adjoint du maire de Nantes, Chargé du dialogue citoyen.</p><p><b><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</b></p><p>Une table ronde « L’open source facteur de croissance et d’emploi » sera animée par Jérôme Cohonner, Fondateur de la société LisActiv\', avec la participation de :</p>
<ul>

<li>Jean Paul CHAPRON, Délégué régional Ouest, Syntec Numérique</li>

<li>Marc SABOUREAU, Président Alliance Libre et Vice-président du Conseil National du Logiciel Libre</li>

<li>Guillaume Thomassian, Référent Open Source GIE IRIS, Système U</li>

<li>Nicolas DUPONT, Co-fondateur et lead developper d\'Akeneo</li>

</ul>
<p>Retrouvez-nous au Sartup Palace, 18 rue Scribe, 44000 Nantes.</p><p>La table ronde sera suivie d\'un cocktail dinatoire.&nbsp;</p><p><b><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">&gt;&gt; Inscrivez-vous !</a>
</b></p><p>Inscriptions et infos pratiques sur :&nbsp;<a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">www.opensourceschool.fr</a>
&nbsp;</p>',
            'date'          => '2016-03-11',
        ]);
        Actualites::create([
            'title'         => 'Ne manquez pas les événements de lancement de l\'Open Source School à Montpellier et Lille',
            'text'          => '<p>Après Lyon le 12 mai, ce sera au tour de Montpellier le 19 mai et Lille le 25 mai d\'accueillir la conférence de l\'Open Source School !</p>				</div>

<p>Ne manquez pas la conférence de lancement de l\'Open Source School en tournée dans toute la France.</p><h5><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">Inscrivez-vous !</a>
</h5><p><b>Au programme</b> :</p>
<ul>

<li>de 11h00-12h30 : présentation de l’école Open Source School, puis table-ronde sur le thème « l’Open source facteur de croissance et d’emploi », animée par Patrick BENICHOU, Fondateur d\'OPEN WIDE et avec la participation de:</li>

<li>12h30-13h30 : cocktail déjeunatoire.</li>

</ul>
<p><b>A Montpellier : le Jeudi 19 Mai 2016.</b></p><p>En présence de Philippe SAUREL, Maire de Montpellier et Président de Montpellier Méditerranée Métropole, la table ronde donnera la parole à des experts de l\'open source : </p>
<ul>

<li>Sébastien DUBOIS, Co-fondateur de Evolix, Vice-president de Medinsoft en charge de Libertis</li>

<li>Pierre DENISET, Président de la French South Digital</li>

<li>Bérengère POYET, Responsable des ressources humaines, CAPGEMINI MONTPELLIER</li>

<li>Sébastien DI CEGLIE, Etudiant en alternance OSS.</li>

</ul>
<p><b>A Lille : le Mercredi 25 Mai 2016.</b></p><h5><a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">Inscrivez-vous !</a>
</h5><p>Inscriptions et infos pratiques sur :&nbsp;<a href="http://www.opensourceschool.fr/oss/roadshow-oss/" target="_blank">www.opensourceschool.fr</a>
</p>				',
            'date'          => '2016-01-19',
        ]);
        Actualites::create([
            'title'         => 'Smile décroche le label HappyAtWork for Starters de l’année 2016 !',
            'text'          => '<p>HappyAtWork for Starters est le premier label des entreprises où il fait bon débuter sa carrière. Il récompense l’excellence dans le management et la motivation des salariés avec la réalisation d’une enquête participative.</p>				</div>

<p>Décerné par Les Echos Start &amp; meilleures-entreprises.com, HappyAtWork for Starters s’appuie sur les critères suivants&nbsp;:</p>
<ul>

<li>6 dimensions prises en compte : développement professionnel - environnement stimulant - management &amp; motivation - salaire et reconnaissance - fierté - plaisir</li>

<li>Collaborateurs de moins de 28 ans.</li>

</ul>
<p>Plus de 100 collaborateurs de Smile ont participé à l’enquête !</p><p>&quot;<i>Nous sommes fiers d\'avoir obtenu ce label qui résulte des avis positifs à l’égard de Smile confirmés par les collaborateurs qui nous ont rejoints depuis moins d\'1 an, car il est le reflet de leur épanouissement au sein de notre ESN et c\'est notre ambition au quotidien d\'y contribuer.</i></p><p><i>Avec plus de 300 recrutements prévus cette année, nous espérons que d\'autres talents auront envie de prendre part à cette belle aventure !</i>&quot;, confie Luc Fayet, Directeur des Ressources Humaines chez Smile.</p><p>Merci à tous nos collaborateurs&nbsp;!</p>  ',
            'date'          => '2016-05-12',
        ]);

        Communiques::create([
            'title'         => 'Smile ouvre 300 nouveaux postes',
            'text'          => 'Après avoir recruté 260 collaborateurs en 2015, Smile, leader européen de l’intégration et l’infogérance de solutions open source, annonce une nouvelle campagne de recrutement de grande envergure',
            'files'          => '/files/CP Recrutement 2016.pdf',
            'date'          => '2016-02-22',
        ]);

        Communiques::create([
            'title'         => 'Nouvelle croissance pour Smile en 2015',
            'text'          => 'Smile annonce une forte croissance en 2015 tant en chiffre d’affaires, qu’en effectif ou en nombre de projets d’envergure réalisés. Une année exceptionnelle qui souligne la demande toujours plus forte des plus grands donneurs d’ordres en matière d’open source.',
            'files'          => '/files/CP Perspectives 2016.pdf',
            'date'          => '2016-02-04',
        ]);

        Communiques::create([
            'title'         => 'Open Source School : lancement de la première école spécialisée dans l’informatique open source',
            'text'          => 'Smile, leader européen de l’intégration et de l’infogérance open source et l’EPSI, établissement privé pionnier de l’enseignement supérieur en informatique lancent OSS - Open Source School, la première école d’enseignement supérieur et de formation continue dans l’open source.',
            'files'          => '/files/CP_OSS_lancement-5.pdf',
            'date'          => '2016-02-01',
        ]);
        Communiques::create([
            'title'         => 'Smile ouvre 300 nouveaux postes',
            'text'          => 'Après avoir recruté 260 collaborateurs en 2015, Smile, leader européen de l’intégration et l’infogérance de solutions open source, annonce une nouvelle campagne de recrutement de grande envergure',
            'files'          => '/files/CP Recrutement 2016.pdf',
            'date'          => '2015-11-22',
        ]);

        Communiques::create([
            'title'         => 'Nouvelle croissance pour Smile en 2015',
            'text'          => 'Smile annonce une forte croissance en 2015 tant en chiffre d’affaires, qu’en effectif ou en nombre de projets d’envergure réalisés. Une année exceptionnelle qui souligne la demande toujours plus forte des plus grands donneurs d’ordres en matière d’open source.',
            'files'          => '/files/CP Perspectives 2016.pdf',
            'date'          => '2015-09-24',
        ]);

        Communiques::create([
            'title'         => 'Open Source School : lancement de la première école spécialisée dans l’informatique open source',
            'text'          => 'Smile, leader européen de l’intégration et de l’infogérance open source et l’EPSI, établissement privé pionnier de l’enseignement supérieur en informatique lancent OSS - Open Source School, la première école d’enseignement supérieur et de formation continue dans l’open source.',
            'files'          => '/files/CP_OSS_lancement-5.pdf',
            'date'          => '2015-11-11',
        ]);

        Communiques::create([
            'title'         => 'Premier prix Communication & Entreprise du site internet décerné à Safran et Smile',
            'text'          => 'Communication & Entreprise récompense chaque année depuis 29 ans les métiers de la profession et a remis ses Grands Prix fin novembre au Cirque d’Hiver Bouglione, à Paris. Le 1er Prix du site internet a été décerné à Safran pour la refonte de son écosystème digital pour lequel Smile a œuvré.',
            'files'          => '/files/12_Prix Communication & Entreprise -Safran-Smile.pdf',
            'date'          => '2015-12-16',
        ]);

        Communiques::create([
            'title'         => 'Smile en négociation exclusive avec le groupe Open Wide',
            'text'          => 'Smile, 1er intégrateur de solutions Open Source, en négociation exclusive avec le groupe Open Wide en vue d’élargir son offre et renforcer son leadership.',
            'files'          => '/files/CP - Open Wide Smile.pdf',
            'date'          => '2015-11-05',
        ]);

        Communiques::create([
            'title'         => 'Big Data, l\'édition 2015 du livre blanc Smile',
            'text'          => 'Smile, 1er intégrateur et expert Européen de solutions open source, publie aujourd\'hui une nouvelle version du livre blanc Big Data. Avec cette nouvelle édition de 59 pages, Smile complète les usages et prend en compte les derniers apports de l\'éco-système Big Data qui voit des évolutions rapides...',
            'files'          => '/files/10_LB-Big_Data_2015.pdf',
            'date'          => '2015-10-15',
        ]);
        Communiques::create([
            'title'         => 'Premier prix Communication & Entreprise du site internet décerné à Safran et Smile',
            'text'          => 'Communication & Entreprise récompense chaque année depuis 29 ans les métiers de la profession et a remis ses Grands Prix fin novembre au Cirque d’Hiver Bouglione, à Paris. Le 1er Prix du site internet a été décerné à Safran pour la refonte de son écosystème digital pour lequel Smile a œuvré.',
            'files'          => '/files/12_Prix Communication & Entreprise -Safran-Smile.pdf',
            'date'          => '2015-02-11',
        ]);

        Communiques::create([
            'title'         => 'Smile en négociation exclusive avec le groupe Open Wide',
            'text'          => 'Smile, 1er intégrateur de solutions Open Source, en négociation exclusive avec le groupe Open Wide en vue d’élargir son offre et renforcer son leadership.',
            'files'          => '/files/CP - Open Wide Smile.pdf',
            'date'          => '2015-10-02',
        ]);

        Communiques::create([
            'title'         => 'Big Data, l\'édition 2015 du livre blanc Smile',
            'text'          => 'Smile, 1er intégrateur et expert Européen de solutions open source, publie aujourd\'hui une nouvelle version du livre blanc Big Data. Avec cette nouvelle édition de 59 pages, Smile complète les usages et prend en compte les derniers apports de l\'éco-système Big Data qui voit des évolutions rapides...',
            'files'          => '/files/10_LB-Big_Data_2015.pdf',
            'date'          => '2015-06-15',
        ]);
//        Communiques::create([
//            'title'         => '',
//            'text'          => '',
//            'files'          => '/files/',
//            'date'          => '2016-06-11',
//        ]);


        References::create([
            'title' => 'KRYS',
            'title2'=> 'Un site mobile pour Krys',
            'image'=> '/img/KRYS_reference_logo.png',
            'domaines'=> '1',
            'offres'=> '2',
            'services'=> '2',
            'outils'=> '4',
            'link'=> 0,
            'slang'=> 'Hélène BARTOLUCCI, Responsable Marketing Digital pour Krys',
            'comment'=> '<p>Smile s\'est occupé de la mise en place du site mobile de Krys.</p><p>« Le retour est complètement positif. Nous observons d’ores et déjà un bon trafic qui s\'est créé sur le site. Il faudra encore un peu de temps pour que le référencement naturel du site se mette en place, mais nous observons déjà un volume de vente significatif issu du site mobile, qui valide notre stratégie de vente. Nous avons maintenant la volonté d\'étendre la gamme des produits disponibles sur le site mobile pour offrir encore plus de service à nos clients. »</p><p>« Ce projet a été un des meilleurs dans notre collaboration avec SMILE en tenant la qualité, le coût et les délais dans un contexte de demande de réactivité très forte. Des ateliers de travail et d\'échange ont été mis en place, de façon à construire itérativement chaque aspect du projet. Nous avons coconstruit le site mobile d\'une manière souple, nous appuyant surtout sur des maquettes et présentations. Cela nous a permis de nous mettre rapidement en accord sur les fondations.&nbsp;»</p><p>«&nbsp;Nous avions pour objectif de maintenir une date de publication très proche du lancement, les délais étaient donc très courts. La mobilisation interne de Krys comme la grande réactivité des équipes SMILE ont permis un temps de réalisation record (4 mois environ) avec des circuits de validations très courts. La mise en place d’une démarche agile a été très profitable.»</p>',
            'text'=> '<p>Le 25 février dernier, Krys, l’enseigne d’optique publiait son site web à destination des appareils mobiles. Ce site, conçu comme une extension de la plateforme e-commerce traditionnelle, a été mis en place avec la collaboration des équipes bordelaises de SMILE et de l’Agence Digitale SMILE. À l’occasion du lancement du site, nous avons interviewé Hélène BARTOLUCCI, Responsable Marketing Digital pour Krys.</p><p>
<b>Quelles sont les caractéristiques du site mobile Krys.com ?</b><br />
H.B. : <br />« Ce site mobile est dédié aux produits de contactologie (lentilles et produits d\'entretien), il permet d’accéder facilement aux pages des magasins d\'optique et se veut une extension mobile du site marchand, permettant de présenter un sous-ensemble des produits disponibles sur la boutique classique avec une continuité de l’expérience consommateur. Le client peut alternativement sélectionner et valider ses achats sur la plateforme standard ou mobile. Le site mobile offre par exemple à nos clients la possibilité de renouveler une commande et de régler leurs achats en un clic. Il permet aussi d’accéder à notre newsletter et à des informations pratiques. »</p><p>
<b>Pourquoi avoir choisi SMILE pour la réalisation du site mobile ?</b><br />
H.B. : <br />« Nous avons appréhendé ce nouveau projet de façon autonome, en lançant un nouvel appel d’offres spécifique dans lequel SMILE était consulté. Nous n\'étions pas focalisés sur les technologies Open Source. À nos yeux, c\'est la réponse au besoin du métier qui nous semblait prépondérante. SMILE avait déjà engrangé une bonne expérience sur la réalisation de notre site « classique ». Cette connaissance acquise a permis à SMILE de proposer la solution la plus pertinente. »</p><p>
<b>Comment s\'est déroulée la collaboration avec SMILE ?</b> <br />
H.B. : <br />« Ce projet a été un des meilleurs dans notre collaboration avec SMILE en tenant la qualité, le coût et les délais dans un contexte de demande de réactivité très forte. Des ateliers de travail et d\'échange ont été mis en place, de façon à construire itérativement chaque aspect du projet. Nous avons coconstruit le site mobile d\'une manière souple, nous appuyant surtout sur des maquettes et présentations. Cela nous a permis de nous mettre rapidement en accord sur les fondations.</p><p>Par ailleurs, les maquettes &quot;animées&quot; proposées par SMILE possédaient déjà des comportements et permettaient de rendre compte de la logique de navigation et de l\'ergonomie du site. Cette méthode nous a permis de converger vers une solution sans écueil.</p><p>Nous avons aussi été accompagnés sur l\'ensemble du projet pour l\'agence digitale de SMILE. L’agence avait pour responsabilité l’ensemble de la partie graphique (pictogrammes, charte des couleurs et dégradés) sur la base des contraintes d’une charte graphique du groupe Krys.&nbsp;</p><p>Nous avions pour objectif de maintenir une date de publication très proche du lancement, les délais étaient donc très courts. La mobilisation interne de Krys comme la grande réactivité des équipes SMILE ont permis un temps de réalisation record (4 mois environ) avec des circuits de validations très courts. La mise en place d’une démarche agile a été très profitable.»</p><p>
<b>Avez-vous un retour d\'expérience suite à ses nouveaux développements ?</b><br />
H.B. :<br />« Le retour est complètement positif. Nous observons d’ores et déjà un bon trafic qui s\'est créé sur le site. Il faudra encore un peu de temps pour que le référencement naturel du site se mette en place, mais nous observons déjà un volume de vente significatif issu du site mobile, qui valide notre stratégie de vente. Nous avons maintenant la volonté d\'étendre la gamme des produits disponibles sur le site mobile pour offrir encore plus de service à nos clients. »</p> ',
        ]);
        References::create([
            'title' => 'Safran',
            'title2'=> 'Retour aux références',
            'image'=> '/img/Safran_reference_logo.jpg',
            'domaines'=> '7',
            'offres'=> '5',
            'services'=> '1',
            'outils'=> '4',
            'link'=> 'www.safran-group.com/fr',
            'slang'=> 'Géraldine BUJADOUX, Responsable Internet, direction de la communication de Safran',
            'comment'=> 'p>Safran a confié à Smile la mise en place d’une usine à sites, développée avec le CMS Drupal.</p><p><i>« Le projet est initialement issu d\'un appel d\'offres rédigé en interne. En phase amont du projet, nous avons étudié les dossiers des agences pertinentes dans le domaine de l’intégration open source. Au cours de cette étape nous avons assisté à un séminaire organisé par Smile. Convaincus par la présentation, nous avons proposé à SMILE après quelques échanges de faire partie de cet appel d’offres. »</i></p><p>
<i>« Durant la phase de développement, Smile a fait preuve d’une grande écoute et d’une compréhension approfondie de nos besoins aboutissant à la rédaction de spécifications techniques efficientes.</i><br /><i>Avec Smile nous avons créé une réelle équipe. L’équipe lyonnaise de Smile est pour beaucoup dans la réussite de ce projet, par son investissement quotidien pendant plus d’une année et une direction de projet très attentive. »</i></p><p><i>« Les retours sont très positifs. Les contributeurs apprécient la facilité d’utilisation de la solution proposée, ce qui représentait pour nous un enjeu majeur du projet.</i></p><p><i>Les utilisateurs des sites sont enthousiastes vis-à-vis des services proposés, comme de l’ergonomie et du graphisme des sites. Il y aura certainement quelques petites évolutions à prévoir, mais quelques semaines après leur déploiement, nous observons déjà une augmentation de fréquentation des sites pilotes. »</i></p>',
            'text'=> '<p>Le projet confié à Smile par SAFRAN avait pour but la mise en place d’une usine à sites. Cette usine permet aux sociétés du groupe SAFRAN de produire des sites selon un schéma cohérent pour l’ensemble du groupe. Chaque entité peut bénéficier d’un template de site orienté vers ses besoins (groupe, société, pays, événementiel, support métier). La gestion des contenus est autonome, mais permet aussi aux différents contributeurs de sélectionner et partager les contenus disponibles sur le portail pour leurs propres besoins de communication (agenda des événements, communiqués de presse). Après une phase de déploiement pilote réussie, SAFRAN souhaite déployer la solution sur l’intégralité de ses sites avant le salon du Bourget 2015.</p><p>Smile a piloté les aspects techniques de la réalisation du projet. Développée avec Drupal, l’usine met notamment en place un répertoire de contenus géré en mode push, une innovation qui permet aux contributeurs de bénéficier en temps réel des contenus produits par SAFRAN.</p><p>Quatre sites ont été déployés à ce jour : <a href="http://www.safran-group.com/fr" target="_blank">www.safran-group.com/fr</a>
, <a href="http://www.safran-usa.com/" target="_blank">www.safran-usa.com</a>
, <a href="http://www.sagem-ds.com/fr" target="_blank">www.sagem-ds.com/fr</a>
 et <a href="http://www.techspace-aero.be/fr" target="_blank">www.techspace-aero.be/fr</a>
.</p><p><b>Pourquoi avoir choisi l’intégrateur Smile pour ce projet ?</b></p><p>G.B. : « Le projet est initialement issu d\'un appel d\'offres rédigé en interne. En phase amont du projet, nous avons étudié les dossiers des agences pertinentes dans le domaine de l’intégration open source. Au cours de cette étape nous avons assisté à un séminaire organisé par Smile. Convaincus par la présentation, nous avons proposé à SMILE après quelques échanges de faire partie de cet appel d’offres.</p><p><b>L’adoption d’une solution open source était-elle une démarche volontaire pour SAFRAN ?</b></p><p>
G.B. : « Tout à fait, c\'était une volonté de l’équipe dès l’amorce du projet et qui a été soutenue par la DSI Groupe. Nous avions déjà une expérience en la matière avec notre précédent gestionnaire de contenu, SPIP, un produit open source.<br />Bénéficier d’une communauté active autour d’une solution open source nous permet de faire évoluer rapidement et plus facilement les services que nous proposons. Une démarche d’intégration open source libère d’un lien contraignant avec un logiciel propriétaire. Cela permet aussi d’avoir plus de choix dans la sélection de nos prestataires. »</p><p><b>La solution préconisée par Smile pour l’usine à sites est Drupal, aviez-vous une expérience sur cette technologie ?</b></p><p>
G.B. : « Absolument pas. Nous avions réalisé en interne un premier benchmark pour connaître l\'état de l\'art en matière de CMS open source et chaque agence a eu la possibilité de proposer une liste de solutions privilégiées. Au sortir de cette phase, nous avions préconisé le maintien d’une solution organisée autour de SPIP.<br />Smile était ouverte à l’utilisation de plusieurs solutions, mais ce sont les discussions avec ses experts qui ont engendré la solution définitive. Devant les particularités de notre projet, nous avons été convaincus que la solution préconisée par Smile, l’utilisation de Drupal, était la plus performante. »</p><p><b>Quelles ont été les phases critiques de la réalisation du projet ?</b></p><p>
G.B. : « La phase de conception a été légèrement plus longue que ce que nous avions planifié. Notamment, le recueil de besoins dans un groupe tel que SAFRAN, en prenant en compte les demandes des sociétés, n’est pas aisé. <br />La volonté de factorisation des besoins est au cœur de la restructuration de l’écosystème digital de notre Groupe. Il faut alors savoir trouver un juste équilibre entre développement spécifique et généralisation des solutions. »</p><p><b>Comment s’est déroulée la collaboration avec Smile ?</b></p><p>
G.B. : « Durant la phase de développement, Smile a fait preuve d’une grande écoute et d’une compréhension approfondie de nos besoins aboutissant à la rédaction de spécifications techniques efficientes.<br />Avec Smile nous avons créé une réelle équipe. L’équipe lyonnaise de Smile est pour beaucoup dans la réussite de ce projet, par son investissement quotidien pendant plus d’une année et une direction de projet très attentive. »</p><p><b>Quels sont les retours utilisateurs quelques semaines après le déploiement des sites pilotes ?</b></p><p>G.B. : « Les retours sont très positifs. Les contributeurs apprécient la facilité d’utilisation de la solution proposée, ce qui représentait pour nous un enjeu majeur du projet.</p><p>Les utilisateurs des sites sont enthousiastes vis-à-vis des services proposés, comme de l’ergonomie et du graphisme des sites. Il y aura certainement quelques petites évolutions à prévoir, mais quelques semaines après leur déploiement, nous observons déjà une augmentation de fréquentation des sites pilotes.»</p>',
        ]);

        References::create([
            'title' => 'KRYS',
            'title2'=> 'Un site mobile pour Krys',
            'image'=> '/img/KRYS_reference_logo.png',
            'domaines'=> '1',
            'offres'=> '2',
            'services'=> '2',
            'outils'=> '4',
            'link'=> 0,
            'slang'=> 'Hélène BARTOLUCCI, Responsable Marketing Digital pour Krys',
            'comment'=> '<p>Smile s\'est occupé de la mise en place du site mobile de Krys.</p><p>« Le retour est complètement positif. Nous observons d’ores et déjà un bon trafic qui s\'est créé sur le site. Il faudra encore un peu de temps pour que le référencement naturel du site se mette en place, mais nous observons déjà un volume de vente significatif issu du site mobile, qui valide notre stratégie de vente. Nous avons maintenant la volonté d\'étendre la gamme des produits disponibles sur le site mobile pour offrir encore plus de service à nos clients. »</p><p>« Ce projet a été un des meilleurs dans notre collaboration avec SMILE en tenant la qualité, le coût et les délais dans un contexte de demande de réactivité très forte. Des ateliers de travail et d\'échange ont été mis en place, de façon à construire itérativement chaque aspect du projet. Nous avons coconstruit le site mobile d\'une manière souple, nous appuyant surtout sur des maquettes et présentations. Cela nous a permis de nous mettre rapidement en accord sur les fondations.&nbsp;»</p><p>«&nbsp;Nous avions pour objectif de maintenir une date de publication très proche du lancement, les délais étaient donc très courts. La mobilisation interne de Krys comme la grande réactivité des équipes SMILE ont permis un temps de réalisation record (4 mois environ) avec des circuits de validations très courts. La mise en place d’une démarche agile a été très profitable.»</p>',
            'text'=> '<p>Le 25 février dernier, Krys, l’enseigne d’optique publiait son site web à destination des appareils mobiles. Ce site, conçu comme une extension de la plateforme e-commerce traditionnelle, a été mis en place avec la collaboration des équipes bordelaises de SMILE et de l’Agence Digitale SMILE. À l’occasion du lancement du site, nous avons interviewé Hélène BARTOLUCCI, Responsable Marketing Digital pour Krys.</p><p>
<b>Quelles sont les caractéristiques du site mobile Krys.com ?</b><br />
H.B. : <br />« Ce site mobile est dédié aux produits de contactologie (lentilles et produits d\'entretien), il permet d’accéder facilement aux pages des magasins d\'optique et se veut une extension mobile du site marchand, permettant de présenter un sous-ensemble des produits disponibles sur la boutique classique avec une continuité de l’expérience consommateur. Le client peut alternativement sélectionner et valider ses achats sur la plateforme standard ou mobile. Le site mobile offre par exemple à nos clients la possibilité de renouveler une commande et de régler leurs achats en un clic. Il permet aussi d’accéder à notre newsletter et à des informations pratiques. »</p><p>
<b>Pourquoi avoir choisi SMILE pour la réalisation du site mobile ?</b><br />
H.B. : <br />« Nous avons appréhendé ce nouveau projet de façon autonome, en lançant un nouvel appel d’offres spécifique dans lequel SMILE était consulté. Nous n\'étions pas focalisés sur les technologies Open Source. À nos yeux, c\'est la réponse au besoin du métier qui nous semblait prépondérante. SMILE avait déjà engrangé une bonne expérience sur la réalisation de notre site « classique ». Cette connaissance acquise a permis à SMILE de proposer la solution la plus pertinente. »</p><p>
<b>Comment s\'est déroulée la collaboration avec SMILE ?</b> <br />
H.B. : <br />« Ce projet a été un des meilleurs dans notre collaboration avec SMILE en tenant la qualité, le coût et les délais dans un contexte de demande de réactivité très forte. Des ateliers de travail et d\'échange ont été mis en place, de façon à construire itérativement chaque aspect du projet. Nous avons coconstruit le site mobile d\'une manière souple, nous appuyant surtout sur des maquettes et présentations. Cela nous a permis de nous mettre rapidement en accord sur les fondations.</p><p>Par ailleurs, les maquettes &quot;animées&quot; proposées par SMILE possédaient déjà des comportements et permettaient de rendre compte de la logique de navigation et de l\'ergonomie du site. Cette méthode nous a permis de converger vers une solution sans écueil.</p><p>Nous avons aussi été accompagnés sur l\'ensemble du projet pour l\'agence digitale de SMILE. L’agence avait pour responsabilité l’ensemble de la partie graphique (pictogrammes, charte des couleurs et dégradés) sur la base des contraintes d’une charte graphique du groupe Krys.&nbsp;</p><p>Nous avions pour objectif de maintenir une date de publication très proche du lancement, les délais étaient donc très courts. La mobilisation interne de Krys comme la grande réactivité des équipes SMILE ont permis un temps de réalisation record (4 mois environ) avec des circuits de validations très courts. La mise en place d’une démarche agile a été très profitable.»</p><p>
<b>Avez-vous un retour d\'expérience suite à ses nouveaux développements ?</b><br />
H.B. :<br />« Le retour est complètement positif. Nous observons d’ores et déjà un bon trafic qui s\'est créé sur le site. Il faudra encore un peu de temps pour que le référencement naturel du site se mette en place, mais nous observons déjà un volume de vente significatif issu du site mobile, qui valide notre stratégie de vente. Nous avons maintenant la volonté d\'étendre la gamme des produits disponibles sur le site mobile pour offrir encore plus de service à nos clients. »</p> ',
        ]);
        References::create([
            'title' => 'Safran',
            'title2'=> 'Retour aux références',
            'image'=> '/img/Safran_reference_logo.jpg',
            'domaines'=> '7',
            'offres'=> '5',
            'services'=> '1',
            'outils'=> '4',
            'link'=> 'www.safran-group.com/fr',
            'slang'=> 'Géraldine BUJADOUX, Responsable Internet, direction de la communication de Safran',
            'comment'=> 'p>Safran a confié à Smile la mise en place d’une usine à sites, développée avec le CMS Drupal.</p><p><i>« Le projet est initialement issu d\'un appel d\'offres rédigé en interne. En phase amont du projet, nous avons étudié les dossiers des agences pertinentes dans le domaine de l’intégration open source. Au cours de cette étape nous avons assisté à un séminaire organisé par Smile. Convaincus par la présentation, nous avons proposé à SMILE après quelques échanges de faire partie de cet appel d’offres. »</i></p><p>
<i>« Durant la phase de développement, Smile a fait preuve d’une grande écoute et d’une compréhension approfondie de nos besoins aboutissant à la rédaction de spécifications techniques efficientes.</i><br /><i>Avec Smile nous avons créé une réelle équipe. L’équipe lyonnaise de Smile est pour beaucoup dans la réussite de ce projet, par son investissement quotidien pendant plus d’une année et une direction de projet très attentive. »</i></p><p><i>« Les retours sont très positifs. Les contributeurs apprécient la facilité d’utilisation de la solution proposée, ce qui représentait pour nous un enjeu majeur du projet.</i></p><p><i>Les utilisateurs des sites sont enthousiastes vis-à-vis des services proposés, comme de l’ergonomie et du graphisme des sites. Il y aura certainement quelques petites évolutions à prévoir, mais quelques semaines après leur déploiement, nous observons déjà une augmentation de fréquentation des sites pilotes. »</i></p>',
            'text'=> '<p>Le projet confié à Smile par SAFRAN avait pour but la mise en place d’une usine à sites. Cette usine permet aux sociétés du groupe SAFRAN de produire des sites selon un schéma cohérent pour l’ensemble du groupe. Chaque entité peut bénéficier d’un template de site orienté vers ses besoins (groupe, société, pays, événementiel, support métier). La gestion des contenus est autonome, mais permet aussi aux différents contributeurs de sélectionner et partager les contenus disponibles sur le portail pour leurs propres besoins de communication (agenda des événements, communiqués de presse). Après une phase de déploiement pilote réussie, SAFRAN souhaite déployer la solution sur l’intégralité de ses sites avant le salon du Bourget 2015.</p><p>Smile a piloté les aspects techniques de la réalisation du projet. Développée avec Drupal, l’usine met notamment en place un répertoire de contenus géré en mode push, une innovation qui permet aux contributeurs de bénéficier en temps réel des contenus produits par SAFRAN.</p><p>Quatre sites ont été déployés à ce jour : <a href="http://www.safran-group.com/fr" target="_blank">www.safran-group.com/fr</a>
, <a href="http://www.safran-usa.com/" target="_blank">www.safran-usa.com</a>
, <a href="http://www.sagem-ds.com/fr" target="_blank">www.sagem-ds.com/fr</a>
 et <a href="http://www.techspace-aero.be/fr" target="_blank">www.techspace-aero.be/fr</a>
.</p><p><b>Pourquoi avoir choisi l’intégrateur Smile pour ce projet ?</b></p><p>G.B. : « Le projet est initialement issu d\'un appel d\'offres rédigé en interne. En phase amont du projet, nous avons étudié les dossiers des agences pertinentes dans le domaine de l’intégration open source. Au cours de cette étape nous avons assisté à un séminaire organisé par Smile. Convaincus par la présentation, nous avons proposé à SMILE après quelques échanges de faire partie de cet appel d’offres.</p><p><b>L’adoption d’une solution open source était-elle une démarche volontaire pour SAFRAN ?</b></p><p>
G.B. : « Tout à fait, c\'était une volonté de l’équipe dès l’amorce du projet et qui a été soutenue par la DSI Groupe. Nous avions déjà une expérience en la matière avec notre précédent gestionnaire de contenu, SPIP, un produit open source.<br />Bénéficier d’une communauté active autour d’une solution open source nous permet de faire évoluer rapidement et plus facilement les services que nous proposons. Une démarche d’intégration open source libère d’un lien contraignant avec un logiciel propriétaire. Cela permet aussi d’avoir plus de choix dans la sélection de nos prestataires. »</p><p><b>La solution préconisée par Smile pour l’usine à sites est Drupal, aviez-vous une expérience sur cette technologie ?</b></p><p>
G.B. : « Absolument pas. Nous avions réalisé en interne un premier benchmark pour connaître l\'état de l\'art en matière de CMS open source et chaque agence a eu la possibilité de proposer une liste de solutions privilégiées. Au sortir de cette phase, nous avions préconisé le maintien d’une solution organisée autour de SPIP.<br />Smile était ouverte à l’utilisation de plusieurs solutions, mais ce sont les discussions avec ses experts qui ont engendré la solution définitive. Devant les particularités de notre projet, nous avons été convaincus que la solution préconisée par Smile, l’utilisation de Drupal, était la plus performante. »</p><p><b>Quelles ont été les phases critiques de la réalisation du projet ?</b></p><p>
G.B. : « La phase de conception a été légèrement plus longue que ce que nous avions planifié. Notamment, le recueil de besoins dans un groupe tel que SAFRAN, en prenant en compte les demandes des sociétés, n’est pas aisé. <br />La volonté de factorisation des besoins est au cœur de la restructuration de l’écosystème digital de notre Groupe. Il faut alors savoir trouver un juste équilibre entre développement spécifique et généralisation des solutions. »</p><p><b>Comment s’est déroulée la collaboration avec Smile ?</b></p><p>
G.B. : « Durant la phase de développement, Smile a fait preuve d’une grande écoute et d’une compréhension approfondie de nos besoins aboutissant à la rédaction de spécifications techniques efficientes.<br />Avec Smile nous avons créé une réelle équipe. L’équipe lyonnaise de Smile est pour beaucoup dans la réussite de ce projet, par son investissement quotidien pendant plus d’une année et une direction de projet très attentive. »</p><p><b>Quels sont les retours utilisateurs quelques semaines après le déploiement des sites pilotes ?</b></p><p>G.B. : « Les retours sont très positifs. Les contributeurs apprécient la facilité d’utilisation de la solution proposée, ce qui représentait pour nous un enjeu majeur du projet.</p><p>Les utilisateurs des sites sont enthousiastes vis-à-vis des services proposés, comme de l’ergonomie et du graphisme des sites. Il y aura certainement quelques petites évolutions à prévoir, mais quelques semaines après leur déploiement, nous observons déjà une augmentation de fréquentation des sites pilotes.»</p>',
        ]);
//        References::create([
//            'title' => '',
//            'title2'=> '',
//            'image'=> '/img/',
//            'domaines'=> '',
//            'offres'=> '',
//            'services'=> '',
//            'outils'=> '',
//            'link'=> '',
//            'slang'=> '',
//            'comment'=> '',
//            'text'=> '',
//        ]);



    }
}

class FeedbackSeeder extends Seeder
{

    public function run()
    {
        Subject::create([
            'subject_title'  => 'Soumettre un appel doffres',
        ]);
        Subject::create([
            'subject_title'  => 'Prendre RDV avec un ingénieur daffaires',
        ]);
        Subject::create([
            'subject_title'  => 'Organiser une formation',
        ]);
        Subject::create([
            'subject_title'  => 'Proposer un partenariat',
        ]);
        Subject::create([
            'subject_title'  => 'Poser une question technique',
        ]);
        Subject::create([
            'subject_title'  => 'Rencontrer nos recruteurs',
        ]);
        Subject::create([
            'subject_title'  => 'Remonter une anomalie',
        ]);
        Subject::create([
            'subject_title'  => 'Vous désabonner',
        ]);
        Pays::create([
            'pay_title'  => 'France',
        ]);
        Pays::create([
            'pay_title'  => 'Afghanistan',
        ]);
        Pays::create([
            'pay_title'  => 'Afrique du sud',
        ]);
        Pays::create([
            'pay_title'  => 'Albanie',
        ]);
        Pays::create([
            'pay_title'  => 'Algérie',
        ]);
        Pays::create([
            'pay_title'  => 'Allemagne',
        ]);
        Pays::create([
            'pay_title'  => 'Angola',
        ]);
        Pays::create([
            'pay_title'  => 'Andorre',
        ]);
        Pays::create([
            'pay_title'  => 'Antarctique',
        ]);
        Pays::create([
            'pay_title'  => 'Antigua et Barbuda',
        ]);
        Pays::create([
            'pay_title'  => 'Arabie Saoudite',
        ]);
        Pays::create([
            'pay_title'  => 'Argentine',
        ]);
        Pays::create([
            'pay_title'  => 'Arménie',
        ]);
        Pays::create([
            'pay_title'  => 'Aruba',
        ]);
        Pays::create([
            'pay_title'  => 'Australie',
        ]);
        Pays::create([
            'pay_title'  => 'Autriche',
        ]);
        Pays::create([
            'pay_title'  => 'Bahamas',
        ]);
    }
}