<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_id');
            $table->string('name');
            $table->string('surname');
            $table->string('email');
            $table->string('department');
            $table->integer('pay_id');
            $table->string('phone');
            $table->string('societe');
            $table->string('fonction');
            $table->text('massage');
            $table->boolean('new');
            $table->timestamps();
        });
        Schema::create('subject', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject_title');
            $table->timestamps();

        });
        Schema::create('pays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pay_title');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
