<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('image');
            $table->string('lines');
            $table->text('quote');
            $table->text('notre_offre');
            $table->text('fiche_detaillee');
            $table->timestamps();
        });

        Schema::create('references', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('title2');
            $table->string('image');
            $table->integer('domaines');
            $table->integer('offres');
            $table->integer('services');
            $table->integer('outils');
            $table->string('link');
            $table->text('slang');
            $table->text('comment');
            $table->text('text');
            $table->timestamps();
        });


        Schema::create('evenements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('text_small');
            $table->string('img_small');
            $table->text('text');
            $table->string('adresse');
            $table->date('date');
            $table->timestamps();
        });

        Schema::create('actualites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('text');
            $table->date('date');
            $table->timestamps();
        });
        Schema::create('communiques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('files');
            $table->text('text');
            $table->date('date');
            $table->timestamps();
        });
        Schema::create('presse', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('link');
            $table->string('source');
            $table->text('text');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
