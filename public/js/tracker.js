/* global document: true */
/* global window: true */
/* global console: true */
/* exported smileCrmTracker */

var smileCrmTracker = (function() {

  "use strict";

  var domainsExeption = ['redmine-projets.smile.fr','www.bargento.fr'];

  var popupHTML = {
    "fr" : "<div style='top: 0px; font-family: arial,helvetica; background-color: #666; color:#FFF; margin: 0px; padding: 10px 0; z-index: 1000000;position:fixed;width:100%;' id='popup'><div style='max-width: 1024px; line-height: 13px; margin: 0 auto;'><div style='float: right; margin: 2px 10px;'><a href='#' style='background: none repeat scroll 0% 0% rgb(232, 93, 26); color: rgb(255, 255, 255); border-radius: 5px; float: right; margin-right: 20px; text-decoration: none; margin: 14px 0; padding: 6px 17px; font-size: 14px; font-weight: bold;'>Continuer la navigation</a></div><div style='font-size: 13px; margin-right: 220px'><p style='padding: 10px; font-size: 13px;color:#FFF;'>En poursuivant votre navigation sur ce site, vous acceptez l&rsquo;utilisation de cookies pour vous proposer des services et offres adapt&eacute;s &agrave; vos centres d&rsquo;int&eacute;r&ecirc;ts.</p><p style='padding: 10px; font-size: 13px;color:#FFF;'>Pour en savoir plus et param&eacute;trer les cookies, visitez la <a target='_blank' class='nosct' style='color: #FFF; text-decoration: underline' href='http://www.smile.fr/Pied-de-page/Politique-de-confidentialite'>page de politique de confidentialit&eacute; du Groupe Smile</a></p></div></div></div>",
    "en" : "<div style='top: 0px; font-family: arial,helvetica; background-color: #666; color:#FFF; margin: 0px; padding: 10px 0; z-index: 1000000;position:fixed;width:100%;' id='popup'><div style='max-width: 1024px; line-height: 13px; ;margin: 0 auto;'><div style='float: right; margin: 2px 10px;'><a href='#' style='background: none repeat scroll 0% 0% rgb(232, 93, 26); color: rgb(255, 255, 255); border-radius: 5px; float: right; margin-right: 20px; text-decoration: none; margin: 14px 0; padding: 6px 17px; font-size: 14px; font-weight: bold;'>Continue navigation</a></div><div><p style='padding: 10px; font-size: 13px;color:#FFF;'>By continuing your visit to this site, you accept the use of cookies to provide you with services and offers tailored to your interests.</p><p style='padding: 10px; font-size: 13px; color:#FFF;'>For more information and set cookies, visit the <a target='_blank' class='nosct'style='color: #FFF; text-decoration: underline' href='http://www.smile-oss.com/Footer/Privacy-policy-notice'>privacy policy of the Smile Group</a></p></div></div></div>"
  }

  function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i=0; i<ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1);
          if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
      }
      return null;
  }

  function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
  }

  var guid = (function() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
                 .toString(16)
                 .substring(1);
    }
    return function() {
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
             s4() + '-' + s4() + s4() + s4();
    };
  })();

  if (getCookie('sct:vid') == null) {setCookie('sct:vid', guid(), 365);}
  if (getCookie('sct:sid') == null) {setCookie('sct:sid', guid(), 0.4);}

  // Retrieve values for a param into URL
  function getQueryStringParameterByName(name) {
      var results = null;
      if (name && name.replace) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(window.location.search);
      }
      return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  // Add page title and page URL to the tracked variables
  function addStandardPageVars() {
      // Website base url tracking (eg. mydomain.com)
      this.pageVars.site = window.location.hostname;

      // Page URL tracking (eg. home.html)
      this.pageVars['url'] = window.location.pathname;

      // Page title tracking
      this.pageVars['title'] = encodeURI(document.title);
  }

  // Append GA campaign variable to the tracked variables
  // Following URL variables are used :
  // - utm_source
  // - utm_campaign
  // - utm_medium
  function addCampaignVars() {

    // GA variables to be fetched from URI
    var urlParams = ['utm_source', 'utm_campaign', 'utm_medium', 'utm_term'];

    for (var paramNameIndex in urlParams) {
      var paramName  = urlParams[paramNameIndex];
      var paramValue = getQueryStringParameterByName(paramName);
      if (paramValue) {
        // Append the GA param to the tracker
        this.pageVars[paramName] = paramValue;
      }
    }
  }

  function addReferrerVars() {
    if (document.referrer) {
      var parser = document.createElement('a');
      parser.href = document.referrer;
      this.pageVars['referrer[domain]'] = parser.hostname;
      this.pageVars['referrer[page]']   = parser.pathname;
    }
  }

  function addAuthVars() {
    var urlParams = ['email', 'eenc'];
    for (var paramNameIndex in urlParams) {
      var paramName  = urlParams[paramNameIndex];
      var paramValue = getQueryStringParameterByName(paramName);
      if (paramValue) {
        // Append the GA param to the tracker
        this.pageVars[paramName] = paramValue;
      }
    }
  }

  //Add page language
  function addLanguageVars() {
	  // Page language
	  var lang = false;

	  // get lang from html
	  if (document.documentElement.lang) {
		  lang = document.documentElement.lang;
	  } else {
	  	  // get lang from meta tags
	  	  var metas = document.getElementsByTagName('meta');

	  	  if (metas['language'] && metas['language'].getAttribute('content')) {
	  		  lang = metas['language'].getAttribute('content');
	  	  } else if (metas['content-language'] && metas['content-language'].getAttribute('content')) {
	  		  lang = metas['content-language'].getAttribute('content')
	  	  }
	  }

	  if (lang !== false) {
		  this.pageVars['page_language'] = lang;
	  }

	  return this;
  }


  //Get language
  function getLanguage() {
    var lang = false;
    // get lang from html
    if (document.documentElement.lang) {
      lang = document.documentElement.lang;
    } else {
        // get lang from meta tags
        var metas = document.getElementsByTagName('meta');
        if (metas['language'] && metas['language'].getAttribute('content')) {
          lang = metas['language'].getAttribute('content');
        } else if (metas['content-language'] && metas['content-language'].getAttribute('content')) {
          lang = metas['content-language'].getAttribute('content')
        }
        for(var i = 0; i < metas.length; i++) {
          if( metas[i].getAttribute('property') != null &&
              metas[i].getAttribute('property') == 'og:locale' &&
              metas[i].getAttribute('content')  != null &&
              metas[i].getAttribute('content')  == 'fre-FR' )
          lang = 'fr';
        }
    }
    return lang;
  }

  function addResolutionVars() {
	  this.pageVars['resX'] = window.screen.availWidth;
	  this.pageVars['resY'] = window.screen.availHeight;
	  return this;
  }

  function addMetaPageVars() {
      var metaTags = document.getElementsByTagName('meta');
      for (var tagIndex = 0; tagIndex < metaTags.length; tagIndex++) {
          if (metaTags[tagIndex].getAttribute('name')) {
              var components = metaTags[tagIndex].getAttribute('name').split(':');
              if (components.length == 2 && components[0] == 'sct') {
                  var varName = components[1];
                  this.addVariable(varName, metaTags[tagIndex].getAttribute('content'));
              }
          }
      }
  }

  function addSessionData() {
    this.addVariable('sct:vid', getCookie('sct:vid'));
    this.addVariable('sct:sid', getCookie('sct:sid'));
  }

  function getTrackerUrl() {

    if (this.trackerSent == false) {
        addStandardPageVars.bind(this)();
        addReferrerVars.bind(this)();
        addCampaignVars.bind(this)();
        addMetaPageVars.bind(this)();
        addAuthVars.bind(this)();
        addLanguageVars.bind(this)();
        addResolutionVars.bind(this)();
        addSessionData.bind(this)();
    }

    var urlParams = [];
    for (var currentVar in this.pageVars) {
      urlParams.push(currentVar + "=" + this.pageVars[currentVar]);
    }

    return this.baseUrl + "?" + urlParams.join('&');
  }

  // Send the tag to the remote server
  // Append a transparent pixel to the body
  function sendTag(forceCollect) {
    if (this.trackerSent == false || forceCollect == true) {
      var trackingUrl = getTrackerUrl.bind(this)();
      var bodyNode = document.getElementsByTagName('body')[0];
      var scriptNode = document.createElement('script');
      scriptNode.setAttribute('src', trackingUrl);
      bodyNode.appendChild(scriptNode);
      this.trackerSent = true;
      this.pageVars = {};
    }
  };

  function authorizeCollect() {
    var popup = document.getElementById('popup');
    popup.parentElement.removeChild(popup);
    document.body.style.paddingTop = '0px';
    setCookie('SCT_AUTH_COLLECT', 1, 365);
  }

  function addListenersOnLinks() {
    var links = document.getElementsByTagName('a');
    for(var linkIndex = 0; linkIndex < links.length; linkIndex++) {
      var link = links[linkIndex];
      var classes = link.className;
      if(classes.indexOf('nosct') == -1)
        link.addEventListener('click', authorizeCollect.bind(this));
    }
  }

  function displayCollectAuthPopup() {

    var fixContentPosition = function() {
      var popupHeight = document.getElementById('popup').clientHeight;
      bodyNode.style.paddingTop = popupHeight+"px";
    }

    var bodyNode = document.getElementsByTagName('body')[0];
    var contentStartNode = bodyNode.firstElementChild;
    var popupNode = document.createElement('div');
    var lang = getLanguage();

    if (lang == 'fr') {
      popupNode.innerHTML = popupHTML.fr;
    } else {
      popupNode.innerHTML = popupHTML.en;
    }
    bodyNode.insertBefore(popupNode, contentStartNode);
    (addListenersOnLinks.bind(this))();
    fixContentPosition();
    window.addEventListener('resize', fixContentPosition);
  };

  // Implementation of the tracker
  var SmileCrmTrackerImpl = function() {
    this.baseUrl = '//t.smile.eu/t.js';
    this.pageVars = {};
    this.trackerSent = false;

    if (!getCookie('SCT_AUTH_COLLECT') && (domainsExeption.indexOf(window.location.host) == -1)) {
      window.addEventListener('load', displayCollectAuthPopup.bind(this));
    }
 
    window.addEventListener('load', sendTag.bind(this));
  };

  //Show popup
  SmileCrmTrackerImpl.prototype.showPopup = displayCollectAuthPopup;


  // Set base url for the tracker.
  //
  // Prefer protocol relative URL (http://www.paulirish.com/2010/the-protocol-relative-url/)
  // to avoid alerts when using tracker https pages.
  SmileCrmTrackerImpl.prototype.setBaseUrl = function(baseUrl) {
      if (baseUrl != '//t.smile.eu/h.png') {
        this.baseUrl = baseUrl;
      }
      return this;
  }

  // Append a variable to the page
  SmileCrmTrackerImpl.prototype.addVariable = function(varName, value) {
      this.pageVars[varName] = encodeURI(value);
      if (this.trackerSent == true && (varName == "email" || varName == "eenc")) {
          sendTag.bind(this)();
      }
      return this;
  }

  SmileCrmTrackerImpl.prototype.saveCookies = function(cookies) {
    setCookie('sct:sid', cookies['sessionId'], 0.4);
    setCookie('sct:vid', cookies['visitorId'], 365);
  }

  return new SmileCrmTrackerImpl();

})();
