
(function(a,b){function cy(a){return f.isWindow(a)?a:a.nodeType===9?a.defaultView||a.parentWindow:!1}function cv(a){if(!ck[a]){var b=c.body,d=f("<"+a+">").appendTo(b),e=d.css("display");d.remove();if(e==="none"||e===""){cl||(cl=c.createElement("iframe"),cl.frameBorder=cl.width=cl.height=0),b.appendChild(cl);if(!cm||!cl.createElement)cm=(cl.contentWindow||cl.contentDocument).document,cm.write((c.compatMode==="CSS1Compat"?"<!doctype html>":"")+"<html><body>"),cm.close();d=cm.createElement(a),cm.body.appendChild(d),e=f.css(d,"display"),b.removeChild(cl)}ck[a]=e}return ck[a]}function cu(a,b){var c={};f.each(cq.concat.apply([],cq.slice(0,b)),function(){c[this]=a});return c}function ct(){cr=b}function cs(){setTimeout(ct,0);return cr=f.now()}function cj(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}function ci(){try{return new a.XMLHttpRequest}catch(b){}}function cc(a,c){a.dataFilter&&(c=a.dataFilter(c,a.dataType));var d=a.dataTypes,e={},g,h,i=d.length,j,k=d[0],l,m,n,o,p;for(g=1;g<i;g++){if(g===1)for(h in a.converters)typeof h=="string"&&(e[h.toLowerCase()]=a.converters[h]);l=k,k=d[g];if(k==="*")k=l;else if(l!=="*"&&l!==k){m=l+" "+k,n=e[m]||e["* "+k];if(!n){p=b;for(o in e){j=o.split(" ");if(j[0]===l||j[0]==="*"){p=e[j[1]+" "+k];if(p){o=e[o],o===!0?n=p:p===!0&&(n=o);break}}}}!n&&!p&&f.error("No conversion from "+m.replace(" "," to ")),n!==!0&&(c=n?n(c):p(o(c)))}}return c}function cb(a,c,d){var e=a.contents,f=a.dataTypes,g=a.responseFields,h,i,j,k;for(i in g)i in d&&(c[g[i]]=d[i]);while(f[0]==="*")f.shift(),h===b&&(h=a.mimeType||c.getResponseHeader("content-type"));if(h)for(i in e)if(e[i]&&e[i].test(h)){f.unshift(i);break}if(f[0]in d)j=f[0];else{for(i in d){if(!f[0]||a.converters[i+" "+f[0]]){j=i;break}k||(k=i)}j=j||k}if(j){j!==f[0]&&f.unshift(j);return d[j]}}function ca(a,b,c,d){if(f.isArray(b))f.each(b,function(b,e){c||bE.test(a)?d(a,e):ca(a+"["+(typeof e=="object"||f.isArray(e)?b:"")+"]",e,c,d)});else if(!c&&b!=null&&typeof b=="object")for(var e in b)ca(a+"["+e+"]",b[e],c,d);else d(a,b)}function b_(a,c){var d,e,g=f.ajaxSettings.flatOptions||{};for(d in c)c[d]!==b&&((g[d]?a:e||(e={}))[d]=c[d]);e&&f.extend(!0,a,e)}function b$(a,c,d,e,f,g){f=f||c.dataTypes[0],g=g||{},g[f]=!0;var h=a[f],i=0,j=h?h.length:0,k=a===bT,l;for(;i<j&&(k||!l);i++)l=h[i](c,d,e),typeof l=="string"&&(!k||g[l]?l=b:(c.dataTypes.unshift(l),l=b$(a,c,d,e,l,g)));(k||!l)&&!g["*"]&&(l=b$(a,c,d,e,"*",g));return l}function bZ(a){return function(b,c){typeof b!="string"&&(c=b,b="*");if(f.isFunction(c)){var d=b.toLowerCase().split(bP),e=0,g=d.length,h,i,j;for(;e<g;e++)h=d[e],j=/^\+/.test(h),j&&(h=h.substr(1)||"*"),i=a[h]=a[h]||[],i[j?"unshift":"push"](c)}}}function bC(a,b,c){var d=b==="width"?a.offsetWidth:a.offsetHeight,e=b==="width"?bx:by,g=0,h=e.length;if(d>0){if(c!=="border")for(;g<h;g++)c||(d-=parseFloat(f.css(a,"padding"+e[g]))||0),c==="margin"?d+=parseFloat(f.css(a,c+e[g]))||0:d-=parseFloat(f.css(a,"border"+e[g]+"Width"))||0;return d+"px"}d=bz(a,b,b);if(d<0||d==null)d=a.style[b]||0;d=parseFloat(d)||0;if(c)for(;g<h;g++)d+=parseFloat(f.css(a,"padding"+e[g]))||0,c!=="padding"&&(d+=parseFloat(f.css(a,"border"+e[g]+"Width"))||0),c==="margin"&&(d+=parseFloat(f.css(a,c+e[g]))||0);return d+"px"}function bp(a,b){b.src?f.ajax({url:b.src,async:!1,dataType:"script"}):f.globalEval((b.text||b.textContent||b.innerHTML||"").replace(bf,"/*$0*/")),b.parentNode&&b.parentNode.removeChild(b)}function bo(a){var b=c.createElement("div");bh.appendChild(b),b.innerHTML=a.outerHTML;return b.firstChild}function bn(a){var b=(a.nodeName||"").toLowerCase();b==="input"?bm(a):b!=="script"&&typeof a.getElementsByTagName!="undefined"&&f.grep(a.getElementsByTagName("input"),bm)}function bm(a){if(a.type==="checkbox"||a.type==="radio")a.defaultChecked=a.checked}function bl(a){return typeof a.getElementsByTagName!="undefined"?a.getElementsByTagName("*"):typeof a.querySelectorAll!="undefined"?a.querySelectorAll("*"):[]}function bk(a,b){var c;if(b.nodeType===1){b.clearAttributes&&b.clearAttributes(),b.mergeAttributes&&b.mergeAttributes(a),c=b.nodeName.toLowerCase();if(c==="object")b.outerHTML=a.outerHTML;else if(c!=="input"||a.type!=="checkbox"&&a.type!=="radio"){if(c==="option")b.selected=a.defaultSelected;else if(c==="input"||c==="textarea")b.defaultValue=a.defaultValue}else a.checked&&(b.defaultChecked=b.checked=a.checked),b.value!==a.value&&(b.value=a.value);b.removeAttribute(f.expando)}}function bj(a,b){if(b.nodeType===1&&!!f.hasData(a)){var c,d,e,g=f._data(a),h=f._data(b,g),i=g.events;if(i){delete h.handle,h.events={};for(c in i)for(d=0,e=i[c].length;d<e;d++)f.event.add(b,c+(i[c][d].namespace?".":"")+i[c][d].namespace,i[c][d],i[c][d].data)}h.data&&(h.data=f.extend({},h.data))}}function bi(a,b){return f.nodeName(a,"table")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function U(a){var b=V.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}function T(a,b,c){b=b||0;if(f.isFunction(b))return f.grep(a,function(a,d){var e=!!b.call(a,d,a);return e===c});if(b.nodeType)return f.grep(a,function(a,d){return a===b===c});if(typeof b=="string"){var d=f.grep(a,function(a){return a.nodeType===1});if(O.test(b))return f.filter(b,d,!c);b=f.filter(b,d)}return f.grep(a,function(a,d){return f.inArray(a,b)>=0===c})}function S(a){return!a||!a.parentNode||a.parentNode.nodeType===11}function K(){return!0}function J(){return!1}function n(a,b,c){var d=b+"defer",e=b+"queue",g=b+"mark",h=f._data(a,d);h&&(c==="queue"||!f._data(a,e))&&(c==="mark"||!f._data(a,g))&&setTimeout(function(){!f._data(a,e)&&!f._data(a,g)&&(f.removeData(a,d,!0),h.fire())},0)}function m(a){for(var b in a){if(b==="data"&&f.isEmptyObject(a[b]))continue;if(b!=="toJSON")return!1}return!0}function l(a,c,d){if(d===b&&a.nodeType===1){var e="data-"+c.replace(k,"-$1").toLowerCase();d=a.getAttribute(e);if(typeof d=="string"){try{d=d==="true"?!0:d==="false"?!1:d==="null"?null:f.isNumeric(d)?parseFloat(d):j.test(d)?f.parseJSON(d):d}catch(g){}f.data(a,c,d)}else d=b}return d}function h(a){var b=g[a]={},c,d;a=a.split(/\s+/);for(c=0,d=a.length;c<d;c++)b[a[c]]=!0;return b}var c=a.document,d=a.navigator,e=a.location,f=function(){function J(){if(!e.isReady){try{c.documentElement.doScroll("left")}catch(a){setTimeout(J,1);return}e.ready()}}var e=function(a,b){return new e.fn.init(a,b,h)},f=a.jQuery,g=a.$,h,i=/^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,j=/\S/,k=/^\s+/,l=/\s+$/,m=/^<(\w+)\s*\/?>(?:<\/\1>)?$/,n=/^[\],:{}\s]*$/,o=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,p=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,q=/(?:^|:|,)(?:\s*\[)+/g,r=/(webkit)[ \/]([\w.]+)/,s=/(opera)(?:.*version)?[ \/]([\w.]+)/,t=/(msie) ([\w.]+)/,u=/(mozilla)(?:.*? rv:([\w.]+))?/,v=/-([a-z]|[0-9])/ig,w=/^-ms-/,x=function(a,b){return(b+"").toUpperCase()},y=d.userAgent,z,A,B,C=Object.prototype.toString,D=Object.prototype.hasOwnProperty,E=Array.prototype.push,F=Array.prototype.slice,G=String.prototype.trim,H=Array.prototype.indexOf,I={};e.fn=e.prototype={constructor:e,init:function(a,d,f){var g,h,j,k;if(!a)return this;if(a.nodeType){this.context=this[0]=a,this.length=1;return this}if(a==="body"&&!d&&c.body){this.context=c,this[0]=c.body,this.selector=a,this.length=1;return this}if(typeof a=="string"){a.charAt(0)!=="<"||a.charAt(a.length-1)!==">"||a.length<3?g=i.exec(a):g=[null,a,null];if(g&&(g[1]||!d)){if(g[1]){d=d instanceof e?d[0]:d,k=d?d.ownerDocument||d:c,j=m.exec(a),j?e.isPlainObject(d)?(a=[c.createElement(j[1])],e.fn.attr.call(a,d,!0)):a=[k.createElement(j[1])]:(j=e.buildFragment([g[1]],[k]),a=(j.cacheable?e.clone(j.fragment):j.fragment).childNodes);return e.merge(this,a)}h=c.getElementById(g[2]);if(h&&h.parentNode){if(h.id!==g[2])return f.find(a);this.length=1,this[0]=h}this.context=c,this.selector=a;return this}return!d||d.jquery?(d||f).find(a):this.constructor(d).find(a)}if(e.isFunction(a))return f.ready(a);a.selector!==b&&(this.selector=a.selector,this.context=a.context);return e.makeArray(a,this)},selector:"",jquery:"1.7.1",length:0,size:function(){return this.length},toArray:function(){return F.call(this,0)},get:function(a){return a==null?this.toArray():a<0?this[this.length+a]:this[a]},pushStack:function(a,b,c){var d=this.constructor();e.isArray(a)?E.apply(d,a):e.merge(d,a),d.prevObject=this,d.context=this.context,b==="find"?d.selector=this.selector+(this.selector?" ":"")+c:b&&(d.selector=this.selector+"."+b+"("+c+")");return d},each:function(a,b){return e.each(this,a,b)},ready:function(a){e.bindReady(),A.add(a);return this},eq:function(a){a=+a;return a===-1?this.slice(a):this.slice(a,a+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(F.apply(this,arguments),"slice",F.call(arguments).join(","))},map:function(a){return this.pushStack(e.map(this,function(b,c){return a.call(b,c,b)}))},end:function(){return this.prevObject||this.constructor(null)},push:E,sort:[].sort,splice:[].splice},e.fn.init.prototype=e.fn,e.extend=e.fn.extend=function(){var a,c,d,f,g,h,i=arguments[0]||{},j=1,k=arguments.length,l=!1;typeof i=="boolean"&&(l=i,i=arguments[1]||{},j=2),typeof i!="object"&&!e.isFunction(i)&&(i={}),k===j&&(i=this,--j);for(;j<k;j++)if((a=arguments[j])!=null)for(c in a){d=i[c],f=a[c];if(i===f)continue;l&&f&&(e.isPlainObject(f)||(g=e.isArray(f)))?(g?(g=!1,h=d&&e.isArray(d)?d:[]):h=d&&e.isPlainObject(d)?d:{},i[c]=e.extend(l,h,f)):f!==b&&(i[c]=f)}return i},e.extend({noConflict:function(b){a.$===e&&(a.$=g),b&&a.jQuery===e&&(a.jQuery=f);return e},isReady:!1,readyWait:1,holdReady:function(a){a?e.readyWait++:e.ready(!0)},ready:function(a){if(a===!0&&!--e.readyWait||a!==!0&&!e.isReady){if(!c.body)return setTimeout(e.ready,1);e.isReady=!0;if(a!==!0&&--e.readyWait>0)return;A.fireWith(c,[e]),e.fn.trigger&&e(c).trigger("ready").off("ready")}},bindReady:function(){if(!A){A=e.Callbacks("once memory");if(c.readyState==="complete")return setTimeout(e.ready,1);if(c.addEventListener)c.addEventListener("DOMContentLoaded",B,!1),a.addEventListener("load",e.ready,!1);else if(c.attachEvent){c.attachEvent("onreadystatechange",B),a.attachEvent("onload",e.ready);var b=!1;try{b=a.frameElement==null}catch(d){}c.documentElement.doScroll&&b&&J()}}},isFunction:function(a){return e.type(a)==="function"},isArray:Array.isArray||function(a){return e.type(a)==="array"},isWindow:function(a){return a&&typeof a=="object"&&"setInterval"in a},isNumeric:function(a){return!isNaN(parseFloat(a))&&isFinite(a)},type:function(a){return a==null?String(a):I[C.call(a)]||"object"},isPlainObject:function(a){if(!a||e.type(a)!=="object"||a.nodeType||e.isWindow(a))return!1;try{if(a.constructor&&!D.call(a,"constructor")&&!D.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}var d;for(d in a);return d===b||D.call(a,d)},isEmptyObject:function(a){for(var b in a)return!1;return!0},error:function(a){throw new Error(a)},parseJSON:function(b){if(typeof b!="string"||!b)return null;b=e.trim(b);if(a.JSON&&a.JSON.parse)return a.JSON.parse(b);if(n.test(b.replace(o,"@").replace(p,"]").replace(q,"")))return(new Function("return "+b))();e.error("Invalid JSON: "+b)},parseXML:function(c){var d,f;try{a.DOMParser?(f=new DOMParser,d=f.parseFromString(c,"text/xml")):(d=new ActiveXObject("Microsoft.XMLDOM"),d.async="false",d.loadXML(c))}catch(g){d=b}(!d||!d.documentElement||d.getElementsByTagName("parsererror").length)&&e.error("Invalid XML: "+c);return d},noop:function(){},globalEval:function(b){b&&j.test(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(w,"ms-").replace(v,x)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toUpperCase()===b.toUpperCase()},each:function(a,c,d){var f,g=0,h=a.length,i=h===b||e.isFunction(a);if(d){if(i){for(f in a)if(c.apply(a[f],d)===!1)break}else for(;g<h;)if(c.apply(a[g++],d)===!1)break}else if(i){for(f in a)if(c.call(a[f],f,a[f])===!1)break}else for(;g<h;)if(c.call(a[g],g,a[g++])===!1)break;return a},trim:G?function(a){return a==null?"":G.call(a)}:function(a){return a==null?"":(a+"").replace(k,"").replace(l,"")},makeArray:function(a,b){var c=b||[];if(a!=null){var d=e.type(a);a.length==null||d==="string"||d==="function"||d==="regexp"||e.isWindow(a)?E.call(c,a):e.merge(c,a)}return c},inArray:function(a,b,c){var d;if(b){if(H)return H.call(b,a,c);d=b.length,c=c?c<0?Math.max(0,d+c):c:0;for(;c<d;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,c){var d=a.length,e=0;if(typeof c.length=="number")for(var f=c.length;e<f;e++)a[d++]=c[e];else while(c[e]!==b)a[d++]=c[e++];a.length=d;return a},grep:function(a,b,c){var d=[],e;c=!!c;for(var f=0,g=a.length;f<g;f++)e=!!b(a[f],f),c!==e&&d.push(a[f]);return d},map:function(a,c,d){var f,g,h=[],i=0,j=a.length,k=a instanceof e||j!==b&&typeof j=="number"&&(j>0&&a[0]&&a[j-1]||j===0||e.isArray(a));if(k)for(;i<j;i++)f=c(a[i],i,d),f!=null&&(h[h.length]=f);else for(g in a)f=c(a[g],g,d),f!=null&&(h[h.length]=f);return h.concat.apply([],h)},guid:1,proxy:function(a,c){if(typeof c=="string"){var d=a[c];c=a,a=d}if(!e.isFunction(a))return b;var f=F.call(arguments,2),g=function(){return a.apply(c,f.concat(F.call(arguments)))};g.guid=a.guid=a.guid||g.guid||e.guid++;return g},access:function(a,c,d,f,g,h){var i=a.length;if(typeof c=="object"){for(var j in c)e.access(a,j,c[j],f,g,d);return a}if(d!==b){f=!h&&f&&e.isFunction(d);for(var k=0;k<i;k++)g(a[k],c,f?d.call(a[k],k,g(a[k],c)):d,h);return a}return i?g(a[0],c):b},now:function(){return(new Date).getTime()},uaMatch:function(a){a=a.toLowerCase();var b=r.exec(a)||s.exec(a)||t.exec(a)||a.indexOf("compatible")<0&&u.exec(a)||[];return{browser:b[1]||"",version:b[2]||"0"}},sub:function(){function a(b,c){return new a.fn.init(b,c)}e.extend(!0,a,this),a.superclass=this,a.fn=a.prototype=this(),a.fn.constructor=a,a.sub=this.sub,a.fn.init=function(d,f){f&&f instanceof e&&!(f instanceof a)&&(f=a(f));return e.fn.init.call(this,d,f,b)},a.fn.init.prototype=a.fn;var b=a(c);return a},browser:{}}),e.each("Boolean Number String Function Array Date RegExp Object".split(" "),function(a,b){I["[object "+b+"]"]=b.toLowerCase()}),z=e.uaMatch(y),z.browser&&(e.browser[z.browser]=!0,e.browser.version=z.version),e.browser.webkit&&(e.browser.safari=!0),j.test(" ")&&(k=/^[\s\xA0]+/,l=/[\s\xA0]+$/),h=e(c),c.addEventListener?B=function(){c.removeEventListener("DOMContentLoaded",B,!1),e.ready()}:c.attachEvent&&(B=function(){c.readyState==="complete"&&(c.detachEvent("onreadystatechange",B),e.ready())});return e}(),g={};f.Callbacks=function(a){a=a?g[a]||h(a):{};var c=[],d=[],e,i,j,k,l,m=function(b){var d,e,g,h,i;for(d=0,e=b.length;d<e;d++)g=b[d],h=f.type(g),h==="array"?m(g):h==="function"&&(!a.unique||!o.has(g))&&c.push(g)},n=function(b,f){f=f||[],e=!a.memory||[b,f],i=!0,l=j||0,j=0,k=c.length;for(;c&&l<k;l++)if(c[l].apply(b,f)===!1&&a.stopOnFalse){e=!0;break}i=!1,c&&(a.once?e===!0?o.disable():c=[]:d&&d.length&&(e=d.shift(),o.fireWith(e[0],e[1])))},o={add:function(){if(c){var a=c.length;m(arguments),i?k=c.length:e&&e!==!0&&(j=a,n(e[0],e[1]))}return this},remove:function(){if(c){var b=arguments,d=0,e=b.length;for(;d<e;d++)for(var f=0;f<c.length;f++)if(b[d]===c[f]){i&&f<=k&&(k--,f<=l&&l--),c.splice(f--,1);if(a.unique)break}}return this},has:function(a){if(c){var b=0,d=c.length;for(;b<d;b++)if(a===c[b])return!0}return!1},empty:function(){c=[];return this},disable:function(){c=d=e=b;return this},disabled:function(){return!c},lock:function(){d=b,(!e||e===!0)&&o.disable();return this},locked:function(){return!d},fireWith:function(b,c){d&&(i?a.once||d.push([b,c]):(!a.once||!e)&&n(b,c));return this},fire:function(){o.fireWith(this,arguments);return this},fired:function(){return!!e}};return o};var i=[].slice;f.extend({Deferred:function(a){var b=f.Callbacks("once memory"),c=f.Callbacks("once memory"),d=f.Callbacks("memory"),e="pending",g={resolve:b,reject:c,notify:d},h={done:b.add,fail:c.add,progress:d.add,state:function(){return e},isResolved:b.fired,isRejected:c.fired,then:function(a,b,c){i.done(a).fail(b).progress(c);return this},always:function(){i.done.apply(i,arguments).fail.apply(i,arguments);return this},pipe:function(a,b,c){return f.Deferred(function(d){f.each({done:[a,"resolve"],fail:[b,"reject"],progress:[c,"notify"]},function(a,b){var c=b[0],e=b[1],g;f.isFunction(c)?i[a](function(){g=c.apply(this,arguments),g&&f.isFunction(g.promise)?g.promise().then(d.resolve,d.reject,d.notify):d[e+"With"](this===i?d:this,[g])}):i[a](d[e])})}).promise()},promise:function(a){if(a==null)a=h;else for(var b in h)a[b]=h[b];return a}},i=h.promise({}),j;for(j in g)i[j]=g[j].fire,i[j+"With"]=g[j].fireWith;i.done(function(){e="resolved"},c.disable,d.lock).fail(function(){e="rejected"},b.disable,d.lock),a&&a.call(i,i);return i},when:function(a){function m(a){return function(b){e[a]=arguments.length>1?i.call(arguments,0):b,j.notifyWith(k,e)}}function l(a){return function(c){b[a]=arguments.length>1?i.call(arguments,0):c,--g||j.resolveWith(j,b)}}var b=i.call(arguments,0),c=0,d=b.length,e=Array(d),g=d,h=d,j=d<=1&&a&&f.isFunction(a.promise)?a:f.Deferred(),k=j.promise();if(d>1){for(;c<d;c++)b[c]&&b[c].promise&&f.isFunction(b[c].promise)?b[c].promise().then(l(c),j.reject,m(c)):--g;g||j.resolveWith(j,b)}else j!==a&&j.resolveWith(j,d?[a]:[]);return k}}),f.support=function(){var b,d,e,g,h,i,j,k,l,m,n,o,p,q=c.createElement("div"),r=c.documentElement;q.setAttribute("className","t"),q.innerHTML="   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>",d=q.getElementsByTagName("*"),e=q.getElementsByTagName("a")[0];if(!d||!d.length||!e)return{};g=c.createElement("select"),h=g.appendChild(c.createElement("option")),i=q.getElementsByTagName("input")[0],b={leadingWhitespace:q.firstChild.nodeType===3,tbody:!q.getElementsByTagName("tbody").length,htmlSerialize:!!q.getElementsByTagName("link").length,style:/top/.test(e.getAttribute("style")),hrefNormalized:e.getAttribute("href")==="/a",opacity:/^0.55/.test(e.style.opacity),cssFloat:!!e.style.cssFloat,checkOn:i.value==="on",optSelected:h.selected,getSetAttribute:q.className!=="t",enctype:!!c.createElement("form").enctype,html5Clone:c.createElement("nav").cloneNode(!0).outerHTML!=="<:nav></:nav>",submitBubbles:!0,changeBubbles:!0,focusinBubbles:!1,deleteExpando:!0,noCloneEvent:!0,inlineBlockNeedsLayout:!1,shrinkWrapBlocks:!1,reliableMarginRight:!0},i.checked=!0,b.noCloneChecked=i.cloneNode(!0).checked,g.disabled=!0,b.optDisabled=!h.disabled;try{delete q.test}catch(s){b.deleteExpando=!1}!q.addEventListener&&q.attachEvent&&q.fireEvent&&(q.attachEvent("onclick",function(){b.noCloneEvent=!1}),q.cloneNode(!0).fireEvent("onclick")),i=c.createElement("input"),i.value="t",i.setAttribute("type","radio"),b.radioValue=i.value==="t",i.setAttribute("checked","checked"),q.appendChild(i),k=c.createDocumentFragment(),k.appendChild(q.lastChild),b.checkClone=k.cloneNode(!0).cloneNode(!0).lastChild.checked,b.appendChecked=i.checked,k.removeChild(i),k.appendChild(q),q.innerHTML="",a.getComputedStyle&&(j=c.createElement("div"),j.style.width="0",j.style.marginRight="0",q.style.width="2px",q.appendChild(j),b.reliableMarginRight=(parseInt((a.getComputedStyle(j,null)||{marginRight:0}).marginRight,10)||0)===0);if(q.attachEvent)for(o in{submit:1,change:1,focusin:1})n="on"+o,p=n in q,p||(q.setAttribute(n,"return;"),p=typeof q[n]=="function"),b[o+"Bubbles"]=p;k.removeChild(q),k=g=h=j=q=i=null,f(function(){var a,d,e,g,h,i,j,k,m,n,o,r=c.getElementsByTagName("body")[0];!r||(j=1,k="position:absolute;top:0;left:0;width:1px;height:1px;margin:0;",m="visibility:hidden;border:0;",n="style='"+k+"border:5px solid #000;padding:0;'",o="<div "+n+"><div></div></div>"+"<table "+n+" cellpadding='0' cellspacing='0'>"+"<tr><td></td></tr></table>",a=c.createElement("div"),a.style.cssText=m+"width:0;height:0;position:static;top:0;margin-top:"+j+"px",r.insertBefore(a,r.firstChild),q=c.createElement("div"),a.appendChild(q),q.innerHTML="<table><tr><td style='padding:0;border:0;display:none'></td><td>t</td></tr></table>",l=q.getElementsByTagName("td"),p=l[0].offsetHeight===0,l[0].style.display="",l[1].style.display="none",b.reliableHiddenOffsets=p&&l[0].offsetHeight===0,q.innerHTML="",q.style.width=q.style.paddingLeft="1px",f.boxModel=b.boxModel=q.offsetWidth===2,typeof q.style.zoom!="undefined"&&(q.style.display="inline",q.style.zoom=1,b.inlineBlockNeedsLayout=q.offsetWidth===2,q.style.display="",q.innerHTML="<div style='width:4px;'></div>",b.shrinkWrapBlocks=q.offsetWidth!==2),q.style.cssText=k+m,q.innerHTML=o,d=q.firstChild,e=d.firstChild,h=d.nextSibling.firstChild.firstChild,i={doesNotAddBorder:e.offsetTop!==5,doesAddBorderForTableAndCells:h.offsetTop===5},e.style.position="fixed",e.style.top="20px",i.fixedPosition=e.offsetTop===20||e.offsetTop===15,e.style.position=e.style.top="",d.style.overflow="hidden",d.style.position="relative",i.subtractsBorderForOverflowNotVisible=e.offsetTop===-5,i.doesNotIncludeMarginInBodyOffset=r.offsetTop!==j,r.removeChild(a),q=a=null,f.extend(b,i))});return b}();var j=/^(?:\{.*\}|\[.*\])$/,k=/([A-Z])/g;f.extend({cache:{},uuid:0,expando:"jQuery"+(f.fn.jquery+Math.random()).replace(/\D/g,""),noData:{embed:!0,object:"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",applet:!0},hasData:function(a){a=a.nodeType?f.cache[a[f.expando]]:a[f.expando];return!!a&&!m(a)},data:function(a,c,d,e){if(!!f.acceptData(a)){var g,h,i,j=f.expando,k=typeof c=="string",l=a.nodeType,m=l?f.cache:a,n=l?a[j]:a[j]&&j,o=c==="events";if((!n||!m[n]||!o&&!e&&!m[n].data)&&k&&d===b)return;n||(l?a[j]=n=++f.uuid:n=j),m[n]||(m[n]={},l||(m[n].toJSON=f.noop));if(typeof c=="object"||typeof c=="function")e?m[n]=f.extend(m[n],c):m[n].data=f.extend(m[n].data,c);g=h=m[n],e||(h.data||(h.data={}),h=h.data),d!==b&&(h[f.camelCase(c)]=d);if(o&&!h[c])return g.events;k?(i=h[c],i==null&&(i=h[f.camelCase(c)])):i=h;return i}},removeData:function(a,b,c){if(!!f.acceptData(a)){var d,e,g,h=f.expando,i=a.nodeType,j=i?f.cache:a,k=i?a[h]:h;if(!j[k])return;if(b){d=c?j[k]:j[k].data;if(d){f.isArray(b)||(b in d?b=[b]:(b=f.camelCase(b),b in d?b=[b]:b=b.split(" ")));for(e=0,g=b.length;e<g;e++)delete d[b[e]];if(!(c?m:f.isEmptyObject)(d))return}}if(!c){delete j[k].data;if(!m(j[k]))return}f.support.deleteExpando||!j.setInterval?delete j[k]:j[k]=null,i&&(f.support.deleteExpando?delete a[h]:a.removeAttribute?a.removeAttribute(h):a[h]=null)}},_data:function(a,b,c){return f.data(a,b,c,!0)},acceptData:function(a){if(a.nodeName){var b=f.noData[a.nodeName.toLowerCase()];if(b)return b!==!0&&a.getAttribute("classid")===b}return!0}}),f.fn.extend({data:function(a,c){var d,e,g,h=null;if(typeof a=="undefined"){if(this.length){h=f.data(this[0]);if(this[0].nodeType===1&&!f._data(this[0],"parsedAttrs")){e=this[0].attributes;for(var i=0,j=e.length;i<j;i++)g=e[i].name,g.indexOf("data-")===0&&(g=f.camelCase(g.substring(5)),l(this[0],g,h[g]));f._data(this[0],"parsedAttrs",!0)}}return h}if(typeof a=="object")return this.each(function(){f.data(this,a)});d=a.split("."),d[1]=d[1]?"."+d[1]:"";if(c===b){h=this.triggerHandler("getData"+d[1]+"!",[d[0]]),h===b&&this.length&&(h=f.data(this[0],a),h=l(this[0],a,h));return h===b&&d[1]?this.data(d[0]):h}return this.each(function(){var b=f(this),e=[d[0],c];b.triggerHandler("setData"+d[1]+"!",e),f.data(this,a,c),b.triggerHandler("changeData"+d[1]+"!",e)})},removeData:function(a){return this.each(function(){f.removeData(this,a)})}}),f.extend({_mark:function(a,b){a&&(b=(b||"fx")+"mark",f._data(a,b,(f._data(a,b)||0)+1))},_unmark:function(a,b,c){a!==!0&&(c=b,b=a,a=!1);if(b){c=c||"fx";var d=c+"mark",e=a?0:(f._data(b,d)||1)-1;e?f._data(b,d,e):(f.removeData(b,d,!0),n(b,c,"mark"))}},queue:function(a,b,c){var d;if(a){b=(b||"fx")+"queue",d=f._data(a,b),c&&(!d||f.isArray(c)?d=f._data(a,b,f.makeArray(c)):d.push(c));return d||[]}},dequeue:function(a,b){b=b||"fx";var c=f.queue(a,b),d=c.shift(),e={};d==="inprogress"&&(d=c.shift()),d&&(b==="fx"&&c.unshift("inprogress"),f._data(a,b+".run",e),d.call(a,function(){f.dequeue(a,b)},e)),c.length||(f.removeData(a,b+"queue "+b+".run",!0),n(a,b,"queue"))}}),f.fn.extend({queue:function(a,c){typeof a!="string"&&(c=a,a="fx");if(c===b)return f.queue(this[0],a);return this.each(function(){var b=f.queue(this,a,c);a==="fx"&&b[0]!=="inprogress"&&f.dequeue(this,a)})},dequeue:function(a){return this.each(function(){f.dequeue(this,a)})},delay:function(a,b){a=f.fx?f.fx.speeds[a]||a:a,b=b||"fx";return this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,c){function m(){--h||d.resolveWith(e,[e])}typeof a!="string"&&(c=a,a=b),a=a||"fx";var d=f.Deferred(),e=this,g=e.length,h=1,i=a+"defer",j=a+"queue",k=a+"mark",l;while(g--)if(l=f.data(e[g],i,b,!0)||(f.data(e[g],j,b,!0)||f.data(e[g],k,b,!0))&&f.data(e[g],i,f.Callbacks("once memory"),!0))h++,l.add(m);m();return d.promise()}});var o=/[\n\t\r]/g,p=/\s+/,q=/\r/g,r=/^(?:button|input)$/i,s=/^(?:button|input|object|select|textarea)$/i,t=/^a(?:rea)?$/i,u=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,v=f.support.getSetAttribute,w,x,y;f.fn.extend({attr:function(a,b){return f.access(this,a,b,!0,f.attr)},removeAttr:function(a){return this.each(function(){f.removeAttr(this,a)})},prop:function(a,b){return f.access(this,a,b,!0,f.prop)},removeProp:function(a){a=f.propFix[a]||a;return this.each(function(){try{this[a]=b,delete this[a]}catch(c){}})},addClass:function(a){var b,c,d,e,g,h,i;if(f.isFunction(a))return this.each(function(b){f(this).addClass(a.call(this,b,this.className))});if(a&&typeof a=="string"){b=a.split(p);for(c=0,d=this.length;c<d;c++){e=this[c];if(e.nodeType===1)if(!e.className&&b.length===1)e.className=a;else{g=" "+e.className+" ";for(h=0,i=b.length;h<i;h++)~g.indexOf(" "+b[h]+" ")||(g+=b[h]+" ");e.className=f.trim(g)}}}return this},removeClass:function(a){var c,d,e,g,h,i,j;if(f.isFunction(a))return this.each(function(b){f(this).removeClass(a.call(this,b,this.className))});if(a&&typeof a=="string"||a===b){c=(a||"").split(p);for(d=0,e=this.length;d<e;d++){g=this[d];if(g.nodeType===1&&g.className)if(a){h=(" "+g.className+" ").replace(o," ");for(i=0,j=c.length;i<j;i++)h=h.replace(" "+c[i]+" "," ");g.className=f.trim(h)}else g.className=""}}return this},toggleClass:function(a,b){var c=typeof a,d=typeof b=="boolean";if(f.isFunction(a))return this.each(function(c){f(this).toggleClass(a.call(this,c,this.className,b),b)});return this.each(function(){if(c==="string"){var e,g=0,h=f(this),i=b,j=a.split(p);while(e=j[g++])i=d?i:!h.hasClass(e),h[i?"addClass":"removeClass"](e)}else if(c==="undefined"||c==="boolean")this.className&&f._data(this,"__className__",this.className),this.className=this.className||a===!1?"":f._data(this,"__className__")||""})},hasClass:function(a){var b=" "+a+" ",c=0,d=this.length;for(;c<d;c++)if(this[c].nodeType===1&&(" "+this[c].className+" ").replace(o," ").indexOf(b)>-1)return!0;return!1},val:function(a){var c,d,e,g=this[0];{if(!!arguments.length){e=f.isFunction(a);return this.each(function(d){var g=f(this),h;if(this.nodeType===1){e?h=a.call(this,d,g.val()):h=a,h==null?h="":typeof h=="number"?h+="":f.isArray(h)&&(h=f.map(h,function(a){return a==null?"":a+""})),c=f.valHooks[this.nodeName.toLowerCase()]||f.valHooks[this.type];if(!c||!("set"in c)||c.set(this,h,"value")===b)this.value=h}})}if(g){c=f.valHooks[g.nodeName.toLowerCase()]||f.valHooks[g.type];if(c&&"get"in c&&(d=c.get(g,"value"))!==b)return d;d=g.value;return typeof d=="string"?d.replace(q,""):d==null?"":d}}}}),f.extend({valHooks:{option:{get:function(a){var b=a.attributes.value;return!b||b.specified?a.value:a.text}},select:{get:function(a){var b,c,d,e,g=a.selectedIndex,h=[],i=a.options,j=a.type==="select-one";if(g<0)return null;c=j?g:0,d=j?g+1:i.length;for(;c<d;c++){e=i[c];if(e.selected&&(f.support.optDisabled?!e.disabled:e.getAttribute("disabled")===null)&&(!e.parentNode.disabled||!f.nodeName(e.parentNode,"optgroup"))){b=f(e).val();if(j)return b;h.push(b)}}if(j&&!h.length&&i.length)return f(i[g]).val();return h},set:function(a,b){var c=f.makeArray(b);f(a).find("option").each(function(){this.selected=f.inArray(f(this).val(),c)>=0}),c.length||(a.selectedIndex=-1);return c}}},attrFn:{val:!0,css:!0,html:!0,text:!0,data:!0,width:!0,height:!0,offset:!0},attr:function(a,c,d,e){var g,h,i,j=a.nodeType;if(!!a&&j!==3&&j!==8&&j!==2){if(e&&c in f.attrFn)return f(a)[c](d);if(typeof a.getAttribute=="undefined")return f.prop(a,c,d);i=j!==1||!f.isXMLDoc(a),i&&(c=c.toLowerCase(),h=f.attrHooks[c]||(u.test(c)?x:w));if(d!==b){if(d===null){f.removeAttr(a,c);return}if(h&&"set"in h&&i&&(g=h.set(a,d,c))!==b)return g;a.setAttribute(c,""+d);return d}if(h&&"get"in h&&i&&(g=h.get(a,c))!==null)return g;g=a.getAttribute(c);return g===null?b:g}},removeAttr:function(a,b){var c,d,e,g,h=0;if(b&&a.nodeType===1){d=b.toLowerCase().split(p),g=d.length;for(;h<g;h++)e=d[h],e&&(c=f.propFix[e]||e,f.attr(a,e,""),a.removeAttribute(v?e:c),u.test(e)&&c in a&&(a[c]=!1))}},attrHooks:{type:{set:function(a,b){if(r.test(a.nodeName)&&a.parentNode)f.error("type property can't be changed");else if(!f.support.radioValue&&b==="radio"&&f.nodeName(a,"input")){var c=a.value;a.setAttribute("type",b),c&&(a.value=c);return b}}},value:{get:function(a,b){if(w&&f.nodeName(a,"button"))return w.get(a,b);return b in a?a.value:null},set:function(a,b,c){if(w&&f.nodeName(a,"button"))return w.set(a,b,c);a.value=b}}},propFix:{tabindex:"tabIndex",readonly:"readOnly","for":"htmlFor","class":"className",maxlength:"maxLength",cellspacing:"cellSpacing",cellpadding:"cellPadding",rowspan:"rowSpan",colspan:"colSpan",usemap:"useMap",frameborder:"frameBorder",contenteditable:"contentEditable"},prop:function(a,c,d){var e,g,h,i=a.nodeType;if(!!a&&i!==3&&i!==8&&i!==2){h=i!==1||!f.isXMLDoc(a),h&&(c=f.propFix[c]||c,g=f.propHooks[c]);return d!==b?g&&"set"in g&&(e=g.set(a,d,c))!==b?e:a[c]=d:g&&"get"in g&&(e=g.get(a,c))!==null?e:a[c]}},propHooks:{tabIndex:{get:function(a){var c=a.getAttributeNode("tabindex");return c&&c.specified?parseInt(c.value,10):s.test(a.nodeName)||t.test(a.nodeName)&&a.href?0:b}}}}),f.attrHooks.tabindex=f.propHooks.tabIndex,x={get:function(a,c){var d,e=f.prop(a,c);return e===!0||typeof e!="boolean"&&(d=a.getAttributeNode(c))&&d.nodeValue!==!1?c.toLowerCase():b},set:function(a,b,c){var d;b===!1?f.removeAttr(a,c):(d=f.propFix[c]||c,d in a&&(a[d]=!0),a.setAttribute(c,c.toLowerCase()));return c}},v||(y={name:!0,id:!0},w=f.valHooks.button={get:function(a,c){var d;d=a.getAttributeNode(c);return d&&(y[c]?d.nodeValue!=="":d.specified)?d.nodeValue:b},set:function(a,b,d){var e=a.getAttributeNode(d);e||(e=c.createAttribute(d),a.setAttributeNode(e));return e.nodeValue=b+""}},f.attrHooks.tabindex.set=w.set,f.each(["width","height"],function(a,b){f.attrHooks[b]=f.extend(f.attrHooks[b],{set:function(a,c){if(c===""){a.setAttribute(b,"auto");return c}}})}),f.attrHooks.contenteditable={get:w.get,set:function(a,b,c){b===""&&(b="false"),w.set(a,b,c)}}),f.support.hrefNormalized||f.each(["href","src","width","height"],function(a,c){f.attrHooks[c]=f.extend(f.attrHooks[c],{get:function(a){var d=a.getAttribute(c,2);return d===null?b:d}})}),f.support.style||(f.attrHooks.style={get:function(a){return a.style.cssText.toLowerCase()||b},set:function(a,b){return a.style.cssText=""+b}}),f.support.optSelected||(f.propHooks.selected=f.extend(f.propHooks.selected,{get:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex);return null}})),f.support.enctype||(f.propFix.enctype="encoding"),f.support.checkOn||f.each(["radio","checkbox"],function(){f.valHooks[this]={get:function(a){return a.getAttribute("value")===null?"on":a.value}}}),f.each(["radio","checkbox"],function(){f.valHooks[this]=f.extend(f.valHooks[this],{set:function(a,b){if(f.isArray(b))return a.checked=f.inArray(f(a).val(),b)>=0}})});var z=/^(?:textarea|input|select)$/i,A=/^([^\.]*)?(?:\.(.+))?$/,B=/\bhover(\.\S+)?\b/,C=/^key/,D=/^(?:mouse|contextmenu)|click/,E=/^(?:focusinfocus|focusoutblur)$/,F=/^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/,G=function(a){var b=F.exec(a);b&&(b[1]=(b[1]||"").toLowerCase(),b[3]=b[3]&&new RegExp("(?:^|\\s)"+b[3]+"(?:\\s|$)"));return b},H=function(a,b){var c=a.attributes||{};return(!b[1]||a.nodeName.toLowerCase()===b[1])&&(!b[2]||(c.id||{}).value===b[2])&&(!b[3]||b[3].test((c["class"]||{}).value))},I=function(a){return f.event.special.hover?a:a.replace(B,"mouseenter$1 mouseleave$1")};
f.event={add:function(a,c,d,e,g){var h,i,j,k,l,m,n,o,p,q,r,s;if(!(a.nodeType===3||a.nodeType===8||!c||!d||!(h=f._data(a)))){d.handler&&(p=d,d=p.handler),d.guid||(d.guid=f.guid++),j=h.events,j||(h.events=j={}),i=h.handle,i||(h.handle=i=function(a){return typeof f!="undefined"&&(!a||f.event.triggered!==a.type)?f.event.dispatch.apply(i.elem,arguments):b},i.elem=a),c=f.trim(I(c)).split(" ");for(k=0;k<c.length;k++){l=A.exec(c[k])||[],m=l[1],n=(l[2]||"").split(".").sort(),s=f.event.special[m]||{},m=(g?s.delegateType:s.bindType)||m,s=f.event.special[m]||{},o=f.extend({type:m,origType:l[1],data:e,handler:d,guid:d.guid,selector:g,quick:G(g),namespace:n.join(".")},p),r=j[m];if(!r){r=j[m]=[],r.delegateCount=0;if(!s.setup||s.setup.call(a,e,n,i)===!1)a.addEventListener?a.addEventListener(m,i,!1):a.attachEvent&&a.attachEvent("on"+m,i)}s.add&&(s.add.call(a,o),o.handler.guid||(o.handler.guid=d.guid)),g?r.splice(r.delegateCount++,0,o):r.push(o),f.event.global[m]=!0}a=null}},global:{},remove:function(a,b,c,d,e){var g=f.hasData(a)&&f._data(a),h,i,j,k,l,m,n,o,p,q,r,s;if(!!g&&!!(o=g.events)){b=f.trim(I(b||"")).split(" ");for(h=0;h<b.length;h++){i=A.exec(b[h])||[],j=k=i[1],l=i[2];if(!j){for(j in o)f.event.remove(a,j+b[h],c,d,!0);continue}p=f.event.special[j]||{},j=(d?p.delegateType:p.bindType)||j,r=o[j]||[],m=r.length,l=l?new RegExp("(^|\\.)"+l.split(".").sort().join("\\.(?:.*\\.)?")+"(\\.|$)"):null;for(n=0;n<r.length;n++)s=r[n],(e||k===s.origType)&&(!c||c.guid===s.guid)&&(!l||l.test(s.namespace))&&(!d||d===s.selector||d==="**"&&s.selector)&&(r.splice(n--,1),s.selector&&r.delegateCount--,p.remove&&p.remove.call(a,s));r.length===0&&m!==r.length&&((!p.teardown||p.teardown.call(a,l)===!1)&&f.removeEvent(a,j,g.handle),delete o[j])}f.isEmptyObject(o)&&(q=g.handle,q&&(q.elem=null),f.removeData(a,["events","handle"],!0))}},customEvent:{getData:!0,setData:!0,changeData:!0},trigger:function(c,d,e,g){if(!e||e.nodeType!==3&&e.nodeType!==8){var h=c.type||c,i=[],j,k,l,m,n,o,p,q,r,s;if(E.test(h+f.event.triggered))return;h.indexOf("!")>=0&&(h=h.slice(0,-1),k=!0),h.indexOf(".")>=0&&(i=h.split("."),h=i.shift(),i.sort());if((!e||f.event.customEvent[h])&&!f.event.global[h])return;c=typeof c=="object"?c[f.expando]?c:new f.Event(h,c):new f.Event(h),c.type=h,c.isTrigger=!0,c.exclusive=k,c.namespace=i.join("."),c.namespace_re=c.namespace?new RegExp("(^|\\.)"+i.join("\\.(?:.*\\.)?")+"(\\.|$)"):null,o=h.indexOf(":")<0?"on"+h:"";if(!e){j=f.cache;for(l in j)j[l].events&&j[l].events[h]&&f.event.trigger(c,d,j[l].handle.elem,!0);return}c.result=b,c.target||(c.target=e),d=d!=null?f.makeArray(d):[],d.unshift(c),p=f.event.special[h]||{};if(p.trigger&&p.trigger.apply(e,d)===!1)return;r=[[e,p.bindType||h]];if(!g&&!p.noBubble&&!f.isWindow(e)){s=p.delegateType||h,m=E.test(s+h)?e:e.parentNode,n=null;for(;m;m=m.parentNode)r.push([m,s]),n=m;n&&n===e.ownerDocument&&r.push([n.defaultView||n.parentWindow||a,s])}for(l=0;l<r.length&&!c.isPropagationStopped();l++)m=r[l][0],c.type=r[l][1],q=(f._data(m,"events")||{})[c.type]&&f._data(m,"handle"),q&&q.apply(m,d),q=o&&m[o],q&&f.acceptData(m)&&q.apply(m,d)===!1&&c.preventDefault();c.type=h,!g&&!c.isDefaultPrevented()&&(!p._default||p._default.apply(e.ownerDocument,d)===!1)&&(h!=="click"||!f.nodeName(e,"a"))&&f.acceptData(e)&&o&&e[h]&&(h!=="focus"&&h!=="blur"||c.target.offsetWidth!==0)&&!f.isWindow(e)&&(n=e[o],n&&(e[o]=null),f.event.triggered=h,e[h](),f.event.triggered=b,n&&(e[o]=n));return c.result}},dispatch:function(c){c=f.event.fix(c||a.event);var d=(f._data(this,"events")||{})[c.type]||[],e=d.delegateCount,g=[].slice.call(arguments,0),h=!c.exclusive&&!c.namespace,i=[],j,k,l,m,n,o,p,q,r,s,t;g[0]=c,c.delegateTarget=this;if(e&&!c.target.disabled&&(!c.button||c.type!=="click")){m=f(this),m.context=this.ownerDocument||this;for(l=c.target;l!=this;l=l.parentNode||this){o={},q=[],m[0]=l;for(j=0;j<e;j++)r=d[j],s=r.selector,o[s]===b&&(o[s]=r.quick?H(l,r.quick):m.is(s)),o[s]&&q.push(r);q.length&&i.push({elem:l,matches:q})}}d.length>e&&i.push({elem:this,matches:d.slice(e)});for(j=0;j<i.length&&!c.isPropagationStopped();j++){p=i[j],c.currentTarget=p.elem;for(k=0;k<p.matches.length&&!c.isImmediatePropagationStopped();k++){r=p.matches[k];if(h||!c.namespace&&!r.namespace||c.namespace_re&&c.namespace_re.test(r.namespace))c.data=r.data,c.handleObj=r,n=((f.event.special[r.origType]||{}).handle||r.handler).apply(p.elem,g),n!==b&&(c.result=n,n===!1&&(c.preventDefault(),c.stopPropagation()))}}return c.result},props:"attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){a.which==null&&(a.which=b.charCode!=null?b.charCode:b.keyCode);return a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,d){var e,f,g,h=d.button,i=d.fromElement;a.pageX==null&&d.clientX!=null&&(e=a.target.ownerDocument||c,f=e.documentElement,g=e.body,a.pageX=d.clientX+(f&&f.scrollLeft||g&&g.scrollLeft||0)-(f&&f.clientLeft||g&&g.clientLeft||0),a.pageY=d.clientY+(f&&f.scrollTop||g&&g.scrollTop||0)-(f&&f.clientTop||g&&g.clientTop||0)),!a.relatedTarget&&i&&(a.relatedTarget=i===a.target?d.toElement:i),!a.which&&h!==b&&(a.which=h&1?1:h&2?3:h&4?2:0);return a}},fix:function(a){if(a[f.expando])return a;var d,e,g=a,h=f.event.fixHooks[a.type]||{},i=h.props?this.props.concat(h.props):this.props;a=f.Event(g);for(d=i.length;d;)e=i[--d],a[e]=g[e];a.target||(a.target=g.srcElement||c),a.target.nodeType===3&&(a.target=a.target.parentNode),a.metaKey===b&&(a.metaKey=a.ctrlKey);return h.filter?h.filter(a,g):a},special:{ready:{setup:f.bindReady},load:{noBubble:!0},focus:{delegateType:"focusin"},blur:{delegateType:"focusout"},beforeunload:{setup:function(a,b,c){f.isWindow(this)&&(this.onbeforeunload=c)},teardown:function(a,b){this.onbeforeunload===b&&(this.onbeforeunload=null)}}},simulate:function(a,b,c,d){var e=f.extend(new f.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?f.event.trigger(e,null,b):f.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},f.event.handle=f.event.dispatch,f.removeEvent=c.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){a.detachEvent&&a.detachEvent("on"+b,c)},f.Event=function(a,b){if(!(this instanceof f.Event))return new f.Event(a,b);a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||a.returnValue===!1||a.getPreventDefault&&a.getPreventDefault()?K:J):this.type=a,b&&f.extend(this,b),this.timeStamp=a&&a.timeStamp||f.now(),this[f.expando]=!0},f.Event.prototype={preventDefault:function(){this.isDefaultPrevented=K;var a=this.originalEvent;!a||(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){this.isPropagationStopped=K;var a=this.originalEvent;!a||(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=K,this.stopPropagation()},isDefaultPrevented:J,isPropagationStopped:J,isImmediatePropagationStopped:J},f.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(a,b){f.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c=this,d=a.relatedTarget,e=a.handleObj,g=e.selector,h;if(!d||d!==c&&!f.contains(c,d))a.type=e.origType,h=e.handler.apply(this,arguments),a.type=b;return h}}}),f.support.submitBubbles||(f.event.special.submit={setup:function(){if(f.nodeName(this,"form"))return!1;f.event.add(this,"click._submit keypress._submit",function(a){var c=a.target,d=f.nodeName(c,"input")||f.nodeName(c,"button")?c.form:b;d&&!d._submit_attached&&(f.event.add(d,"submit._submit",function(a){this.parentNode&&!a.isTrigger&&f.event.simulate("submit",this.parentNode,a,!0)}),d._submit_attached=!0)})},teardown:function(){if(f.nodeName(this,"form"))return!1;f.event.remove(this,"._submit")}}),f.support.changeBubbles||(f.event.special.change={setup:function(){if(z.test(this.nodeName)){if(this.type==="checkbox"||this.type==="radio")f.event.add(this,"propertychange._change",function(a){a.originalEvent.propertyName==="checked"&&(this._just_changed=!0)}),f.event.add(this,"click._change",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1,f.event.simulate("change",this,a,!0))});return!1}f.event.add(this,"beforeactivate._change",function(a){var b=a.target;z.test(b.nodeName)&&!b._change_attached&&(f.event.add(b,"change._change",function(a){this.parentNode&&!a.isSimulated&&!a.isTrigger&&f.event.simulate("change",this.parentNode,a,!0)}),b._change_attached=!0)})},handle:function(a){var b=a.target;if(this!==b||a.isSimulated||a.isTrigger||b.type!=="radio"&&b.type!=="checkbox")return a.handleObj.handler.apply(this,arguments)},teardown:function(){f.event.remove(this,"._change");return z.test(this.nodeName)}}),f.support.focusinBubbles||f.each({focus:"focusin",blur:"focusout"},function(a,b){var d=0,e=function(a){f.event.simulate(b,a.target,f.event.fix(a),!0)};f.event.special[b]={setup:function(){d++===0&&c.addEventListener(a,e,!0)},teardown:function(){--d===0&&c.removeEventListener(a,e,!0)}}}),f.fn.extend({on:function(a,c,d,e,g){var h,i;if(typeof a=="object"){typeof c!="string"&&(d=c,c=b);for(i in a)this.on(i,c,d,a[i],g);return this}d==null&&e==null?(e=c,d=c=b):e==null&&(typeof c=="string"?(e=d,d=b):(e=d,d=c,c=b));if(e===!1)e=J;else if(!e)return this;g===1&&(h=e,e=function(a){f().off(a);return h.apply(this,arguments)},e.guid=h.guid||(h.guid=f.guid++));return this.each(function(){f.event.add(this,a,e,d,c)})},one:function(a,b,c,d){return this.on.call(this,a,b,c,d,1)},off:function(a,c,d){if(a&&a.preventDefault&&a.handleObj){var e=a.handleObj;f(a.delegateTarget).off(e.namespace?e.type+"."+e.namespace:e.type,e.selector,e.handler);return this}if(typeof a=="object"){for(var g in a)this.off(g,c,a[g]);return this}if(c===!1||typeof c=="function")d=c,c=b;d===!1&&(d=J);return this.each(function(){f.event.remove(this,a,d,c)})},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},live:function(a,b,c){f(this.context).on(a,this.selector,b,c);return this},die:function(a,b){f(this.context).off(a,this.selector||"**",b);return this},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return arguments.length==1?this.off(a,"**"):this.off(b,a,c)},trigger:function(a,b){return this.each(function(){f.event.trigger(a,b,this)})},triggerHandler:function(a,b){if(this[0])return f.event.trigger(a,b,this[0],!0)},toggle:function(a){var b=arguments,c=a.guid||f.guid++,d=0,e=function(c){var e=(f._data(this,"lastToggle"+a.guid)||0)%d;f._data(this,"lastToggle"+a.guid,e+1),c.preventDefault();return b[e].apply(this,arguments)||!1};e.guid=c;while(d<b.length)b[d++].guid=c;return this.click(e)},hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),f.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){f.fn[b]=function(a,c){c==null&&(c=a,a=null);return arguments.length>0?this.on(b,null,a,c):this.trigger(b)},f.attrFn&&(f.attrFn[b]=!0),C.test(b)&&(f.event.fixHooks[b]=f.event.keyHooks),D.test(b)&&(f.event.fixHooks[b]=f.event.mouseHooks)}),function(){function x(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}if(j.nodeType===1){g||(j[d]=c,j.sizset=h);if(typeof b!="string"){if(j===b){k=!0;break}}else if(m.filter(b,[j]).length>0){k=j;break}}j=j[a]}e[h]=k}}}function w(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}j.nodeType===1&&!g&&(j[d]=c,j.sizset=h);if(j.nodeName.toLowerCase()===b){k=j;break}j=j[a]}e[h]=k}}}var a=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,d="sizcache"+(Math.random()+"").replace(".",""),e=0,g=Object.prototype.toString,h=!1,i=!0,j=/\\/g,k=/\r\n/g,l=/\W/;[0,0].sort(function(){i=!1;return 0});var m=function(b,d,e,f){e=e||[],d=d||c;var h=d;if(d.nodeType!==1&&d.nodeType!==9)return[];if(!b||typeof b!="string")return e;var i,j,k,l,n,q,r,t,u=!0,v=m.isXML(d),w=[],x=b;do{a.exec(""),i=a.exec(x);if(i){x=i[3],w.push(i[1]);if(i[2]){l=i[3];break}}}while(i);if(w.length>1&&p.exec(b))if(w.length===2&&o.relative[w[0]])j=y(w[0]+w[1],d,f);else{j=o.relative[w[0]]?[d]:m(w.shift(),d);while(w.length)b=w.shift(),o.relative[b]&&(b+=w.shift()),j=y(b,j,f)}else{!f&&w.length>1&&d.nodeType===9&&!v&&o.match.ID.test(w[0])&&!o.match.ID.test(w[w.length-1])&&(n=m.find(w.shift(),d,v),d=n.expr?m.filter(n.expr,n.set)[0]:n.set[0]);if(d){n=f?{expr:w.pop(),set:s(f)}:m.find(w.pop(),w.length===1&&(w[0]==="~"||w[0]==="+")&&d.parentNode?d.parentNode:d,v),j=n.expr?m.filter(n.expr,n.set):n.set,w.length>0?k=s(j):u=!1;while(w.length)q=w.pop(),r=q,o.relative[q]?r=w.pop():q="",r==null&&(r=d),o.relative[q](k,r,v)}else k=w=[]}k||(k=j),k||m.error(q||b);if(g.call(k)==="[object Array]")if(!u)e.push.apply(e,k);else if(d&&d.nodeType===1)for(t=0;k[t]!=null;t++)k[t]&&(k[t]===!0||k[t].nodeType===1&&m.contains(d,k[t]))&&e.push(j[t]);else for(t=0;k[t]!=null;t++)k[t]&&k[t].nodeType===1&&e.push(j[t]);else s(k,e);l&&(m(l,h,e,f),m.uniqueSort(e));return e};m.uniqueSort=function(a){if(u){h=i,a.sort(u);if(h)for(var b=1;b<a.length;b++)a[b]===a[b-1]&&a.splice(b--,1)}return a},m.matches=function(a,b){return m(a,null,null,b)},m.matchesSelector=function(a,b){return m(b,null,null,[a]).length>0},m.find=function(a,b,c){var d,e,f,g,h,i;if(!a)return[];for(e=0,f=o.order.length;e<f;e++){h=o.order[e];if(g=o.leftMatch[h].exec(a)){i=g[1],g.splice(1,1);if(i.substr(i.length-1)!=="\\"){g[1]=(g[1]||"").replace(j,""),d=o.find[h](g,b,c);if(d!=null){a=a.replace(o.match[h],"");break}}}}d||(d=typeof b.getElementsByTagName!="undefined"?b.getElementsByTagName("*"):[]);return{set:d,expr:a}},m.filter=function(a,c,d,e){var f,g,h,i,j,k,l,n,p,q=a,r=[],s=c,t=c&&c[0]&&m.isXML(c[0]);while(a&&c.length){for(h in o.filter)if((f=o.leftMatch[h].exec(a))!=null&&f[2]){k=o.filter[h],l=f[1],g=!1,f.splice(1,1);if(l.substr(l.length-1)==="\\")continue;s===r&&(r=[]);if(o.preFilter[h]){f=o.preFilter[h](f,s,d,r,e,t);if(!f)g=i=!0;else if(f===!0)continue}if(f)for(n=0;(j=s[n])!=null;n++)j&&(i=k(j,f,n,s),p=e^i,d&&i!=null?p?g=!0:s[n]=!1:p&&(r.push(j),g=!0));if(i!==b){d||(s=r),a=a.replace(o.match[h],"");if(!g)return[];break}}if(a===q)if(g==null)m.error(a);else break;q=a}return s},m.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)};var n=m.getText=function(a){var b,c,d=a.nodeType,e="";if(d){if(d===1||d===9){if(typeof a.textContent=="string")return a.textContent;if(typeof a.innerText=="string")return a.innerText.replace(k,"");for(a=a.firstChild;a;a=a.nextSibling)e+=n(a)}else if(d===3||d===4)return a.nodeValue}else for(b=0;c=a[b];b++)c.nodeType!==8&&(e+=n(c));return e},o=m.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(a){return a.getAttribute("href")},type:function(a){return a.getAttribute("type")}},relative:{"+":function(a,b){var c=typeof b=="string",d=c&&!l.test(b),e=c&&!d;d&&(b=b.toLowerCase());for(var f=0,g=a.length,h;f<g;f++)if(h=a[f]){while((h=h.previousSibling)&&h.nodeType!==1);a[f]=e||h&&h.nodeName.toLowerCase()===b?h||!1:h===b}e&&m.filter(b,a,!0)},">":function(a,b){var c,d=typeof b=="string",e=0,f=a.length;if(d&&!l.test(b)){b=b.toLowerCase();for(;e<f;e++){c=a[e];if(c){var g=c.parentNode;a[e]=g.nodeName.toLowerCase()===b?g:!1}}}else{for(;e<f;e++)c=a[e],c&&(a[e]=d?c.parentNode:c.parentNode===b);d&&m.filter(b,a,!0)}},"":function(a,b,c){var d,f=e++,g=x;typeof b=="string"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g("parentNode",b,f,a,d,c)},"~":function(a,b,c){var d,f=e++,g=x;typeof b=="string"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g("previousSibling",b,f,a,d,c)}},find:{ID:function(a,b,c){if(typeof b.getElementById!="undefined"&&!c){var d=b.getElementById(a[1]);return d&&d.parentNode?[d]:[]}},NAME:function(a,b){if(typeof b.getElementsByName!="undefined"){var c=[],d=b.getElementsByName(a[1]);for(var e=0,f=d.length;e<f;e++)d[e].getAttribute("name")===a[1]&&c.push(d[e]);return c.length===0?null:c}},TAG:function(a,b){if(typeof b.getElementsByTagName!="undefined")return b.getElementsByTagName(a[1])}},preFilter:{CLASS:function(a,b,c,d,e,f){a=" "+a[1].replace(j,"")+" ";if(f)return a;for(var g=0,h;(h=b[g])!=null;g++)h&&(e^(h.className&&(" "+h.className+" ").replace(/[\t\n\r]/g," ").indexOf(a)>=0)?c||d.push(h):c&&(b[g]=!1));return!1},ID:function(a){return a[1].replace(j,"")},TAG:function(a,b){return a[1].replace(j,"").toLowerCase()},CHILD:function(a){if(a[1]==="nth"){a[2]||m.error(a[0]),a[2]=a[2].replace(/^\+|\s*/g,"");var b=/(-?)(\d*)(?:n([+\-]?\d*))?/.exec(a[2]==="even"&&"2n"||a[2]==="odd"&&"2n+1"||!/\D/.test(a[2])&&"0n+"+a[2]||a[2]);a[2]=b[1]+(b[2]||1)-0,a[3]=b[3]-0}else a[2]&&m.error(a[0]);a[0]=e++;return a},ATTR:function(a,b,c,d,e,f){var g=a[1]=a[1].replace(j,"");!f&&o.attrMap[g]&&(a[1]=o.attrMap[g]),a[4]=(a[4]||a[5]||"").replace(j,""),a[2]==="~="&&(a[4]=" "+a[4]+" ");return a},PSEUDO:function(b,c,d,e,f){if(b[1]==="not")if((a.exec(b[3])||"").length>1||/^\w/.test(b[3]))b[3]=m(b[3],null,null,c);else{var g=m.filter(b[3],c,d,!0^f);d||e.push.apply(e,g);return!1}else if(o.match.POS.test(b[0])||o.match.CHILD.test(b[0]))return!0;return b},POS:function(a){a.unshift(!0);return a}},filters:{enabled:function(a){return a.disabled===!1&&a.type!=="hidden"},disabled:function(a){return a.disabled===!0},checked:function(a){return a.checked===!0},selected:function(a){a.parentNode&&a.parentNode.selectedIndex;return a.selected===!0},parent:function(a){return!!a.firstChild},empty:function(a){return!a.firstChild},has:function(a,b,c){return!!m(c[3],a).length},header:function(a){return/h\d/i.test(a.nodeName)},text:function(a){var b=a.getAttribute("type"),c=a.type;return a.nodeName.toLowerCase()==="input"&&"text"===c&&(b===c||b===null)},radio:function(a){return a.nodeName.toLowerCase()==="input"&&"radio"===a.type},checkbox:function(a){return a.nodeName.toLowerCase()==="input"&&"checkbox"===a.type},file:function(a){return a.nodeName.toLowerCase()==="input"&&"file"===a.type},password:function(a){return a.nodeName.toLowerCase()==="input"&&"password"===a.type},submit:function(a){var b=a.nodeName.toLowerCase();return(b==="input"||b==="button")&&"submit"===a.type},image:function(a){return a.nodeName.toLowerCase()==="input"&&"image"===a.type},reset:function(a){var b=a.nodeName.toLowerCase();return(b==="input"||b==="button")&&"reset"===a.type},button:function(a){var b=a.nodeName.toLowerCase();return b==="input"&&"button"===a.type||b==="button"},input:function(a){return/input|select|textarea|button/i.test(a.nodeName)},focus:function(a){return a===a.ownerDocument.activeElement}},setFilters:{first:function(a,b){return b===0},last:function(a,b,c,d){return b===d.length-1},even:function(a,b){return b%2===0},odd:function(a,b){return b%2===1},lt:function(a,b,c){return b<c[3]-0},gt:function(a,b,c){return b>c[3]-0},nth:function(a,b,c){return c[3]-0===b},eq:function(a,b,c){return c[3]-0===b}},filter:{PSEUDO:function(a,b,c,d){var e=b[1],f=o.filters[e];if(f)return f(a,c,b,d);if(e==="contains")return(a.textContent||a.innerText||n([a])||"").indexOf(b[3])>=0;if(e==="not"){var g=b[3];for(var h=0,i=g.length;h<i;h++)if(g[h]===a)return!1;return!0}m.error(e)},CHILD:function(a,b){var c,e,f,g,h,i,j,k=b[1],l=a;switch(k){case"only":case"first":while(l=l.previousSibling)if(l.nodeType===1)return!1;if(k==="first")return!0;l=a;case"last":while(l=l.nextSibling)if(l.nodeType===1)return!1;return!0;case"nth":c=b[2],e=b[3];if(c===1&&e===0)return!0;f=b[0],g=a.parentNode;if(g&&(g[d]!==f||!a.nodeIndex)){i=0;for(l=g.firstChild;l;l=l.nextSibling)l.nodeType===1&&(l.nodeIndex=++i);g[d]=f}j=a.nodeIndex-e;return c===0?j===0:j%c===0&&j/c>=0}},ID:function(a,b){return a.nodeType===1&&a.getAttribute("id")===b},TAG:function(a,b){return b==="*"&&a.nodeType===1||!!a.nodeName&&a.nodeName.toLowerCase()===b},CLASS:function(a,b){return(" "+(a.className||a.getAttribute("class"))+" ").indexOf(b)>-1},ATTR:function(a,b){var c=b[1],d=m.attr?m.attr(a,c):o.attrHandle[c]?o.attrHandle[c](a):a[c]!=null?a[c]:a.getAttribute(c),e=d+"",f=b[2],g=b[4];return d==null?f==="!=":!f&&m.attr?d!=null:f==="="?e===g:f==="*="?e.indexOf(g)>=0:f==="~="?(" "+e+" ").indexOf(g)>=0:g?f==="!="?e!==g:f==="^="?e.indexOf(g)===0:f==="$="?e.substr(e.length-g.length)===g:f==="|="?e===g||e.substr(0,g.length+1)===g+"-":!1:e&&d!==!1},POS:function(a,b,c,d){var e=b[2],f=o.setFilters[e];if(f)return f(a,c,b,d)}}},p=o.match.POS,q=function(a,b){return"\\"+(b-0+1)};for(var r in o.match)o.match[r]=new RegExp(o.match[r].source+/(?![^\[]*\])(?![^\(]*\))/.source),o.leftMatch[r]=new RegExp(/(^(?:.|\r|\n)*?)/.source+o.match[r].source.replace(/\\(\d+)/g,q));var s=function(a,b){a=Array.prototype.slice.call(a,0);if(b){b.push.apply(b,a);return b}return a};try{Array.prototype.slice.call(c.documentElement.childNodes,0)[0].nodeType}catch(t){s=function(a,b){var c=0,d=b||[];if(g.call(a)==="[object Array]")Array.prototype.push.apply(d,a);else if(typeof a.length=="number")for(var e=a.length;c<e;c++)d.push(a[c]);else for(;a[c];c++)d.push(a[c]);return d}}var u,v;c.documentElement.compareDocumentPosition?u=function(a,b){if(a===b){h=!0;return 0}if(!a.compareDocumentPosition||!b.compareDocumentPosition)return a.compareDocumentPosition?-1:1;return a.compareDocumentPosition(b)&4?-1:1}:(u=function(a,b){if(a===b){h=!0;return 0}if(a.sourceIndex&&b.sourceIndex)return a.sourceIndex-b.sourceIndex;var c,d,e=[],f=[],g=a.parentNode,i=b.parentNode,j=g;if(g===i)return v(a,b);if(!g)return-1;if(!i)return 1;while(j)e.unshift(j),j=j.parentNode;j=i;while(j)f.unshift(j),j=j.parentNode;c=e.length,d=f.length;for(var k=0;k<c&&k<d;k++)if(e[k]!==f[k])return v(e[k],f[k]);return k===c?v(a,f[k],-1):v(e[k],b,1)},v=function(a,b,c){if(a===b)return c;var d=a.nextSibling;while(d){if(d===b)return-1;d=d.nextSibling}return 1}),function(){var a=c.createElement("div"),d="script"+(new Date).getTime(),e=c.documentElement;a.innerHTML="<a name='"+d+"'/>",e.insertBefore(a,e.firstChild),c.getElementById(d)&&(o.find.ID=function(a,c,d){if(typeof c.getElementById!="undefined"&&!d){var e=c.getElementById(a[1]);return e?e.id===a[1]||typeof e.getAttributeNode!="undefined"&&e.getAttributeNode("id").nodeValue===a[1]?[e]:b:[]}},o.filter.ID=function(a,b){var c=typeof a.getAttributeNode!="undefined"&&a.getAttributeNode("id");return a.nodeType===1&&c&&c.nodeValue===b}),e.removeChild(a),e=a=null}(),function(){var a=c.createElement("div");a.appendChild(c.createComment("")),a.getElementsByTagName("*").length>0&&(o.find.TAG=function(a,b){var c=b.getElementsByTagName(a[1]);if(a[1]==="*"){var d=[];for(var e=0;c[e];e++)c[e].nodeType===1&&d.push(c[e]);c=d}return c}),a.innerHTML="<a href='#'></a>",a.firstChild&&typeof a.firstChild.getAttribute!="undefined"&&a.firstChild.getAttribute("href")!=="#"&&(o.attrHandle.href=function(a){return a.getAttribute("href",2)}),a=null}(),c.querySelectorAll&&function(){var a=m,b=c.createElement("div"),d="__sizzle__";b.innerHTML="<p class='TEST'></p>";if(!b.querySelectorAll||b.querySelectorAll(".TEST").length!==0){m=function(b,e,f,g){e=e||c;if(!g&&!m.isXML(e)){var h=/^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(b);if(h&&(e.nodeType===1||e.nodeType===9)){if(h[1])return s(e.getElementsByTagName(b),f);if(h[2]&&o.find.CLASS&&e.getElementsByClassName)return s(e.getElementsByClassName(h[2]),f)}if(e.nodeType===9){if(b==="body"&&e.body)return s([e.body],f);if(h&&h[3]){var i=e.getElementById(h[3]);if(!i||!i.parentNode)return s([],f);if(i.id===h[3])return s([i],f)}try{return s(e.querySelectorAll(b),f)}catch(j){}}else if(e.nodeType===1&&e.nodeName.toLowerCase()!=="object"){var k=e,l=e.getAttribute("id"),n=l||d,p=e.parentNode,q=/^\s*[+~]/.test(b);l?n=n.replace(/'/g,"\\$&"):e.setAttribute("id",n),q&&p&&(e=e.parentNode);try{if(!q||p)return s(e.querySelectorAll("[id='"+n+"'] "+b),f)}catch(r){}finally{l||k.removeAttribute("id")}}}return a(b,e,f,g)};for(var e in a)m[e]=a[e];b=null}}(),function(){var a=c.documentElement,b=a.matchesSelector||a.mozMatchesSelector||a.webkitMatchesSelector||a.msMatchesSelector;if(b){var d=!b.call(c.createElement("div"),"div"),e=!1;try{b.call(c.documentElement,"[test!='']:sizzle")}catch(f){e=!0}m.matchesSelector=function(a,c){c=c.replace(/\=\s*([^'"\]]*)\s*\]/g,"='$1']");if(!m.isXML(a))try{if(e||!o.match.PSEUDO.test(c)&&!/!=/.test(c)){var f=b.call(a,c);if(f||!d||a.document&&a.document.nodeType!==11)return f}}catch(g){}return m(c,null,null,[a]).length>0}}}(),function(){var a=c.createElement("div");a.innerHTML="<div class='test e'></div><div class='test'></div>";if(!!a.getElementsByClassName&&a.getElementsByClassName("e").length!==0){a.lastChild.className="e";if(a.getElementsByClassName("e").length===1)return;o.order.splice(1,0,"CLASS"),o.find.CLASS=function(a,b,c){if(typeof b.getElementsByClassName!="undefined"&&!c)return b.getElementsByClassName(a[1])},a=null}}(),c.documentElement.contains?m.contains=function(a,b){return a!==b&&(a.contains?a.contains(b):!0)}:c.documentElement.compareDocumentPosition?m.contains=function(a,b){return!!(a.compareDocumentPosition(b)&16)}:m.contains=function(){return!1},m.isXML=function(a){var b=(a?a.ownerDocument||a:0).documentElement;return b?b.nodeName!=="HTML":!1};var y=function(a,b,c){var d,e=[],f="",g=b.nodeType?[b]:b;while(d=o.match.PSEUDO.exec(a))f+=d[0],a=a.replace(o.match.PSEUDO,"");a=o.relative[a]?a+"*":a;for(var h=0,i=g.length;h<i;h++)m(a,g[h],e,c);return m.filter(f,e)};m.attr=f.attr,m.selectors.attrMap={},f.find=m,f.expr=m.selectors,f.expr[":"]=f.expr.filters,f.unique=m.uniqueSort,f.text=m.getText,f.isXMLDoc=m.isXML,f.contains=m.contains}();var L=/Until$/,M=/^(?:parents|prevUntil|prevAll)/,N=/,/,O=/^.[^:#\[\.,]*$/,P=Array.prototype.slice,Q=f.expr.match.POS,R={children:!0,contents:!0,next:!0,prev:!0};f.fn.extend({find:function(a){var b=this,c,d;if(typeof a!="string")return f(a).filter(function(){for(c=0,d=b.length;c<d;c++)if(f.contains(b[c],this))return!0});var e=this.pushStack("","find",a),g,h,i;for(c=0,d=this.length;c<d;c++){g=e.length,f.find(a,this[c],e);if(c>0)for(h=g;h<e.length;h++)for(i=0;i<g;i++)if(e[i]===e[h]){e.splice(h--,1);break}}return e},has:function(a){var b=f(a);return this.filter(function(){for(var a=0,c=b.length;a<c;a++)if(f.contains(this,b[a]))return!0})},not:function(a){return this.pushStack(T(this,a,!1),"not",a)},filter:function(a){return this.pushStack(T(this,a,!0),"filter",a)},is:function(a){return!!a&&(typeof a=="string"?Q.test(a)?f(a,this.context).index(this[0])>=0:f.filter(a,this).length>0:this.filter(a).length>0)},closest:function(a,b){var c=[],d,e,g=this[0];if(f.isArray(a)){var h=1;while(g&&g.ownerDocument&&g!==b){for(d=0;d<a.length;d++)f(g).is(a[d])&&c.push({selector:a[d],elem:g,level:h});g=g.parentNode,h++}return c}var i=Q.test(a)||typeof a!="string"?f(a,b||this.context):0;for(d=0,e=this.length;d<e;d++){g=this[d];while(g){if(i?i.index(g)>-1:f.find.matchesSelector(g,a)){c.push(g);break}g=g.parentNode;if(!g||!g.ownerDocument||g===b||g.nodeType===11)break}}c=c.length>1?f.unique(c):c;return this.pushStack(c,"closest",a)},index:function(a){if(!a)return this[0]&&this[0].parentNode?this.prevAll().length:-1;if(typeof a=="string")return f.inArray(this[0],f(a));return f.inArray(a.jquery?a[0]:a,this)},add:function(a,b){var c=typeof a=="string"?f(a,b):f.makeArray(a&&a.nodeType?[a]:a),d=f.merge(this.get(),c);return this.pushStack(S(c[0])||S(d[0])?d:f.unique(d))},andSelf:function(){return this.add(this.prevObject)}}),f.each({parent:function(a){var b=a.parentNode;return b&&b.nodeType!==11?b:null},parents:function(a){return f.dir(a,"parentNode")},parentsUntil:function(a,b,c){return f.dir(a,"parentNode",c)},next:function(a){return f.nth(a,2,"nextSibling")},prev:function(a){return f.nth(a,2,"previousSibling")},nextAll:function(a){return f.dir(a,"nextSibling")},prevAll:function(a){return f.dir(a,"previousSibling")},nextUntil:function(a,b,c){return f.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return f.dir(a,"previousSibling",c)},siblings:function(a){return f.sibling(a.parentNode.firstChild,a)},children:function(a){return f.sibling(a.firstChild)},contents:function(a){return f.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:f.makeArray(a.childNodes)}},function(a,b){f.fn[a]=function(c,d){var e=f.map(this,b,c);L.test(a)||(d=c),d&&typeof d=="string"&&(e=f.filter(d,e)),e=this.length>1&&!R[a]?f.unique(e):e,(this.length>1||N.test(d))&&M.test(a)&&(e=e.reverse());return this.pushStack(e,a,P.call(arguments).join(","))}}),f.extend({filter:function(a,b,c){c&&(a=":not("+a+")");return b.length===1?f.find.matchesSelector(b[0],a)?[b[0]]:[]:f.find.matches(a,b)},dir:function(a,c,d){var e=[],g=a[c];while(g&&g.nodeType!==9&&(d===b||g.nodeType!==1||!f(g).is(d)))g.nodeType===1&&e.push(g),g=g[c];return e},nth:function(a,b,c,d){b=b||1;var e=0;for(;a;a=a[c])if(a.nodeType===1&&++e===b)break;return a},sibling:function(a,b){var c=[];for(;a;a=a.nextSibling)a.nodeType===1&&a!==b&&c.push(a);return c}});var V="abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",W=/ jQuery\d+="(?:\d+|null)"/g,X=/^\s+/,Y=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,Z=/<([\w:]+)/,$=/<tbody/i,_=/<|&#?\w+;/,ba=/<(?:script|style)/i,bb=/<(?:script|object|embed|option|style)/i,bc=new RegExp("<(?:"+V+")","i"),bd=/checked\s*(?:[^=]|=\s*.checked.)/i,be=/\/(java|ecma)script/i,bf=/^\s*<!(?:\[CDATA\[|\-\-)/,bg={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]},bh=U(c);bg.optgroup=bg.option,bg.tbody=bg.tfoot=bg.colgroup=bg.caption=bg.thead,bg.th=bg.td,f.support.htmlSerialize||(bg._default=[1,"div<div>","</div>"]),f.fn.extend({text:function(a){if(f.isFunction(a))return this.each(function(b){var c=f(this);c.text(a.call(this,b,c.text()))});if(typeof a!="object"&&a!==b)return this.empty().append((this[0]&&this[0].ownerDocument||c).createTextNode(a));return f.text(this)},wrapAll:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapAll(a.call(this,b))});if(this[0]){var b=f(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&a.firstChild.nodeType===1)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapInner(a.call(this,b))});return this.each(function(){var b=f(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=f.isFunction(a);return this.each(function(c){f(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){f.nodeName(this,"body")||f(this).replaceWith(this.childNodes)}).end()},append:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.appendChild(a)})},prepend:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.insertBefore(a,this.firstChild)})},before:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this)});if(arguments.length){var a=f.clean(arguments);a.push.apply(a,this.toArray());return this.pushStack(a,"before",arguments)}},after:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this.nextSibling)});if(arguments.length){var a=this.pushStack(this,"after",arguments);a.push.apply(a,f.clean(arguments));return a}},remove:function(a,b){for(var c=0,d;(d=this[c])!=null;c++)if(!a||f.filter(a,[d]).length)!b&&d.nodeType===1&&(f.cleanData(d.getElementsByTagName("*")),f.cleanData([d])),d.parentNode&&d.parentNode.removeChild(d);return this},empty:function()
{for(var a=0,b;(b=this[a])!=null;a++){b.nodeType===1&&f.cleanData(b.getElementsByTagName("*"));while(b.firstChild)b.removeChild(b.firstChild)}return this},clone:function(a,b){a=a==null?!1:a,b=b==null?a:b;return this.map(function(){return f.clone(this,a,b)})},html:function(a){if(a===b)return this[0]&&this[0].nodeType===1?this[0].innerHTML.replace(W,""):null;if(typeof a=="string"&&!ba.test(a)&&(f.support.leadingWhitespace||!X.test(a))&&!bg[(Z.exec(a)||["",""])[1].toLowerCase()]){a=a.replace(Y,"<$1></$2>");try{for(var c=0,d=this.length;c<d;c++)this[c].nodeType===1&&(f.cleanData(this[c].getElementsByTagName("*")),this[c].innerHTML=a)}catch(e){this.empty().append(a)}}else f.isFunction(a)?this.each(function(b){var c=f(this);c.html(a.call(this,b,c.html()))}):this.empty().append(a);return this},replaceWith:function(a){if(this[0]&&this[0].parentNode){if(f.isFunction(a))return this.each(function(b){var c=f(this),d=c.html();c.replaceWith(a.call(this,b,d))});typeof a!="string"&&(a=f(a).detach());return this.each(function(){var b=this.nextSibling,c=this.parentNode;f(this).remove(),b?f(b).before(a):f(c).append(a)})}return this.length?this.pushStack(f(f.isFunction(a)?a():a),"replaceWith",a):this},detach:function(a){return this.remove(a,!0)},domManip:function(a,c,d){var e,g,h,i,j=a[0],k=[];if(!f.support.checkClone&&arguments.length===3&&typeof j=="string"&&bd.test(j))return this.each(function(){f(this).domManip(a,c,d,!0)});if(f.isFunction(j))return this.each(function(e){var g=f(this);a[0]=j.call(this,e,c?g.html():b),g.domManip(a,c,d)});if(this[0]){i=j&&j.parentNode,f.support.parentNode&&i&&i.nodeType===11&&i.childNodes.length===this.length?e={fragment:i}:e=f.buildFragment(a,this,k),h=e.fragment,h.childNodes.length===1?g=h=h.firstChild:g=h.firstChild;if(g){c=c&&f.nodeName(g,"tr");for(var l=0,m=this.length,n=m-1;l<m;l++)d.call(c?bi(this[l],g):this[l],e.cacheable||m>1&&l<n?f.clone(h,!0,!0):h)}k.length&&f.each(k,bp)}return this}}),f.buildFragment=function(a,b,d){var e,g,h,i,j=a[0];b&&b[0]&&(i=b[0].ownerDocument||b[0]),i.createDocumentFragment||(i=c),a.length===1&&typeof j=="string"&&j.length<512&&i===c&&j.charAt(0)==="<"&&!bb.test(j)&&(f.support.checkClone||!bd.test(j))&&(f.support.html5Clone||!bc.test(j))&&(g=!0,h=f.fragments[j],h&&h!==1&&(e=h)),e||(e=i.createDocumentFragment(),f.clean(a,i,e,d)),g&&(f.fragments[j]=h?e:1);return{fragment:e,cacheable:g}},f.fragments={},f.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){f.fn[a]=function(c){var d=[],e=f(c),g=this.length===1&&this[0].parentNode;if(g&&g.nodeType===11&&g.childNodes.length===1&&e.length===1){e[b](this[0]);return this}for(var h=0,i=e.length;h<i;h++){var j=(h>0?this.clone(!0):this).get();f(e[h])[b](j),d=d.concat(j)}return this.pushStack(d,a,e.selector)}}),f.extend({clone:function(a,b,c){var d,e,g,h=f.support.html5Clone||!bc.test("<"+a.nodeName)?a.cloneNode(!0):bo(a);if((!f.support.noCloneEvent||!f.support.noCloneChecked)&&(a.nodeType===1||a.nodeType===11)&&!f.isXMLDoc(a)){bk(a,h),d=bl(a),e=bl(h);for(g=0;d[g];++g)e[g]&&bk(d[g],e[g])}if(b){bj(a,h);if(c){d=bl(a),e=bl(h);for(g=0;d[g];++g)bj(d[g],e[g])}}d=e=null;return h},clean:function(a,b,d,e){var g;b=b||c,typeof b.createElement=="undefined"&&(b=b.ownerDocument||b[0]&&b[0].ownerDocument||c);var h=[],i;for(var j=0,k;(k=a[j])!=null;j++){typeof k=="number"&&(k+="");if(!k)continue;if(typeof k=="string")if(!_.test(k))k=b.createTextNode(k);else{k=k.replace(Y,"<$1></$2>");var l=(Z.exec(k)||["",""])[1].toLowerCase(),m=bg[l]||bg._default,n=m[0],o=b.createElement("div");b===c?bh.appendChild(o):U(b).appendChild(o),o.innerHTML=m[1]+k+m[2];while(n--)o=o.lastChild;if(!f.support.tbody){var p=$.test(k),q=l==="table"&&!p?o.firstChild&&o.firstChild.childNodes:m[1]==="<table>"&&!p?o.childNodes:[];for(i=q.length-1;i>=0;--i)f.nodeName(q[i],"tbody")&&!q[i].childNodes.length&&q[i].parentNode.removeChild(q[i])}!f.support.leadingWhitespace&&X.test(k)&&o.insertBefore(b.createTextNode(X.exec(k)[0]),o.firstChild),k=o.childNodes}var r;if(!f.support.appendChecked)if(k[0]&&typeof (r=k.length)=="number")for(i=0;i<r;i++)bn(k[i]);else bn(k);k.nodeType?h.push(k):h=f.merge(h,k)}if(d){g=function(a){return!a.type||be.test(a.type)};for(j=0;h[j];j++)if(e&&f.nodeName(h[j],"script")&&(!h[j].type||h[j].type.toLowerCase()==="text/javascript"))e.push(h[j].parentNode?h[j].parentNode.removeChild(h[j]):h[j]);else{if(h[j].nodeType===1){var s=f.grep(h[j].getElementsByTagName("script"),g);h.splice.apply(h,[j+1,0].concat(s))}d.appendChild(h[j])}}return h},cleanData:function(a){var b,c,d=f.cache,e=f.event.special,g=f.support.deleteExpando;for(var h=0,i;(i=a[h])!=null;h++){if(i.nodeName&&f.noData[i.nodeName.toLowerCase()])continue;c=i[f.expando];if(c){b=d[c];if(b&&b.events){for(var j in b.events)e[j]?f.event.remove(i,j):f.removeEvent(i,j,b.handle);b.handle&&(b.handle.elem=null)}g?delete i[f.expando]:i.removeAttribute&&i.removeAttribute(f.expando),delete d[c]}}}});var bq=/alpha\([^)]*\)/i,br=/opacity=([^)]*)/,bs=/([A-Z]|^ms)/g,bt=/^-?\d+(?:px)?$/i,bu=/^-?\d/,bv=/^([\-+])=([\-+.\de]+)/,bw={position:"absolute",visibility:"hidden",display:"block"},bx=["Left","Right"],by=["Top","Bottom"],bz,bA,bB;f.fn.css=function(a,c){if(arguments.length===2&&c===b)return this;return f.access(this,a,c,!0,function(a,c,d){return d!==b?f.style(a,c,d):f.css(a,c)})},f.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=bz(a,"opacity","opacity");return c===""?"1":c}return a.style.opacity}}},cssNumber:{fillOpacity:!0,fontWeight:!0,lineHeight:!0,opacity:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":f.support.cssFloat?"cssFloat":"styleFloat"},style:function(a,c,d,e){if(!!a&&a.nodeType!==3&&a.nodeType!==8&&!!a.style){var g,h,i=f.camelCase(c),j=a.style,k=f.cssHooks[i];c=f.cssProps[i]||i;if(d===b){if(k&&"get"in k&&(g=k.get(a,!1,e))!==b)return g;return j[c]}h=typeof d,h==="string"&&(g=bv.exec(d))&&(d=+(g[1]+1)*+g[2]+parseFloat(f.css(a,c)),h="number");if(d==null||h==="number"&&isNaN(d))return;h==="number"&&!f.cssNumber[i]&&(d+="px");if(!k||!("set"in k)||(d=k.set(a,d))!==b)try{j[c]=d}catch(l){}}},css:function(a,c,d){var e,g;c=f.camelCase(c),g=f.cssHooks[c],c=f.cssProps[c]||c,c==="cssFloat"&&(c="float");if(g&&"get"in g&&(e=g.get(a,!0,d))!==b)return e;if(bz)return bz(a,c)},swap:function(a,b,c){var d={};for(var e in b)d[e]=a.style[e],a.style[e]=b[e];c.call(a);for(e in b)a.style[e]=d[e]}}),f.curCSS=f.css,f.each(["height","width"],function(a,b){f.cssHooks[b]={get:function(a,c,d){var e;if(c){if(a.offsetWidth!==0)return bC(a,b,d);f.swap(a,bw,function(){e=bC(a,b,d)});return e}},set:function(a,b){if(!bt.test(b))return b;b=parseFloat(b);if(b>=0)return b+"px"}}}),f.support.opacity||(f.cssHooks.opacity={get:function(a,b){return br.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?parseFloat(RegExp.$1)/100+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=f.isNumeric(b)?"alpha(opacity="+b*100+")":"",g=d&&d.filter||c.filter||"";c.zoom=1;if(b>=1&&f.trim(g.replace(bq,""))===""){c.removeAttribute("filter");if(d&&!d.filter)return}c.filter=bq.test(g)?g.replace(bq,e):g+" "+e}}),f(function(){f.support.reliableMarginRight||(f.cssHooks.marginRight={get:function(a,b){var c;f.swap(a,{display:"inline-block"},function(){b?c=bz(a,"margin-right","marginRight"):c=a.style.marginRight});return c}})}),c.defaultView&&c.defaultView.getComputedStyle&&(bA=function(a,b){var c,d,e;b=b.replace(bs,"-$1").toLowerCase(),(d=a.ownerDocument.defaultView)&&(e=d.getComputedStyle(a,null))&&(c=e.getPropertyValue(b),c===""&&!f.contains(a.ownerDocument.documentElement,a)&&(c=f.style(a,b)));return c}),c.documentElement.currentStyle&&(bB=function(a,b){var c,d,e,f=a.currentStyle&&a.currentStyle[b],g=a.style;f===null&&g&&(e=g[b])&&(f=e),!bt.test(f)&&bu.test(f)&&(c=g.left,d=a.runtimeStyle&&a.runtimeStyle.left,d&&(a.runtimeStyle.left=a.currentStyle.left),g.left=b==="fontSize"?"1em":f||0,f=g.pixelLeft+"px",g.left=c,d&&(a.runtimeStyle.left=d));return f===""?"auto":f}),bz=bA||bB,f.expr&&f.expr.filters&&(f.expr.filters.hidden=function(a){var b=a.offsetWidth,c=a.offsetHeight;return b===0&&c===0||!f.support.reliableHiddenOffsets&&(a.style&&a.style.display||f.css(a,"display"))==="none"},f.expr.filters.visible=function(a){return!f.expr.filters.hidden(a)});var bD=/%20/g,bE=/\[\]$/,bF=/\r?\n/g,bG=/#.*$/,bH=/^(.*?):[ \t]*([^\r\n]*)\r?$/mg,bI=/^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,bJ=/^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,bK=/^(?:GET|HEAD)$/,bL=/^\/\//,bM=/\?/,bN=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,bO=/^(?:select|textarea)/i,bP=/\s+/,bQ=/([?&])_=[^&]*/,bR=/^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,bS=f.fn.load,bT={},bU={},bV,bW,bX=["*/"]+["*"];try{bV=e.href}catch(bY){bV=c.createElement("a"),bV.href="",bV=bV.href}bW=bR.exec(bV.toLowerCase())||[],f.fn.extend({load:function(a,c,d){if(typeof a!="string"&&bS)return bS.apply(this,arguments);if(!this.length)return this;var e=a.indexOf(" ");if(e>=0){var g=a.slice(e,a.length);a=a.slice(0,e)}var h="GET";c&&(f.isFunction(c)?(d=c,c=b):typeof c=="object"&&(c=f.param(c,f.ajaxSettings.traditional),h="POST"));var i=this;f.ajax({url:a,type:h,dataType:"html",data:c,complete:function(a,b,c){c=a.responseText,a.isResolved()&&(a.done(function(a){c=a}),i.html(g?f("<div>").append(c.replace(bN,"")).find(g):c)),d&&i.each(d,[c,b,a])}});return this},serialize:function(){return f.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?f.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||bO.test(this.nodeName)||bI.test(this.type))}).map(function(a,b){var c=f(this).val();return c==null?null:f.isArray(c)?f.map(c,function(a,c){return{name:b.name,value:a.replace(bF,"\r\n")}}):{name:b.name,value:c.replace(bF,"\r\n")}}).get()}}),f.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(a,b){f.fn[b]=function(a){return this.on(b,a)}}),f.each(["get","post"],function(a,c){f[c]=function(a,d,e,g){f.isFunction(d)&&(g=g||e,e=d,d=b);return f.ajax({type:c,url:a,data:d,success:e,dataType:g})}}),f.extend({getScript:function(a,c){return f.get(a,b,c,"script")},getJSON:function(a,b,c){return f.get(a,b,c,"json")},ajaxSetup:function(a,b){b?b_(a,f.ajaxSettings):(b=a,a=f.ajaxSettings),b_(a,b);return a},ajaxSettings:{url:bV,isLocal:bJ.test(bW[1]),global:!0,type:"GET",contentType:"application/x-www-form-urlencoded",processData:!0,async:!0,accepts:{xml:"application/xml, text/xml",html:"text/html",text:"text/plain",json:"application/json, text/javascript","*":bX},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText"},converters:{"* text":a.String,"text html":!0,"text json":f.parseJSON,"text xml":f.parseXML},flatOptions:{context:!0,url:!0}},ajaxPrefilter:bZ(bT),ajaxTransport:bZ(bU),ajax:function(a,c){function w(a,c,l,m){if(s!==2){s=2,q&&clearTimeout(q),p=b,n=m||"",v.readyState=a>0?4:0;var o,r,u,w=c,x=l?cb(d,v,l):b,y,z;if(a>=200&&a<300||a===304){if(d.ifModified){if(y=v.getResponseHeader("Last-Modified"))f.lastModified[k]=y;if(z=v.getResponseHeader("Etag"))f.etag[k]=z}if(a===304)w="notmodified",o=!0;else try{r=cc(d,x),w="success",o=!0}catch(A){w="parsererror",u=A}}else{u=w;if(!w||a)w="error",a<0&&(a=0)}v.status=a,v.statusText=""+(c||w),o?h.resolveWith(e,[r,w,v]):h.rejectWith(e,[v,w,u]),v.statusCode(j),j=b,t&&g.trigger("ajax"+(o?"Success":"Error"),[v,d,o?r:u]),i.fireWith(e,[v,w]),t&&(g.trigger("ajaxComplete",[v,d]),--f.active||f.event.trigger("ajaxStop"))}}typeof a=="object"&&(c=a,a=b),c=c||{};var d=f.ajaxSetup({},c),e=d.context||d,g=e!==d&&(e.nodeType||e instanceof f)?f(e):f.event,h=f.Deferred(),i=f.Callbacks("once memory"),j=d.statusCode||{},k,l={},m={},n,o,p,q,r,s=0,t,u,v={readyState:0,setRequestHeader:function(a,b){if(!s){var c=a.toLowerCase();a=m[c]=m[c]||a,l[a]=b}return this},getAllResponseHeaders:function(){return s===2?n:null},getResponseHeader:function(a){var c;if(s===2){if(!o){o={};while(c=bH.exec(n))o[c[1].toLowerCase()]=c[2]}c=o[a.toLowerCase()]}return c===b?null:c},overrideMimeType:function(a){s||(d.mimeType=a);return this},abort:function(a){a=a||"abort",p&&p.abort(a),w(0,a);return this}};h.promise(v),v.success=v.done,v.error=v.fail,v.complete=i.add,v.statusCode=function(a){if(a){var b;if(s<2)for(b in a)j[b]=[j[b],a[b]];else b=a[v.status],v.then(b,b)}return this},d.url=((a||d.url)+"").replace(bG,"").replace(bL,bW[1]+"//"),d.dataTypes=f.trim(d.dataType||"*").toLowerCase().split(bP),d.crossDomain==null&&(r=bR.exec(d.url.toLowerCase()),d.crossDomain=!(!r||r[1]==bW[1]&&r[2]==bW[2]&&(r[3]||(r[1]==="http:"?80:443))==(bW[3]||(bW[1]==="http:"?80:443)))),d.data&&d.processData&&typeof d.data!="string"&&(d.data=f.param(d.data,d.traditional)),b$(bT,d,c,v);if(s===2)return!1;t=d.global,d.type=d.type.toUpperCase(),d.hasContent=!bK.test(d.type),t&&f.active++===0&&f.event.trigger("ajaxStart");if(!d.hasContent){d.data&&(d.url+=(bM.test(d.url)?"&":"?")+d.data,delete d.data),k=d.url;if(d.cache===!1){var x=f.now(),y=d.url.replace(bQ,"$1_="+x);d.url=y+(y===d.url?(bM.test(d.url)?"&":"?")+"_="+x:"")}}(d.data&&d.hasContent&&d.contentType!==!1||c.contentType)&&v.setRequestHeader("Content-Type",d.contentType),d.ifModified&&(k=k||d.url,f.lastModified[k]&&v.setRequestHeader("If-Modified-Since",f.lastModified[k]),f.etag[k]&&v.setRequestHeader("If-None-Match",f.etag[k])),v.setRequestHeader("Accept",d.dataTypes[0]&&d.accepts[d.dataTypes[0]]?d.accepts[d.dataTypes[0]]+(d.dataTypes[0]!=="*"?", "+bX+"; q=0.01":""):d.accepts["*"]);for(u in d.headers)v.setRequestHeader(u,d.headers[u]);if(d.beforeSend&&(d.beforeSend.call(e,v,d)===!1||s===2)){v.abort();return!1}for(u in{success:1,error:1,complete:1})v[u](d[u]);p=b$(bU,d,c,v);if(!p)w(-1,"No Transport");else{v.readyState=1,t&&g.trigger("ajaxSend",[v,d]),d.async&&d.timeout>0&&(q=setTimeout(function(){v.abort("timeout")},d.timeout));try{s=1,p.send(l,w)}catch(z){if(s<2)w(-1,z);else throw z}}return v},param:function(a,c){var d=[],e=function(a,b){b=f.isFunction(b)?b():b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};c===b&&(c=f.ajaxSettings.traditional);if(f.isArray(a)||a.jquery&&!f.isPlainObject(a))f.each(a,function(){e(this.name,this.value)});else for(var g in a)ca(g,a[g],c,e);return d.join("&").replace(bD,"+")}}),f.extend({active:0,lastModified:{},etag:{}});var cd=f.now(),ce=/(\=)\?(&|$)|\?\?/i;f.ajaxSetup({jsonp:"callback",jsonpCallback:function(){return f.expando+"_"+cd++}}),f.ajaxPrefilter("json jsonp",function(b,c,d){var e=b.contentType==="application/x-www-form-urlencoded"&&typeof b.data=="string";if(b.dataTypes[0]==="jsonp"||b.jsonp!==!1&&(ce.test(b.url)||e&&ce.test(b.data))){var g,h=b.jsonpCallback=f.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,i=a[h],j=b.url,k=b.data,l="$1"+h+"$2";b.jsonp!==!1&&(j=j.replace(ce,l),b.url===j&&(e&&(k=k.replace(ce,l)),b.data===k&&(j+=(/\?/.test(j)?"&":"?")+b.jsonp+"="+h))),b.url=j,b.data=k,a[h]=function(a){g=[a]},d.always(function(){a[h]=i,g&&f.isFunction(i)&&a[h](g[0])}),b.converters["script json"]=function(){g||f.error(h+" was not called");return g[0]},b.dataTypes[0]="json";return"script"}}),f.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/javascript|ecmascript/},converters:{"text script":function(a){f.globalEval(a);return a}}}),f.ajaxPrefilter("script",function(a){a.cache===b&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),f.ajaxTransport("script",function(a){if(a.crossDomain){var d,e=c.head||c.getElementsByTagName("head")[0]||c.documentElement;return{send:function(f,g){d=c.createElement("script"),d.async="async",a.scriptCharset&&(d.charset=a.scriptCharset),d.src=a.url,d.onload=d.onreadystatechange=function(a,c){if(c||!d.readyState||/loaded|complete/.test(d.readyState))d.onload=d.onreadystatechange=null,e&&d.parentNode&&e.removeChild(d),d=b,c||g(200,"success")},e.insertBefore(d,e.firstChild)},abort:function(){d&&d.onload(0,1)}}}});var cf=a.ActiveXObject?function(){for(var a in ch)ch[a](0,1)}:!1,cg=0,ch;f.ajaxSettings.xhr=a.ActiveXObject?function(){return!this.isLocal&&ci()||cj()}:ci,function(a){f.extend(f.support,{ajax:!!a,cors:!!a&&"withCredentials"in a})}(f.ajaxSettings.xhr()),f.support.ajax&&f.ajaxTransport(function(c){if(!c.crossDomain||f.support.cors){var d;return{send:function(e,g){var h=c.xhr(),i,j;c.username?h.open(c.type,c.url,c.async,c.username,c.password):h.open(c.type,c.url,c.async);if(c.xhrFields)for(j in c.xhrFields)h[j]=c.xhrFields[j];c.mimeType&&h.overrideMimeType&&h.overrideMimeType(c.mimeType),!c.crossDomain&&!e["X-Requested-With"]&&(e["X-Requested-With"]="XMLHttpRequest");try{for(j in e)h.setRequestHeader(j,e[j])}catch(k){}h.send(c.hasContent&&c.data||null),d=function(a,e){var j,k,l,m,n;try{if(d&&(e||h.readyState===4)){d=b,i&&(h.onreadystatechange=f.noop,cf&&delete ch[i]);if(e)h.readyState!==4&&h.abort();else{j=h.status,l=h.getAllResponseHeaders(),m={},n=h.responseXML,n&&n.documentElement&&(m.xml=n),m.text=h.responseText;try{k=h.statusText}catch(o){k=""}!j&&c.isLocal&&!c.crossDomain?j=m.text?200:404:j===1223&&(j=204)}}}catch(p){e||g(-1,p)}m&&g(j,k,m,l)},!c.async||h.readyState===4?d():(i=++cg,cf&&(ch||(ch={},f(a).unload(cf)),ch[i]=d),h.onreadystatechange=d)},abort:function(){d&&d(0,1)}}}});var ck={},cl,cm,cn=/^(?:toggle|show|hide)$/,co=/^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,cp,cq=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]],cr;f.fn.extend({show:function(a,b,c){var d,e;if(a||a===0)return this.animate(cu("show",3),a,b,c);for(var g=0,h=this.length;g<h;g++)d=this[g],d.style&&(e=d.style.display,!f._data(d,"olddisplay")&&e==="none"&&(e=d.style.display=""),e===""&&f.css(d,"display")==="none"&&f._data(d,"olddisplay",cv(d.nodeName)));for(g=0;g<h;g++){d=this[g];if(d.style){e=d.style.display;if(e===""||e==="none")d.style.display=f._data(d,"olddisplay")||""}}return this},hide:function(a,b,c){if(a||a===0)return this.animate(cu("hide",3),a,b,c);var d,e,g=0,h=this.length;for(;g<h;g++)d=this[g],d.style&&(e=f.css(d,"display"),e!=="none"&&!f._data(d,"olddisplay")&&f._data(d,"olddisplay",e));for(g=0;g<h;g++)this[g].style&&(this[g].style.display="none");return this},_toggle:f.fn.toggle,toggle:function(a,b,c){var d=typeof a=="boolean";f.isFunction(a)&&f.isFunction(b)?this._toggle.apply(this,arguments):a==null||d?this.each(function(){var b=d?a:f(this).is(":hidden");f(this)[b?"show":"hide"]()}):this.animate(cu("toggle",3),a,b,c);return this},fadeTo:function(a,b,c,d){return this.filter(":hidden").css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){function g(){e.queue===!1&&f._mark(this);var b=f.extend({},e),c=this.nodeType===1,d=c&&f(this).is(":hidden"),g,h,i,j,k,l,m,n,o;b.animatedProperties={};for(i in a){g=f.camelCase(i),i!==g&&(a[g]=a[i],delete a[i]),h=a[g],f.isArray(h)?(b.animatedProperties[g]=h[1],h=a[g]=h[0]):b.animatedProperties[g]=b.specialEasing&&b.specialEasing[g]||b.easing||"swing";if(h==="hide"&&d||h==="show"&&!d)return b.complete.call(this);c&&(g==="height"||g==="width")&&(b.overflow=[this.style.overflow,this.style.overflowX,this.style.overflowY],f.css(this,"display")==="inline"&&f.css(this,"float")==="none"&&(!f.support.inlineBlockNeedsLayout||cv(this.nodeName)==="inline"?this.style.display="inline-block":this.style.zoom=1))}b.overflow!=null&&(this.style.overflow="hidden");for(i in a)j=new f.fx(this,b,i),h=a[i],cn.test(h)?(o=f._data(this,"toggle"+i)||(h==="toggle"?d?"show":"hide":0),o?(f._data(this,"toggle"+i,o==="show"?"hide":"show"),j[o]()):j[h]()):(k=co.exec(h),l=j.cur(),k?(m=parseFloat(k[2]),n=k[3]||(f.cssNumber[i]?"":"px"),n!=="px"&&(f.style(this,i,(m||1)+n),l=(m||1)/j.cur()*l,f.style(this,i,l+n)),k[1]&&(m=(k[1]==="-="?-1:1)*m+l),j.custom(l,m,n)):j.custom(l,h,""));return!0}var e=f.speed(b,c,d);if(f.isEmptyObject(a))return this.each(e.complete,[!1]);a=f.extend({},a);return e.queue===!1?this.each(g):this.queue(e.queue,g)},stop:function(a,c,d){typeof a!="string"&&(d=c,c=a,a=b),c&&a!==!1&&this.queue(a||"fx",[]);return this.each(function(){function h(a,b,c){var e=b[c];f.removeData(a,c,!0),e.stop(d)}var b,c=!1,e=f.timers,g=f._data(this);d||f._unmark(!0,this);if(a==null)for(b in g)g[b]&&g[b].stop&&b.indexOf(".run")===b.length-4&&h(this,g,b);else g[b=a+".run"]&&g[b].stop&&h(this,g,b);for(b=e.length;b--;)e[b].elem===this&&(a==null||e[b].queue===a)&&(d?e[b](!0):e[b].saveState(),c=!0,e.splice(b,1));(!d||!c)&&f.dequeue(this,a)})}}),f.each({slideDown:cu("show",1),slideUp:cu("hide",1),slideToggle:cu("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){f.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),f.extend({speed:function(a,b,c){var d=a&&typeof a=="object"?f.extend({},a):{complete:c||!c&&b||f.isFunction(a)&&a,duration:a,easing:c&&b||b&&!f.isFunction(b)&&b};d.duration=f.fx.off?0:typeof d.duration=="number"?d.duration:d.duration in f.fx.speeds?f.fx.speeds[d.duration]:f.fx.speeds._default;if(d.queue==null||d.queue===!0)d.queue="fx";d.old=d.complete,d.complete=function(a){f.isFunction(d.old)&&d.old.call(this),d.queue?f.dequeue(this,d.queue):a!==!1&&f._unmark(this)};return d},easing:{linear:function(a,b,c,d){return c+d*a},swing:function(a,b,c,d){return(-Math.cos(a*Math.PI)/2+.5)*d+c}},timers:[],fx:function(a,b,c){this.options=b,this.elem=a,this.prop=c,b.orig=b.orig||{}}}),f.fx.prototype={update:function(){this.options.step&&this.options.step.call(this.elem,this.now,this),(f.fx.step[this.prop]||f.fx.step._default)(this)},cur:function(){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null))return this.elem[this.prop];var a,b=f.css(this.elem,this.prop);return isNaN(a=parseFloat(b))?!b||b==="auto"?0:b:a},custom:function(a,c,d){function h(a){return e.step(a)}var e=this,g=f.fx;this.startTime=cr||cs(),this.end=c,this.now=this.start=a,this.pos=this.state=0,this.unit=d||this.unit||(f.cssNumber[this.prop]?"":"px"),h.queue=this.options.queue,h.elem=this.elem,h.saveState=function(){e.options.hide&&f._data(e.elem,"fxshow"+e.prop)===b&&f._data(e.elem,"fxshow"+e.prop,e.start)},h()&&f.timers.push(h)&&!cp&&(cp=setInterval(g.tick,g.interval))},show:function(){var a=f._data(this.elem,"fxshow"+this.prop);this.options.orig[this.prop]=a||f.style(this.elem,this.prop),this.options.show=!0,a!==b?this.custom(this.cur(),a):this.custom(this.prop==="width"||this.prop==="height"?1:0,this.cur()),f(this.elem).show()},hide:function(){this.options.orig[this.prop]=f._data(this.elem,"fxshow"+this.prop)||f.style(this.elem,this.prop),this.options.hide=!0,this.custom(this.cur(),0)},step:function(a){var b,c,d,e=cr||cs(),g=!0,h=this.elem,i=this.options;if(a||e>=i.duration+this.startTime){this.now=this.end,this.pos=this.state=1,this.update(),i.animatedProperties[this.prop]=!0;for(b in i.animatedProperties)i.animatedProperties[b]!==!0&&(g=!1);if(g){i.overflow!=null&&!f.support.shrinkWrapBlocks&&f.each(["","X","Y"],function(a,b){h.style["overflow"+b]=i.overflow[a]}),i.hide&&f(h).hide();if(i.hide||i.show)for(b in i.animatedProperties)f.style(h,b,i.orig[b]),f.removeData(h,"fxshow"+b,!0),f.removeData(h,"toggle"+b,!0);d=i.complete,d&&(i.complete=!1,d.call(h))}return!1}i.duration==Infinity?this.now=e:(c=e-this.startTime,this.state=c/i.duration,this.pos=f.easing[i.animatedProperties[this.prop]](this.state,c,0,1,i.duration),this.now=this.start+(this.end-this.start)*this.pos),this.update();return!0}},f.extend(f.fx,{tick:function(){var a,b=f.timers,c=0;for(;c<b.length;c++)a=b[c],!a()&&b[c]===a&&b.splice(c--,1);b.length||f.fx.stop()},interval:13,stop:function(){clearInterval(cp),cp=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(a){f.style(a.elem,"opacity",a.now)},_default:function(a){a.elem.style&&a.elem.style[a.prop]!=null?a.elem.style[a.prop]=a.now+a.unit:a.elem[a.prop]=a.now}}}),f.each(["width","height"],function(a,b){f.fx.step[b]=function(a){f.style(a.elem,b,Math.max(0,a.now)+a.unit)}}),f.expr&&f.expr.filters&&(f.expr.filters.animated=function(a){return f.grep(f.timers,function(b){return a===b.elem}).length});var cw=/^t(?:able|d|h)$/i,cx=/^(?:body|html)$/i;"getBoundingClientRect"in c.documentElement?f.fn.offset=function(a){var b=this[0],c;if(a)return this.each(function(b){f.offset.setOffset(this,a,b)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return f.offset.bodyOffset(b);try{c=b.getBoundingClientRect()}catch(d){}var e=b.ownerDocument,g=e.documentElement;if(!c||!f.contains(g,b))return c?{top:c.top,left:c.left}:{top:0,left:0};var h=e.body,i=cy(e),j=g.clientTop||h.clientTop||0,k=g.clientLeft||h.clientLeft||0,l=i.pageYOffset||f.support.boxModel&&g.scrollTop||h.scrollTop,m=i.pageXOffset||f.support.boxModel&&g.scrollLeft||h.scrollLeft,n=c.top+l-j,o=c.left+m-k;return{top:n,left:o}}:f.fn.offset=function(a){var b=this[0];if(a)return this.each(function(b){f.offset.setOffset(this,a,b)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return f.offset.bodyOffset(b);var c,d=b.offsetParent,e=b,g=b.ownerDocument,h=g.documentElement,i=g.body,j=g.defaultView,k=j?j.getComputedStyle(b,null):b.currentStyle,l=b.offsetTop,m=b.offsetLeft;while((b=b.parentNode)&&b!==i&&b!==h){if(f.support.fixedPosition&&k.position==="fixed")break;c=j?j.getComputedStyle(b,null):b.currentStyle,l-=b.scrollTop,m-=b.scrollLeft,b===d&&(l+=b.offsetTop,m+=b.offsetLeft,f.support.doesNotAddBorder&&(!f.support.doesAddBorderForTableAndCells||!cw.test(b.nodeName))&&(l+=parseFloat(c.borderTopWidth)||0,m+=parseFloat(c.borderLeftWidth)||0),e=d,d=b.offsetParent),f.support.subtractsBorderForOverflowNotVisible&&c.overflow!=="visible"&&(l+=parseFloat(c.borderTopWidth)||0,m+=parseFloat(c.borderLeftWidth)||0),k=c}if(k.position==="relative"||k.position==="static")l+=i.offsetTop,m+=i.offsetLeft;f.support.fixedPosition&&k.position==="fixed"&&(l+=Math.max(h.scrollTop,i.scrollTop),m+=Math.max(h.scrollLeft,i.scrollLeft));return{top:l,left:m}},f.offset={bodyOffset:function(a){var b=a.offsetTop,c=a.offsetLeft;f.support.doesNotIncludeMarginInBodyOffset&&(b+=parseFloat(f.css(a,"marginTop"))||0,c+=parseFloat(f.css(a,"marginLeft"))||0);return{top:b,left:c}},setOffset:function(a,b,c){var d=f.css(a,"position");d==="static"&&(a.style.position="relative");var e=f(a),g=e.offset(),h=f.css(a,"top"),i=f.css(a,"left"),j=(d==="absolute"||d==="fixed")&&f.inArray("auto",[h,i])>-1,k={},l={},m,n;j?(l=e.position(),m=l.top,n=l.left):(m=parseFloat(h)||0,n=parseFloat(i)||0),f.isFunction(b)&&(b=b.call(a,c,g)),b.top!=null&&(k.top=b.top-g.top+m),b.left!=null&&(k.left=b.left-g.left+n),"using"in b?b.using.call(a,k):e.css(k)}},f.fn.extend({position:function(){if(!this[0])return null;var a=this[0],b=this.offsetParent(),c=this.offset(),d=cx.test(b[0].nodeName)?{top:0,left:0}:b.offset();c.top-=parseFloat(f.css(a,"marginTop"))||0,c.left-=parseFloat(f.css(a,"marginLeft"))||0,d.top+=parseFloat(f.css(b[0],"borderTopWidth"))||0,d.left+=parseFloat(f.css(b[0],"borderLeftWidth"))||0;return{top:c.top-d.top,left:c.left-d.left}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||c.body;while(a&&!cx.test(a.nodeName)&&f.css(a,"position")==="static")a=a.offsetParent;return a})}}),f.each(["Left","Top"],function(a,c){var d="scroll"+c;f.fn[d]=function(c){var e,g;if(c===b){e=this[0];if(!e)return null;g=cy(e);return g?"pageXOffset"in g?g[a?"pageYOffset":"pageXOffset"]:f.support.boxModel&&g.document.documentElement[d]||g.document.body[d]:e[d]}return this.each(function(){g=cy(this),g?g.scrollTo(a?f(g).scrollLeft():c,a?c:f(g).scrollTop()):this[d]=c})}}),f.each(["Height","Width"],function(a,c){var d=c.toLowerCase();f.fn["inner"+c]=function(){var a=this[0];return a?a.style?parseFloat(f.css(a,d,"padding")):this[d]():null},f.fn["outer"+c]=function(a){var b=this[0];return b?b.style?parseFloat(f.css(b,d,a?"margin":"border")):this[d]():null},f.fn[d]=function(a){var e=this[0];if(!e)return a==null?null:this;if(f.isFunction(a))return this.each(function(b){var c=f(this);c[d](a.call(this,b,c[d]()))});if(f.isWindow(e)){var g=e.document.documentElement["client"+c],h=e.document.body;return e.document.compatMode==="CSS1Compat"&&g||h&&h["client"+c]||g}if(e.nodeType===9)return Math.max(e.documentElement["client"+c],e.body["scroll"+c],e.documentElement["scroll"+c],e.body["offset"+c],e.documentElement["offset"+c]);if(a===b){var i=f.css(e,d),j=parseFloat(i);return f.isNumeric(j)?j:i}return this.css(d,typeof a=="string"?a:a+"px")}}),a.jQuery=a.$=f,typeof define=="function"&&define.amd&&define.amd.jQuery&&define("jquery",[],function(){return f})})(window);
if(!window._ate){var _atd="www.addthis.com/",_atr="//s7.addthis.com/",_atn="//l.addthiscdn.com/",_euc=encodeURIComponent,_duc=decodeURIComponent,_atc={dr:0,ver:250,loc:0,enote:"",cwait:500,bamp:0.25,camp:1,damp:1,famp:0.02,pamp:0.2,tamp:1,lamp:0.01,vamp:1,vrmp:0.0001,ltj:1,xamp:0.5,abf:!!window.addthis_do_ab};(function(){var l;try{l=window.location;if(l.protocol.indexOf("file")===0||l.protocol.indexOf("safari-extension")===0||l.protocol.indexOf("chrome-extension")===0){_atr="http:"+_atr;}if(l.hostname.indexOf("localhost")!=-1){_atc.loc=1;}}catch(e){}var ua=navigator.userAgent.toLowerCase(),d=document,w=window,dl=d.location,b={win:/windows/.test(ua),xp:(/windows nt 5.1/.test(ua))||(/windows nt 5.2/.test(ua)),osx:/os x/.test(ua),chr:/chrome/.test(ua),iph:/iphone/.test(ua),dro:/android/.test(ua),ipa:/ipad/.test(ua),saf:/safari/.test(ua)&&!(/chrome/.test(ua)),opr:/opera/.test(ua),msi:(/msie/.test(ua))&&!(/opera/.test(ua)),ffx:/firefox/.test(ua),ff2:/firefox\/2/.test(ua),ffn:/firefox\/((3.[6789][0-9a-z]*)|(4.[0-9a-z]*))/.test(ua),ie6:/msie 6.0/.test(ua),ie7:/msie 7.0/.test(ua),ie8:/msie 8.0/.test(ua),ie9:/msie 9.0/.test(ua),mod:-1},_7={rev:"103325",bro:b,wlp:(l||{}).protocol,dl:dl,upm:!!w.postMessage&&(""+w.postMessage).toLowerCase().indexOf("[native code]")!==-1,bamp:_atc.bamp-Math.random(),camp:_atc.camp-Math.random(),xamp:_atc.xamp-Math.random(),vamp:_atc.vamp-Math.random(),tamp:_atc.tamp-Math.random(),pamp:_atc.pamp-Math.random(),ab:"-",inst:1,wait:500,tmo:null,sub:!!window.at_sub,dbm:0,uid:null,spt:"static/r07/widget33.png",api:{},imgz:[],hash:window.location.hash};d.ce=d.createElement;d.gn=d.getElementsByTagName;window._ate=_7;_7.evl=function(_8,_9){if(_9){var _a;eval("evl = "+_8);return _a;}else{return eval(_8);}};var _b=function(o,fn,_e,_f){if(!o){return _e;}if(o instanceof Array||(o.length&&(typeof o!=="function"))){for(var i=0,len=o.length,v=o[0];i<len;v=o[++i]){_e=fn.call(_f||o,_e,v,i,o);}}else{for(var _13 in o){_e=fn.call(_f||o,_e,o[_13],_13,o);}}return _e;},_14=Array.prototype.slice,_15=function(a){return _14.apply(a,_14.call(arguments,1));},_17=function(s){return(""+s).replace(/(^\s+|\s+$)/g,"");},_19=function(A,B){return _b(_15(arguments,1),function(A,_1d){return _b(_1d,function(o,v,k){if(o){o[k]=v;}return o;},A);},A);},_21=function(o,del){return _b(o,function(acc,v,k){k=_17(k);if(k){acc.push(_euc(k)+"="+_euc(_17((typeof(v)=="object"?_21(v,(del||"&")):(v)))));}return acc;},[]).join(del||"&");},_27=function(o,del){return _b(o,function(acc,v,k){k=_17(k);if(k){acc.push(_euc(k)+"="+_euc(_17(v)));}return acc;},[]).join(del||"&");},_2d=function(q,del){return _b((q||"").split(del||"&"),function(acc,_31){try{var kv=_31.split("="),k=_17(_duc(kv[0])),v=_17(_duc(kv.slice(1).join("=")));if(v.indexOf(del||"&")>-1||v.indexOf("=")>-1){v=_2d(v,del||"&");}if(k){acc[k]=v;}}catch(e){}return acc;},{});},_35=function(q,del){return _b((q||"").split(del||"&"),function(acc,_39){try{var kv=_39.split("="),k=_17(_duc(kv[0])),v=_17(_duc(kv.slice(1).join("=")));if(k){acc[k]=v;}}catch(e){}return acc;},{});},_3d=function(){var _3e=_15(arguments,0),fn=_3e.shift(),_40=_3e.shift();return function(){return fn.apply(_40,_3e.concat(_15(arguments,0)));};},_41=function(un,obj,evt,fn){if(!obj){return;}if(we){obj[(un?"detach":"attach")+"Event"]("on"+evt,fn);}else{obj[(un?"remove":"add")+"EventListener"](evt,fn,false);}},_46=function(obj,evt,fn){_41(0,obj,evt,fn);},_4a=function(obj,evt,fn){_41(1,obj,evt,fn);},_4e=function(s){return(s.match(/(([^\/\/]*)\/\/|\/\/)?([^\/\?\&\#]+)/i))[0];},_50=function(s){return s.replace(_4e(s),"");},_52={reduce:_b,slice:_15,strip:_17,extend:_19,toKV:_27,rtoKV:_21,fromKV:_35,rfromKV:_2d,bind:_3d,listen:_46,unlisten:_4a,gUD:_4e,gUQS:_50};_7.util=_52;_19(_7,_52);(function(i,k,l){var g,n=i.util;function j(q,p,s,o,r){this.type=q;this.triggerType=p||q;this.target=s||o;this.triggerTarget=o||s;this.data=r||{};}n.extend(j.prototype,{constructor:j,bubbles:false,preventDefault:n.noop,stopPropagation:n.noop,clone:function(){return new this.constructor(this.type,this.triggerType,this.target,this.triggerTarget,n.extend({},this.data));}});function e(o,p){this.target=o;this.queues={};this.defaultEventType=p||j;}function a(o){var p=this.queues;if(!p[o]){p[o]=[];}return p[o];}function h(o,p){this.getQueue(o).push(p);}function d(p,r){var s=this.getQueue(p),o=s.indexOf(r);if(o!==-1){s.splice(o,1);}}function b(o,s,r,q){var p=this;if(!q){setTimeout(function(){p.dispatchEvent(new p.defaultEventType(o,o,s,p.target,r));},10);}else{p.dispatchEvent(new p.defaultEventType(o,o,s,p.target,r));}}function m(p){for(var r=0,t=p.target,s=this.getQueue(p.type),o=s.length;r<o;r++){s[r].call(t,p.clone());}}function c(p){if(!p){return;}for(var o in f){p[o]=n.bind(f[o],this);}return p;}var f={constructor:e,getQueue:a,addEventListener:h,removeEventListener:d,dispatchEvent:m,fire:b,decorate:c};n.extend(e.prototype,f);i.event={PolyEvent:j,EventDispatcher:e};})(_7,_7.api,_7);_7.ed=new _7.event.EventDispatcher(_7);var _75={isBound:0,isReady:0,readyList:[],onReady:function(){if(!_75.isReady){_75.isReady=1;var l=_75.readyList.concat(window.addthis_onload||[]);for(var fn=0;fn<l.length;fn++){l[fn].call(window);}_75.readyList=[];}},addLoad:function(_78){var o=w.onload;if(typeof w.onload!="function"){w.onload=_78;}else{w.onload=function(){if(o){o();}_78();};}},bindReady:function(){if(r.isBound||_atc.xol){return;}r.isBound=1;if(d.addEventListener&&!b.opr){d.addEventListener("DOMContentLoaded",r.onReady,false);}var apc=window.addthis_product;if(apc&&apc.indexOf("f")>-1){r.onReady();return;}if(b.msi&&!b.ie9&&window==top){(function(){if(r.isReady){return;}try{d.documentElement.doScroll("left");}catch(error){setTimeout(arguments.callee,0);return;}r.onReady();})();}if(b.opr){d.addEventListener("DOMContentLoaded",function(){if(r.isReady){return;}for(var i=0;i<d.styleSheets.length;i++){if(d.styleSheets[i].disabled){setTimeout(arguments.callee,0);return;}}r.onReady();},false);}if(b.saf){var _7c;(function(){if(r.isReady){return;}if(d.readyState!="loaded"&&d.readyState!="complete"){setTimeout(arguments.callee,0);return;}if(_7c===undefined){var _7e=d.gn("link");for(var i=0;i<_7e.length;i++){if(_7e[i].getAttribute("rel")=="stylesheet"){_7c++;}}var _80=d.gn("style");_7c+=_80.length;}if(d.styleSheets.length!=_7c){setTimeout(arguments.callee,0);return;}r.onReady();})();}r.addLoad(r.onReady);},append:function(fn,_82){r.bindReady();if(r.isReady){fn.call(window,[]);}else{r.readyList.push(function(){return fn.call(window,[]);});}}},r=_75,a=_7;_19(_7,{plo:[],lad:function(x){_7.plo.push(x);}});(function(c,e,d){var a=window;c.pub=function(){return _euc((window.addthis_config||{}).pubid||(window.addthis_config||{}).username||window.addthis_pub||"");};c.usu=function(g,h){if(!a.addthis_share){a.addthis_share={};}if(h||g!=addthis_share.url){addthis_share.imp_url=0;}};c.rsu=function(){var h=document,g=h.title,f=h.location?h.location.href:"";if(_atc.ver>=250&&addthis_share.imp_url&&f&&f!=a.addthis_share.url&&!(_7.util.ivc((h.location.hash||"").substr(1).split(",").shift()))){a.addthis_share.url=a.addthis_url=f;a.addthis_share.title=a.addthis_title=g;return 1;}return 0;};c.igv=function(f,g){if(!a.addthis_config){a.addthis_config={username:a.addthis_pub};}else{if(addthis_config.data_use_cookies===false){_atc.xck=1;}}if(!a.addthis_share){a.addthis_share={};}if(!addthis_share.url){if(!a.addthis_url&&addthis_share.imp_url===undefined){addthis_share.imp_url=1;}addthis_share.url=(a.addthis_url||f||"").split("#{").shift();}if(!addthis_share.title){addthis_share.title=(a.addthis_title||g||"").split("#{").shift();}};if(!_atc.ost){if(!a.addthis_conf){a.addthis_conf={};}for(var b in addthis_conf){_atc[b]=addthis_conf[b];}_atc.ost=1;}})(_7,_7.api,_7);(function(b,f,c){var h,g=document,a=b.util;b.ckv=a.fromKV(g.cookie,";");function e(d){return a.fromKV(g.cookie,";")[d];}if(!b.cookie){b.cookie={};}b.cookie.rck=e;})(_7,_7.api,_7);(function(b,c,e){var a,h=document,g=0,m=b.util;function j(){if(g){return 1;}k("xtc",1);if(1==b.cookie.rck("xtc")){g=1;}f("xtc",1);return g;}function l(o){if(_atc.xck){return;}var n=o||_7.dh||_7.du||(_7.dl?_7.dl.hostname:"");if(n.indexOf(".gov")>-1||n.indexOf(".mil")>-1){_atc.xck=1;}var q=typeof(b.pub)==="function"?b.pub():b.pub,d=["usarmymedia","govdelivery"];for(i in d){if(q==d[i]){_atc.xck=1;break;}}}function f(n,d){if(h.cookie){h.cookie=n+"=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/"+(d?"; domain="+(b.bro.msi?"":".")+"addthis.com":"");}}function k(o,n,p,q,d){l();if(!_atc.xck){if(!d){var d=new Date();d.setYear(d.getFullYear()+2);}document.cookie=o+"="+n+(!p?"; expires="+d.toUTCString():"")+"; path=/;"+(!q?" domain="+(b.bro.msi?"":".")+"addthis.com":"");}}if(!b.cookie){b.cookie={};}b.cookie.sck=k;b.cookie.kck=f;b.cookie.cww=j;b.cookie.gov=l;})(_7,_7.api,_7);(function(c,f,d){var b=c.util,a={};if(!c.cbs){c.cbs={};}function e(h,g,k,i){var j=h+"_"+(_euc(g)).replace(/[0-3][A-Z]|[^a-zA-Z0-9]/g,"")+Math.floor(Math.random()*100);if(!_7.cbs[j]){_7.cbs[j]=function(){if(a[j]){clearTimeout(a[j]);}k.apply(this,arguments);};}_7.cbs["time_"+j]=(new Date()).getTime();if(i){clearTimeout(a[j]);a[j]=setTimeout(i,10000);}return"_ate.cbs."+_euc(j);}b.scb=e;})(_7,_7.api,_7);(function(b,d,c){function e(){var k=a(navigator.userAgent,16),f=((new Date()).getTimezoneOffset())+""+navigator.javaEnabled()+(navigator.userLanguage||navigator.language),h=window.screen.colorDepth+""+window.screen.width+window.screen.height+window.screen.availWidth+window.screen.availHeight,g=navigator.plugins,l=g.length;if(l>0){for(var j=0;j<Math.min(10,l);j++){if(j<5){f+=g[j].name+g[j].description;}else{h+=g[j].name+g[j].description;}}}return k.substr(0,2)+a(f,16).substr(0,3)+a(h,16).substr(0,3);}function a(h,j){var f=291;if(h){for(var g=0;g<h.length;g++){f=(f*(h.charCodeAt(g)+g)+3)&1048575;}}return(f&16777215).toString(j||32);}b.mun=a;b.gub=e;})(_7,_7.api,_7);(function(d,e,g){var c,l=d.util,j=4294967295,b=new Date().getTime();function h(){return((b/1000)&j).toString(16)+("00000000"+(Math.floor(Math.random()*(j+1))).toString(16)).slice(-8);}function a(m){return k(m)?(new Date((parseInt(m.substr(0,8),16)*1000))):new Date();}function i(m){var n=a();return((n.getTime()-1000*86400)>(new Date()).getTime());}function f(m,o){var n=a(m);return(((new Date()).getTime()-n.getTime())>o*1000);}function k(m){return m&&m.match(/^[0-9a-f]{16}$/)&&!i(m);}l.cuid=h;l.ivc=k;l.ioc=f;})(_7,_7.api,_7);(function(a,d,c){function e(g,f){var h=g.indexOf("#")>-1&&!f?g.replace(/^[^\#]+\#?|^\#?/,""):g.replace(/^[^\?]+\??|^\??/,""),i=a.util.fromKV(h);return i;}function b(k){var g=document.gn("script"),l=g.length,h=g[l-1],j=e(h.src);if(k||(h.src&&h.src.indexOf("addthis")==-1)){for(var f=0;f<l;f++){if((g[f].src||"").indexOf(k||"addthis.com")>-1){j=e(g[f].src);break;}}}return j;}if(!a.util){a.util={};}a.util.gsp=b;a.util.ghp=e;})(_7,_7.api,_7);(function(e,g,f){var d=e.util,b="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_=";function a(k){var j="",n,l,h,p,o,m=0;if(/[0-9a-fA-F]+/.test(k)){while(m<k.length){n=parseInt(k.charAt(m++),16);l=parseInt(k.charAt(m++),16);h=parseInt(k.charAt(m++),16);p=(n<<2)|(isNaN(h)?l&3:(l>>2));o=((l&3)<<4)|h;j+=b.charAt(p)+(isNaN(h)?"":b.charAt(o));}}else{}return j;}function c(k){var j="",n,l,h,p,o,m=0;while(m<k.length){p=b.indexOf(k.charAt(m++));o=m>=k.length?NaN:b.indexOf(k.charAt(m++));n=p>>2;l=isNaN(o)?(p&3):(((p&3)<<2)|(o>>4));h=o&15;j+=n.toString(16)+l.toString(16)+(isNaN(o)?"":h.toString(16));}return j;}d.hbtoa=a;d.atohb=c;})(_7,_7.api,_7);(function(f,s,u){var v=f,j=new Date().getTime(),r=function(){return Math.floor(Math.random()*4294967295).toString(36);},w=function(){return Math.floor((new Date().getTime()-j)/100).toString(16);},g=0,i=function(a){if(g===0){v.sid=g=(a||v.util.cuid());}return g;},d=null,c=function(a,x){if(d!==null){clearTimeout(d);}if(a){d=setTimeout(function(){x(false);},_7.wait);}},o=function(x,a){return _euc(x)+"="+_euc(a)+";"+w();},n=1,h=function(x,z){var a=(x||"").split("?"),x=a.shift(),y=(a.pop()||"").split("&");return z(x,y);},k=function(a,x,z,y){if(!x){x={};}if(!x.remove){x.remove=[];}if(x.remove.push){x.remove.push("sms_ss");x.remove.push("at_xt");x.remove.push("fb_ref");x.remove.push("fb_source");}if(x.remove){a=t(a,x.remove);}if(x.clean){a=l(a);}if(x.defrag){a=e(a);}if(x.add){a=m(a,x.add,z,y);}return a;},m=function(z,B,A,x){var a={};if(B){for(var y in B){if(z.indexOf(y+"=")>-1){continue;}a[y]=p(B[y],z,A,x);}B=_7.util.toKV(a);}return z+(B.length?((z.indexOf("?")>-1?"&":"?")+B):"");},p=function(y,x,z,a){var z=z||addthis_share;return y.replace(/{{service}}/g,_euc(a||"")).replace(/{{code}}/g,_euc(a||"")).replace(/{{title}}/g,_euc(z.title)).replace(/{{url}}/g,_euc(x));},t=function(x,z){var a={},z=z||[];for(var y=0;y<z.length;y++){a[z[y]]=1;}return h(x,function(A,D){var E=[];if(D){for(var B in D){if(typeof(D[B])=="string"){var C=(D[B]||"").split("=");if(C.length!=2&&D[B]){E.push(D[B]);}else{if(a[C[0]]){continue;}else{if(D[B]){E.push(D[B]);}}}}}A+=(E.length?("?"+E.join("&")):"");}return A;});},q=function(a){var x=a.split("#").pop().split(",").shift().split("=").pop();if(_7.util.ivc(x)){return a.split("#").pop().split(",");}return[""];},e=function(a){var x=q(a).shift().split("=").pop();if(_7.util.ivc(x)){return a.split("#").shift();}else{x=a.split("#").slice(1).join("#").split(";").shift();if(x.split(".").length==3){x=x.split(".").slice(0,-1).join(".");}if(x.length==12&&x.substr(0,1)=="."&&/[a-zA-Z0-9\-_]{11}/.test(x.substr(1))){return a.split("#").shift();}}return a;},l=function(a){return h(a,function(y,B){var x=y.indexOf(";jsessionid"),C=[];if(x>-1){y=y.substr(0,x);}if(B){for(var z in B){if(typeof(B[z])=="string"){var A=(B[z]||"").split("=");if(A.length==2){if(A[0].indexOf("utm_")===0||A[0]=="gclid"||A[0]=="sms_ss"||A[0]=="at_xt"||A[0]=="fb_ref"||A[0]=="fb_source"){continue;}}if(B[z]){C.push(B[z]);}}}y+=(C.length?("?"+C.join("&")):"");}return y;});},b=function(){var a=(typeof(v.pub||"")=="function"?v.pub():v.pub)||"unknown";return"AT-"+a+"/-/"+v.ab+"/"+i()+"/"+(n++)+(v.uid!==null?"/"+v.uid:"");};if(!_7.track){_7.track={};}f.util.extend(_7.track,{fcv:o,ran:r,rup:t,aup:m,cof:e,gof:q,clu:l,mgu:k,ssid:i,sta:b,sxm:c});})(_7,_7.api,_7);(function(c,e,i){var m=".",h=";",r=".",l=m.length,k=0,p={wpp:1,blg:1};function b(t){var u=t.split(";").shift();if(u.split(".").length==3){u=u.split(".").slice(0,-1).join(".");}if(u.length==12&&u.substr(0,1)=="."&&/[a-zA-Z0-9\-_]{11}/.test(u.substr(1))){return 1;}return 0;}function q(t){return(t.length==(11+l)&&(t.substr(0,l)==m)&&/[a-zA-Z0-9\-_]{11}/.test(t.substr(l)));}function n(u){var t=_7.util.atohb(u.substr(l));return{id:(t.substr(0,8)+"00000000,"+parseInt(t.substr(16),10)),fuid:t.substr(8,8)};}function g(I,G){if(!I){I=document.location;}if(!G){G=d.referer||d.referrer||"";}var H,N,L,u,C,w=0,x=0,E=I?I.href:"",z=(E||"").split("#").shift(),t=I.hash.substr(1),D=_7.util.ghp(I.search,1),F=_7.util.ghp(I.hash);x=0,at_st=F.at_st,u=D.sms_ss,fb_ref=D.fb_ref,at_xt=D.at_xt,q_at_st=D.at_st;if(!at_st){if(q(t)){var O=_7.util.atohb(t.substr(l));C=O.substr(8,8);at_st=O.substr(0,8)+"00000000,";at_st+=parseInt(O.substr(16),10);}}if(fb_ref&&!at_st){var K=r,A=fb_ref.split(K);if(A.length<2&&fb_ref.indexOf("_")>-1){K="_";A=fb_ref.split(K);}var v=A.length>1?A.pop():"",s=A.join(K);if(!q(s)){s=fb_ref;v="";}if(q(s)){var O=_7.util.atohb(s.substr(l));at_xt=O.substr(0,16)+","+parseInt(O.substr(16),10);u="facebook_"+(v||"like");}else{var M=fb_ref.split("=").pop().split(r);if(M.length==2&&_7.util.ivc(M[0])){at_xt=M.join(",");u="facebook_"+(v||"like");}}}at_st=(at_st&&_7.util.ivc(at_st.split(",").shift()))?at_st:"";if(!at_xt){var K=(t.indexOf(h)>-1)?h:r,y=t.substr(l).split(K);if(y.length==2&&q(t.substr(0,1)+y[0])){var O=_7.util.atohb(y[0]);at_xt=O.substr(0,16)+","+parseInt(O.substr(16),10);u=y[1];w=1;}}if(at_st){x=parseInt(at_st.split(",").pop())+1;N=at_st.split(",").shift();}else{if(E.indexOf(_atd+"book")==-1&&z!=G){var B=[],J;if(at_xt){J=at_xt.split(",");H=_duc(J.shift());if(H.indexOf(",")>-1){J=H.split(",");H=J.shift();}}else{if(q_at_st){J=q_at_st.split(",");L=_duc(J.shift());if(L.indexOf(",")>-1){J=L.split(",");L=J.shift();}}}if(J&&J.length){x=Math.min(3,parseInt(J.pop())+1);}}}if(!_7.util.ivc(N)){N=null;}if(!_7.util.ivc(L)){L=null;}u=(u||"").split("#").shift().split("?").shift();return{rsi:N,hash:w,rsiq:L,fuid:C,rxi:H,rsc:u,gen:x};}function f(u,s){if(!s||(s.data_track_clickback!==false&&s.data_track_linkback!==false)){if(k){return true;}if(_atc.ver>=250){return(k=true);}u=(u||window.addthis_product||"").split(",");for(var t=0;t<u.length;t++){if(p[u[t].split("-").shift()]){return(k=true);}}}return false;}function j(s,t){s=s||a.util.cuid();return m+_7.util.hbtoa(s+Math.min(3,t||0));}function o(t,u,s){s=s||a.util.cuid();return t.indexOf("#")>-1?t:t+"#"+j((u?s:s.substr(0,8)+_7.gub()),(a.smd||{}).gen)+(u?r+u:"");}_7.extend(_7.track,{cur:o,gcc:j,cpf:m,ctp:f,eop:g,ich:b});})(_7,_7.api,_7);(function(){var d=document,a=_7,cvt=[],avt=null,qtp=[],xtp=function(){var p;while(p=qtp.pop()){trk(p);}},pcs=[],spc=null,apc=function(c){c=c.split("-").shift();for(var i=0;i<pcs.length;i++){if(pcs[i]==c){return;}}pcs.push(c);},gat=function(){},atf=null,_15b=function(){var div=d.getElementById("_atssh");if(!div){div=d.ce("div");div.style.visibility="hidden";div.id="_atssh";a.opp(div.style);d.body.insertBefore(div,d.body.firstChild);}return div;},ctf=function(url){var ifr,r=Math.floor(Math.random()*1000),div=_15b();if(!a.bro.msi){ifr=d.ce("iframe");ifr.id="_atssh"+r;ifr.title="AddThis utility frame";}else{if(a.bro.ie6&&!url&&d.location.protocol.indexOf("https")==0){url="javascript:''";}div.innerHTML="<iframe id=\"_atssh"+r+"\" width=\"1\" height=\"1\" title=\"AddThis utility frame\" name=\"_atssh"+r+"\" "+(url?"src=\""+url+"\"":"")+">";ifr=d.getElementById("_atssh"+r);}a.opp(ifr.style);ifr.frameborder=ifr.style.border=0;ifr.style.top=ifr.style.left=0;return ifr;},_161=function(e){var _163=300;if(e&&e.data&&e.data.service){if(a.dcp>=_163){return;}trk({gen:e.data.service.indexOf("facebook")>-1?-1:_163,sh:e.data.service});a.dcp=_163;}},_164=function(evt){var t={},data=evt.data||{},svc=data.svc,pco=data.pco,_16a=data.cmo,_16b=data.crs,_16c=data.cso;if(svc){t.sh=svc;}if(_16a){t.cm=_16a;}if(_16c){t.cs=1;}if(_16b){t.cr=1;}if(pco){t.spc=pco;}img("sh","3",null,t);},trk=function(t){var dr=a.dr,rev=(a.rev||"");if(!t){return;}t.xck=_atc.xck?1:0;t.xxl=1;t.sid=a.track.ssid();t.pub=a.pub();t.ssl=a.ssl||0;t.du=a.tru(a.du||a.dl.href);if(a.dt){t.dt=a.dt;}if(a.cb){t.cb=a.cb;}t.lng=a.lng();t.ver=_atc.ver;if(!a.upm&&a.uid){t.uid=a.uid;}t.pc=t.spc||pcs.join(",");if(dr){t.dr=a.tru(dr);}if(a.dh){t.dh=a.dh;}if(rev){t.rev=rev;}if(a.xfr){if(a.upm){if(atf){atf.contentWindow.postMessage(_27(t),"*");}}else{var div=_15b(),base="static/r07/sh49.html"+(false?"?t="+new Date().getTime():"");if(atf){div.removeChild(div.firstChild);}atf=ctf();atf.src=_atr+base+"#"+_27(t);div.appendChild(atf);}}else{qtp.push(t);}},img=function(i,c,x,obj,_177){if(!window.at_sub&&!_atc.xtr){var t=obj||{};t.evt=i;if(x){t.ext=x;}avt=t;if(_177===1){xmi(true);}else{a.track.sxm(true,xmi);}}},cev=function(k,v){cvt.push(a.track.fcv(k,v));a.track.sxm(true,xmi);},xmi=function(_17d){var h=a.dl?a.dl.hostname:"";if(cvt.length>0||avt){a.track.sxm(false,xmi);if(_atc.xtr){return;}var t=avt||{};t.ce=cvt.join(",");cvt=[];avt=null;trk(t);if(_17d){var i=d.ce("iframe");i.id="_atf";_7.opp(i.style);d.body.appendChild(i);i=d.getElementById("_atf");}}};a.ed.addEventListener("addthis-internal.compact",_164);a.ed.addEventListener("addthis.menu.share",_161);if(!a.track){a.track={};}a.util.extend(a.track,{pcs:pcs,apc:apc,cev:cev,ctf:ctf,gtf:_15b,qtp:function(p){qtp.push(p);},stf:function(f){atf=f;},trk:trk,xtp:xtp});})();_19(_7,{_rec:[],xfr:!_7.upm||!_7.bro.ffx,pmh:function(e){if(e.origin.slice(-12)==".addthis.com"){if(!e.data){return;}var data=_7.util.rfromKV(e.data),r=_7._rec;for(var n=0;n<r.length;n++){r[n](data);}}}});_19(_7,{lng:function(){return window.addthis_language||(window.addthis_config||{}).ui_language||(_7.bro.msi?navigator.userLanguage:navigator.language)||"en";},iwb:function(l){var wd={th:1,pl:1,sl:1,gl:1,hu:1,is:1,nb:1,se:1,su:1,sw:1};return!!wd[l];},gfl:function(l){var map={ca:"es",cs:"CZ",cy:"GB",da:"DK",de:"DE",eu:"ES",ck:"US",en:"US",es:"LA",fb:"FI",gl:"ES",ja:"JP",ko:"KR",nb:"NO",nn:"NO",sv:"SE",ku:"TR",zh:"CN","zh-tr":"CN","zh-hk":"HK","zh-tw":"TW",fo:"FO",fb:"LT",af:"ZA",sq:"AL",hy:"AM",be:"BY",bn:"IN",bs:"BA",nl:"NL",et:"EE",fr:"FR",ka:"GE",el:"GR",gu:"IN",hi:"IN",ga:"IE",jv:"ID",kn:"IN",kk:"KZ",la:"VA",li:"NL",ms:"MY",mr:"IN",ne:"NP",pa:"IN",pt:"PT",rm:"CH",sa:"IN",sr:"RS",sw:"KE",tl:"PH",ta:"IN",pl:"PL",tt:"RU",te:"IN",ml:"IN",uk:"UA",vi:"VN",tr:"TR",xh:"ZA",zu:"ZA",km:"KH",tg:"TJ",he:"IL",ur:"PK",fa:"IR",yi:"DE",gn:"PY",qu:"PE",ay:"BO",se:"NO",ps:"AF",tl:"ST"},rv=map[l]||map[l.split("-").shift()];if(rv){return l.split("-").shift()+"_"+rv;}else{return"en_US";}},ivl:function(l){var lg={af:1,afr:"af",ar:1,ara:"ar",az:1,aze:"az",be:1,bye:"be",bg:1,bul:"bg",bn:1,ben:"bn",bs:1,bos:"bs",ca:1,cat:"ca",cs:1,ces:"cs",cze:"cs",cy:1,cym:"cy",da:1,dan:"da",de:1,deu:"de",ger:"de",el:1,gre:"el",ell:"ell",en:1,eo:1,es:1,esl:"es",spa:"spa",et:1,est:"et",eu:1,fa:1,fas:"fa",per:"fa",fi:1,fin:"fi",fo:1,fao:"fo",fr:1,fra:"fr",fre:"fr",ga:1,gae:"ga",gdh:"ga",gl:1,glg:"gl",gu:1,he:1,heb:"he",hi:1,hin:"hin",hr:1,ht:1,hy:1,cro:"hr",hu:1,hun:"hu",id:1,ind:"id",is:1,ice:"is",it:1,ita:"it",ja:1,jpn:"ja",ko:1,kor:"ko",ku:1,lb:1,ltz:"lb",lt:1,lit:"lt",lv:1,lav:"lv",mk:1,mac:"mk",mak:"mk",ml:1,mn:1,ms:1,msa:"ms",may:"ms",nb:1,nl:1,nla:"nl",dut:"nl",no:1,nds:1,nn:1,nno:"no",oc:1,oci:"oc",pl:1,pol:"pl",ps:1,pt:1,por:"pt",ro:1,ron:"ro",rum:"ro",ru:1,rus:"ru",sk:1,slk:"sk",slo:"sk",sl:1,slv:"sl",sq:1,alb:"sq",sr:1,se:1,si:1,ser:"sr",su:1,sv:1,sve:"sv",sw:1,swe:"sv",ta:1,tam:"ta",te:1,teg:"te",th:1,tha:"th",tl:1,tgl:"tl",tn:1,tr:1,tur:"tr",tt:1,uk:1,ukr:"uk",ur:1,urd:"ur",vi:1,vec:1,vie:"vi","zh-hk":1,"chi-hk":"zh-hk","zho-hk":"zh-hk","zh-tr":1,"chi-tr":"zh-tr","zho-tr":"zh-tr","zh-tw":1,"chi-tw":"zh-tw","zho-tw":"zh-tw",zh:1,chi:"zh",zho:"zh"};if(lg[l]){return lg[l];}l=l.split("-").shift();if(lg[l]){if(lg[l]===1){return l;}else{return lg[l];}}return 0;},gvl:function(l){var rv=_7.ivl(l)||"en";if(rv===1){rv=l;}return rv;},alg:function(al,f){var l=_7.gvl((al||_7.lng()).toLowerCase());if(l.indexOf("en")!==0&&(!_7.pll||f)){_7.pll=_7.ajs("static/r07/lang12/"+l+".js");}}});_19(_7,{trim:function(s,e){try{s=s.replace(/^[\s\u3000]+|[\s\u3000]+$/g,"");if(e){s=_euc(s);}}catch(e){}return s||"";},trl:[],tru:function(u,k){var rv="",_197=0,_198=-1;if(u){rv=u.substr(0,300);if(rv!==u){if((_198=rv.lastIndexOf("%"))>=rv.length-4){rv=rv.substr(0,_198);}if(rv!=u){for(var i in _7.trl){if(_7.trl[i]==k){_197=1;}}if(!_197){_7.trl.push(k);}}}}return rv;},opp:function(st){st.width=st.height="1px";st.position="absolute";st.zIndex=100000;},jlr:{},ajs:function(name,_19c,_19d,id,el){if(!_7.jlr[name]){var o=d.ce("script"),head=(el)?el:d.gn("head")[0]||d.documentElement;o.setAttribute("type","text/javascript");if(_19d){o.setAttribute("async","true");}if(id){o.setAttribute("id",id);}o.src=(_19c?"":_atr)+name;head.insertBefore(o,head.firstChild);_7.jlr[name]=1;return o;}return 1;},jlo:function(){try{var a=_7,al=a.lng(),aig=function(src){var img=new Image();_7.imgz.push(img);img.src=src;};a.alg(al);if(!a.pld){if(a.bro.ie6){aig(_atr+a.spt);aig(_atr+"static/t00/logo1414.gif");aig(_atr+"static/t00/logo88.gif");if(window.addthis_feed){aig("static/r05/feed00.gif",1);}}if(a.pll&&!window.addthis_translations){setTimeout(function(){a.pld=a.ajs("static/r07/menu82.js");},10);}else{a.pld=a.ajs("static/r07/menu82.js");}}}catch(e){}},ao:function(elt,pane,iurl,_1aa,_1ab,_1ac){_7.lad(["open",elt,pane,iurl,_1aa,_1ab,_1ac]);_7.jlo();return false;},ac:function(){},as:function(s,cf,sh){_7.lad(["send",s,cf,sh]);_7.jlo();}});(function(e,f,j){var m=document,k=1,a=["cbea","kkk","zvys","phz","gvgf","shpxf"],g=a.length,c={};function b(d){return d.replace(/[a-zA-Z]/g,function(i){return String.fromCharCode((i<="Z"?90:122)>=(i=i.charCodeAt(0)+13)?i:i-26);});}while(g--){c[b(a[g])]=1;}function h(d){var i=0;if(!d||typeof(d)!="string"){return i;}d=((d||"").toLowerCase()+"").replace(/ /g,"");if(d=="mature"||d=="rta-5042-1996-1400-1577-rta"){i|=k;}return i;}function l(o){var q=0;if(!o||typeof(o)!="string"){return q;}o=((o||"").toLowerCase()+"").replace(/[^a-zA-Z]/g," ").split(" ");for(var d=0,p=o.length;d<p;d++){if(c[o[d]]){q|=k;return q;}}return q;}function n(){var q=(w.addthis_title||m.title),i=l(q),p=m.all?m.all.tags("META"):m.getElementsByTagName?m.getElementsByTagName("META"):new Array(),o=(p||"").length;if(p&&o){while(o--){var d=p[o]||{},s=(d.name||"").toLowerCase(),r=d.content;if(s=="description"||s=="keywords"){i|=l(r);}if(s=="rating"){i|=h(r);}}}return i;}if(!e.ad){e.ad={};}_7.extend(e.ad,{cla:n});})(_7,_7.api,_7);(function(f,g,h){var c,j=document,m=f.util,b=f.event.EventDispatcher,k=25,e=[];function i(p,r,o){var d=[];function d(){d.push(arguments);}function q(){o[p]=r;while(d.length){r.apply(o,d.shift());}}d.ready=q;return d;}function l(p){if(p&&p instanceof a){e.push(p);}for(var d=0;d<e.length;){var o=e[d];if(o&&o.test()){e.splice(d,1);a.fire("load",o,{resource:o});}else{d++;}}if(e.length){setTimeout(l,k);}}function a(r,o,q){var d=this,p=new b(d);p.decorate(p).decorate(d);this.ready=false;this.loading=false;this.id=r;this.url=o;if(typeof(q)==="function"){this.test=q;}else{this.test=function(){return(!!_window[q]);};}a.addEventListener("load",function(s){var t=s.resource;if(!t||t.id!==d.id){return;}d.loading=false;d.ready=true;p.fire(s.type,t,{resource:t});});}m.extend(a.prototype,{load:function(){if(!this.loading){var d;if(this.url.substr(this.url.length-4)==".css"){var o=(j.gn("head")[0]||j.documentElement);d=j.ce("link");d.rel="stylesheet";d.type="text/css";d.href=this.url;d.media="all";o.insertBefore(d,o.firstChild);}else{d=_7.ajs(this.url,1);}this.loading=true;a.monitor(this);return d;}else{return 1;}}});var n=new b(a);n.decorate(n).decorate(a);m.extend(a,{known:{},loading:e,monitor:l});f.resource={Resource:a,ApiQueueFactory:i};})(_7,_7.api,_7);(function(e,u,w){var y=document,l=y.gn("body").item(0),g={},k={},o,x=[],c=0,s=0,t=0,i=true,m=[],A=0,v=0,h=0;function p(){return((_atc.ltj&&j()&&n())||(q()&&FB.XFBML&&FB.XFBML.parse));}function n(){if(o===undefined){try{var B=(document.getElementsByTagName("html"))[0];if(B){if(B.getAttribute&&B.getAttribute("xmlns:fb")){o=true;}else{if(_7.bro.msi){var d=B.outerHTML.substr(0,B.outerHTML.indexOf(">"));if(d.indexOf("xmlns:fb")>-1){o=true;}}}}}catch(C){o=false;}}return o;}function q(){return(typeof(window.FB)=="object"&&FB.Event&&typeof(FB.Event.subscribe)=="function");}function j(){return!window.FB_RequireFeatures&&(!window.FB||(!FB.Share&&!FB.Bootstrap));}function f(){if(y.location.href.indexOf(_atr)==-1&&!_7.sub&&!c){if(q()){var d=(addthis_config.data_ga_tracker||addthis_config.data_ga_property);c=1;FB.Event.subscribe("message.send",function(D){var B={};for(var C in addthis_share){B[C]=addthis_share[C];}B.url=D;if(k[D]){B.xid=k[D];}_7.share.track("facebook_send",0,B,addthis_config);if(d){_7.gat("facebook_send",D,addthis_config,B);}});FB.Event.subscribe("edge.create",function(D){if(!g[D]){var B={};for(var C in addthis_share){B[C]=addthis_share[C];}B.url=D;if(k[D]){B.xid=k[D];}_7.share.track("facebook_like",0,B,addthis_config);if(d){_7.gat("facebook_like",D,addthis_config,B);}g[D]=1;}});FB.Event.subscribe("edge.remove",function(D){if(g[D]){var B={};for(var C in addthis_share){B[C]=addthis_share[C];}B.url=D;if(k[D]){B.xid=k[D];}_7.share.track("facebook_dislike",0,B,addthis_config);g[D]=0;}});}else{if(window.fbAsyncInit&&!t){if(s<3){setTimeout(f,3000+1000*2*(s++));}t=1;}}}}function r(d,E){var D="fb-root",C=y.getElementById(D),B=window.fbAsyncInit;x.push(d);if(q()&&FB.XFBML&&FB.XFBML.parse){FB.XFBML.parse(d);f();}else{if(B){}else{if(!C){C=y.ce("div");C.id=D;document.body.appendChild(C);}if(!B){var F=y.createElement("script");F.src=y.location.protocol+"//connect.facebook.net/"+(E||_7.gfl(_7.lng()))+"/all.js";F.async=true;C.appendChild(F);B=function(){FB.init({appId:h?"140586622674265":"172525162793917",status:true,cookie:true});};}}if(i){i=false;window.__orig__fbAsyncInit=B;window.fbAsyncInit=function(){window.__orig__fbAsyncInit();for(var G=0;G<x.length;G++){FB.XFBML.parse(x[G]);}f();};}}}function z(H,F){if(H.ost){return;}var I,G=_7.api.ptpa(H,"fb:like"),C="",E=G.layout||"button_count",J=G.locale||_7.gfl(_7.lng()),d={standard:[450,G.show_faces?80:35],button_count:[90,25],box_count:[55,65]},K=G.width||(d[E]?d[E][0]:100),D=G.height||(d[E]?d[E][1]:25);passthrough=_7.util.toKV(G);_7.ufbl=1;if(p()){if(G.layout===undefined){G.layout="button_count";}if(G.show_faces===undefined){G.show_faces="false";}if(G.action===undefined){G.action="like";}if(G.width===undefined){G.width=K;}if(G.font===undefined){G.font="arial";}if(G.href===undefined){G.href=_7.track.mgu(F.share.url,{defrag:1});}for(var B in G){C+=" "+B+"=\""+G[B]+"\"";}if(!F.share.xid){F.share.xid=_7.util.cuid();}k[G.href]=F.share.xid;H.innerHTML="<fb:like ref=\""+_7.share.gcp(F.share,F.conf,".like").replace(",","_")+"\" "+C+"></fb:like>";r(H);}else{if(!_7.bro.msi){I=y.ce("iframe");}else{H.innerHTML="<iframe frameborder=\"0\" scrolling=\"no\" allowTransparency=\"true\" scrollbars=\"no\""+(_7.bro.ie6?" src=\"javascript:''\"":"")+"></iframe>";I=H.firstChild;}I.style.overflow="hidden";I.style.scrolling="no";I.style.scrollbars="no";I.style.border="none";I.style.borderWidth="0px";I.style.width=K+"px";I.style.height=D+"px";I.src="//www.facebook.com/plugins/like.php?href="+_euc(_7.track.mgu(F.share.url,{defrag:1}))+"&layout=button_count&show_faces=false&width=100&action=like&font=arial&"+passthrough;if(!_7.bro.msi){H.appendChild(I);}}H.noh=H.ost=1;}function b(E,C,G,d){var D=E.share_url_transforms||E.url_transforms||{},F=(E.passthrough||{}).facebook||{},B=a.track.cof(a.track.mgu(E.url,D,E,"facebook"));B=A?("http://www.facebook.com/sharer.php?&t="+_euc(E.title)+"&u="+_euc(_7.share.acb("facebook",E,C))):(v?("http://www.facebook.com/connect/prompt_feed.php?message="+_euc(E.title)+"%0A%0D"+_euc(_7.share.acb("facebook",E,C))):h?"http://www.facebook.com/dialog/feed?redirect_uri="+_euc("http://s7.addthis.com/static/postshare/c00.html")+"&app_id=140586622674265&link="+_euc(B)+"&name="+_euc(E.title)+"&description="+_euc(E.description||""):_7.share.genurl("facebook",0,E,C));if(A||v||h){_7.share.track("facebook",0,E,C,1);}if(C.ui_use_same_window||d){window.location.href=B;}else{_7.share.ocw(B,550,450,"facebook");}return false;}e.share=e.share||{};e.util.extend(e.share,{fb:{like:z,has:q,ns:n,ready:p,compat:j,share:b,sub:f,load:r}});})(_7,_7.api,_7);(function(e,p,s){var u=document,y=e,g=function(){var d=u.gn("link"),C={};for(var B=0;B<d.length;B++){var a=d[B];if(a.href&&a.rel){C[a.rel]=a.href;}}return C;},b=g(),x=function(){var a=u.location.protocol;if(a=="file:"){a="http:";}return a+"//"+_atd;},j=function(){if(y.dr){return"&pre="+_euc(y.track.cof(y.dr));}else{return"";}},n=function(B,C,d,a){return x()+(C?"feed.php":(B=="email"&&_atc.ver>=300?"tellfriend.php":"bookmark.php"))+"?v="+(_atc.ver)+"&winname=addthis&"+A(B,C,d,a)+j()+"&tt=0"+(B==="more"&&y.bro.ipa?"&imore=1":"");},A=function(S,H,V,aa){var O=y.trim,X=window,T=y.pub(),M=window._atw||{},N=(V&&V.url?V.url:(M.share&&M.share.url?M.share.url:(X.addthis_url||X.location.href))),Z,G=function(ad){if(N&&N!=""){var d=N.indexOf("#at"+ad);if(d>-1){N=N.substr(0,d);}}};if(!aa){aa=M.conf||{};}else{for(var U in M.conf){if(!(aa[U])){aa[U]=M.conf[U];}}}if(!V){V=M.share||{};}else{for(var U in M.share){if(!(V[U])){V[U]=M.share[U];}}}if(y.rsu()){V.url=window.addthis_url;V.title=window.addthis_title;N=V.url;}if(!T||T=="undefined"){T="unknown";}Z=aa.services_custom;G("pro");G("opp");G("cle");G("clb");G("abc");if(N.indexOf("addthis.com/static/r07/ab")>-1){N=N.split("&");for(var W=0;W<N.length;W++){var Q=N[W].split("=");if(Q.length==2){if(Q[0]=="url"){N=Q[1];break;}}}}if(Z instanceof Array){for(var W=0;W<Z.length;W++){if(Z[W].code==S){Z=Z[W];break;}}}var Y=((V.templates&&V.templates[S])?V.templates[S]:""),B=((V.modules&&V.modules[S])?V.modules[S]:""),E=V.share_url_transforms||V.url_transforms||{},K=V.track_url_transforms||V.url_transforms,ac=((E&&E.shorten&&V.shorteners)?(typeof(E.shorten)=="string"?E.shorten:(E.shorten[S]||E.shorten["default"]||"")):""),I="",R=(aa.product||X.addthis_product||("men-"+_atc.ver)),C=M.crs,J="",P=y.track.gof(N),ab=P.length==2?P.shift().split("=").pop():"",a=P.length==2?P.pop():"",L=(aa.data_track_clickback||aa.data_track_linkback||!T||T=="AddThis")||(aa.data_track_clickback!==false&&_atc.ver>=250);if(V.email_vars){for(var U in V.email_vars){J+=(J==""?"":"&")+_euc(U)+"="+_euc(V.email_vars[U]);}}if(y.track.spc&&R.indexOf(y.track.spc)==-1){R+=","+y.track.spc;}if(E&&E.shorten&&V.shorteners){for(var U in V.shorteners){for(var D in V.shorteners[U]){I+=(I.length?"&":"")+_euc(U+"."+D)+"="+_euc(V.shorteners[U][D]);}}}N=y.track.cof(N);N=y.track.mgu(N,E,V,S);if(K){V.trackurl=y.track.mgu(V.trackurl||N,K,V,S);}var F="pub="+T+"&source="+R+"&lng="+(y.lng()||"xx")+"&s="+S+(aa.ui_508_compliant?"&u508=1":"")+(H?"&h1="+O((V.feed||V.url).replace("feed://",""),1)+"&t1=":"&url="+O(N,1)+"&title=")+O(V.title||X.addthis_title,1)+(_atc.ver<200?"&logo="+O(X.addthis_logo,1)+"&logobg="+O(X.addthis_logo_background,1)+"&logocolor="+O(X.addthis_logo_color,1):"")+"&ate="+y.track.sta()+((S!="email"||_atc.ver<300)?"&frommenu=1":"")+((window.addthis_ssh&&(!C||addthis_ssh!=C)&&(addthis_ssh==S||addthis_ssh.search(new RegExp("(?:^|,)("+S+")(?:$|,)"))>-1))?"&ips=1":"")+(C?"&cr="+(S==C?1:0):"")+"&uid="+_euc(y.uid&&y.uid!="x"?y.uid:y.util.cuid())+(V.email_template?"&email_template="+_euc(V.email_template):"")+(J?"&email_vars="+_euc(J):"")+(ac?"&shortener="+_euc(typeof(ac)=="array"?ac.join(","):ac):"")+(ac&&I?"&"+I:"")+((V.passthrough||{})[S]?"&passthrough="+O((typeof(V.passthrough[S])=="object"?y.util.toKV(V.passthrough[S]):V.passthrough[S]),1):"")+(V.description?"&description="+O(V.description,1):"")+(V.html?"&html="+O(V.html,1):(V.content?"&html="+O(V.content,1):""))+(V.trackurl&&V.trackurl!=N?"&trackurl="+O(V.trackurl,1):"")+(V.screenshot?"&screenshot="+O(V.screenshot,1):"")+(V.swfurl?"&swfurl="+O(V.swfurl,1):"")+(y.cb?"&cb="+y.cb:"")+(y.ufbl?"&ufbl=1":"")+(V.iframeurl?"&iframeurl="+O(V.iframeurl,1):"")+(V.width?"&width="+V.width:"")+(V.height?"&height="+V.height:"")+(aa.data_track_p32?"&p32="+aa.data_track_p32:"")+(L||_7.track.ctp(aa.product,aa)?"&ct=1":"")+((L||_7.track.ctp(aa.product,aa))&&N.indexOf("#")>-1?"&uct=1":"")+((Z&&Z.url)?"&acn="+_euc(Z.name)+"&acc="+_euc(Z.code)+"&acu="+_euc(Z.url):"")+(y.smd?(y.smd.rxi?"&rxi="+y.smd.rxi:"")+(y.smd.rsi?"&rsi="+y.smd.rsi:"")+(y.smd.gen?"&gen="+y.smd.gen:""):((ab?"&rsi="+ab:"")+(a?"&gen="+a:"")))+(V.xid?"&xid="+O(V.xid,1):"")+(Y?"&template="+O(Y,1):"")+(B?"&module="+O(B,1):"")+(aa.ui_cobrand?"&ui_cobrand="+O(aa.ui_cobrand,1):"")+(aa.ui_header_color?"&ui_header_color="+O(aa.ui_header_color,1):"")+(aa.ui_header_background?"&ui_header_background="+O(aa.ui_header_background,1):"");return F;},z=function(B,d,C){var a=B.xid;if(d.data_track_clickback||d.data_track_linkback||_7.track.ctp(d.product,d)){return y.track.gcc(a,(y.smd||{}).gen||0)+(C||"");}else{return"";}},r=function(H,J,D,I,d,K){var G=y.pub(),a=I||J.url||"",C=J.xid||y.util.cuid(),E=(D.data_track_clickback||D.data_track_linkback||!G||G=="AddThis")||(D.data_track_clickback!==false&&_atc.ver>=250);if(a.toLowerCase().indexOf("http%3a%2f%2f")===0){a=_duc(a);}if(d){var B={};for(var F in J){B[F]=J[F];}B.xid=C;setTimeout(function(){(new Image()).src=n(H=="twitter"&&K?"tweet":H,0,B,D);},100);}return(E?y.track.cur(a,H,C):a);},h=function(D,B,a){var B=B||{},C=D.share_url_transforms||D.url_transforms||{},d=y.track.cof(y.track.mgu(D.url,C,D,"mailto"));return"mailto:?subject="+_euc(D.title?D.title:d)+"&body="+_euc(r("mailto",D,B,d,a));},i=function(a){return((!a.templates||!a.templates.twitter)&&(!y.wlp||y.wlp=="http:"));},f=function(d,C,J,B){var H=C||550,D=J||450,I=screen.width,F=screen.height,G=Math.round((I/2)-(H/2)),a=0,E;if(F>D){G=Math.round((F/2)-(D/2));}w.open(d,B||"addthis_share","left="+G+",top="+a+",width="+H+",height="+D+",personalbar=no,toolbar=no,scrollbars=yes,location=yes,resizable=yes");return false;},v=function(d,B,a){w.open(n(d,0,B,a),"addthis_share");return false;},l=function(d){var a={twitter:1,wordpress:1,email:_atc.ver>=300,more:_atc.ver>=300,vk:1};return a[d];},q=function(G,F,C,E,a,B){var D={wordpress:{width:720,height:570},linkedin:{width:600,height:400},email:_atc.ver>=300?{width:660,height:660}:{width:735,height:450},more:_atc.ver>=300?{width:660,height:716}:{width:735,height:450},vk:{width:720,height:290},"default":{width:550,height:450}},d=n(G,0,F,C);if(C.ui_use_same_window){window.location.href=d;}else{f(d,E||(D[G]||D["default"]).width,a||(D[G]||D["default"]).height,B);}return false;},c=function(F,C,G,d){var E=F.share_url_transforms||F.url_transforms||{},a,D=(F.passthrough||{}).twitter||{},B=y.track.cof(y.track.mgu(F.url,E,F,"twitter"));if(!F.templates){F.templates={};}if(!F.templates.twitter){F.templates.twitter=(D.text||"{{title}}:")+" {{url}} via @"+(D.via||"AddThis");}B=n("twitter",0,F,C);if(a){F.title=a;}if(C.ui_use_same_window||d){window.location.href=B;}else{f(B,550,450,"twitter_tweet");}return false;},k=[],m=function(C,B,a,d){_7.ed.fire("addthis.menu.share",window.addthis||{},{element:d||{},service:C||"unknown",url:B.trackurl||B.url});},o=function(D,E,C,d,B){var a=n(D,E,C,d);k.push(y.ajs(a,1));if(!B){m(D,C,d);}},t=function(B,d,a){return x()+"tellfriend.php?&fromname=aaa&fromemail="+_euc(d.from)+"&frommenu=1&tofriend="+_euc(d.to)+(B.email_template?"&template="+_euc(B.email_template):"")+(d.vars?"&vars="+_euc(d.vars):"")+"&lng="+(y.lng()||"xx")+"&note="+_euc(d.note)+"&"+A("email",null,null,a);};e.share=e.share||{};e.util.extend(e.share,{auw:l,ocw:f,stw:q,siw:v,pts:c,unt:i,uadd:A,genurl:n,geneurl:t,genieu:h,acb:r,gcp:z,svcurl:x,track:o,notify:m,links:b});})(_7,_7.api,_7);var w=window,ac=w.addthis_config||{},css=new _7.resource.Resource("widgetcss",_atr+"static/r07/widget64.css",function(){return true;}),_28e=new _7.resource.Resource("widget32css",_atr+"static/r07/widgetbig64.css",function(){return true;});function main(){try{if(_atc.xol&&!_atc.xcs&&ac.ui_use_css!==false){css.load();if(_7.bro.ipa){_28e.load();}}var a=_7,msi=a.bro.msi,hp=0,_292=window.addthis_config||{},dt=d.title,dr=(typeof(a.rdr)!=="undefined")?a.rdr:(d.referer||d.referrer||""),du=dl?dl.href:null,dh=dl.hostname,_297=du,_298=0,al=(_7.lng().split("-")).shift(),_29a=_7.track.eop(dl,dr),cvt=[],rsiq=_29a.rsiq,rsi=_29a.rsi,rxi=_29a.rxi,rsc=_29a.rsc.split("&").shift().split("%").shift().replace(/[^a-z0-9_]/g,""),gen=_29a.gen,fuid=_29a.fuid,ifr,_2a3=_atr+"static/r07/sh49.html#",data,_2a5=function(){if(!_7.track.pcs.length){_7.track.apc(window.addthis_product||("men-"+_atc.ver));}data.pc=_7.track.pcs.join(",");};if(rsc=="tweet"){rsc="twitter";}if(window.addthis_product){_7.track.apc(addthis_product);if(addthis_product.indexOf("fxe")==-1&&addthis_product.indexOf("bkm")==-1){_7.track.spc=addthis_product;}}var l=_7.share.links.canonical;if(l){if(l.indexOf("http")!==0){_297=(du||"").split("//").pop().split("/");if(l.indexOf("/")===0){_297=_297.shift()+l;}else{_297.pop();_297=_297.join("/")+"/"+l;}_297=dl.protocol+"//"+_297;}else{_297=l;}_7.usu(0,1);}_297=_297.split("#{").shift();a.igv(_297,d.title||"");var _2a7=addthis_share.view_url_transforms||addthis_share.track_url_transforms||addthis_share.url_transforms;if(_2a7){_297=_7.track.mgu(_297,_2a7);}if(rsi){rsi=rsi.substr(0,8)+fuid;}a.smd={rsi:rsi,rxi:rxi,gen:gen,rsc:rsc};a.dr=a.tru(dr,"fr");a.du=a.tru(_297,"fp");a.dt=dt=w.addthis_share.title;a.cb=a.ad.cla();a.dh=dl.hostname;a.ssl=du&&du.indexOf("https")===0?1:0;data={iit:(new Date()).getTime(),cb:a.cb,ab:a.ab,dh:a.dh,dr:a.dr,du:a.du,dt:dt,inst:a.inst,lng:a.lng(),pc:w.addthis_product||"men",pub:a.pub(),ssl:a.ssl,sid:_7.track.ssid(),srd:_atc.damp,srf:_atc.famp,srp:_atc.pamp,srx:_atc.xamp,ver:_atc.ver,xck:_atc.xck||0};if(a.trl.length){data.trl=a.trl.join(",");}if(a.rev){data.rev=a.rev;}if(_292.data_track_clickback||_292.data_track_linkback||_7.track.ctp(data.pc,_292)){data.ct=a.ct=1;}if(a.prv){data.prv=_27(a.prv);}if(rsc){data.sr=rsc;}if(a.vamp>=0&&!a.sub){if(rsi&&(fuid!=a.gub())){cvt.push(a.track.fcv("plv",Math.round(1/_atc.vamp)));cvt.push(a.track.fcv("rsi",rsi));cvt.push(a.track.fcv("gen",gen));cvt.push(a.track.fcv("abc",1));cvt.push(a.track.fcv("fcu",a.gub()));cvt.push(a.track.fcv("rcf",dl.hash));data.ce=cvt.join(",");_298="addressbar";}else{if(rxi||rsiq||rsc){cvt.push(a.track.fcv("plv",Math.round(1/_atc.vamp)));if(rsc){cvt.push(a.track.fcv("rsc",rsc));}if(rxi){cvt.push(a.track.fcv("rxi",rxi));}else{if(rsiq){cvt.push(a.track.fcv("rsi",rsiq));}}if(rsiq||rxi){cvt.push(a.track.fcv("gen",gen));}data.ce=cvt.join(",");_298=rsc||"unknown";}}}if(_298&&a.bamp>=0){data.clk=1;a.dcp=data.gen=50;_7.ed.fire("addthis.user.clickback",window.addthis||{},{service:_298});}if(a.upm){data.xd=1;if(_7.bro.ffx){data.xld=1;}}if(window.history&&typeof(history.replaceState)=="function"&&!_7.bro.chr&&(_292.data_track_addressbar||_292.data_track_addressbar_paths)&&((du||"").split("#").shift()!=dr)&&(du.indexOf("#")==-1||rsi||(_29a.hash&&rxi))){var path=dl.pathname||"",_2a9,_2aa=path!="/";if(_292.data_track_addressbar_paths){_2aa=0;for(var i=0;i<_292.data_track_addressbar_paths.length;i++){_2a9=new RegExp(_292.data_track_addressbar_paths[i].replace(/\*/g,".*")+"$");if(_2a9.test(path)){_2aa=1;break;}}}if(_2aa&&(!rsi||a.util.ioc(rsi,5))){var _2ac=function(){history.replaceState({d:(new Date()),g:gen},d.title,_7.track.cur(dl.href.split("#").shift(),null,_7.track.ssid()));};_2ac();}}if(dl.href.indexOf(_atr)==-1&&!a.sub){if(a.upm){if(msi){setTimeout(function(){_2a5();ifr=a.track.ctf(_2a3+_27(data));a.track.stf(ifr);},_7.wait);w.attachEvent("onmessage",a.pmh);}else{ifr=a.track.ctf();w.addEventListener("message",a.pmh,false);}if(_7.bro.ffx){ifr.src=_2a3;_7.track.qtp(data);}else{if(!msi){setTimeout(function(){_2a5();ifr.src=_2a3+_27(data);},_7.wait);}}}else{ifr=a.track.ctf();setTimeout(function(){_2a5();ifr.src=_2a3+_27(data);},_7.wait);}if(ifr){ifr=a.track.gtf().appendChild(ifr);a.track.stf(ifr);}}if(w.addthis_language||ac.ui_language){a.alg();}if(a.plo.length>0){a.jlo();}}catch(e){window.console&&console.log("lod",e);}}w._ate=a;w._adr=r;a._rec.push(function(data){if(data.sshs){var s=window.addthis_ssh=_duc(data.sshs);a.gssh=1;a._ssh=s.split(",");}if(data.uss){var u=a._uss=_duc(data.uss).split(",");if(window.addthis_ssh){var seen={},u=u.concat(a._ssh),_2b1=[];for(var i=0;i<u.length;i++){var s=u[i];if(!seen[s]){_2b1.push(s);}seen[s]=1;}u=_2b1;}a._ssh=u;window.addthis_ssh=u.join(",");}if(data.ups){var s=data.ups.split(",");a.ups={};for(var i=0;i<s.length;i++){if(s[i]){var o=_35(_duc(s[i]));a.ups[o.name]=o;}}a._ups=a.ups;}if(data.uid){a.uid=data.uid;_7.ed.fire("addthis-internal.data.uid",{},{uid:data.uid});}if(data.bti){a.bti=data.bti;_7.ed.fire("addthis-internal.data.bti",{},{bt:data.bti});}if(data.bts){a.bts=data.bts;_7.ed.fire("addthis-internal.data.bts",{},{bt:data.bts});}if(data.geo){a.geo=data.geo;_7.ed.fire("addthis-internal.data.geo",{},{geo:data.geo});}if(data.dbm){a.dbm=data.dbm;}if(data.rdy){a.xfr=1;a.track.xtp();return;}});try{var _2b4={},_2b5=_7.util.gsp("addthis_widget.js");if(typeof(_2b5)=="object"){if(_2b5.provider){_2b4={provider:_7.mun(_2b5.provider_code||_2b5.provider),auth:_2b5.auth||_2b5.provider_auth||""};if(_2b5.uid||_2b5.provider_uid){_2b4.uid=_7.mun(_2b5.uid||_2b5.provider_uid);}if(_2b5.logout){_2b4.logout=1;}_7.prv=_2b4;}if(_2b5.pubid||_2b5.pub||_2b5.username){w.addthis_pub=_duc(_2b5.pubid||_2b5.pub||_2b5.username);}if(w.addthis_pub&&w.addthis_config){w.addthis_config.username=w.addthis_pub;}if(_2b5.domready){_atc.dr=1;}if(_2b5.onready&&_2b5.onready.match(/[a-zA-Z0-9_\.\$]+/)){try{_7.onr=_7.evl(_2b5.onready);}catch(e){window.console&&console.log("addthis: onready function ("+_2b5.onready+") not defined",e);}}if(_2b5.async){_atc.xol=1;}}if((window.addthis_conf||{}).xol){_atc.xol=1;}if(_atc.ver===120){var rc="atb"+_7.util.cuid();d.write("<span id=\""+rc+"\"></span>");_7.igv();_7.lad(["span",rc,addthis_share.url||"[url]",addthis_share.title||"[title]"]);}if(w.addthis_clickout){_7.lad(["cout"]);}if(!_atc.xol&&!_atc.xcs&&ac.ui_use_css!==false){css.load();if(_7.bro.ipa){_28e.load();}}}catch(e){if(window.console){console.log("main",e);}}_75.bindReady();_75.append(main);})();function addthis_open(){if(typeof iconf=="string"){iconf=null;}return _ate.ao.apply(_ate,arguments);}function addthis_close(){_ate.ac();}function addthis_sendto(){_ate.as.apply(_ate,arguments);return false;}if(_atc.dr){_adr.onReady();}}else{_ate.inst++;}if(_atc.abf){addthis_open(document.getElementById("ab"),"emailab",window.addthis_url||"[URL]",window.addthis_title||"[TITLE]");}if(!window.addthis||window.addthis.nodeType!==undefined){window.addthis=(function(){var e={a1webmarks:"A1&#8209;Webmarks",aim:"AOL Lifestream",amazonwishlist:"Amazon",aolmail:"AOL Mail",aviary:"Aviary Capture",domaintoolswhois:"Whois Lookup",googlebuzz:"Google Buzz",googlereader:"Google Reader",googletranslate:"Google Translate",linkagogo:"Link-a-Gogo",meneame:"Men&eacute;ame",misterwong:"Mister Wong",mailto:"Email App",myaol:"myAOL",myspace:"MySpace",readitlater:"Read It Later",rss:"RSS",stumbleupon:"StumbleUpon",typepad:"TypePad",wordpress:"WordPress",yahoobkm:"Y! Bookmarks",yahoomail:"Y! Mail",youtube:"YouTube"},g=document,c=g.gn("body").item(0),f=_ate.util.bind;function b(d,l){var m;if(window._atw&&_atw.list){m=_atw.list[d]}else{if(e[d]){m=e[d]}else{m=(l?d:(d.substr(0,1).toUpperCase()+d.substr(1)))}}return(m||"").replace(/&nbsp;/g," ")}function i(d,u,s,r,t){u=u.toUpperCase();var p=(d==c&&addthis.cache[u]?addthis.cache[u]:(d||c||g.body).getElementsByTagName(u)),n=[],q,m;if(d==c){addthis.cache[u]=p}if(t){for(q=0;q<p.length;q++){m=p[q];if((m.className||"").indexOf(s)>-1){n.push(m)}}}else{s=s.replace(/\-/g,"\\-");var l=new RegExp("(^|\\s)"+s+(r?"\\w*":"")+"(\\s|$)");for(q=0;q<p.length;q++){m=p[q];if(l.test(m.className)){n.push(m)}}}return(n)}var k=g.getElementsByClassname||i;function j(d){if(typeof d=="string"){var l=d.substr(0,1);if(l=="#"){d=g.getElementById(d.substr(1))}else{if(l=="."){d=k(c,"*",d.substr(1))}else{}}}if(!d){d=[]}else{if(!(d instanceof Array)){d=[d]}}return d}function a(l,d){return function(){addthis.plo.push({call:l,args:arguments,ns:d})}}function h(m){var l=this,d=this.queue=[];this.name=m;this.call=function(){d.push(arguments)};this.call.queuer=this;this.flush=function(p,o){for(var n=0;n<d.length;n++){p.apply(o||l,d[n])}return p}}return{ost:0,cache:{},plo:[],links:[],ems:[],init:_adr.onReady,_Queuer:h,_queueFor:a,_select:j,_gebcn:i,data:{getShareCount:a("getShareCount","data")},button:a("button"),counter:a("counter"),count:a("counter"),toolbox:a("toolbox"),update:a("update"),util:{getServiceName:b},addEventListener:f(_ate.ed.addEventListener,_ate.ed),removeEventListener:f(_ate.ed.removeEventListener,_ate.ed)}})()}_adr.append((function(){if(!window.addthis.ost){_ate.extend(z,_ate.api);var V=document,K=undefined,J=window,G=0,f={},X={compact:1,expanded:1,facebook:1,email:1,twitter:1,print:1,google:1,live:1,stumbleupon:1,myspace:1,favorites:1,digg:1,delicious:1,blogger:1,googlebuzz:1,friendfeed:1,vk:1,mymailru:1,gmail:1,yahoomail:1,reddit:1,orkut:1},D=new _ate.resource.Resource("widget32css",_atr+"static/r07/widgetbig64.css",function(){return true}),P=false,s=J.addthis_config,M=J.addthis_share,E={},y={},q=V.gn("body").item(0),z=window.addthis,c=z._select,v=z._gebcn(q,"A","addthis_button_",true,true),T={rss:"Subscribe via RSS"},S={tweet:"Tweet",email:"Email",mailto:"Email",print:"Print",favorites:"Save to Favorites",twitter:"Tweet This",digg:"Digg This",more:"View more services"},L={email_vars:1,passthrough:1,modules:1,templates:1,services_custom:1},W={feed:1,more:_atc.ver<300,email:_atc.ver<300,mailto:1},F={feed:1,email:_atc.ver<300,mailto:1,print:1,more:!_ate.bro.ipa&&_atc.ver<300,favorites:1},x={print:1,favorites:1,mailto:1},O={email:_atc.ver>=300,more:_atc.ver>=300},H=0,l=0,C=0,R=0;function k(d){if(d.indexOf("&")>-1){d=d.replace(/&([aeiou]).+;/g,"$1")}return d}function e(u,w){if(w&&u!==w){for(var d in w){if(u[d]===K){u[d]=w[d]}}}}function n(Z,u,aa){var w=Z.onclick||function(){},d=x[u]?function(){_ate.share.track(u,0,Z.share,Z.conf)}:function(){_ate.share.notify(u,Z.share,Z.conf,Z)};if(Z.conf.data_ga_tracker||addthis_config.data_ga_tracker||Z.conf.data_ga_property||addthis_config.data_ga_property){Z.onclick=function(){_ate.gat(u,aa,Z.conf,Z.share);d();return w()}}else{Z.onclick=function(){d();return w()}}}function r(u,d){var w={googlebuzz:"http://www.google.com/profiles/%s",youtube:"http://www.youtube.com/user/%s",facebook:"http://www.facebook.com/profile.php?id=%s",facebook_url:"http://www.facebook.com/%s",rss:"%s",flickr:"http://www.flickr.com/photos/%s",twitter:"http://twitter.com/%s",linkedin:"http://www.linkedin.com/in/%s"};if(u=="facebook"&&isNaN(parseInt(d))){u="facebook_url"}return(w[u]||"").replace("%s",d)||""}function o(u,d){if(P&&!d){return true}var w=(u.parentNode||{}).className||"";P=(w.indexOf("32x32")>-1||u.className.indexOf("32x32")>-1);return P}function A(u){var w=(u.parentNode||{}).className||"",d=u.conf&&u.conf.product&&w.indexOf("toolbox")==-1?u.conf.product:"tbx"+(u.className.indexOf("32x32")>-1||w.indexOf("32x32")>-1?"32":"")+"-"+_atc.ver;if(d.indexOf(32)>-1){P=true}_ate.track.apc(d);return d}function h(w,Z){var u={};for(var d in w){if(Z[d]){u[d]=Z[d]}else{u[d]=w[d]}}return u}function b(u,d){var Z={};for(var w=0;w<u.length;w++){Z[u[w]]=1}for(var w=0;w<d.length;w++){if(!Z[d[w]]){u.push(d[w]);Z[d[w]]=1}}return u}function U(d,aa,ab,Z){var u=V.ce("img");u.width=d;u.height=aa;u.border=0;u.alt=ab;u.src=Z;return u}function i(Z,aa){var w,d=[],ab={};for(var u=0;u<Z.attributes.length;u++){w=Z.attributes[u];d=w.name.split(aa+":");if(d.length==2){ab[d.pop()]=w.value}}return ab}_ate.api.ptpa=i;function B(u,ad,d,Z){var ad=ad||{},w={},ab=i(u,"addthis");for(var aa in ad){w[aa]=ad[aa]}if(Z){for(var aa in u[d]){w[aa]=u[d][aa]}}for(var aa in ab){if(ad[aa]&&!Z){w[aa]=ad[aa]}else{var ae=ab[aa];if(ae){w[aa]=ae}else{if(ad[aa]){w[aa]=ad[aa]}}if(w[aa]==="true"){w[aa]=true}else{if(w[aa]==="false"){w[aa]=false}}}if(w[aa]!==K&&L[aa]&&(typeof w[aa]=="string")){try{w[aa]=JSON.parse(w[aa].replace(/'/g,'"'))}catch(ac){w[aa]=_ate.evl("("+w[aa]+");",true)}}}return w}function I(w){var u=(w||{}).services_custom;if(!u){return}if(!(u instanceof Array)){u=[u]}for(var Z=0;Z<u.length;Z++){var d=u[Z];if(d.name&&d.icon&&d.url){d.code=d.url=d.url.replace(/ /g,"");d.code=d.code.split("//").pop().split("?").shift().split("/").shift().toLowerCase();f[d.code]=d}}}function p(u,d){return f[u]||{}}function a(u,d,w,Z){var aa={conf:d||{},share:w||{}};aa.conf=B(u,d,"conf",Z);aa.share=B(u,w,"share",Z);return aa}function N(aq,ad,aj,ab){_ate.igv();if(aq){ad=ad||{};aj=aj||{};var ar=ad.conf||s,ao=ad.share||M,aa=aj.onmouseover,w=aj.onmouseout,au=aj.onclick,ag=aj.internal,al=aj.singleservice;if(al){if(au===K){au=W[al]?function(ax,av,ay){var aw=h(ay,y);return addthis_open(ax,al,aw.url,aw.title,h(av,E),aw)}:F[al]?function(ax,av,ay){var aw=h(ay,y);return addthis_sendto(al,h(av,E),aw)}:O[al]?function(ax,av,ay){var aw=h(ay,y);return _ate.share.stw(al,aw,av,735)}:null}}else{if(!aj.noevents){if(!aj.nohover){if(aa===K){aa=function(aw,av,ax){return addthis_open(aw,"",null,null,h(av,E),h(ax,y))}}if(w===K){w=function(av){return addthis_close()}}if(au===K){au=function(aw,av,ax){return addthis_sendto("more",h(av,E),h(ax,y))}}}else{if(au===K){au=function(aw,av,ax){return addthis_open(aw,"more",null,null,h(av,E),h(ax,y))}}}}}aq=c(aq);for(var ap=0;ap<aq.length;ap++){var ai=aq[ap],am=ai.parentNode,u=a(ai,ar,ao,!ab)||{};e(u.conf,s);e(u.share,M);ai.conf=u.conf;ai.share=u.share;if(ai.conf.ui_language){_ate.alg(ai.conf.ui_language)}I(ai.conf);if(am&&am.className.indexOf("toolbox")>-1&&(ai.conf.product||"").indexOf("men")===0){ai.conf.product="tbx"+(am.className.indexOf("32x32")>-1?"32":"")+"-"+_atc.ver;_ate.track.apc(ai.conf.product)}if(al&&al!=="more"){ai.conf.product=A(ai)}if((!ai.conf||(!ai.conf.ui_click&&!ai.conf.ui_window_panes))&&!_ate.bro.ipa){if(aa){ai.onmouseover=function(){return aa(this,this.conf,this.share)}}if(w){ai.onmouseout=function(){return w(this)}}if(au){ai.onclick=function(){return au(ai,ai.conf,ai.share)}}}else{if(au){if(al){ai.onclick=function(){return au(this,this.conf,this.share)}}else{if(!ai.conf.ui_window_panes){ai.onclick=function(){return addthis_open(this,"",null,null,this.conf,this.share)}}else{ai.onclick=function(){return addthis_sendto("more",this.conf,this.share)}}}}}if(ai.tagName.toLowerCase()=="a"){var Z=ai.share.url||addthis_share.url;_ate.usu(Z);if(al){var af=p(al,ai.conf),d=ai.firstChild;if(af&&af.code&&af.icon){if(d&&d.className.indexOf("at300bs")>-1){var ah="16";if(o(ai,1)){d.className=d.className.split("at15nc").join("");ah="32"}d.style.background="url("+af.icon+") no-repeat top left transparent";if(!d.style.cssText){d.style.cssText=""}d.style.cssText="line-height:"+ah+"px!important;width:"+ah+"px!important;height:"+ah+"px!important;background:"+d.style.background+"!important"}}if(!F[al]){if(aj.follow){ai.href=Z;ai.onclick=function(){_ate.share.track(al,1,ai.share,ai.conf)};if(ai.children&&ai.children.length==1&&ai.parentNode&&ai.parentNode.className.indexOf("toolbox")>-1){var an=V.ce("span");an.className="addthis_follow_label";an.innerHTML=z.util.getServiceName(al);ai.appendChild(an)}}else{if(al=="twitter"){ai.onclick=function(av){return _ate.share.pts(ai.share,ai.conf)};ai.noh=1}else{if(al=="facebook"){ai.onclick=function(av){return _ate.share.fb.share(ai.share,ai.conf)};ai.noh=1}else{if(al=="google_plusone"){ai.onclick=function(av){return false}}else{if(!ai.noh){if(ai.conf.ui_open_windows||_ate.share.auw(al)){ai.onclick=function(av){return _ate.share.stw(al,ai.share,ai.conf)}}else{ai.onclick=function(av){return _ate.share.siw(al,ai.share,ai.conf)};ai.href=_ate.share.genurl(al,0,ai.share,ai.conf)}}}}}}n(ai,al,Z);if(!ai.noh&&!ai.target){ai.target="_blank"}z.links.push(ai)}else{if(al=="mailto"||(al=="email"&&(ai.conf.ui_use_mailto||_ate.bro.iph||_ate.bro.ipa||_ate.bro.dro))){ai.onclick=function(){ai.share.xid=_ate.util.cuid();(new Image()).src=_ate.share.genurl("mailto",0,ai.share,ai.config);_ate.gat(al,Z,ai.conf,ai.share)};ai.href=_ate.share.genieu(ai.share);z.ems.push(ai)}}if(!ai.title||ai.at_titled){var ae=z.util.getServiceName(al,!af);ai.title=k(aj.follow?(T[al]?T[al]:"Follow on "+ae):(S[al]?S[al]:"Send to "+ae));ai.at_titled=1}if(!ai.href){ai.href="#"}}else{if(ai.conf.product&&ai.parentNode.className.indexOf("toolbox")==-1){A(ai)}}}var ac;switch(ag){case"img":if(!ai.hasChildNodes()){var at=(ai.conf.ui_language||_ate.lng()).split("-").shift(),ak=_ate.ivl(at);if(!ak){at="en"}else{if(ak!==1){at=ak}}ac=U(_ate.iwb(at)?150:125,16,"Share",_atr+"static/btn/v2/lg-share-"+at.substr(0,2)+".gif")}break}if(ac){ai.appendChild(ac)}}}}function g(){if(window.gapi&&window.gapi.plusone){gapi.plusone.go();return}else{if(!C){var d=_ate.ajs("//apis.google.com/js/plusone.js",1,1);C=1}}if(H<3){setTimeout(g,3000+1000*2*(H++))}}function Q(){if(window.twttr&&!G&&twttr.events){G=1;twttr.events.bind("click",function(ab){if(ab.region=="tweetcount"){return}var aa=(ab.target.parentNode&&ab.target.parentNode.share)?ab.target.parentNode.share:{},w=aa.url||ab.target.baseURI,ac=aa.title||addthis_share.title,d={};for(var u in addthis_share){d[u]=addthis_share[u]}d.url=w;if(ac){d.title=ac}var Z=(ab.region!="follow")?true:false;_ate.share.track(((Z)?"tweet":"twitter_follow_native"),((Z)?0:1),d,addthis_config)})}}function t(d){if(window.twttr&&window.twttr.events&&R==1){Q();return}else{if(!R){_ate.ajs("//platform.twitter.com/widgets.js",1);R=1}}if(l<3){setTimeout(t,3000+1000*2*(l++))}}function Y(aR,aT,aU,aA,a1){for(var a4=0;a4<aR.length;a4++){var ba=aR[a4];if(ba==null){continue}if(aA!==false||!ba.ost){var at=a(ba,aT,aU,!a1),w=0,bc="at300",a9=ba.className||"",af="",aY=a9.match(/addthis_button_([\w\.]+)(?:\s|$)/),aH={},al=aY&&aY.length?aY[1]:0;e(at.conf,s);e(at.share,M);if(al){if(al.indexOf("amazonwishlist_native")>-1){}else{if(al==="tweetmeme"&&ba.className.indexOf("chiclet_style")==-1){if(ba.ost){continue}var a3=i(ba,"tm"),aW=50,a6=61;af=_ate.util.toKV(a3);if(a3.style==="compact"){aW=95;a6=25}ba.innerHTML='<iframe frameborder="0" width="'+aW+'" height="'+a6+'" scrolling="no" allowTransparency="true" scrollbars="no"'+(_ate.bro.ie6?" src=\"javascript:''\"":"")+"></iframe>";var be=ba.firstChild;be.src="//api.tweetmeme.com/button.js?url="+_euc(at.share.url)+"&"+af;ba.noh=ba.ost=1}else{if(al==="linkedin_counter"){if(ba.ost){continue}var aZ=i(ba,"li"),aU=at.share,bb=aZ.width||100,ag=aZ.height||18,af,a5="",ao;if(!aZ.counter){aZ.counter="horizontal"}if(!aU.passthrough){aU.passthrough={}}aU.passthrough.linkedin=_ate.util.toKV(aZ);a5=_ate.util.rtoKV(aU);if(aZ.counter==="top"){ag=55;bb=57;if(!aZ.height){aZ.height=ag}if(!aZ.width){aZ.width=bb}}else{if(aZ.counter==="right"){bb=100;if(!aZ.width){aZ.width=aI}}}if(aZ.width){bb=aZ.width}if(aZ.height){ag=aZ.height}af=_ate.util.toKV(aZ),ba.innerHTML='<iframe frameborder="0" role="presentation" scrolling="no" allowTransparency="true" scrollbars="no"'+(_ate.bro.ie6?" src=\"javascript:''\"":"")+' style="width:'+bb+"px; height:"+ag+'px;"></iframe>';ao=ba.firstChild;if(!at.conf.pubid){at.conf.pubid=addthis_config.pubid||_ate.pub()}ao.src=_atr+"static/r07/linkedin08.html"+((_ate.bro.ie6||_ate.bro.ie7)?"?":"#")+"href="+_euc(at.share.url)+"&dr="+_euc(_ate.dr)+"&conf="+_euc(_ate.util.toKV(at.conf))+"&share="+_euc(a5)+"&li="+_euc(af);ba.noh=ba.ost=1}else{if(al==="twitter_follow_native"){var an=i(ba,"tf"),aa=i(ba,"tw"),am=V.ce("a");an.screen_name=aa.screen_name||an.screen_name||"addthis";am.href="http://twitter.com/"+an.screen_name;am.className="twitter-follow-button";am.innerHTML="Follow @"+an.screen_name;for(var ax in an){if(an.hasOwnProperty(ax)){am.setAttribute("data-"+ax,an[ax])}}for(var ax in aa){if(aa.hasOwnProperty(ax)){am.setAttribute("data-"+ax,aa[ax])}}ba.appendChild(am);if(!at.conf.pubid){at.conf.pubid=addthis_config.pubid||_ate.pub()}t(ba)}else{if(al==="tweet"){if(ba.ost){continue}var aa=i(ba,"tw"),aU=at.share,aI=aa.width||55,aO=aa.height||20,af,a5="",aD;at.share.url_transforms=at.share.url_transforms||{};at.share.url_transforms.defrag=1;aa.url=at.share.url=aa.url||_ate.track.mgu(at.share.url,at.share.url_transforms,at.share,"twitter");aa.url=_ate.share.acb("twitter",at.share,addthis_config);aa.count=aa.count||"horizontal";aU.passthrough=aU.passthrough||{};aU.passthrough.twitter=_ate.util.toKV(aa,"#@!");a5=_ate.util.rtoKV(aU,"#@!");if(aa.count==="vertical"){aO=62;aa.height=aa.height||aO}else{if(aa.count==="horizontal"){aI=110;aa.width=aa.width||aI}}if(aa.width){aI=aa.width}if(aa.height){aO=aa.height}af=_ate.util.toKV(aa,"#@!");if((_ate.bro.msi&&V.compatMode=="BackCompat")||at.conf.ui_use_tweet_iframe){ba.innerHTML='<iframe frameborder="0" role="presentation" scrolling="no" allowTransparency="true" scrollbars="no"'+(_ate.bro.ie6?" src=\"javascript:''\"":"")+' style="width:'+aI+"px; height:"+aO+'px;"></iframe>';aD=ba.firstChild;if(!at.conf.pubid){at.conf.pubid=addthis_config.pubid||_ate.pub()}aD.src=_atr+"static/r07/tweet08.html"+((_ate.bro.ie6||_ate.bro.ie7)?"?":"#")+"href="+_euc(at.share.url)+"&dr="+_euc(_ate.dr)+"&conf="+_euc(_ate.util.toKV(at.conf))+"&share="+_euc(a5)+"&tw="+_euc(af)}else{var ae=(aU.templates||{}).twitter||"";at.via=aa.via=aa.via||"AddThis";if(!aa.text){aa.text=aU.title==""?"":aU.title+":"}var aN=V.ce("a");aN.href="http://twitter.com/share";aN.className="twitter-share-button";aN.innerHTML="Tweet";for(var ax in aa){if(aa.hasOwnProperty(ax)){aN.setAttribute("data-"+ax,aa[ax])}}ba.appendChild(aN);if(!at.conf.pubid){at.conf.pubid=addthis_config.pubid||_ate.pub()}t(ba)}ba.noh=ba.ost=1}else{if(al==="google_plusone"){if(ba.ost){continue}var aK=i(ba,"g:plusone"),aV=V.ce("g:plusone"),av="";aK.href=aK.href||_ate.track.mgu(at.share.url,{defrag:1});aK.size=aK.size||(o(ba,true)?"standard":"small");window._at_gpocbh=window._at_gpocbh||function(bh){var bf={};for(var bg in addthis_share){bf[bg]=at.share[bg]}bf.url=bh.href;_ate.share.track("google_"+(bh.state=="off"?"un":"")+"plusone",0,bf,at.conf)};aK.callback=aK.callback||"_at_gpocbh";for(var a2 in aK){if(aK.hasOwnProperty(a2)){aV.setAttribute(a2,aK[a2])}}ba.appendChild(aV);ba.noh=ba.ost=1;g()}else{if(al==="facebook_send"){if(ba.ost||_ate.bro.ie6){continue}var ay,ai=i(ba,"fb:send"),az="",a8=ai.width||55,ac=ai.height||20;af=_ate.util.toKV(ai);_ate.ufbl=1;if(_ate.share.fb.ready()){ai.href=ai.href||_ate.track.mgu(at.share.url,{defrag:1});for(var a2 in ai){az+=" "+a2+'="'+ai[a2]+'"'}ba.innerHTML='<fb:send ref="'+_ate.share.gcp(at.share,at.conf,".send").replace(",","_")+'" '+az+"></fb:send>";_ate.share.fb.load(ba)}else{ba.className="";ba.innerHTML="<span></span>";ba.style.width=ba.style.height="0px"}ba.noh=ba.ost=1}else{if(al==="facebook_share"){at.conf=at.conf||{};at.conf.data_track_clickback=at.conf.data_track_linkback=false;function aS(bg,bf){if(!bg){return}bg.setAttribute("style",bf);bg.style.cssText=bf;return}var aQ="AT"+_ate.util.cuid(),ai=i(ba,"fb:share"),aw=V.ce("span"),aj=V.ce("div"),aJ=V.ce("div"),bd=V.ce("div"),aB=V.ce("div"),aq=V.ce("div"),ab=at.share.url=ai.href||_ate.track.mgu(at.share.url,{defrag:1}),a0=typeof(a0)!="undefined"?a0:{};a0[aQ]=ab.replace(/\#.*/,"");aS(aw,"text-decoration:none;color:#000000;display:inline-block;cursor:pointer;");aS(bd,"text-decoration:none;margin-top:10px;");aS(aJ,"display:block;z-index:-1;background:none repeat scroll 0 0 #ECEEF5; border:1px solid #CAD4E7; filter:none; border-radius: 4px; color:#000000; font-family:Verdana,Helvetica,sans-serif; font-size:18px; line-height:16px; height:39px; text-align:center; width:58px;");aS(aB,"display:block;margin:-1px 0 0px 10px;height:4px;width:10px;font-size:1px;line-height:4px;background:url('"+_atr+"static/t00/fb_arrow.png') no-repeat ;");aS(aq,"background-image:url('"+_atr+"static/t00/fb_btn.png');background-repeat:no-repeat; display:inline-block;font-family:Verdana,Helvetica,sans-serif; font-size:1px; height:22px; line-height:16px; white-space:nowrap; width:60px;");bd.innerHTML="0";bd.id=aQ;aB.innerHTML="&nbsp;";at.share.passthrough=at.share.passthrough||{};at.share.passthrough.facebook_share=_ate.util.toKV({src:"sp"});aq.onmouseover=function(){this.style.opacity="0.75"};aq.onmouseout=function(){this.style.opacity="1.0"};aq.onclick=function(){var bf=this.parentNode.firstChild.firstChild;if(bf&&isNaN(bf.innerHTML)!=true){var bg=parseInt(bf.innerHTML)+1;bf.removeChild(bf.firstChild);bf.appendChild(document.createTextNode(bg))}};aJ.appendChild(bd);aj.appendChild(aJ);aj.appendChild(aB);aj.appendChild(aq);aw.appendChild(aj);ba.appendChild(aw);ba.style.textDecoration="none";var aG=_ate.util.scb("fbsc",ab,function(bh){if(bh.length>0){for(var bf in a0){if(a0[bf]==bh[0].url){var bi=bh[0].share_count,bg=document.getElementById(bf);if(bi>10000){bi=parseInt(bi/1000)+"K"}if(bg.firstChild){bg.removeChild(bg.firstChild)}bg.appendChild(document.createTextNode(bi))}}}},function(){});_ate.ajs("//api.facebook.com/restserver.php?method=links.getStats&format=json&callback="+aG+"&urls="+ab,1)}else{if(al==="facebook_like"){_ate.share.fb.like(ba,at)}else{if(al.indexOf("stumbleupon_badge")>-1){if(_ate.bro.ie6){continue}var u=i(ba,"su:badge"),ah=u.style||"1",aE=at.share.url=u.href||_ate.track.mgu(at.share.url,{defrag:1}),aC=u.height||"20px",au=u.width||"75px";if(ah=="5"){aC=u.height||"60px"}else{if(ah=="6"){aC=u.height||"31px"}}ba.innerHTML='<iframe src="//www.stumbleupon.com/badge/embed/{{STYLE}}/?url={{URL}}" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:{{WIDTH}}; height:{{HEIGHT}};" allowtransparency="true"></iframe>'.replace("{{STYLE}}",ah).replace("{{URL}}",_euc(aE)).replace("{{HEIGHT}}",aC).replace("{{WIDTH}}",au);ba.noh=ba.ost=1}else{if(al.indexOf("hyves_respect")>-1){var Z=i(ba,"hy:respect"),aP=at.share.url=Z.url||_ate.track.mgu(at.share.url,{defrag:1}),aF=Z.width||"140px",aX='<iframe src="//www.hyves.nl/respect/button?url={{URL}}" style="border: medium none; overflow:hidden; width:{{WIDTH}}; height:22px;" scrolling="no" frameborder="0" allowTransparency="true" ></iframe>'.replace("{{URL}}",_ate.share.acb("hyves",at.share,addthis_config)).replace("{{WIDTH}}",aF);ba.innerHTML=aX;ba.noh=ba.ost=1}else{if(al.indexOf("preferred")>-1){if(ba._iss){continue}aY=a9.match(/addthis_button_preferred_([0-9]+)(?:\s|$)/);var a7=((aY&&aY.length)?Math.min(16,Math.max(1,parseInt(aY[1]))):1)-1;if(!ba.conf){ba.conf={}}ba.conf.product="tbx-"+_atc.ver;A(ba);if(window._atw){if(!ba.parentNode.services){ba.parentNode.services={}}var ad=_atw.conf.services_exclude||"",d=_atw.loc,ak=ba.parentNode.services,aM=b(addthis_options.replace(",more","").split(","),d.split(","));do{al=aM[a7++]}while(a7<aM.length&&(ad.indexOf(al)>-1||ak[al]));if(ak[al]){for(var a2 in _atw.list){if(!ak[a2]&&ad.indexOf(a2)==-1){al=a2;break}}}ba._ips=1;if(ba.className.indexOf(al)==-1){ba.className+=" addthis_button_"+al;ba._iss=1}ba.parentNode.services[al]=1}else{_ate.alg(at.conf.ui_language||window.addthis_language);_ate.plo.unshift(["deco",Y,[ba],aT,aU,true]);if(_ate.gssh){_ate.pld=_ate.ajs("static/r07/menu82.js")}else{if(!_ate.pld){_ate.pld=1;var ar=function(){_ate.pld=_ate.ajs("static/r07/menu82.js")};if(_ate.upm){_ate._rec.push(function(bf){if(bf.ssh){ar()}});setTimeout(ar,500)}else{ar()}}}continue}}else{if(al.indexOf("follow")>-1){al=al.split("_follow").shift();aH.follow=true;at.share.url=r(al,at.share.userid)}}}}}}}}}}}}}if(!X[al]&&(P||o(ba))){D.load()}if(!ba.childNodes.length){var ap=V.ce("span");ba.appendChild(ap);ap.className=bc+"bs at15nc at15t_"+al}else{if(ba.childNodes.length==1){var aL=ba.childNodes[0];if(aL.nodeType==3){var ap=V.ce("span");ba.insertBefore(ap,aL);ap.className=bc+"bs at15nc at15t_"+al}}else{w=1}}if(al==="compact"||al==="expanded"){if(!w&&a9.indexOf(bc)==-1){ba.className+=" "+bc+"m"}if(at.conf.product&&at.conf.product.indexOf("men-")==-1){at.conf.product+=",men-"+_atc.ver}if(!ba.href){ba.href="#"}if(ba.parentNode&&ba.parentNode.services){at.conf.parentServices=ba.parentNode.services}if(al==="expanded"){aH.nohover=true;aH.singleservice="more"}}else{if((ba.parentNode.className||"").indexOf("toolbox")>-1){if(!ba.parentNode.services){ba.parentNode.services={}}ba.parentNode.services[al]=1}if(!w&&a9.indexOf(bc)==-1){ba.className+=" "+bc+"b"}aH.singleservice=al}if(ba._ips){aH.issh=true}N([ba],at,aH,a1);ba.ost=1;A(ba)}}}}function j(af,d,ac,ae){if(ac.data_ga_social&&(af=="facebook_unlike"||af=="google_unplusone")){return}var w=ac.data_ga_tracker,aa=ac.data_ga_property;if(aa){if(typeof(window._gat)=="object"&&_gat._getTracker){w=_gat._getTracker(aa)}else{if(typeof(window._gaq)=="object"&&_gaq._getAsyncTracker){w=_gaq._getAsyncTracker(aa)}else{if(typeof(window._gaq)=="array"){_gaq.push([function(){_ate.gat(af,d,ac,ae)}])}}}}if(w&&typeof(w)=="string"){w=window[w]}if(w&&typeof(w)=="object"){var ad=d||(ae||{}).url||location.href,u=af,Z="share";if(u.indexOf("facebook_")>-1||u.indexOf("google_")>-1){u=u.split("_");Z=u.pop();u=u.shift()}if(ad.toLowerCase().replace("https","http").indexOf("http%3a%2f%2f")==0){ad=_duc(ad)}try{if(ac.data_ga_social&&w._trackSocial){w._trackSocial(u,Z,ae.url)}else{w._trackEvent("addthis",af,ad)}}catch(ab){try{w._initData();if(ac.data_ga_social&&w._trackSocial){w._trackSocial(u,Z,ae.url)}else{w._trackEvent("addthis",af,ad)}}catch(ab){}}}}_ate.gat=j;z.update=function(ac,aa,w){if(ac=="share"){if(aa=="url"){_ate.usu(0,1)}if(!window.addthis_share){window.addthis_share={}}window.addthis_share[aa]=w;y[aa]=w;for(var d in z.links){var ab=z.links[d],Z=new RegExp("&"+aa+"=(.*)&"),u="&"+aa+"="+_euc(w)+"&";if(ab.share){ab.share[aa]=w}if(!ab.noh){ab.href=ab.href.replace(Z,u);if(ab.href.indexOf(aa)==-1){ab.href+=u}}}for(var d in z.ems){var ab=z.ems[d];ab.href=_ate.share.genieu(addthis_share)}}else{if(ac=="config"){if(!window.addthis_config){window.addthis_config={}}window.addthis_config[aa]=w;E[aa]=w}}};z._render=N;var m=[new _ate.resource.Resource("countercss",_atr+"static/r07/counter64.css",function(){return true}),new _ate.resource.Resource("counter",_atr+"js/250/plugin.sharecounter.js",function(){return window.addthis.counter.ost})];if(!J.JSON||!J.JSON.stringify){m.unshift(new _ate.resource.Resource("json2",_atr+"static/r07/json2.js",function(){return J.JSON&&J.JSON.stringify}))}z.counter=function(Z,u,w){if(Z){Z=z._select(Z);if(Z.length){if(!z.counter.selects){z.counter.selects=[]}z.counter.selects=z.counter.selects.concat({counter:Z,config:u,share:w});for(var d in m){if((m[d]||{}).load){m[d].load()}}}}};z.count=function(Z,u,w){if(Z){Z=z._select(Z);if(Z.length){if(!z.count.selects){z.count.selects=[]}z.count.selects=z.count.selects.concat({counter:Z,config:u,share:w});for(var d in m){if((m[d]||{}).load){m[d].load()}}}}};z.data.getShareCount=function(w,u){if(!z.counter.reqs){z.counter.reqs=[]}z.counter.reqs.push({share:u,callback:w});for(var d in m){if((m[d]||{}).load){m[d].load()}}};z.button=function(w,d,u){d=d||{};if(!d.product){d.product="men-"+_atc.ver}N(w,{conf:d,share:u},{internal:"img"})};z.toolbox=function(ac,u,ad,ae){var af=c(ac);for(var Z=0;Z<af.length;Z++){var w=af[Z],aa=a(w,u,ad,ae),d=V.ce("div"),ab;w.services={};if(!aa.conf.product){aa.conf.product="tbx"+(w.className.indexOf("32x32")>-1?"32":"")+"-"+_atc.ver}if(w){ab=w.getElementsByTagName("a");if(ab){Y(ab,aa.conf,aa.share,!ae,!ae)}w.appendChild(d)}d.className="atclear"}};z.log=z.log||{};z.log.share=function(d,w,u){var Z=u||addthis_config;Z.product="hdl-"+_atc.ver;_ate.share.track(d,0,w||addthis_share,u||addthis_config)};z.ready=function(){var d=z,u=".addthis_";if(d.ost){return}d.ost=1;z.toolbox(u+"toolbox",null,null,true);z.button(u+"button");z.counter(u+"counter");z.count(u+"count");Y(v,null,null,false);_ate.ed.fire("addthis.ready",z);if(_ate.onr){_ate.onr(z)}for(var w=0,aa=d.plo,Z;w<aa.length;w++){Z=aa[w];(Z.ns?d[Z.ns]:d)[Z.call].apply(this,Z.args)}_ate.share.fb.sub();Q()};z.util.getAttributes=a;window.addthis=z;window.addthis.ready()}}));_ate.extend(addthis,{user:(function(){var k=_ate,f=addthis,l={},c=0,m=0,e=0,d;function j(a,n){return k.reduce(["getID","getGeolocation","getServiceShareHistory"],a,n)}function g(a,n){return function(o){setTimeout(function(){o(k[a]||n)},0)}}function i(a){if(c){return}if(!a||!a.uid){return}if(d!==null){clearTimeout(d)}d=null;c=1;j(function(p,n,o){l[n]=l[n].queuer.flush(g.apply(f,p[o]),f);return p},[["uid",""],["geo",""],["_ssh",[]]])}function h(){if(!_ate.pld){_ate.pld=(new _ate.resource.Resource("menujs",_atr+"static/r07/menu82.js",function(){return true})).load()}}function b(a){if(m&&(a.uid||a.ssh!==undefined)){h();m=0}}d=setTimeout(function(){var a={uid:"x",geo:"",ssh:"",ups:""};e=1;i(a);b(a)},5000);k._rec.push(i);l.getPreferredServices=function(a){if(window._atw){_atw.gps(a)}else{_ate.ed.addEventListener("addthis.menu.ready",function(){_atw.gps(a)});_ate.alg();if(k.gssh||e){h()}else{if(!k.pld&&!m){_ate._rec.push(b)}}m=1}};return j(function(n,a){n[a]=(new f._Queuer(a)).call;return n},l)})()});
var smile = smile || {}
var host   = "http://" + window.location.host;
jQuery(document).ready(function(){
if(jQuery(".minwidth_ie").length !=0){
jQuery.ajax({
url: "/redirectextension/getreferrer/"+document.referrer
});
}
jQuery('.shareNetwork').click(function(event){
event.preventDefault();
var url = jQuery(this).attr("href");
window.open(url,'mywin','width=650,height=500,left=300');
});
jQuery('.sitesLink').click(function(event){
event.preventDefault();
var nextBlock = $(".contenu_site");
if(nextBlock.is(':visible')){
nextBlock.hide();
}
else{
nextBlock.show();
}
});
var current = jQuery("#promotions").children( '.active' );
var title   = current.children('h1');
var link    = current.children('a');
var href    = link.attr('href');
if(title.is(':hidden'))
{
if(href !='undefined'){
current.click(function() {
window.location = href;
});
}
}
});
if(typeof deconcept=="undefined"){var deconcept=new Object();}
if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}
if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}
deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a,_b){if(!document.getElementById){return;}
this.DETECT_KEY=_b?_b:"detectflash";this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);this.params=new Object();this.variables=new Object();this.attributes=new Array();if(_1){this.setAttribute("swf",_1);}
if(id){this.setAttribute("id",id);}
if(w){this.setAttribute("width",w);}
if(h){this.setAttribute("height",h);}
if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}
this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion();if(c){this.addParam("bgcolor",c);}
var q=_8?_8:"high";this.addParam("quality",q);this.setAttribute("useExpressInstall",_7);this.setAttribute("doExpressInstall",false);var _d=(_9)?_9:window.location;this.setAttribute("xiRedirectUrl",_d);this.setAttribute("redirectUrl","");if(_a){this.setAttribute("redirectUrl",_a);}};deconcept.SWFObject.prototype={setAttribute:function(_e,_f){this.attributes[_e]=_f;},getAttribute:function(_10){return this.attributes[_10];},addParam:function(_11,_12){this.params[_11]=_12;},getParams:function(){return this.params;},addVariable:function(_13,_14){this.variables[_13]=_14;},getVariable:function(_15){return this.variables[_15];},getVariables:function(){return this.variables;},getVariablePairs:function(){var _16=new Array();var key;var _18=this.getVariables();for(key in _18){_16.push(key+"="+_18[key]);}
return _16;},getSWFHTML:function(){var _19="";if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","PlugIn");}
_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\"";_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";var _1a=this.getParams();for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}
var _1c=this.getVariablePairs().join("&");if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}_19+="/>";}else{if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");}
_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\">";_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";var _1d=this.getParams();for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}
var _1f=this.getVariablePairs().join("&");if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}_19+="</object>";}
return _19;},write:function(_20){if(this.getAttribute("useExpressInstall")){var _21=new deconcept.PlayerVersion([6,0,65]);if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){this.setAttribute("doExpressInstall",true);this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));document.title=document.title.slice(0,47)+" - Flash Player Installation";this.addVariable("MMdoctitle",document.title);}}
if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){var n=(typeof _20=="string")?document.getElementById(_20):_20;n.innerHTML=this.getSWFHTML();return true;}else{if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}
return false;}};deconcept.SWFObjectUtil.getPlayerVersion=function(){var _23=new deconcept.PlayerVersion([0,0,0]);if(navigator.plugins&&navigator.mimeTypes.length){var x=navigator.plugins["Shockwave Flash"];if(x&&x.description){_23=new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}}else{try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");}
catch(e){try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");_23=new deconcept.PlayerVersion([6,0,21]);axo.AllowScriptAccess="always";}
catch(e){if(_23.major==6){return _23;}}try{axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");}
catch(e){}}if(axo!=null){_23=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}}
return _23;};deconcept.PlayerVersion=function(_27){this.major=_27[0]!=null?parseInt(_27[0]):0;this.minor=_27[1]!=null?parseInt(_27[1]):0;this.rev=_27[2]!=null?parseInt(_27[2]):0;};deconcept.PlayerVersion.prototype.versionIsValid=function(fv){if(this.major<fv.major){return false;}
if(this.major>fv.major){return true;}
if(this.minor<fv.minor){return false;}
if(this.minor>fv.minor){return true;}
if(this.rev<fv.rev){return false;}return true;};deconcept.util={getRequestParameter:function(_29){var q=document.location.search||document.location.hash;if(q){var _2b=q.substring(1).split("&");for(var i=0;i<_2b.length;i++){if(_2b[i].substring(0,_2b[i].indexOf("="))==_29){return _2b[i].substring((_2b[i].indexOf("=")+1));}}}
return"";}};deconcept.SWFObjectUtil.cleanupSWFs=function(){if(window.opera||!document.all){return;}
var _2d=document.getElementsByTagName("OBJECT");for(var i=0;i<_2d.length;i++){_2d[i].style.display="none";for(var x in _2d[i]){if(typeof _2d[i][x]=="function"){_2d[i][x]=function(){};}}}};deconcept.SWFObjectUtil.prepUnload=function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){};if(typeof window.onunload=="function"){var _30=window.onunload;window.onunload=function(){deconcept.SWFObjectUtil.cleanupSWFs();_30();};}else{window.onunload=deconcept.SWFObjectUtil.cleanupSWFs;}};if(typeof window.onbeforeunload=="function"){var oldBeforeUnload=window.onbeforeunload;window.onbeforeunload=function(){deconcept.SWFObjectUtil.prepUnload();oldBeforeUnload();};}else{window.onbeforeunload=deconcept.SWFObjectUtil.prepUnload;}
if(Array.prototype.push==null){Array.prototype.push=function(_31){this[this.length]=_31;return this.length;};}
var getQueryParamValue=deconcept.util.getRequestParameter;var FlashObject=deconcept.SWFObject;var SWFObject=deconcept.SWFObject;
function get_formations(e){empty="",jQuery().ajaxSend(function(){jQuery("#loading").show()}),jQuery().ajaxStop(function(){jQuery("#loading").hide()}),jQuery.ajax({url:e,success:function(e){jQuery("#bloc_formations").html(e)},error:function(){jQuery("#loading").hide(),jQuery("#bloc_formations").text()}})}function show(e){(obj=document.getElementById(e))&&(obj.style.display="block")}function hide(e){(obj=document.getElementById(e))&&(obj.style.display="none")}function hideAll(e){targeted_obj=getElementsByClass(e);for(var t=0;t<targeted_obj.length;t++)targeted_obj[t].style.display="none"}function showHide(e){(obj=document.getElementById(e))&&("block"!=obj.style.display?show(e):hide(e))}function no_status(){return window.status="",!0}function addClassName(e,t){e.className=""!=e.className?e.className+" "+t:t}function removeClassName(e,t){var n=new RegExp(" "+t+"|"+t+" |"+t),c=e.className,i=new RegExp(t);i.test(c)&&(e.className=e.className.replace(n,""))}function getElementsByClass(e,t,n){var c=new Array;null==t&&(t=document),null==n&&(n="*");var l=t.getElementsByTagName(n),a=l.length,s=new RegExp("(^|\\s)"+e+"(\\s|$)");for(i=0,j=0;i<a;i++)s.test(l[i].className)&&(c[j]=l[i],j++);return c}function reset_ref_cat(e){targeted_obj=getElementsByClass(e);for(var t=0;t<targeted_obj.length;t++)targeted_obj[t].style.display="none",removeClassName(targeted_obj[t],"current")}function reset_ref_cat_menu(e){targeted_obj=getElementsByClass(e);for(var t=0;t<targeted_obj.length;t++)removeClassName(targeted_obj[t],"current")}function handle_ref_cat(e,t){e?(reset_ref_cat_menu("ref_menu_item"),addClassName(t.parentNode,"current")):(reset_ref_cat("reference_category_block"),reset_ref_cat_menu("ref_menu_item"),addClassName(t.parentNode,"current"),ref_cat_title=document.getElementById("reference_category_title"),ref_cat_title.innerHTML=t.innerHTML,(targeted_obj=document.getElementById(e))&&("block"!=targeted_obj.style.display?targeted_obj.style.display="block":targeted_obj.style.display="none"))}function handle_faq_results_item(e,t){showHide(e),(e=document.getElementById(e))&&("block"!=e.style.display?(removeClassName(t,"expanded"),addClassName(t,"default")):(removeClassName(t,"default"),removeClassName(t,"collpsed"),addClassName(t,"expanded")))}function search_offers_filter(){}function filtersListener(){$("#search_offers_filter_form input").click(function(){updateFilters()}),$("#filter_canceler").click(function(){resetFilters("filter_agence","#agence_0","#filters_agence_callback"),resetFilters("filter_metier","#pole_metier_0","#filters_metier_callback"),resetFilters("filter_contrat","#contrat_0","#filters_contrat_callback")})}function updateFilters(){updateFilterInputs("filter_agence","#agence_0","#filters_agence_callback"),updateFilterInputs("filter_metier","#pole_metier_0","#filters_metier_callback"),updateFilterInputs("filter_contrat","#contrat_0","#filters_contrat_callback"),checkResultsNb()}function updateFilterInputs(e,t,n){total_elements=$("input[name="+e+"]").length,limit=$("input[name="+e+"]:checked").length,limit==total_elements-1&&0==$(t+":checked").length?($("input[name="+e+"]:checked").uncheck(),$(t).check()):($("input[name="+e+"]:checked").not($(t)).each(function(e){$(t).uncheck()}),$(t).click(function(){resetFilters(e,t,n)})),updateFilterCallback(e,t,n),updateFilteredDatasblock(e,t)}function updateFilterCallback(e,t,n){var c="";limit=$("input[name="+e+"]:checked").length,select_all=$(t+":checked").length,$("input[name="+e+"]:checked").each(function(e){c+=e==limit-1||limit<2?this.alt:this.alt+", &nbsp;"}),0==select_all&&0==limit&&("#agence_0"==t?c+="Aucune agence":"#pole_metier_0"==t?c+="Aucun m&eacute;tier":"#contrat_0"==t&&(c+="Aucun contrat")),$(n).html(c)}function updateFilteredDatasblock(e,t){1==$(t+":checked").length?$("input[name="+e+"]").each(function(e){$("."+this.id).show()}):($("input[name="+e+"]:checked").each(function(e){$(".search_offers_results ."+this.id).show("slow"),$(".search_offers_results ."+this.id).css("display","block")}),$("input[name="+e+"]").not($(":checked")).each(function(e){$(".search_offers_results ."+this.id).hide("slow"),$(".search_offers_results ."+this.id).css("display","none")})),$(".carriere-home__search__location-container").each(function(e,t){var n=!1;$(t).find(".lv_02_item").each(function(e,t){return console.log(t),console.log($(t).is(":visible")),$(t).is(":visible")?(n=!0,!1):void 0}),$(t).find(".message_no_result").hide(),0==n&&$(t).find(".message_no_result").show()})}function resetFilters(e,t,n){$("input[name="+e+"]:checked").length>0&&($("input[name="+e+"]").uncheck(),$(t).check()),updateFilteredDatasblock(e,t),updateFilterCallback(e,t,n)}function checkResultsNb(){$(".filtered_results").hide().html(""),$(".search_offers_results .lv_02_item").each(function(){var e=0;$(this).children(".item_block").each(function(){"block"==$(this).css("display")&&e++}),1>e&&$(this).children(".filtered_results").show().html("<em>Aucune offre d&acute;emploi n&acute;est actuellement &agrave; pourvoir pour ce m&eacute;tier </em>")})}function SchoolsRelationItemsHandler(){$(".rte_hidden_full_content").hide(),$(".testimony_block a.read_more").click(function(){parent_item_id=this.parentNode.parentNode.id,"block"==$("#"+parent_item_id+" .rte_hidden_full_content").css("display")?$(this).html("Lire"):$(this).html("Replier"),$("#"+parent_item_id+" .rte_hidden_full_content").slideToggle("slow")})}function showHideMenu(e,t){"block"==document.getElementById(e+t).style.display?document.getElementById(e+t).style.display="none":document.getElementById(e+t).style.display="block"}$(document).ready(function(){if(filtersListener(),$(".container .menu li").mouseover(function(){$(this).addClass("current_item")}),$(".container .menu li").mouseout(function(){$(this).removeClass("current_item")}),0!=jQuery("#bloc_formations").length){var e=jQuery(".redirectLink").attr("href");get_formations(e)}0!=jQuery("#choiseProject").length&&jQuery("#choiseProject").click(function(){$(this).attr("checked")?(jQuery(".choiseProject").css("display","inline"),jQuery(this).attr("value","oui")):(jQuery(".choiseProject").css("display","none"),jQuery(this).attr("value",""))})}),jQuery.fn.extend({check:function(){return this.each(function(){this.checked=!0})},uncheck:function(){return this.each(function(){this.checked=!1})}});
;window.Modernizr=function(a,b,c){function D(a){j.cssText=a}function E(a,b){return D(n.join(a+";")+(b||""))}function F(a,b){return typeof a===b}function G(a,b){return!!~(""+a).indexOf(b)}function H(a,b){for(var d in a)if(j[a[d]]!==c)return b=="pfx"?a[d]:!0;return!1}function I(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:F(f,"function")?f.bind(d||b):f}return!1}function J(a,b,c){var d=a.charAt(0).toUpperCase()+a.substr(1),e=(a+" "+p.join(d+" ")+d).split(" ");return F(b,"string")||F(b,"undefined")?H(e,b):(e=(a+" "+q.join(d+" ")+d).split(" "),I(e,b,c))}function L(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)u[c[d]]=c[d]in k;return u.list&&(u.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),u}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),e.inputtypes=function(a){for(var d=0,e,f,h,i=a.length;d<i;d++)k.setAttribute("type",f=a[d]),e=k.type!=="text",e&&(k.value=l,k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(f)&&k.style.WebkitAppearance!==c?(g.appendChild(k),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(k,null).WebkitAppearance!=="textfield"&&k.offsetHeight!==0,g.removeChild(k)):/^(search|tel)$/.test(f)||(/^(url|email)$/.test(f)?e=k.checkValidity&&k.checkValidity()===!1:/^color$/.test(f)?(g.appendChild(k),g.offsetWidth,e=k.value!=l,g.removeChild(k)):e=k.value!=l)),t[a[d]]=!!e;return t}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.5.3",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k=b.createElement("input"),l=":)",m={}.toString,n=" -webkit- -moz- -o- -ms- ".split(" "),o="Webkit Moz O ms",p=o.split(" "),q=o.toLowerCase().split(" "),r={svg:"http://www.w3.org/2000/svg"},s={},t={},u={},v=[],w=v.slice,x,y=function(a,c,d,e){var f,i,j,k=b.createElement("div"),l=b.body,m=l?l:b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),k.appendChild(j);return f=["&#173;","<style>",a,"</style>"].join(""),k.id=h,m.innerHTML+=f,m.appendChild(k),l||(m.style.background="",g.appendChild(m)),i=c(k,a),l?k.parentNode.removeChild(k):m.parentNode.removeChild(m),!!i},z=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b).matches;var d;return y("@media "+b+" { #"+h+" { position: absolute; } }",function(b){d=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle)["position"]=="absolute"}),d},A=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=F(e[d],"function"),F(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),B={}.hasOwnProperty,C;!F(B,"undefined")&&!F(B.call,"undefined")?C=function(a,b){return B.call(a,b)}:C=function(a,b){return b in a&&F(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=w.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(w.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(w.call(arguments)))};return e});var K=function(c,d){var f=c.join(""),g=d.length;y(f,function(c,d){var f=b.styleSheets[b.styleSheets.length-1],h=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"",i=c.childNodes,j={};while(g--)j[i[g].id]=i[g];e.touch="ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch||(j.touch&&j.touch.offsetTop)===9,e.csstransforms3d=(j.csstransforms3d&&j.csstransforms3d.offsetLeft)===9&&j.csstransforms3d.offsetHeight===3,e.generatedcontent=(j.generatedcontent&&j.generatedcontent.offsetHeight)>=1,e.fontface=/src/i.test(h)&&h.indexOf(d.split(" ")[0])===0},g,d)}(['@font-face {font-family:"font";src:url("https://")}',["@media (",n.join("touch-enabled),("),h,")","{#touch{top:9px;position:absolute}}"].join(""),["@media (",n.join("transform-3d),("),h,")","{#csstransforms3d{left:9px;position:absolute;height:3px;}}"].join(""),['#generatedcontent:after{content:"',l,'";visibility:hidden}'].join("")],["fontface","touch","csstransforms3d","generatedcontent"]);s.flexbox=function(){return J("flexOrder")},s.canvas=function(){var a=b.createElement("canvas");return!!a.getContext&&!!a.getContext("2d")},s.canvastext=function(){return!!e.canvas&&!!F(b.createElement("canvas").getContext("2d").fillText,"function")},s.webgl=function(){try{var d=b.createElement("canvas"),e;e=!(!a.WebGLRenderingContext||!d.getContext("experimental-webgl")&&!d.getContext("webgl")),d=c}catch(f){e=!1}return e},s.touch=function(){return e.touch},s.geolocation=function(){return!!navigator.geolocation},s.postmessage=function(){return!!a.postMessage},s.websqldatabase=function(){return!!a.openDatabase},s.indexedDB=function(){return!!J("indexedDB",a)},s.hashchange=function(){return A("hashchange",a)&&(b.documentMode===c||b.documentMode>7)},s.history=function(){return!!a.history&&!!history.pushState},s.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a},s.websockets=function(){for(var b=-1,c=p.length;++b<c;)if(a[p[b]+"WebSocket"])return!0;return"WebSocket"in a},s.rgba=function(){return D("background-color:rgba(150,255,150,.5)"),G(j.backgroundColor,"rgba")},s.hsla=function(){return D("background-color:hsla(120,40%,100%,.5)"),G(j.backgroundColor,"rgba")||G(j.backgroundColor,"hsla")},s.multiplebgs=function(){return D("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(j.background)},s.backgroundsize=function(){return J("backgroundSize")},s.borderimage=function(){return J("borderImage")},s.borderradius=function(){return J("borderRadius")},s.boxshadow=function(){return J("boxShadow")},s.textshadow=function(){return b.createElement("div").style.textShadow===""},s.opacity=function(){return E("opacity:.55"),/^0.55$/.test(j.opacity)},s.cssanimations=function(){return J("animationName")},s.csscolumns=function(){return J("columnCount")},s.cssgradients=function(){var a="background-image:",b="gradient(linear,left top,right bottom,from(#9f9),to(white));",c="linear-gradient(left top,#9f9, white);";return D((a+"-webkit- ".split(" ").join(b+a)+n.join(c+a)).slice(0,-a.length)),G(j.backgroundImage,"gradient")},s.cssreflections=function(){return J("boxReflect")},s.csstransforms=function(){return!!J("transform")},s.csstransforms3d=function(){var a=!!J("perspective");return a&&"webkitPerspective"in g.style&&(a=e.csstransforms3d),a},s.csstransitions=function(){return J("transition")},s.fontface=function(){return e.fontface},s.generatedcontent=function(){return e.generatedcontent},s.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},s.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},s.localstorage=function(){try{return localStorage.setItem(h,h),localStorage.removeItem(h),!0}catch(a){return!1}},s.sessionstorage=function(){try{return sessionStorage.setItem(h,h),sessionStorage.removeItem(h),!0}catch(a){return!1}},s.webworkers=function(){return!!a.Worker},s.applicationcache=function(){return!!a.applicationCache},s.svg=function(){return!!b.createElementNS&&!!b.createElementNS(r.svg,"svg").createSVGRect},s.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==r.svg},s.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(m.call(b.createElementNS(r.svg,"animate")))},s.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(m.call(b.createElementNS(r.svg,"clipPath")))};for(var M in s)C(s,M)&&(x=M.toLowerCase(),e[x]=s[M](),v.push((e[x]?"":"no-")+x));return e.input||L(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)C(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,g.className+=" "+(b?"":"no-")+a,e[a]=b}return e},D(""),i=k=null,function(a,b){function g(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function h(){var a=k.elements;return typeof a=="string"?a.split(" "):a}function i(a){var b={},c=a.createElement,e=a.createDocumentFragment,f=e();a.createElement=function(a){var e=(b[a]||(b[a]=c(a))).cloneNode();return k.shivMethods&&e.canHaveChildren&&!d.test(a)?f.appendChild(e):e},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+h().join().replace(/\w+/g,function(a){return b[a]=c(a),f.createElement(a),'c("'+a+'")'})+");return n}")(k,f)}function j(a){var b;return a.documentShived?a:(k.shivCSS&&!e&&(b=!!g(a,"article,aside,details,figcaption,figure,footer,header,hgroup,nav,section{display:block}audio{display:none}canvas,video{display:inline-block;*display:inline;*zoom:1}[hidden]{display:none}audio[controls]{display:inline-block;*display:inline;*zoom:1}mark{background:#FF0;color:#000}")),f||(b=!i(a)),b&&(a.documentShived=b),a)}var c=a.html5||{},d=/^<|^(?:button|form|map|select|textarea)$/i,e,f;(function(){var a=b.createElement("a");a.innerHTML="<xyz></xyz>",e="hidden"in a,f=a.childNodes.length==1||function(){try{b.createElement("a")}catch(a){return!0}var c=b.createDocumentFragment();return typeof c.cloneNode=="undefined"||typeof c.createDocumentFragment=="undefined"||typeof c.createElement=="undefined"}()})();var k={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:j};a.html5=k,j(b)}(this,b),e._version=d,e._prefixes=n,e._domPrefixes=q,e._cssomPrefixes=p,e.mq=z,e.hasEvent=A,e.testProp=function(a){return H([a])},e.testAllProps=J,e.testStyles=y,e.prefixed=function(a,b,c){return b?J(a,b,c):J(a,"pfx")},g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+v.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return o.call(a)=="[object Function]"}function e(a){return typeof a=="string"}function f(){}function g(a){return!a||a=="loaded"||a=="complete"||a=="uninitialized"}function h(){var a=p.shift();q=1,a?a.t?m(function(){(a.t=="c"?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){a!="img"&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l={},o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};y[c]===1&&(r=1,y[c]=[],l=b.createElement(a)),a=="object"?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),a!="img"&&(r||y[c]===2?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i(b=="c"?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),p.length==1&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&o.call(a.opera)=="[object Opera]",l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return o.call(a)=="[object Array]"},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,i){var j=b(a),l=j.autoCallback;j.url.split(".").pop().split("?").shift(),j.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]||h),j.instead?j.instead(a,e,f,g,i):(y[j.url]?j.noexec=!0:y[j.url]=1,f.load(j.url,j.forceCSS||!j.forceJS&&"css"==j.url.split(".").pop().split("?").shift()?"c":c,j.noexec,j.attrs,j.timeout),(d(e)||d(l))&&f.load(function(){k(),e&&e(j.origUrl,i,g),l&&l(j.origUrl,i,g),y[j.url]=2})))}function i(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var j,l,m=this.yepnope.loader;if(e(a))g(a,0,m,0);else if(w(a))for(j=0;j<a.length;j++)l=a[j],e(l)?g(l,0,m,0):w(l)?B(l):Object(l)===l&&i(l,m);else Object(a)===a&&i(a,m)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,b.readyState==null&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};
(function(){function h(a,b){return[].slice.call((b||document).querySelectorAll(a))}if(window.addEventListener){var b=window.StyleFix={link:function(a){try{if("stylesheet"!==a.rel||a.hasAttribute("data-noprefix"))return}catch(c){return}var d=a.href||a.getAttribute("data-href"),f=d.replace(/[^\/]+$/,""),g=a.parentNode,e=new XMLHttpRequest;e.open("GET",d);e.onreadystatechange=function(){if(4===e.readyState){var c=e.responseText;if(c&&a.parentNode){c=b.fix(c,!0,a);f&&(c=c.replace(/url\(('?|"?)(.+?)\1\)/gi,
function(a,c,b){return!/^([a-z]{3,10}:|\/|#)/i.test(b)?'url("'+f+b+'")':a}),c=c.replace(RegExp("\\b(behavior:\\s*?url\\('?\"?)"+f,"gi"),"$1"));var d=document.createElement("style");d.textContent=c;d.media=a.media;d.disabled=a.disabled;d.setAttribute("data-href",a.getAttribute("href"));g.insertBefore(d,a);g.removeChild(a)}}};e.send(null);a.setAttribute("data-inprogress","")},styleElement:function(a){var c=a.disabled;a.textContent=b.fix(a.textContent,!0,a);a.disabled=c},styleAttribute:function(a){var c=
a.getAttribute("style"),c=b.fix(c,!1,a);a.setAttribute("style",c)},process:function(){h('link[rel="stylesheet"]:not([data-inprogress])').forEach(StyleFix.link);h("style").forEach(StyleFix.styleElement);h("[style]").forEach(StyleFix.styleAttribute)},register:function(a,c){(b.fixers=b.fixers||[]).splice(void 0===c?b.fixers.length:c,0,a)},fix:function(a,c){for(var d=0;d<b.fixers.length;d++)a=b.fixers[d](a,c)||a;return a},camelCase:function(a){return a.replace(/-([a-z])/g,function(a,b){return b.toUpperCase()}).replace("-",
"")},deCamelCase:function(a){return a.replace(/[A-Z]/g,function(a){return"-"+a.toLowerCase()})}};(function(){setTimeout(function(){h('link[rel="stylesheet"]').forEach(StyleFix.link)},10);document.addEventListener("DOMContentLoaded",StyleFix.process,!1)})()}})();
(function(h){if(window.StyleFix&&window.getComputedStyle){var b=window.PrefixFree={prefixCSS:function(a,c){function d(c,d,f,g){c=b[c];c.length&&(c=RegExp(d+"("+c.join("|")+")"+f,"gi"),a=a.replace(c,g))}var f=b.prefix;d("functions","(\\s|:|,)","\\s*\\(","$1"+f+"$2(");d("keywords","(\\s|:)","(\\s|;|\\}|$)","$1"+f+"$2$3");d("properties","(^|\\{|\\s|;)","\\s*:","$1"+f+"$2:");if(b.properties.length){var g=RegExp("\\b("+b.properties.join("|")+")(?!:)","gi");d("valueProperties","\\b",":(.+?);",function(a){return a.replace(g,
f+"$1")})}c&&(d("selectors","","\\b",b.prefixSelector),d("atrules","@","\\b","@"+f+"$1"));return a=a.replace(RegExp("-"+f,"g"),"-")},prefixSelector:function(a){return a.replace(/^:{1,2}/,function(a){return a+b.prefix})},prefixProperty:function(a,c){var d=b.prefix+a;return c?StyleFix.camelCase(d):d}};(function(){var a={},c=[],d=getComputedStyle(document.documentElement,null),f=document.createElement("div").style,g=function(b){if("-"===b.charAt(0)){c.push(b);var b=b.split("-"),d=b[1];for(a[d]=++a[d]||
1;3<b.length;)b.pop(),d=b.join("-"),StyleFix.camelCase(d)in f&&-1===c.indexOf(d)&&c.push(d)}};if(0<d.length)for(var e=0;e<d.length;e++)g(d[e]);else for(var i in d)g(StyleFix.deCamelCase(i));var e=0,j,h;for(h in a)d=a[h],e<d&&(j=h,e=d);b.prefix="-"+j+"-";b.Prefix=StyleFix.camelCase(b.prefix);b.properties=[];for(e=0;e<c.length;e++)i=c[e],0===i.indexOf(b.prefix)&&(j=i.slice(b.prefix.length),StyleFix.camelCase(j)in f||b.properties.push(j));"Ms"==b.Prefix&&!("transform"in f)&&!("MsTransform"in f)&&"msTransform"in
f&&b.properties.push("transform","transform-origin");b.properties.sort()})();(function(){function a(a,b){f[b]="";f[b]=a;return!!f[b]}var c={"linear-gradient":{property:"backgroundImage",params:"red, teal"},calc:{property:"width",params:"1px + 5%"},element:{property:"backgroundImage",params:"#foo"}};c["repeating-linear-gradient"]=c["repeating-radial-gradient"]=c["radial-gradient"]=c["linear-gradient"];var d={initial:"color","zoom-in":"cursor","zoom-out":"cursor",box:"display",flexbox:"display","inline-flexbox":"display"};
b.functions=[];b.keywords=[];var f=document.createElement("div").style,g;for(g in c){var e=c[g],i=e.property,e=g+"("+e.params+")";!a(e,i)&&a(b.prefix+e,i)&&b.functions.push(g)}for(var h in d)i=d[h],!a(h,i)&&a(b.prefix+h,i)&&b.keywords.push(h)})();(function(){function a(a){f.textContent=a+"{}";return!!f.sheet.cssRules.length}var c={":read-only":null,":read-write":null,":any-link":null,"::selection":null},d={keyframes:"name",viewport:null,document:'regexp(".")'};b.selectors=[];b.atrules=[];var f=h.appendChild(document.createElement("style")),
g;for(g in c){var e=g+(c[g]?"("+c[g]+")":"");!a(e)&&a(b.prefixSelector(e))&&b.selectors.push(g)}for(var i in d)e=i+" "+(d[i]||""),!a("@"+e)&&a("@"+b.prefix+e)&&b.atrules.push(i);h.removeChild(f)})();b.valueProperties=["transition","transition-property"];h.className+=" "+b.prefix;StyleFix.register(b.prefixCSS)}})(document.documentElement);
jQuery.easing['jswing'] = jQuery.easing['swing'];
jQuery.extend( jQuery.easing,
{
def: 'easeOutQuad',
swing: function (x, t, b, c, d) {
return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
},
easeInQuad: function (x, t, b, c, d) {
return c*(t/=d)*t + b;
},
easeOutQuad: function (x, t, b, c, d) {
return -c *(t/=d)*(t-2) + b;
},
easeInOutQuad: function (x, t, b, c, d) {
if ((t/=d/2) < 1) return c/2*t*t + b;
return -c/2 * ((--t)*(t-2) - 1) + b;
},
easeInCubic: function (x, t, b, c, d) {
return c*(t/=d)*t*t + b;
},
easeOutCubic: function (x, t, b, c, d) {
return c*((t=t/d-1)*t*t + 1) + b;
},
easeInOutCubic: function (x, t, b, c, d) {
if ((t/=d/2) < 1) return c/2*t*t*t + b;
return c/2*((t-=2)*t*t + 2) + b;
},
easeInQuart: function (x, t, b, c, d) {
return c*(t/=d)*t*t*t + b;
},
easeOutQuart: function (x, t, b, c, d) {
return -c * ((t=t/d-1)*t*t*t - 1) + b;
},
easeInOutQuart: function (x, t, b, c, d) {
if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
return -c/2 * ((t-=2)*t*t*t - 2) + b;
},
easeInQuint: function (x, t, b, c, d) {
return c*(t/=d)*t*t*t*t + b;
},
easeOutQuint: function (x, t, b, c, d) {
return c*((t=t/d-1)*t*t*t*t + 1) + b;
},
easeInOutQuint: function (x, t, b, c, d) {
if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
return c/2*((t-=2)*t*t*t*t + 2) + b;
},
easeInSine: function (x, t, b, c, d) {
return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
},
easeOutSine: function (x, t, b, c, d) {
return c * Math.sin(t/d * (Math.PI/2)) + b;
},
easeInOutSine: function (x, t, b, c, d) {
return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
},
easeInExpo: function (x, t, b, c, d) {
return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
},
easeOutExpo: function (x, t, b, c, d) {
return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
},
easeInOutExpo: function (x, t, b, c, d) {
if (t==0) return b;
if (t==d) return b+c;
if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
},
easeInCirc: function (x, t, b, c, d) {
return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
},
easeOutCirc: function (x, t, b, c, d) {
return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
},
easeInOutCirc: function (x, t, b, c, d) {
if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
},
easeInElastic: function (x, t, b, c, d) {
var s=1.70158;var p=0;var a=c;
if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
if (a < Math.abs(c)) { a=c; var s=p/4; }
else var s = p/(2*Math.PI) * Math.asin (c/a);
return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
},
easeOutElastic: function (x, t, b, c, d) {
var s=1.70158;var p=0;var a=c;
if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
if (a < Math.abs(c)) { a=c; var s=p/4; }
else var s = p/(2*Math.PI) * Math.asin (c/a);
return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
},
easeInOutElastic: function (x, t, b, c, d) {
var s=1.70158;var p=0;var a=c;
if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
if (a < Math.abs(c)) { a=c; var s=p/4; }
else var s = p/(2*Math.PI) * Math.asin (c/a);
if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
},
easeInBack: function (x, t, b, c, d, s) {
if (s == undefined) s = 1.70158;
return c*(t/=d)*t*((s+1)*t - s) + b;
},
easeOutBack: function (x, t, b, c, d, s) {
if (s == undefined) s = 1.70158;
return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
},
easeInOutBack: function (x, t, b, c, d, s) {
if (s == undefined) s = 1.70158;
if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
},
easeInBounce: function (x, t, b, c, d) {
return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
},
easeOutBounce: function (x, t, b, c, d) {
if ((t/=d) < (1/2.75)) {
return c*(7.5625*t*t) + b;
} else if (t < (2/2.75)) {
return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
} else if (t < (2.5/2.75)) {
return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
} else {
return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
}
},
easeInOutBounce: function (x, t, b, c, d) {
if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
}
});
(function($) {
var types = ['DOMMouseScroll', 'mousewheel'];
$.event.special.mousewheel = {
setup: function() {
if ( this.addEventListener ) {
for ( var i=types.length; i; ) {
this.addEventListener( types[--i], handler, false );
}
} else {
this.onmousewheel = handler;
}
},
teardown: function() {
if ( this.removeEventListener ) {
for ( var i=types.length; i; ) {
this.removeEventListener( types[--i], handler, false );
}
} else {
this.onmousewheel = null;
}
}
};
$.fn.extend({
mousewheel: function(fn) {
return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
},
unmousewheel: function(fn) {
return this.unbind("mousewheel", fn);
}
});
function handler(event) {
var orgEvent = event || window.event, args = [].slice.call( arguments, 1 ), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
event = $.event.fix(orgEvent);
event.type = "mousewheel";
if ( event.wheelDelta ) { delta = event.wheelDelta/120; }
if ( event.detail     ) { delta = -event.detail/3; }
deltaY = delta;
if ( orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
deltaY = 0;
deltaX = -1*delta;
}
if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY/120; }
if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = -1*orgEvent.wheelDeltaX/120; }
args.unshift(event, delta, deltaX, deltaY);
return $.event.handle.apply(this, args);
}
})(jQuery);
(function($) {
var cache	= {
idx_expanded	: -1, // the index of the current expanded slice
sliceH			: 0,  // the default slice's height
current			: 0,  // controls the current slider position
totalSlices		: 0	  // total number of slices
},
aux		= {
selectSlice		: function( $el, $slices, $navNext, $navPrev, settings ) {
return $.Deferred(
function( dfd ) {
var	expanded	= $el.data('expanded'),
pos			= $el.data('position'),
itemHeight, othersHeight,
$others		= $slices.not( $el );
if( expanded ) {
$el.data( 'expanded', false );
cache.idx_expanded	= -1;
itemHeight	= cache.sliceH;
othersHeight= cache.sliceH;
$el.find('.va-content').hide();
if( aux.canSlideUp( $slices, settings ) )
$navPrev.fadeIn();
else
$navPrev.fadeOut();
if( aux.canSlideDown( $slices, settings ) )
$navNext.fadeIn();
else
$navNext.fadeOut();
}
else {
$el.data( 'expanded', true );
cache.idx_expanded	= $el.index();
$others.data( 'expanded', false );
itemHeight	= settings.expandedHeight;
othersHeight= Math.ceil( ( settings.accordionH - settings.expandedHeight ) / ( settings.visibleSlices - 1 ) );
if( cache.idx_expanded > 0 )
$navPrev.fadeIn();
else
$navPrev.fadeOut();
if( cache.idx_expanded < cache.totalSlices - 1 )
$navNext.fadeIn();
else
$navNext.fadeOut();
}
var	animParam	= {
height	: itemHeight + 'px',
opacity : 1,
top		: ( pos - 1 ) * othersHeight + 'px'
};
$el.stop()
.animate( animParam, settings.animSpeed, settings.animEasing, function() {
if( !expanded )
$el.find('.va-content').fadeIn( settings.contentAnimSpeed );
});
$others.each(function(i){
var $other	= $(this),
posother= $other.data('position'),
t;
if( expanded )
t	= ( posother - 1 ) * othersHeight ;
else {
if( posother < pos )
t	= ( posother - 1 ) * othersHeight ;
else
t	= ( ( posother - 2 ) * othersHeight ) + settings.expandedHeight;
}
$other.stop()
.animate( {
top		: t + 'px',
height	: othersHeight + 'px',
opacity	: ( expanded ) ? 1 : settings.animOpacity
}, settings.animSpeed, settings.animEasing, dfd.resolve )
.find('.va-title')
.stop()
.animate({
}, settings.animSpeed, settings.animEasing )
.end()
.find('.va-content')
.hide();
});
}
).promise();
},
navigate		: function( dir, $slices, $navNext, $navPrev, settings ) {
if( $slices.is(':animated') )
return false;
var $el;
if( cache.idx_expanded != -1 && !settings.savePositions ) {
$el	= $slices.eq( cache.idx_expanded );
$.when( aux.selectSlice( $el, $slices, $navNext, $navPrev, settings ) ).done(function(){
setTimeout(function() {
aux.slide( dir, $slices, $navNext, $navPrev, settings );
}, 10);
});
}
else {
aux.slide( dir, $slices, $navNext, $navPrev, settings );
}
},
slide			: function( dir, $slices, $navNext, $navPrev, settings ) {
if( cache.idx_expanded === -1 || !settings.savePositions ) {
if( dir === 1 && cache.current + settings.visibleSlices >= cache.totalSlices )
return false;
else if( dir === -1 && cache.current === 0 )
return false;
if( dir === -1 && cache.current === 1 )
$navPrev.fadeOut();
else
$navPrev.fadeIn();
if( dir === 1 && cache.current + settings.visibleSlices === cache.totalSlices - 1 )
$navNext.fadeOut();
else
$navNext.fadeIn();
}
else {
if( dir === 1 && cache.idx_expanded === cache.totalSlices - 1 )
return false;
else if( dir === -1 && cache.idx_expanded === 0 )
return false;
if( dir === -1 && cache.idx_expanded === 1 )
$navPrev.fadeOut();
else
$navPrev.fadeIn();
if( dir === 1 && cache.idx_expanded === cache.totalSlices - 2 )
$navNext.fadeOut();
else
$navNext.fadeIn();
}
var $currentSlice	= $slices.eq( cache.idx_expanded ),
$nextSlice,
t;
( dir === 1 ) ? $nextSlice = $currentSlice.next() : $nextSlice = $currentSlice.prev();
if( ( dir === 1 && !aux.canSlideDown( $slices, settings ) ) ||
( dir === -1 && !aux.canSlideUp( $slices, settings ) ) ) {
aux.selectSlice( $nextSlice, $slices, $navNext, $navPrev, settings );
return false;
}
if( dir === 1 ) {
cache.current++;
t = '-=' + cache.sliceH;
pos_increment	= -1;
}
else {
cache.current--;
t = '+=' + cache.sliceH;
pos_increment	= 1;
}
$slices.each(function(i) {
var $slice		= $(this),
pos			= $slice.data('position');
if( !settings.savePositions || cache.idx_expanded === -1 )
$slice.stop().animate({top : t}, settings.animSpeed, settings.animEasing);
else {
var itemHeight, othersHeight;
if( i === $nextSlice.index() ) {
$slice.data( 'expanded', true );
cache.idx_expanded	= $slice.index();
itemHeight			= settings.expandedHeight;
othersHeight		= ( settings.accordionH - settings.expandedHeight ) / ( settings.visibleSlices - 1 );
$slice.stop()
.animate({
height		: itemHeight + 'px',
opacity 	: 1,
top			: ( dir === 1 ) ? ( pos - 2 ) * othersHeight + 'px' : pos * othersHeight + 'px'
}, settings.animSpeed, settings.animEasing, function() {
$slice.find('.va-content').fadeIn( settings.contentAnimSpeed );
})
.find('.va-title')
.stop()
.animate({
}, settings.animSpeed, settings.animEasing );
}
else if( $slice.data('expanded') ){
$slice.data( 'expanded', false );
othersHeight		= ( settings.accordionH - settings.expandedHeight ) / ( settings.visibleSlices - 1 );
$slice.stop()
.animate({
height	: othersHeight + 'px',
opacity : settings.animOpacity,
top		: ( dir === 1 ) ? '-=' + othersHeight : '+=' + settings.expandedHeight
}, settings.animSpeed, settings.animEasing )
.find('.va-title')
.stop()
.animate({
}, settings.animSpeed, settings.animEasing )
.end()
.find('.va-content')
.hide();
}
else {
$slice.data( 'expanded', false );
othersHeight		= ( settings.accordionH - settings.expandedHeight ) / ( settings.visibleSlices - 1 );
$slice.stop()
.animate({
top		: ( dir === 1 ) ? '-=' + othersHeight : '+=' + othersHeight
}, settings.animSpeed, settings.animEasing );
}
}
$slice.data().position += pos_increment;
});
},
canSlideUp		: function( $slices, settings ) {
var $first			= $slices.eq( cache.current );
if( $first.index() !== 0 )
return true;
},
canSlideDown	: function( $slices, settings ) {
var $last			= $slices.eq( cache.current + settings.visibleSlices - 1 );
if( $last.index() !== cache.totalSlices - 1 )
return true;
}
},
methods = {
init 		: function( options ) {
if( this.length ) {
var settings = {
accordionW		: 535,
visibleSlices	: 3,
animSpeed		: 250,
animEasing		: 'jswing',
animOpacity		: 0.2,
contentAnimSpeed: 900,
savePositions	: true
};
return this.each(function() {
if ( options ) {
$.extend( settings, options );
}
var $el 			= $(this),
$slices			= $el.find('div.va-slice'),
$navNext		= $el.find('span.va-nav-next'),
$navPrev		= $el.find('span.va-nav-prev');
cache.sliceH		= Math.ceil( settings.accordionH / settings.visibleSlices );
cache.totalSlices	= $slices.length;
if( settings.expandedHeight > settings.accordionH )
settings.expandedHeight = settings.accordionH;
else if( settings.expandedHeight <= cache.sliceH )
settings.expandedHeight = cache.sliceH + 50; // give it a minimum
$el.css({
width	: settings.accordionW + 'px',
height	: settings.accordionH + 'px'
});
if( settings.visibleSlices < cache.totalSlices  )
$navNext.show();
$slices.each(function(i){
var $slice	= $(this);
$slice.css({
top		: i * cache.sliceH + 'px',
height	: cache.sliceH + 'px'
}).data( 'position', (i + 1) );
});
$slices.bind('click.vaccordion', function(e) {
if( settings.visibleSlices > 1 ) {
var $el			= $(this);
aux.selectSlice( $el, $slices, $navNext, $navPrev, settings );
}
});
$navNext.bind('click.vaccordion', function(e){
aux.navigate( 1, $slices, $navNext, $navPrev, settings );
});
$navPrev.bind('click.vaccordion', function(e){
aux.navigate( -1, $slices, $navNext, $navPrev, settings );
});
$el.bind('mousewheel.vaccordion', function(e, delta) {
if(delta > 0) {
aux.navigate( -1, $slices, $navNext, $navPrev, settings );
}
else {
aux.navigate( 1, $slices, $navNext, $navPrev, settings );
}
return false;
});
});
}
}
};
$.fn.vaccordion = function(method) {
if ( methods[method] ) {
return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
} else if ( typeof method === 'object' || ! method ) {
return methods.init.apply( this, arguments );
} else {
$.error( 'Method ' +  method + ' does not exist on jQuery.vaccordion' );
}
};
})(jQuery);
(function(){var dep={"jQuery":"http://code.jquery.com/jquery-latest.min.js"};var init=function(){(function($){$.fn.idTabs=function(){var s={};for(var i=0;i<arguments.length;++i){var a=arguments[i];switch(a.constructor){case Object:$.extend(s,a);break;case Boolean:s.change=a;break;case Number:s.start=a;break;case Function:s.click=a;break;case String:if(a.charAt(0)=='.')s.selected=a;else if(a.charAt(0)=='!')s.event=a;else s.start=a;break;}}
if(typeof s['return']=="function")
s.change=s['return'];return this.each(function(){$.idTabs(this,s);});}
$.idTabs=function(tabs,options){var meta=($.metadata)?$(tabs).metadata():{};var s=$.extend({},$.idTabs.settings,meta,options);if(s.selected.charAt(0)=='.')s.selected=s.selected.substr(1);if(s.event.charAt(0)=='!')s.event=s.event.substr(1);if(s.start==null)s.start=-1;var showId=function(){if($(this).is('.'+s.selected))
return s.change;var id="#"+this.href.split('#')[1];var aList=[];var idList=[];$("a",tabs).each(function(){if(this.href.match(/#/)){aList.push(this);idList.push("#"+this.href.split('#')[1]);}});if(s.click&&!s.click.apply(this,[id,idList,tabs,s]))return s.change;for(i in aList)$(aList[i]).removeClass(s.selected);for(i in idList)$(idList[i]).hide();$(this).addClass(s.selected);$(id).show();return s.change;}
var list=$("a[href*='#']",tabs).unbind(s.event,showId).bind(s.event,showId);list.each(function(){$("#"+this.href.split('#')[1]).hide();});var test=false;if((test=list.filter('.'+s.selected)).length);else if(typeof s.start=="number"&&(test=list.eq(s.start)).length);else if(typeof s.start=="string"&&(test=list.filter("[href*='#"+s.start+"']")).length);if(test){test.removeClass(s.selected);test.trigger(s.event);}
return s;}
$.idTabs.settings={start:0,change:false,click:null,selected:".selected",event:"!click"};$.idTabs.version="2.2";$(function(){$(".idTabs").idTabs();});})(jQuery);}
var check=function(o,s){s=s.split('.');while(o&&s.length)o=o[s.shift()];return o;}
var head=document.getElementsByTagName("head")[0];var add=function(url){var s=document.createElement("script");s.type="text/javascript";s.src=url;head.appendChild(s);}
var s=document.getElementsByTagName('script');var src=s[s.length-1].src;var ok=true;for(d in dep){if(check(this,d))continue;ok=false;add(dep[d]);}if(ok)return init();add(src);})();
(function ($, window) {
var
defaults = {
transition: "elastic",
speed: 300,
width: false,
initialWidth: "600",
innerWidth: false,
maxWidth: false,
height: false,
initialHeight: "450",
innerHeight: false,
maxHeight: false,
scalePhotos: true,
scrolling: true,
inline: false,
html: false,
iframe: false,
photo: false,
href: false,
title: false,
rel: false,
opacity: 0.9,
preloading: true,
current: "image {current} of {total}",
previous: "previous",
next: "next",
close: "close",
open: false,
returnFocus: true,
loop: true,
slideshow: false,
slideshowAuto: true,
slideshowSpeed: 2500,
slideshowStart: "start slideshow",
slideshowStop: "stop slideshow",
onOpen: false,
onLoad: false,
onComplete: false,
onCleanup: false,
onClosed: false,
overlayClose: true,
escKey: true,
arrowKey: true
},
colorbox = 'colorbox',
prefix = 'cbox',
event_open = prefix + '_open',
event_load = prefix + '_load',
event_complete = prefix + '_complete',
event_cleanup = prefix + '_cleanup',
event_closed = prefix + '_closed',
event_purge = prefix + '_purge',
event_loaded = prefix + '_loaded',
isIE = $.browser.msie && !$.support.opacity, // feature detection alone gave a false positive on at least one phone browser and on some development versions of Chrome.
isIE6 = isIE && $.browser.version < 7,
event_ie6 = prefix + '_IE6',
$overlay,
$box,
$wrap,
$content,
$topBorder,
$leftBorder,
$rightBorder,
$bottomBorder,
$related,
$window,
$loaded,
$loadingBay,
$loadingOverlay,
$title,
$current,
$slideshow,
$next,
$prev,
$close,
interfaceHeight,
interfaceWidth,
loadedHeight,
loadedWidth,
element,
index,
settings,
open,
active,
closing = false,
publicMethod,
boxElement = prefix + 'Element';
function $div(id, css) {
id = id ? ' id="' + prefix + id + '"' : '';
css = css ? ' style="' + css + '"' : '';
return $('<div' + id + css + '/>');
}
function setSize(size, dimension) {
dimension = dimension === 'x' ? $window.width() : $window.height();
return (typeof size === 'string') ? Math.round((/%/.test(size) ? (dimension / 100) * parseInt(size, 10) : parseInt(size, 10))) : size;
}
function isImage(url) {
return settings.photo || /\.(gif|png|jpg|jpeg|bmp)(?:\?([^#]*))?(?:#(\.*))?$/i.test(url);
}
function process(settings) {
for (var i in settings) {
if ($.isFunction(settings[i]) && i.substring(0, 2) !== 'on') { // checks to make sure the function isn't one of the callbacks, they will be handled at the appropriate time.
settings[i] = settings[i].call(element);
}
}
settings.rel = settings.rel || element.rel || 'nofollow';
settings.href = settings.href || $(element).attr('href');
settings.title = settings.title || element.title;
return settings;
}
function trigger(event, callback) {
if (callback) {
callback.call(element);
}
$.event.trigger(event);
}
function slideshow() {
var
timeOut,
className = prefix + "Slideshow_",
click = "click." + prefix,
start,
stop,
clear;
if (settings.slideshow && $related[1]) {
start = function () {
$slideshow
.text(settings.slideshowStop)
.unbind(click)
.bind(event_complete, function () {
if (index < $related.length - 1 || settings.loop) {
timeOut = setTimeout(publicMethod.next, settings.slideshowSpeed);
}
})
.bind(event_load, function () {
clearTimeout(timeOut);
})
.one(click + ' ' + event_cleanup, stop);
$box.removeClass(className + "off").addClass(className + "on");
timeOut = setTimeout(publicMethod.next, settings.slideshowSpeed);
};
stop = function () {
clearTimeout(timeOut);
$slideshow
.text(settings.slideshowStart)
.unbind([event_complete, event_load, event_cleanup, click].join(' '))
.one(click, start);
$box.removeClass(className + "on").addClass(className + "off");
};
if (settings.slideshowAuto) {
start();
} else {
stop();
}
}
}
function launch(elem) {
if (!closing) {
element = elem;
settings = process($.extend({}, $.data(element, colorbox)));
$related = $(element);
index = 0;
if (settings.rel !== 'nofollow') {
$related = $('.' + boxElement).filter(function () {
var relRelated = $.data(this, colorbox).rel || this.rel;
return (relRelated === settings.rel);
});
index = $related.index(element);
if (index === -1) {
$related = $related.add(element);
index = $related.length - 1;
}
}
if (!open) {
open = active = true; // Prevents the page-change action from queuing up if the visitor holds down the left or right keys.
$box.show();
if (settings.returnFocus) {
try {
element.blur();
$(element).one(event_closed, function () {
try {
this.focus();
} catch (e) {
}
});
} catch (e) {
}
}
$overlay.css({"opacity": +settings.opacity, "cursor": settings.overlayClose ? "pointer" : "auto"}).show();
settings.w = setSize(settings.initialWidth, 'x');
settings.h = setSize(settings.initialHeight, 'y');
publicMethod.position(0);
if (isIE6) {
$window.bind('resize.' + event_ie6 + ' scroll.' + event_ie6, function () {
$overlay.css({width: $window.width(), height: $window.height(), top: $window.scrollTop(), left: $window.scrollLeft()});
}).trigger('scroll.' + event_ie6);
}
trigger(event_open, settings.onOpen);
$current.add($prev).add($next).add($slideshow).add($title).hide();
$close.html(settings.close).show();
}
publicMethod.load(true);
}
}
publicMethod = $.fn[colorbox] = $[colorbox] = function (options, callback) {
var $this = this, autoOpen;
if (!$this[0] && $this.selector) { // if a selector was given and it didn't match any elements, go ahead and exit.
return $this;
}
options = options || {};
if (callback) {
options.onComplete = callback;
}
if (!$this[0] || $this.selector === undefined) { // detects $.colorbox() and $.fn.colorbox()
$this = $('<a/>');
options.open = true; // assume an immediate open
}
$this.each(function () {
$.data(this, colorbox, $.extend({}, $.data(this, colorbox) || defaults, options));
$(this).addClass(boxElement);
});
autoOpen = options.open;
if ($.isFunction(autoOpen)) {
autoOpen = autoOpen.call($this);
}
if (autoOpen) {
launch($this[0]);
}
return $this;
};
publicMethod.init = function () {
$window = $(window);
$box = $div().attr({id: colorbox, 'class': isIE ? prefix + 'IE' : ''});
$overlay = $div("Overlay", isIE6 ? 'position:absolute' : '').hide();
$wrap = $div("Wrapper");
$content = $div("Content").append(
$loaded = $div("LoadedContent", 'width:0; height:0; overflow:hidden'),
$loadingOverlay = $div("LoadingOverlay").add($div("LoadingGraphic")),
$title = $div("Title"),
$current = $div("Current"),
$next = $div("Next"),
$prev = $div("Previous"),
$slideshow = $div("Slideshow").bind(event_open, slideshow),
$close = $div("Close")
);
$wrap.append( // The 3x3 Grid that makes up ColorBox
$div().append(
$div("TopLeft"),
$topBorder = $div("TopCenter"),
$div("TopRight")
),
$div(false, 'clear:left').append(
$leftBorder = $div("MiddleLeft"),
$content,
$rightBorder = $div("MiddleRight")
),
$div(false, 'clear:left').append(
$div("BottomLeft"),
$bottomBorder = $div("BottomCenter"),
$div("BottomRight")
)
).children().children().css({'float': 'left'});
$loadingBay = $div(false, 'position:absolute; width:9999px; visibility:hidden; display:none');
$('body').prepend($overlay, $box.append($wrap, $loadingBay));
$content.children()
.hover(function () {
$(this).addClass('hover');
}, function () {
$(this).removeClass('hover');
}).addClass('hover');
interfaceHeight = $topBorder.height() + $bottomBorder.height() + $content.outerHeight(true) - $content.height();//Subtraction needed for IE6
interfaceWidth = $leftBorder.width() + $rightBorder.width() + $content.outerWidth(true) - $content.width();
loadedHeight = $loaded.outerHeight(true);
loadedWidth = $loaded.outerWidth(true);
$box.css({"padding-bottom": interfaceHeight, "padding-right": interfaceWidth}).hide();
$next.click(publicMethod.next);
$prev.click(publicMethod.prev);
$close.click(publicMethod.close);
$content.children().removeClass('hover');
$('.' + boxElement).live('click', function (e) {
if (!((e.button !== 0 && typeof e.button !== 'undefined') || e.ctrlKey || e.shiftKey || e.altKey)) {
e.preventDefault();
launch(this);
}
});
$overlay.click(function () {
if (settings.overlayClose) {
publicMethod.close();
}
});
$(document).bind("keydown", function (e) {
if (open && settings.escKey && e.keyCode === 27) {
e.preventDefault();
publicMethod.close();
}
if (open && settings.arrowKey && !active && $related[1]) {
if (e.keyCode === 37 && (index || settings.loop)) {
e.preventDefault();
$prev.click();
} else if (e.keyCode === 39 && (index < $related.length - 1 || settings.loop)) {
e.preventDefault();
$next.click();
}
}
});
};
publicMethod.remove = function () {
$box.add($overlay).remove();
$('.' + boxElement).die('click').removeData(colorbox).removeClass(boxElement);
};
publicMethod.position = function (speed, loadedCallback) {
var
animate_speed,
posTop = Math.max(document.documentElement.clientHeight - settings.h - loadedHeight - interfaceHeight, 0) / 2 + $window.scrollTop(),
posLeft = Math.max($window.width() - settings.w - loadedWidth - interfaceWidth, 0) / 2 + $window.scrollLeft();
animate_speed = ($box.width() === settings.w + loadedWidth && $box.height() === settings.h + loadedHeight) ? 0 : speed;
$wrap[0].style.width = $wrap[0].style.height = "9999px";
function modalDimensions(that) {
$topBorder[0].style.width = $bottomBorder[0].style.width = $content[0].style.width = that.style.width;
$loadingOverlay[0].style.height = $loadingOverlay[1].style.height = $content[0].style.height = $leftBorder[0].style.height = $rightBorder[0].style.height = that.style.height;
}
$box.dequeue().animate({width: settings.w + loadedWidth, height: settings.h + loadedHeight, top: posTop, left: posLeft}, {
duration: animate_speed,
complete: function () {
modalDimensions(this);
active = false;
$wrap[0].style.width = (settings.w + loadedWidth + interfaceWidth) + "px";
$wrap[0].style.height = (settings.h + loadedHeight + interfaceHeight) + "px";
if (loadedCallback) {
loadedCallback();
}
},
step: function () {
modalDimensions(this);
}
});
};
publicMethod.resize = function (options) {
if (open) {
options = options || {};
if (options.width) {
settings.w = setSize(options.width, 'x') - loadedWidth - interfaceWidth;
}
if (options.innerWidth) {
settings.w = setSize(options.innerWidth, 'x');
}
$loaded.css({width: settings.w});
if (options.height) {
settings.h = setSize(options.height, 'y') - loadedHeight - interfaceHeight;
}
if (options.innerHeight) {
settings.h = setSize(options.innerHeight, 'y');
}
if (!options.innerHeight && !options.height) {
var $child = $loaded.wrapInner("<div style='overflow:auto'></div>").children(); // temporary wrapper to get an accurate estimate of just how high the total content should be.
settings.h = $child.height();
$child.replaceWith($child.children()); // ditch the temporary wrapper div used in height calculation
}
$loaded.css({height: settings.h});
publicMethod.position(settings.transition === "none" ? 0 : settings.speed);
}
};
publicMethod.prep = function (object) {
if (!open) {
return;
}
var photo,
speed = settings.transition === "none" ? 0 : settings.speed;
$window.unbind('resize.' + prefix);
$loaded.remove();
$loaded = $div('LoadedContent').html(object);
function getWidth() {
settings.w = settings.w || $loaded.width();
settings.w = settings.mw && settings.mw < settings.w ? settings.mw : settings.w;
return settings.w;
}
function getHeight() {
settings.h = settings.h || $loaded.height();
settings.h = settings.mh && settings.mh < settings.h ? settings.mh : settings.h;
return settings.h;
}
$loaded.hide()
.appendTo($loadingBay.show())// content has to be appended to the DOM for accurate size calculations.
.css({width: getWidth(), overflow: settings.scrolling ? 'auto' : 'hidden'})
.css({height: getHeight()})// sets the height independently from the width in case the new width influences the value of height.
.prependTo($content);
$loadingBay.hide();
$('#' + prefix + 'Photo').css({cssFloat: 'none', marginLeft: 'auto', marginRight: 'auto'});
if (isIE6) {
$('select').not($box.find('select')).filter(function () {
return this.style.visibility !== 'hidden';
}).css({'visibility': 'hidden'}).one(event_cleanup, function () {
this.style.visibility = 'inherit';
});
}
function setPosition(s) {
var prev, prevSrc, next, nextSrc, total = $related.length, loop = settings.loop;
publicMethod.position(s, function () {
function defilter() {
if (isIE) {
$box[0].style.removeAttribute("filter");
}
}
if (!open) {
return;
}
if (isIE) {
if (photo) {
$loaded.fadeIn(100);
}
}
$loaded.show();
trigger(event_loaded);
$title.show().html(settings.title);
if (total > 1) { // handle grouping
if (typeof settings.current === "string") {
$current.html(settings.current.replace(/\{current\}/, index + 1).replace(/\{total\}/, total)).show();
}
$next[(loop || index < total - 1) ? "show" : "hide"]().html(settings.next);
$prev[(loop || index) ? "show" : "hide"]().html(settings.previous);
prev = index ? $related[index - 1] : $related[total - 1];
next = index < total - 1 ? $related[index + 1] : $related[0];
if (settings.slideshow) {
$slideshow.show();
}
if (settings.preloading) {
nextSrc = $.data(next, colorbox).href || next.href;
prevSrc = $.data(prev, colorbox).href || prev.href;
nextSrc = $.isFunction(nextSrc) ? nextSrc.call(next) : nextSrc;
prevSrc = $.isFunction(prevSrc) ? prevSrc.call(prev) : prevSrc;
if (isImage(nextSrc)) {
$('<img/>')[0].src = nextSrc;
}
if (isImage(prevSrc)) {
$('<img/>')[0].src = prevSrc;
}
}
}
$loadingOverlay.hide();
if (settings.transition === 'fade') {
$box.fadeTo(speed, 1, function () {
defilter();
});
} else {
defilter();
}
$window.bind('resize.' + prefix, function () {
publicMethod.position(0);
});
trigger(event_complete, settings.onComplete);
});
}
if (settings.transition === 'fade') {
$box.fadeTo(speed, 0, function () {
setPosition(0);
});
} else {
setPosition(speed);
}
};
publicMethod.load = function (launched) {
var href, img, setResize, prep = publicMethod.prep;
active = true;
element = $related[index];
if (!launched) {
settings = process($.extend({}, $.data(element, colorbox)));
}
trigger(event_purge);
trigger(event_load, settings.onLoad);
settings.h = settings.height ?
setSize(settings.height, 'y') - loadedHeight - interfaceHeight :
settings.innerHeight && setSize(settings.innerHeight, 'y');
settings.w = settings.width ?
setSize(settings.width, 'x') - loadedWidth - interfaceWidth :
settings.innerWidth && setSize(settings.innerWidth, 'x');
settings.mw = settings.w;
settings.mh = settings.h;
if (settings.maxWidth) {
settings.mw = setSize(settings.maxWidth, 'x') - loadedWidth - interfaceWidth;
settings.mw = settings.w && settings.w < settings.mw ? settings.w : settings.mw;
}
if (settings.maxHeight) {
settings.mh = setSize(settings.maxHeight, 'y') - loadedHeight - interfaceHeight;
settings.mh = settings.h && settings.h < settings.mh ? settings.h : settings.mh;
}
href = settings.href;
$loadingOverlay.show();
if (settings.inline) {
$div().hide().insertBefore($(href)[0]).one(event_purge, function () {
$(this).replaceWith($loaded.children());
});
prep($(href));
} else if (settings.iframe) {
$box.one(event_loaded, function () {
var iframe = $("<iframe frameborder='0' style='width:100%; height:100%; border:0; display:block'/>")[0];
iframe.name = prefix + (+new Date());
iframe.src = settings.href;
if (!settings.scrolling) {
iframe.scrolling = "no";
}
if (isIE) {
iframe.allowtransparency = "true";
}
$(iframe).appendTo($loaded).one(event_purge, function () {
iframe.src = "//about:blank";
});
});
prep(" ");
} else if (settings.html) {
prep(settings.html);
} else if (isImage(href)) {
img = new Image();
img.onload = function () {
var percent;
img.onload = null;
img.id = prefix + 'Photo';
$(img).css({border: 'none', display: 'block', cssFloat: 'left'});
if (settings.scalePhotos) {
setResize = function () {
img.height -= img.height * percent;
img.width -= img.width * percent;
};
if (settings.mw && img.width > settings.mw) {
percent = (img.width - settings.mw) / img.width;
setResize();
}
if (settings.mh && img.height > settings.mh) {
percent = (img.height - settings.mh) / img.height;
setResize();
}
}
if (settings.h) {
img.style.marginTop = Math.max(settings.h - img.height, 0) / 2 + 'px';
}
if ($related[1] && (index < $related.length - 1 || settings.loop)) {
$(img).css({cursor: 'pointer'}).click(publicMethod.next);
}
if (isIE) {
img.style.msInterpolationMode = 'bicubic';
}
setTimeout(function () { // Chrome will sometimes report a 0 by 0 size if there isn't pause in execution
prep(img);
}, 1);
};
setTimeout(function () { // Opera 10.6+ will sometimes load the src before the onload function is set
img.src = href;
}, 1);
} else if (href) {
$loadingBay.load(href, function (data, status, xhr) {
prep(status === 'error' ? 'Request unsuccessful: ' + xhr.statusText : $(this).children());
});
}
};
publicMethod.next = function () {
if (!active) {
index = index < $related.length - 1 ? index + 1 : 0;
publicMethod.load();
}
};
publicMethod.prev = function () {
if (!active) {
index = index ? index - 1 : $related.length - 1;
publicMethod.load();
}
};
publicMethod.close = function () {
if (open && !closing) {
closing = true;
open = false;
trigger(event_cleanup, settings.onCleanup);
$window.unbind('.' + prefix + ' .' + event_ie6);
$overlay.fadeTo('fast', 0);
$box.stop().fadeTo('fast', 0, function () {
trigger(event_purge);
$loaded.remove();
$box.add($overlay).css({'opacity': 1, cursor: 'auto'}).hide();
setTimeout(function () {
closing = false;
trigger(event_closed, settings.onClosed);
}, 1);
});
}
};
publicMethod.element = function () {
return $(element);
};
publicMethod.settings = defaults;
$(publicMethod.init);
}(jQuery, this));
(function(g){var q={vertical:!1,rtl:!1,start:1,offset:1,size:null,scroll:3,visible:null,animation:"normal",easing:"swing",auto:0,wrap:null,initCallback:null,setupCallback:null,reloadCallback:null,itemLoadCallback:null,itemFirstInCallback:null,itemFirstOutCallback:null,itemLastInCallback:null,itemLastOutCallback:null,itemVisibleInCallback:null,itemVisibleOutCallback:null,animationStepCallback:null,buttonNextHTML:"<div></div>",buttonPrevHTML:"<div></div>",buttonNextEvent:"click",buttonPrevEvent:"click", buttonNextCallback:null,buttonPrevCallback:null,itemFallbackDimension:null},m=!1;g(window).bind("load.jcarousel",function(){m=!0});g.jcarousel=function(a,c){this.options=g.extend({},q,c||{});this.autoStopped=this.locked=!1;this.buttonPrevState=this.buttonNextState=this.buttonPrev=this.buttonNext=this.list=this.clip=this.container=null;if(!c||c.rtl===void 0)this.options.rtl=(g(a).attr("dir")||g("html").attr("dir")||"").toLowerCase()=="rtl";this.wh=!this.options.vertical?"width":"height";this.lt=!this.options.vertical? this.options.rtl?"right":"left":"top";for(var b="",d=a.className.split(" "),f=0;f<d.length;f++)if(d[f].indexOf("jcarousel-skin")!=-1){g(a).removeClass(d[f]);b=d[f];break}a.nodeName.toUpperCase()=="UL"||a.nodeName.toUpperCase()=="OL"?(this.list=g(a),this.clip=this.list.parents(".jcarousel-clip"),this.container=this.list.parents(".jcarousel-container")):(this.container=g(a),this.list=this.container.find("ul,ol").eq(0),this.clip=this.container.find(".jcarousel-clip"));if(this.clip.size()===0)this.clip= this.list.wrap("<div></div>").parent();if(this.container.size()===0)this.container=this.clip.wrap("<div></div>").parent();b!==""&&this.container.parent()[0].className.indexOf("jcarousel-skin")==-1&&this.container.wrap('<div class=" '+b+'"></div>');this.buttonPrev=g(".jcarousel-prev",this.container);if(this.buttonPrev.size()===0&&this.options.buttonPrevHTML!==null)this.buttonPrev=g(this.options.buttonPrevHTML).appendTo(this.container);this.buttonPrev.addClass(this.className("jcarousel-prev"));this.buttonNext= g(".jcarousel-next",this.container);if(this.buttonNext.size()===0&&this.options.buttonNextHTML!==null)this.buttonNext=g(this.options.buttonNextHTML).appendTo(this.container);this.buttonNext.addClass(this.className("jcarousel-next"));this.clip.addClass(this.className("jcarousel-clip")).css({position:"relative"});this.list.addClass(this.className("jcarousel-list")).css({overflow:"hidden",position:"relative",top:0,margin:0,padding:0}).css(this.options.rtl?"right":"left",0);this.container.addClass(this.className("jcarousel-container")).css({position:"relative"}); !this.options.vertical&&this.options.rtl&&this.container.addClass("jcarousel-direction-rtl").attr("dir","rtl");var j=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null,b=this.list.children("li"),e=this;if(b.size()>0){var h=0,i=this.options.offset;b.each(function(){e.format(this,i++);h+=e.dimension(this,j)});this.list.css(this.wh,h+100+"px");if(!c||c.size===void 0)this.options.size=b.size()}this.container.css("display","block");this.buttonNext.css("display","block");this.buttonPrev.css("display", "block");this.funcNext=function(){e.next()};this.funcPrev=function(){e.prev()};this.funcResize=function(){e.resizeTimer&&clearTimeout(e.resizeTimer);e.resizeTimer=setTimeout(function(){e.reload()},100)};this.options.initCallback!==null&&this.options.initCallback(this,"init");!m&&g.browser.safari?(this.buttons(!1,!1),g(window).bind("load.jcarousel",function(){e.setup()})):this.setup()};var f=g.jcarousel;f.fn=f.prototype={jcarousel:"0.2.8"};f.fn.extend=f.extend=g.extend;f.fn.extend({setup:function(){this.prevLast= this.prevFirst=this.last=this.first=null;this.animating=!1;this.tail=this.resizeTimer=this.timer=null;this.inTail=!1;if(!this.locked){this.list.css(this.lt,this.pos(this.options.offset)+"px");var a=this.pos(this.options.start,!0);this.prevFirst=this.prevLast=null;this.animate(a,!1);g(window).unbind("resize.jcarousel",this.funcResize).bind("resize.jcarousel",this.funcResize);this.options.setupCallback!==null&&this.options.setupCallback(this)}},reset:function(){this.list.empty();this.list.css(this.lt, "0px");this.list.css(this.wh,"10px");this.options.initCallback!==null&&this.options.initCallback(this,"reset");this.setup()},reload:function(){this.tail!==null&&this.inTail&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+this.tail);this.tail=null;this.inTail=!1;this.options.reloadCallback!==null&&this.options.reloadCallback(this);if(this.options.visible!==null){var a=this,c=Math.ceil(this.clipping()/this.options.visible),b=0,d=0;this.list.children("li").each(function(f){b+=a.dimension(this, c);f+1<a.first&&(d=b)});this.list.css(this.wh,b+"px");this.list.css(this.lt,-d+"px")}this.scroll(this.first,!1)},lock:function(){this.locked=!0;this.buttons()},unlock:function(){this.locked=!1;this.buttons()},size:function(a){if(a!==void 0)this.options.size=a,this.locked||this.buttons();return this.options.size},has:function(a,c){if(c===void 0||!c)c=a;if(this.options.size!==null&&c>this.options.size)c=this.options.size;for(var b=a;b<=c;b++){var d=this.get(b);if(!d.length||d.hasClass("jcarousel-item-placeholder"))return!1}return!0}, get:function(a){return g(">.jcarousel-item-"+a,this.list)},add:function(a,c){var b=this.get(a),d=0,p=g(c);if(b.length===0)for(var j,e=f.intval(a),b=this.create(a);;){if(j=this.get(--e),e<=0||j.length){e<=0?this.list.prepend(b):j.after(b);break}}else d=this.dimension(b);p.get(0).nodeName.toUpperCase()=="LI"?(b.replaceWith(p),b=p):b.empty().append(c);this.format(b.removeClass(this.className("jcarousel-item-placeholder")),a);p=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible): null;d=this.dimension(b,p)-d;a>0&&a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))-d+"px");this.list.css(this.wh,f.intval(this.list.css(this.wh))+d+"px");return b},remove:function(a){var c=this.get(a);if(c.length&&!(a>=this.first&&a<=this.last)){var b=this.dimension(c);a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+b+"px");c.remove();this.list.css(this.wh,f.intval(this.list.css(this.wh))-b+"px")}},next:function(){this.tail!==null&&!this.inTail?this.scrollTail(!1): this.scroll((this.options.wrap=="both"||this.options.wrap=="last")&&this.options.size!==null&&this.last==this.options.size?1:this.first+this.options.scroll)},prev:function(){this.tail!==null&&this.inTail?this.scrollTail(!0):this.scroll((this.options.wrap=="both"||this.options.wrap=="first")&&this.options.size!==null&&this.first==1?this.options.size:this.first-this.options.scroll)},scrollTail:function(a){if(!this.locked&&!this.animating&&this.tail){this.pauseAuto();var c=f.intval(this.list.css(this.lt)), c=!a?c-this.tail:c+this.tail;this.inTail=!a;this.prevFirst=this.first;this.prevLast=this.last;this.animate(c)}},scroll:function(a,c){!this.locked&&!this.animating&&(this.pauseAuto(),this.animate(this.pos(a),c))},pos:function(a,c){var b=f.intval(this.list.css(this.lt));if(this.locked||this.animating)return b;this.options.wrap!="circular"&&(a=a<1?1:this.options.size&&a>this.options.size?this.options.size:a);for(var d=this.first>a,g=this.options.wrap!="circular"&&this.first<=1?1:this.first,j=d?this.get(g): this.get(this.last),e=d?g:g-1,h=null,i=0,k=!1,l=0;d?--e>=a:++e<a;){h=this.get(e);k=!h.length;if(h.length===0&&(h=this.create(e).addClass(this.className("jcarousel-item-placeholder")),j[d?"before":"after"](h),this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)));j=h;l=this.dimension(h);k&&(i+=l);if(this.first!==null&&(this.options.wrap=="circular"||e>=1&&(this.options.size===null||e<= this.options.size)))b=d?b+l:b-l}for(var g=this.clipping(),m=[],o=0,n=0,j=this.get(a-1),e=a;++o;){h=this.get(e);k=!h.length;if(h.length===0){h=this.create(e).addClass(this.className("jcarousel-item-placeholder"));if(j.length===0)this.list.prepend(h);else j[d?"before":"after"](h);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)))}j=h;l=this.dimension(h);if(l===0)throw Error("jCarousel: No width/height set for items. This will cause an infinite loop. Aborting..."); this.options.wrap!="circular"&&this.options.size!==null&&e>this.options.size?m.push(h):k&&(i+=l);n+=l;if(n>=g)break;e++}for(h=0;h<m.length;h++)m[h].remove();i>0&&(this.list.css(this.wh,this.dimension(this.list)+i+"px"),d&&(b-=i,this.list.css(this.lt,f.intval(this.list.css(this.lt))-i+"px")));i=a+o-1;if(this.options.wrap!="circular"&&this.options.size&&i>this.options.size)i=this.options.size;if(e>i){o=0;e=i;for(n=0;++o;){h=this.get(e--);if(!h.length)break;n+=this.dimension(h);if(n>=g)break}}e=i-o+ 1;this.options.wrap!="circular"&&e<1&&(e=1);if(this.inTail&&d)b+=this.tail,this.inTail=!1;this.tail=null;if(this.options.wrap!="circular"&&i==this.options.size&&i-o+1>=1&&(d=f.intval(this.get(i).css(!this.options.vertical?"marginRight":"marginBottom")),n-d>g))this.tail=n-g-d;if(c&&a===this.options.size&&this.tail)b-=this.tail,this.inTail=!0;for(;a-- >e;)b+=this.dimension(this.get(a));this.prevFirst=this.first;this.prevLast=this.last;this.first=e;this.last=i;return b},animate:function(a,c){if(!this.locked&& !this.animating){this.animating=!0;var b=this,d=function(){b.animating=!1;a===0&&b.list.css(b.lt,0);!b.autoStopped&&(b.options.wrap=="circular"||b.options.wrap=="both"||b.options.wrap=="last"||b.options.size===null||b.last<b.options.size||b.last==b.options.size&&b.tail!==null&&!b.inTail)&&b.startAuto();b.buttons();b.notify("onAfterAnimation");if(b.options.wrap=="circular"&&b.options.size!==null)for(var c=b.prevFirst;c<=b.prevLast;c++)c!==null&&!(c>=b.first&&c<=b.last)&&(c<1||c>b.options.size)&&b.remove(c)}; this.notify("onBeforeAnimation");if(!this.options.animation||c===!1)this.list.css(this.lt,a+"px"),d();else{var f=!this.options.vertical?this.options.rtl?{right:a}:{left:a}:{top:a},d={duration:this.options.animation,easing:this.options.easing,complete:d};if(g.isFunction(this.options.animationStepCallback))d.step=this.options.animationStepCallback;this.list.animate(f,d)}}},startAuto:function(a){if(a!==void 0)this.options.auto=a;if(this.options.auto===0)return this.stopAuto();if(this.timer===null){this.autoStopped= !1;var c=this;this.timer=window.setTimeout(function(){c.next()},this.options.auto*1E3)}},stopAuto:function(){this.pauseAuto();this.autoStopped=!0},pauseAuto:function(){if(this.timer!==null)window.clearTimeout(this.timer),this.timer=null},buttons:function(a,c){if(a==null&&(a=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="first"||this.options.size===null||this.last<this.options.size),!this.locked&&(!this.options.wrap||this.options.wrap=="first")&&this.options.size!==null&& this.last>=this.options.size))a=this.tail!==null&&!this.inTail;if(c==null&&(c=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="last"||this.first>1),!this.locked&&(!this.options.wrap||this.options.wrap=="last")&&this.options.size!==null&&this.first==1))c=this.tail!==null&&this.inTail;var b=this;this.buttonNext.size()>0?(this.buttonNext.unbind(this.options.buttonNextEvent+".jcarousel",this.funcNext),a&&this.buttonNext.bind(this.options.buttonNextEvent+".jcarousel",this.funcNext), this.buttonNext[a?"removeClass":"addClass"](this.className("jcarousel-next-disabled")).attr("disabled",a?!1:!0),this.options.buttonNextCallback!==null&&this.buttonNext.data("jcarouselstate")!=a&&this.buttonNext.each(function(){b.options.buttonNextCallback(b,this,a)}).data("jcarouselstate",a)):this.options.buttonNextCallback!==null&&this.buttonNextState!=a&&this.options.buttonNextCallback(b,null,a);this.buttonPrev.size()>0?(this.buttonPrev.unbind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev), c&&this.buttonPrev.bind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev),this.buttonPrev[c?"removeClass":"addClass"](this.className("jcarousel-prev-disabled")).attr("disabled",c?!1:!0),this.options.buttonPrevCallback!==null&&this.buttonPrev.data("jcarouselstate")!=c&&this.buttonPrev.each(function(){b.options.buttonPrevCallback(b,this,c)}).data("jcarouselstate",c)):this.options.buttonPrevCallback!==null&&this.buttonPrevState!=c&&this.options.buttonPrevCallback(b,null,c);this.buttonNextState= a;this.buttonPrevState=c},notify:function(a){var c=this.prevFirst===null?"init":this.prevFirst<this.first?"next":"prev";this.callback("itemLoadCallback",a,c);this.prevFirst!==this.first&&(this.callback("itemFirstInCallback",a,c,this.first),this.callback("itemFirstOutCallback",a,c,this.prevFirst));this.prevLast!==this.last&&(this.callback("itemLastInCallback",a,c,this.last),this.callback("itemLastOutCallback",a,c,this.prevLast));this.callback("itemVisibleInCallback",a,c,this.first,this.last,this.prevFirst, this.prevLast);this.callback("itemVisibleOutCallback",a,c,this.prevFirst,this.prevLast,this.first,this.last)},callback:function(a,c,b,d,f,j,e){if(!(this.options[a]==null||typeof this.options[a]!="object"&&c!="onAfterAnimation")){var h=typeof this.options[a]=="object"?this.options[a][c]:this.options[a];if(g.isFunction(h)){var i=this;if(d===void 0)h(i,b,c);else if(f===void 0)this.get(d).each(function(){h(i,this,d,b,c)});else for(var a=function(a){i.get(a).each(function(){h(i,this,a,b,c)})},k=d;k<=f;k++)k!== null&&!(k>=j&&k<=e)&&a(k)}}},create:function(a){return this.format("<li></li>",a)},format:function(a,c){for(var a=g(a),b=a.get(0).className.split(" "),d=0;d<b.length;d++)b[d].indexOf("jcarousel-")!=-1&&a.removeClass(b[d]);a.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-"+c)).css({"float":this.options.rtl?"right":"left","list-style":"none"}).attr("jcarouselindex",c);return a},className:function(a){return a+" "+a+(!this.options.vertical?"-horizontal":"-vertical")}, dimension:function(a,c){var b=g(a);if(c==null)return!this.options.vertical?b.outerWidth(!0)||f.intval(this.options.itemFallbackDimension):b.outerHeight(!0)||f.intval(this.options.itemFallbackDimension);else{var d=!this.options.vertical?c-f.intval(b.css("marginLeft"))-f.intval(b.css("marginRight")):c-f.intval(b.css("marginTop"))-f.intval(b.css("marginBottom"));g(b).css(this.wh,d+"px");return this.dimension(b)}},clipping:function(){return!this.options.vertical?this.clip[0].offsetWidth-f.intval(this.clip.css("borderLeftWidth"))- f.intval(this.clip.css("borderRightWidth")):this.clip[0].offsetHeight-f.intval(this.clip.css("borderTopWidth"))-f.intval(this.clip.css("borderBottomWidth"))},index:function(a,c){if(c==null)c=this.options.size;return Math.round(((a-1)/c-Math.floor((a-1)/c))*c)+1}});f.extend({defaults:function(a){return g.extend(q,a||{})},intval:function(a){a=parseInt(a,10);return isNaN(a)?0:a},windowLoaded:function(){m=!0}});g.fn.jcarousel=function(a){if(typeof a=="string"){var c=g(this).data("jcarousel"),b=Array.prototype.slice.call(arguments, 1);return c[a].apply(c,b)}else return this.each(function(){var b=g(this).data("jcarousel");b?(a&&g.extend(b.options,a),b.reload()):g(this).data("jcarousel",new f(this,a))})}})(jQuery);
(function ($) {
var Slideshow = function ($container, containersCount, milliseconds) {
this.currentIndex = 0;
this.slideshowTimer = undefined;
this.containersCount = containersCount;
this.$container = $container;
this.milliseconds = milliseconds;
if (containersCount > 1) {
this.init();
}
};
Slideshow.prototype.init = function () {
this.startTimer();
};
Slideshow.prototype.getCurrentIndex = function () {
return this.currentIndex;
};
Slideshow.prototype.incrementCurrentIndex = function () {
this.currentIndex = (this.currentIndex + 1) % this.containersCount;
this.updateContainer();
};
Slideshow.prototype.setCurrentIndex = function (currentindex) {
if (currentindex !== this.currentIndex) {
this.currentIndex = currentindex;
this.updateContainer();
}
};
Slideshow.prototype.decrementCurrentIndex = function () {
this.currentIndex = (this.currentIndex + this.containersCount - 1) % this.containersCount;
this.updateContainer();
};
Slideshow.prototype.updateContainer = function () {
this.$container.trigger("update.slideshow");
};
Slideshow.prototype.startTimer = function () {
this.stopTimer();
var slideshow = this;
this.slideshowTimer = setInterval(function () {
slideshow.incrementCurrentIndex();
}, this.milliseconds);
};
Slideshow.prototype.stopTimer = function () {
if (this.slideshowTimer !== undefined) {
clearInterval(this.slideshowTimer);
}
};
var $promotions = $("#promotions");
if ($promotions.length > 0) {
var $containers = $promotions.find(">div");
var containersCount = $containers.length;
if (containersCount > 1) {
var slideshow = new Slideshow($promotions, containersCount, 4000);
var $pager = $("<ul></ul>");
var i;
for (i = 0; i < containersCount; i++) {
var txtLabel = i + 1;
txtLabel = "";
$pager.append("<li>" + txtLabel + "</li>");
}
$promotions.on("update.slideshow", function () {
var currentIndex = slideshow.getCurrentIndex();
$containers.removeClass("active").eq(currentIndex).addClass("active");
$pager.find("li").removeClass("active").eq(currentIndex).addClass("active");
}).hover(function () {
slideshow.stopTimer();
}, function () {
slideshow.startTimer();
}).trigger("update.slideshow");
$pager.on("click.slideshow", "li", function () {
var currentLi = this;
var $allLi = $(this).parent().children();
slideshow.setCurrentIndex($allLi.index(currentLi));
});
$promotions.append($pager);
}
}
}(jQuery));
(function($){
var base64module = {};
var base64 = new function()
{
var utfLibName  = "utf";
var b64char     = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
var b64encTable = b64char.split("");
var b64decTable = [];
for (var i=0; i<b64char.length; i++) b64decTable[b64char.charAt(i)] = i;
this.encode = function(_dat, _strMode)
{
return encoder( _strMode? unpackUTF8(_dat): unpackChar(_dat) );
}
var encoder = function(_ary)
{
var md  = _ary.length % 3;
var b64 = "";
var i, tmp = 0;
if (md) for (i=3-md; i>0; i--) _ary[_ary.length] = 0;
for (i=0; i<_ary.length; i+=3)
{
tmp = (_ary[i]<<16) | (_ary[i+1]<<8) | _ary[i+2];
b64 +=  b64encTable[ (tmp >>>18) & 0x3f]
+   b64encTable[ (tmp >>>12) & 0x3f]
+   b64encTable[ (tmp >>> 6) & 0x3f]
+   b64encTable[ tmp & 0x3f];
}
if (md) // 3の倍数にパディングした 0x0 分 = に置き換え
{
md = 3- md;
b64 = b64.substr(0, b64.length- md);
while (md--) b64 += "=";
}
return b64;
}
this.decode = function(_b64, _strMode)
{
var tmp = decoder( _b64 );
return _strMode? packUTF8(tmp): packChar(tmp);
}
var decoder = function(_b64)
{
_b64    = _b64.replace(/[^A-Za-z0-9\+\/]/g, "");
var md  = _b64.length % 4;
var j, i, tmp;
var dat = [];
if (md) for (i=0; i<4-md; i++) _b64 += "A";
for (j=i=0; i<_b64.length; i+=4, j+=3)
{
tmp = (b64decTable[_b64.charAt( i )] <<18)
| (b64decTable[_b64.charAt(i+1)] <<12)
| (b64decTable[_b64.charAt(i+2)] << 6)
|  b64decTable[_b64.charAt(i+3)];
dat[ j ]    = tmp >>> 16;
dat[j+1]    = (tmp >>> 8) & 0xff;
dat[j+2]    = tmp & 0xff;
}
if (md) dat.length -= [0,0,2,1][md];
return dat;
}
var packUTF8    = function(_x){ return utf.packUTF8(_x) };
var unpackUTF8  = function(_x){ return utf.unpackUTF8(_x) };
var packChar    = function(_x){ return utf.packChar(_x) };
var unpackChar  = function(_x){ return utf.unpackChar(_x) };
}
var utf = new function()
{
this.unpackUTF16 = function(_str)
{
var i, utf16=[];
for (i=0; i<_str.length; i++) utf16[i] = _str.charCodeAt(i);
return utf16;
}
this.unpackChar = function(_str)
{
var utf16 = this.unpackUTF16(_str);
var i,n, tmp = [];
for (n=i=0; i<utf16.length; i++) {
if (utf16[i]<=0xff) tmp[n++] = utf16[i];
else {
tmp[n++] = utf16[i] >> 8;
tmp[n++] = utf16[i] &  0xff;
}
}
return tmp;
}
this.packChar  =
this.packUTF16 = function(_utf16)
{
var i, str = "";
for (i in _utf16) str += String.fromCharCode(_utf16[i]);
return str;
}
this.unpackUTF8 = function(_str)
{
return this.toUTF8( this.unpackUTF16(_str) );
}
this.packUTF8 = function(_utf8)
{
return this.packUTF16( this.toUTF16(_utf8) );
}
this.toUTF8 = function(_utf16)
{
var utf8 = [];
var idx = 0;
var i, j, c;
for (i=0; i<_utf16.length; i++)
{
c = _utf16[i];
if (c <= 0x7f) utf8[idx++] = c;
else if (c <= 0x7ff)
{
utf8[idx++] = 0xc0 | (c >>> 6 );
utf8[idx++] = 0x80 | (c & 0x3f);
}
else if (c <= 0xffff)
{
utf8[idx++] = 0xe0 | (c >>> 12 );
utf8[idx++] = 0x80 | ((c >>> 6 ) & 0x3f);
utf8[idx++] = 0x80 | (c & 0x3f);
}
else
{
j = 4;
while (c >> (6*j)) j++;
utf8[idx++] = ((0xff00 >>> j) & 0xff) | (c >>> (6*--j) );
while (j--)
utf8[idx++] = 0x80 | ((c >>> (6*j)) & 0x3f);
}
}
return utf8;
}
this.toUTF16 = function(_utf8)
{
var utf16 = [];
var idx = 0;
var i,s;
for (i=0; i<_utf8.length; i++, idx++)
{
if (_utf8[i] <= 0x7f) utf16[idx] = _utf8[i];
else
{
if ( (_utf8[i]>>5) == 0x6)
{
utf16[idx] = ( (_utf8[i] & 0x1f) << 6 )
| ( _utf8[++i] & 0x3f );
}
else if ( (_utf8[i]>>4) == 0xe)
{
utf16[idx] = ( (_utf8[i] & 0xf) << 12 )
| ( (_utf8[++i] & 0x3f) << 6 )
| ( _utf8[++i] & 0x3f );
}
else
{
s = 1;
while (_utf8[i] & (0x20 >>> s) ) s++;
utf16[idx] = _utf8[i] & (0x1f >>> s);
while (s-->=0) utf16[idx] = (utf16[idx] << 6) ^ (_utf8[++i] & 0x3f);
}
}
}
return utf16;
}
this.URLencode = function(_str)
{
return _str.replace(/([^a-zA-Z0-9_\-\.])/g, function(_tmp, _c)
{
if (_c == "\x20") return "+";
var tmp = utf.toUTF8( [_c.charCodeAt(0)] );
var c = "";
for (var i in tmp)
{
i = tmp[i].toString(16);
if (i.length == 1) i = "0"+ i;
c += "%"+ i;
}
return c;
} );
}
this.URLdecode = function(_dat)
{
_dat = _dat.replace(/\+/g, "\x20");
_dat = _dat.replace( /%([a-fA-F0-9][a-fA-F0-9])/g,
function(_tmp, _hex){ return String.fromCharCode( parseInt(_hex, 16) ) } );
return this.packChar( this.toUTF16( this.unpackUTF16(_dat) ) );
}
}
$.extend({
base64: {
encode: base64.encode,
decode: base64.decode,
codec: typeof atob == 'function' ? 'builtin' : 'alternate'
}
})
$.ajax = (function(ajax){
return function(option){
var flg = 0
if (option.dataType && option.dataType.match(/:b64/)){
option.dataType = option.dataType.replace(':b64', '')
flg = 1
}
if (flg){
option.success = (function(callback){
return function(data, status, xhr){
data = $.base64.decode(data)
callback(data, status, xhr)
}
})(option.success || function(data, status, xhr){})
}
return ajax.apply(this, arguments)
}
})($.ajax)
})(jQuery)
$(document).ready(function() {
$("#formLivreBlanc").bind("submit", function(e) {
e.preventDefault();
$(".error").hide();
var hasError = false;
var emailaddressVal = $("#email").val();
if(emailaddressVal == 'Email professionnel *') {
$("#invalide").hide();
$("#vide").show();
hasError = true;
}
else if(! emailaddressVal.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]{2,}[.][a-zA-Z]{2,8}$/)){
$("#vide").hide();
$("#invalide").show();
hasError = true;
}
if(hasError == true) {
return false;
}
else{
$.ajax({
type        : "POST",
cache       : false,
url         : $(this).attr('action')+"/(email)/"+emailaddressVal,
data        : $(this).serializeArray(),
success: function(data) {
$.colorbox({
html:data,
overlayClose: false,
scrolling:false
});
submitForm();
}
});
return false;
}
});
});
function submitForm(){
$("#get_whitepaper_form").submit(function() {
var siteUrl= $("#siteUrlwhitepaper").val();
$.ajax({
url: $(this).attr('action'),
type: $(this).attr('method'),
data: $(this).serialize(),
success: function( html) {
var regexp = new RegExp("OK_-_-_");
if( regexp.test( html )){
var siteUrl= $("#siteUrlwhitepaper").val();
window.location.href = 'http://'+siteUrl+html.substring(html.indexOf('OK_-_-_') + 7);
}else{
$("#error").html(html);
parent.jQuery.colorbox.resize();
}
}
});
return false;
});
}
var position;
$(document).bind('cbox_open', function() {
position = $("html,body").scrollTop();
$('html').css({
overflow: 'hidden'
});
}).bind('cbox_closed', function() {
$('html').css({
overflow: 'auto'
});
$("html,body").scrollTop(position);
});
function fixColumns(){
var c1 = document.getElementById("colonne0");
var c2 = document.getElementById("colonne1");
var c3 = document.getElementById("colonne2");
if(c1.offsetHeight && c2.offsetHeight && c3.offsetHeight ){
maxheight=Math.max(c1.offsetHeight,c2.offsetHeight,c3.offsetHeight)+'px';
}
var max2 = Math.max(c1.offsetHeight,c2.offsetHeight,c3.offsetHeight)+'px';
c1.style.height = max2;
c2.style.height = max2;
c3.style.height = maxheight;
}
function fixColumns2(){
var c1 = document.getElementById("colonne3");
var c2 = document.getElementById("colonne4");
var c3 = document.getElementById("colonne5");
if(c1.offsetHeight && c2.offsetHeight && c3.offsetHeight ){
maxheight=Math.max(c1.offsetHeight,c2.offsetHeight,c3.offsetHeight)+'px';
}
var max2 = Math.max(c1.offsetHeight,c2.offsetHeight,c3.offsetHeight)+'px';
c1.style.height = max2;
c2.style.height = max2;
c3.style.height = maxheight;
}
!function(a){a.fn.nicefileinput=function(b){var c={label:"Browse...",fullPath:!1};return b&&a.extend(c,b),this.each(function(){var b=this;if(void 0===a(b).attr("data-styled")){var d=Math.round(1e4*Math.random()),e=new Date,f=e.getTime()+d.toString(),g=a('<input type="text" readonly="readonly">').css({display:"block","float":"left",margin:0,padding:"0 5px"}).addClass("NFI-filename NFI"+f),h=a("<div>").css({overflow:"hidden",position:"relative",display:"block","float":"left","white-space":"nowrap","text-align":"center"}).addClass("NFI-button NFI"+f).html(c.label);a(b).after(g),a(b).wrap(h),a(".NFI"+f).wrapAll('<div class="NFI-wrapper" id="NFI-wrapper-'+f+'" />'),a(".NFI-wrapper").css({overflow:"auto",display:"inline-block"}),a("#NFI-wrapper-"+f).addClass(a(b).attr("class")),a(b).css({opacity:0,position:"absolute",border:"none",margin:0,padding:0,top:0,right:0,cursor:"pointer",height:"60px"}).addClass("NFI-current"),a(b).on("change",function(){var d=a(b).val();if(c.fullPath)g.val(d);else{var e=d.split(/[/\\]/);g.val(e[e.length-1])}}),a(b).attr("data-styled",!0)}})}}(jQuery);
;(function() {
"use strict";
function setup($) {
$.fn._fadeIn = $.fn.fadeIn;
var noOp = $.noop || function() {};
var msie = /MSIE/.test(navigator.userAgent);
var ie6  = /MSIE 6.0/.test(navigator.userAgent) && ! /MSIE 8.0/.test(navigator.userAgent);
var mode = document.documentMode || 0;
var setExpr = $.isFunction( document.createElement('div').style.setExpression );
$.blockUI   = function(opts) { install(window, opts); };
$.unblockUI = function(opts) { remove(window, opts); };
$.growlUI = function(title, message, timeout, onClose) {
var $m = $('<div class="growlUI"></div>');
if (title) $m.append('<h1>'+title+'</h1>');
if (message) $m.append('<h2>'+message+'</h2>');
if (timeout === undefined) timeout = 3000;
var callBlock = function(opts) {
opts = opts || {};
$.blockUI({
message: $m,
fadeIn : typeof opts.fadeIn  !== 'undefined' ? opts.fadeIn  : 700,
fadeOut: typeof opts.fadeOut !== 'undefined' ? opts.fadeOut : 1000,
timeout: typeof opts.timeout !== 'undefined' ? opts.timeout : timeout,
centerY: false,
showOverlay: false,
onUnblock: onClose,
css: $.blockUI.defaults.growlCSS
});
};
callBlock();
var nonmousedOpacity = $m.css('opacity');
$m.mouseover(function() {
callBlock({
fadeIn: 0,
timeout: 30000
});
var displayBlock = $('.blockMsg');
displayBlock.stop(); // cancel fadeout if it has started
displayBlock.fadeTo(300, 1); // make it easier to read the message by removing transparency
}).mouseout(function() {
$('.blockMsg').fadeOut(1000);
});
};
$.fn.block = function(opts) {
if ( this[0] === window ) {
$.blockUI( opts );
return this;
}
var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
this.each(function() {
var $el = $(this);
if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked'))
return;
$el.unblock({ fadeOut: 0 });
});
return this.each(function() {
if ($.css(this,'position') == 'static') {
this.style.position = 'relative';
$(this).data('blockUI.static', true);
}
this.style.zoom = 1; // force 'hasLayout' in ie
install(this, opts);
});
};
$.fn.unblock = function(opts) {
if ( this[0] === window ) {
$.unblockUI( opts );
return this;
}
return this.each(function() {
remove(this, opts);
});
};
$.blockUI.version = 2.66; // 2nd generation blocking at no extra cost!
$.blockUI.defaults = {
message:  '<h1>Please wait...</h1>',
title: null,		// title string; only used when theme == true
draggable: true,	// only used when theme == true (requires jquery-ui.js to be loaded)
theme: false, // set to true to use with jQuery UI themes
css: {
padding:	0,
margin:		0,
width:		'30%',
top:		'40%',
left:		'35%',
textAlign:	'center',
color:		'#000',
border:		'3px solid #aaa',
backgroundColor:'#fff',
cursor:		'wait'
},
themedCSS: {
width:	'30%',
top:	'40%',
left:	'35%'
},
overlayCSS:  {
backgroundColor:	'#000',
opacity:			0.6,
cursor:				'wait'
},
cursorReset: 'default',
growlCSS: {
width:		'350px',
top:		'10px',
left:		'',
right:		'10px',
border:		'none',
padding:	'5px',
opacity:	0.6,
cursor:		'default',
color:		'#fff',
backgroundColor: '#000',
'-webkit-border-radius':'10px',
'-moz-border-radius':	'10px',
'border-radius':		'10px'
},
iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',
forceIframe: false,
baseZ: 1000,
centerX: true, // <-- only effects element blocking (page block controlled via css above)
centerY: true,
allowBodyStretch: true,
bindEvents: true,
constrainTabKey: true,
fadeIn:  200,
fadeOut:  400,
timeout: 0,
showOverlay: true,
focusInput: true,
focusableElements: ':input:enabled:visible',
onBlock: null,
onUnblock: null,
onOverlayClick: null,
quirksmodeOffsetHack: 4,
blockMsgClass: 'blockMsg',
ignoreIfBlocked: false
};
var pageBlock = null;
var pageBlockEls = [];
function install(el, opts) {
var css, themedCSS;
var full = (el == window);
var msg = (opts && opts.message !== undefined ? opts.message : undefined);
opts = $.extend({}, $.blockUI.defaults, opts || {});
if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked'))
return;
opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
if (opts.onOverlayClick)
opts.overlayCSS.cursor = 'pointer';
themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
msg = msg === undefined ? opts.message : msg;
if (full && pageBlock)
remove(window, {fadeOut:0});
if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
var node = msg.jquery ? msg[0] : msg;
var data = {};
$(el).data('blockUI.history', data);
data.el = node;
data.parent = node.parentNode;
data.display = node.style.display;
data.position = node.style.position;
if (data.parent)
data.parent.removeChild(node);
}
$(el).data('blockUI.onUnblock', opts.onUnblock);
var z = opts.baseZ;
var lyr1, lyr2, lyr3, s;
if (msie || opts.forceIframe)
lyr1 = $('<iframe class="blockUI" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>');
else
lyr1 = $('<div class="blockUI" style="display:none"></div>');
if (opts.theme)
lyr2 = $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+ (z++) +';display:none"></div>');
else
lyr2 = $('<div class="blockUI blockOverlay" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');
if (opts.theme && full) {
s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:fixed">';
if ( opts.title ) {
s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
}
s += '<div class="ui-widget-content ui-dialog-content"></div>';
s += '</div>';
}
else if (opts.theme) {
s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:absolute">';
if ( opts.title ) {
s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
}
s += '<div class="ui-widget-content ui-dialog-content"></div>';
s += '</div>';
}
else if (full) {
s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:'+(z+10)+';display:none;position:fixed"></div>';
}
else {
s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:'+(z+10)+';display:none;position:absolute"></div>';
}
lyr3 = $(s);
if (msg) {
if (opts.theme) {
lyr3.css(themedCSS);
lyr3.addClass('ui-widget-content');
}
else
lyr3.css(css);
}
if (!opts.theme)
lyr2.css(opts.overlayCSS);
lyr2.css('position', full ? 'fixed' : 'absolute');
if (msie || opts.forceIframe)
lyr1.css('opacity',0.0);
var layers = [lyr1,lyr2,lyr3], $par = full ? $('body') : $(el);
$.each(layers, function() {
this.appendTo($par);
});
if (opts.theme && opts.draggable && $.fn.draggable) {
lyr3.draggable({
handle: '.ui-dialog-titlebar',
cancel: 'li'
});
}
var expr = setExpr && (!$.support.boxModel || $('object,embed', full ? null : el).length > 0);
if (ie6 || expr) {
if (full && opts.allowBodyStretch && $.support.boxModel)
$('html,body').css('height','100%');
if ((ie6 || !$.support.boxModel) && !full) {
var t = sz(el,'borderTopWidth'), l = sz(el,'borderLeftWidth');
var fixT = t ? '(0 - '+t+')' : 0;
var fixL = l ? '(0 - '+l+')' : 0;
}
$.each(layers, function(i,o) {
var s = o[0].style;
s.position = 'absolute';
if (i < 2) {
if (full)
s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"');
else
s.setExpression('height','this.parentNode.offsetHeight + "px"');
if (full)
s.setExpression('width','jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"');
else
s.setExpression('width','this.parentNode.offsetWidth + "px"');
if (fixL) s.setExpression('left', fixL);
if (fixT) s.setExpression('top', fixT);
}
else if (opts.centerY) {
if (full) s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
s.marginTop = 0;
}
else if (!opts.centerY && full) {
var top = (opts.css && opts.css.top) ? parseInt(opts.css.top, 10) : 0;
var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';
s.setExpression('top',expression);
}
});
}
if (msg) {
if (opts.theme)
lyr3.find('.ui-widget-content').append(msg);
else
lyr3.append(msg);
if (msg.jquery || msg.nodeType)
$(msg).show();
}
if ((msie || opts.forceIframe) && opts.showOverlay)
lyr1.show(); // opacity is zero
if (opts.fadeIn) {
var cb = opts.onBlock ? opts.onBlock : noOp;
var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
var cb2 = msg ? cb : noOp;
if (opts.showOverlay)
lyr2._fadeIn(opts.fadeIn, cb1);
if (msg)
lyr3._fadeIn(opts.fadeIn, cb2);
}
else {
if (opts.showOverlay)
lyr2.show();
if (msg)
lyr3.show();
if (opts.onBlock)
opts.onBlock();
}
bind(1, el, opts);
if (full) {
pageBlock = lyr3[0];
pageBlockEls = $(opts.focusableElements,pageBlock);
if (opts.focusInput)
setTimeout(focus, 20);
}
else
center(lyr3[0], opts.centerX, opts.centerY);
if (opts.timeout) {
var to = setTimeout(function() {
if (full)
$.unblockUI(opts);
else
$(el).unblock(opts);
}, opts.timeout);
$(el).data('blockUI.timeout', to);
}
}
function remove(el, opts) {
var count;
var full = (el == window);
var $el = $(el);
var data = $el.data('blockUI.history');
var to = $el.data('blockUI.timeout');
if (to) {
clearTimeout(to);
$el.removeData('blockUI.timeout');
}
opts = $.extend({}, $.blockUI.defaults, opts || {});
bind(0, el, opts); // unbind events
if (opts.onUnblock === null) {
opts.onUnblock = $el.data('blockUI.onUnblock');
$el.removeData('blockUI.onUnblock');
}
var els;
if (full) // crazy selector to handle odd field errors in ie6/7
els = $('body').children().filter('.blockUI').add('body > .blockUI');
else
els = $el.find('>.blockUI');
if ( opts.cursorReset ) {
if ( els.length > 1 )
els[1].style.cursor = opts.cursorReset;
if ( els.length > 2 )
els[2].style.cursor = opts.cursorReset;
}
if (full)
pageBlock = pageBlockEls = null;
if (opts.fadeOut) {
count = els.length;
els.stop().fadeOut(opts.fadeOut, function() {
if ( --count === 0)
reset(els,data,opts,el);
});
}
else
reset(els, data, opts, el);
}
function reset(els,data,opts,el) {
var $el = $(el);
if ( $el.data('blockUI.isBlocked') )
return;
els.each(function(i,o) {
if (this.parentNode)
this.parentNode.removeChild(this);
});
if (data && data.el) {
data.el.style.display = data.display;
data.el.style.position = data.position;
if (data.parent)
data.parent.appendChild(data.el);
$el.removeData('blockUI.history');
}
if ($el.data('blockUI.static')) {
$el.css('position', 'static'); // #22
}
if (typeof opts.onUnblock == 'function')
opts.onUnblock(el,opts);
var body = $(document.body), w = body.width(), cssW = body[0].style.width;
body.width(w-1).width(w);
body[0].style.width = cssW;
}
function bind(b, el, opts) {
var full = el == window, $el = $(el);
if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
return;
$el.data('blockUI.isBlocked', b);
if (!full || !opts.bindEvents || (b && !opts.showOverlay))
return;
var events = 'mousedown mouseup keydown keypress keyup touchstart touchend touchmove';
if (b)
$(document).bind(events, opts, handler);
else
$(document).unbind(events, handler);
}
function handler(e) {
if (e.type === 'keydown' && e.keyCode && e.keyCode == 9) {
if (pageBlock && e.data.constrainTabKey) {
var els = pageBlockEls;
var fwd = !e.shiftKey && e.target === els[els.length-1];
var back = e.shiftKey && e.target === els[0];
if (fwd || back) {
setTimeout(function(){focus(back);},10);
return false;
}
}
}
var opts = e.data;
var target = $(e.target);
if (target.hasClass('blockOverlay') && opts.onOverlayClick)
opts.onOverlayClick(e);
if (target.parents('div.' + opts.blockMsgClass).length > 0)
return true;
return target.parents().children().filter('div.blockUI').length === 0;
}
function focus(back) {
if (!pageBlockEls)
return;
var e = pageBlockEls[back===true ? pageBlockEls.length-1 : 0];
if (e)
e.focus();
}
function center(el, x, y) {
var p = el.parentNode, s = el.style;
var l = ((p.offsetWidth - el.offsetWidth)/2) - sz(p,'borderLeftWidth');
var t = ((p.offsetHeight - el.offsetHeight)/2) - sz(p,'borderTopWidth');
if (x) s.left = l > 0 ? (l+'px') : '0';
if (y) s.top  = t > 0 ? (t+'px') : '0';
}
function sz(el, p) {
return parseInt($.css(el,p),10)||0;
}
}
if (typeof define === 'function' && define.amd && define.amd.jQuery) {
define(['jquery'], setup);
} else {
setup(jQuery);
}
})();
!function(a){function b(){}function c(a){function c(b){b.prototype.option||(b.prototype.option=function(b){a.isPlainObject(b)&&(this.options=a.extend(!0,this.options,b))})}function e(b,c){a.fn[b]=function(e){if("string"==typeof e){for(var g=d.call(arguments,1),h=0,i=this.length;i>h;h++){var j=this[h],k=a.data(j,b);if(k)if(a.isFunction(k[e])&&"_"!==e.charAt(0)){var l=k[e].apply(k,g);if(void 0!==l)return l}else f("no such method '"+e+"' for "+b+" instance");else f("cannot call methods on "+b+" prior to initialization; attempted to call '"+e+"'")}return this}return this.each(function(){var d=a.data(this,b);d?(d.option(e),d._init()):(d=new c(this,e),a.data(this,b,d))})}}if(a){var f="undefined"==typeof console?b:function(a){console.error(a)};return a.bridget=function(a,b){c(b),e(a,b)},a.bridget}}var d=Array.prototype.slice;"function"==typeof define&&define.amd?define("jquery-bridget/jquery.bridget",["jquery"],c):c("object"==typeof exports?require("jquery"):a.jQuery)}(window),function(a){function b(b){var c=a.event;return c.target=c.target||c.srcElement||b,c}var c=document.documentElement,d=function(){};c.addEventListener?d=function(a,b,c){a.addEventListener(b,c,!1)}:c.attachEvent&&(d=function(a,c,d){a[c+d]=d.handleEvent?function(){var c=b(a);d.handleEvent.call(d,c)}:function(){var c=b(a);d.call(a,c)},a.attachEvent("on"+c,a[c+d])});var e=function(){};c.removeEventListener?e=function(a,b,c){a.removeEventListener(b,c,!1)}:c.detachEvent&&(e=function(a,b,c){a.detachEvent("on"+b,a[b+c]);try{delete a[b+c]}catch(d){a[b+c]=void 0}});var f={bind:d,unbind:e};"function"==typeof define&&define.amd?define("eventie/eventie",f):"object"==typeof exports?module.exports=f:a.eventie=f}(window),function(){function a(){}function b(a,b){for(var c=a.length;c--;)if(a[c].listener===b)return c;return-1}function c(a){return function(){return this[a].apply(this,arguments)}}var d=a.prototype,e=this,f=e.EventEmitter;d.getListeners=function(a){var b,c,d=this._getEvents();if(a instanceof RegExp){b={};for(c in d)d.hasOwnProperty(c)&&a.test(c)&&(b[c]=d[c])}else b=d[a]||(d[a]=[]);return b},d.flattenListeners=function(a){var b,c=[];for(b=0;b<a.length;b+=1)c.push(a[b].listener);return c},d.getListenersAsObject=function(a){var b,c=this.getListeners(a);return c instanceof Array&&(b={},b[a]=c),b||c},d.addListener=function(a,c){var d,e=this.getListenersAsObject(a),f="object"==typeof c;for(d in e)e.hasOwnProperty(d)&&-1===b(e[d],c)&&e[d].push(f?c:{listener:c,once:!1});return this},d.on=c("addListener"),d.addOnceListener=function(a,b){return this.addListener(a,{listener:b,once:!0})},d.once=c("addOnceListener"),d.defineEvent=function(a){return this.getListeners(a),this},d.defineEvents=function(a){for(var b=0;b<a.length;b+=1)this.defineEvent(a[b]);return this},d.removeListener=function(a,c){var d,e,f=this.getListenersAsObject(a);for(e in f)f.hasOwnProperty(e)&&(d=b(f[e],c),-1!==d&&f[e].splice(d,1));return this},d.off=c("removeListener"),d.addListeners=function(a,b){return this.manipulateListeners(!1,a,b)},d.removeListeners=function(a,b){return this.manipulateListeners(!0,a,b)},d.manipulateListeners=function(a,b,c){var d,e,f=a?this.removeListener:this.addListener,g=a?this.removeListeners:this.addListeners;if("object"!=typeof b||b instanceof RegExp)for(d=c.length;d--;)f.call(this,b,c[d]);else for(d in b)b.hasOwnProperty(d)&&(e=b[d])&&("function"==typeof e?f.call(this,d,e):g.call(this,d,e));return this},d.removeEvent=function(a){var b,c=typeof a,d=this._getEvents();if("string"===c)delete d[a];else if(a instanceof RegExp)for(b in d)d.hasOwnProperty(b)&&a.test(b)&&delete d[b];else delete this._events;return this},d.removeAllListeners=c("removeEvent"),d.emitEvent=function(a,b){var c,d,e,f,g=this.getListenersAsObject(a);for(e in g)if(g.hasOwnProperty(e))for(d=g[e].length;d--;)c=g[e][d],c.once===!0&&this.removeListener(a,c.listener),f=c.listener.apply(this,b||[]),f===this._getOnceReturnValue()&&this.removeListener(a,c.listener);return this},d.trigger=c("emitEvent"),d.emit=function(a){var b=Array.prototype.slice.call(arguments,1);return this.emitEvent(a,b)},d.setOnceReturnValue=function(a){return this._onceReturnValue=a,this},d._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},d._getEvents=function(){return this._events||(this._events={})},a.noConflict=function(){return e.EventEmitter=f,a},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return a}):"object"==typeof module&&module.exports?module.exports=a:e.EventEmitter=a}.call(this),function(a){function b(a){if(a){if("string"==typeof d[a])return a;a=a.charAt(0).toUpperCase()+a.slice(1);for(var b,e=0,f=c.length;f>e;e++)if(b=c[e]+a,"string"==typeof d[b])return b}}var c="Webkit Moz ms Ms O".split(" "),d=document.documentElement.style;"function"==typeof define&&define.amd?define("get-style-property/get-style-property",[],function(){return b}):"object"==typeof exports?module.exports=b:a.getStyleProperty=b}(window),function(a){function b(a){var b=parseFloat(a),c=-1===a.indexOf("%")&&!isNaN(b);return c&&b}function c(){}function d(){for(var a={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},b=0,c=g.length;c>b;b++){var d=g[b];a[d]=0}return a}function e(c){function e(){if(!m){m=!0;var d=a.getComputedStyle;if(j=function(){var a=d?function(a){return d(a,null)}:function(a){return a.currentStyle};return function(b){var c=a(b);return c||f("Style returned "+c+". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),c}}(),k=c("boxSizing")){var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style[k]="border-box";var g=document.body||document.documentElement;g.appendChild(e);var h=j(e);l=200===b(h.width),g.removeChild(e)}}}function h(a){if(e(),"string"==typeof a&&(a=document.querySelector(a)),a&&"object"==typeof a&&a.nodeType){var c=j(a);if("none"===c.display)return d();var f={};f.width=a.offsetWidth,f.height=a.offsetHeight;for(var h=f.isBorderBox=!(!k||!c[k]||"border-box"!==c[k]),m=0,n=g.length;n>m;m++){var o=g[m],p=c[o];p=i(a,p);var q=parseFloat(p);f[o]=isNaN(q)?0:q}var r=f.paddingLeft+f.paddingRight,s=f.paddingTop+f.paddingBottom,t=f.marginLeft+f.marginRight,u=f.marginTop+f.marginBottom,v=f.borderLeftWidth+f.borderRightWidth,w=f.borderTopWidth+f.borderBottomWidth,x=h&&l,y=b(c.width);y!==!1&&(f.width=y+(x?0:r+v));var z=b(c.height);return z!==!1&&(f.height=z+(x?0:s+w)),f.innerWidth=f.width-(r+v),f.innerHeight=f.height-(s+w),f.outerWidth=f.width+t,f.outerHeight=f.height+u,f}}function i(b,c){if(a.getComputedStyle||-1===c.indexOf("%"))return c;var d=b.style,e=d.left,f=b.runtimeStyle,g=f&&f.left;return g&&(f.left=b.currentStyle.left),d.left=c,c=d.pixelLeft,d.left=e,g&&(f.left=g),c}var j,k,l,m=!1;return h}var f="undefined"==typeof console?c:function(a){console.error(a)},g=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"];"function"==typeof define&&define.amd?define("get-size/get-size",["get-style-property/get-style-property"],e):"object"==typeof exports?module.exports=e(require("desandro-get-style-property")):a.getSize=e(a.getStyleProperty)}(window),function(a){function b(a){"function"==typeof a&&(b.isReady?a():g.push(a))}function c(a){var c="readystatechange"===a.type&&"complete"!==f.readyState;b.isReady||c||d()}function d(){b.isReady=!0;for(var a=0,c=g.length;c>a;a++){var d=g[a];d()}}function e(e){return"complete"===f.readyState?d():(e.bind(f,"DOMContentLoaded",c),e.bind(f,"readystatechange",c),e.bind(a,"load",c)),b}var f=a.document,g=[];b.isReady=!1,"function"==typeof define&&define.amd?define("doc-ready/doc-ready",["eventie/eventie"],e):"object"==typeof exports?module.exports=e(require("eventie")):a.docReady=e(a.eventie)}(window),function(a){function b(a,b){return a[g](b)}function c(a){if(!a.parentNode){var b=document.createDocumentFragment();b.appendChild(a)}}function d(a,b){c(a);for(var d=a.parentNode.querySelectorAll(b),e=0,f=d.length;f>e;e++)if(d[e]===a)return!0;return!1}function e(a,d){return c(a),b(a,d)}var f,g=function(){if(a.matches)return"matches";if(a.matchesSelector)return"matchesSelector";for(var b=["webkit","moz","ms","o"],c=0,d=b.length;d>c;c++){var e=b[c],f=e+"MatchesSelector";if(a[f])return f}}();if(g){var h=document.createElement("div"),i=b(h,"div");f=i?b:e}else f=d;"function"==typeof define&&define.amd?define("matches-selector/matches-selector",[],function(){return f}):"object"==typeof exports?module.exports=f:window.matchesSelector=f}(Element.prototype),function(a,b){"function"==typeof define&&define.amd?define("fizzy-ui-utils/utils",["doc-ready/doc-ready","matches-selector/matches-selector"],function(c,d){return b(a,c,d)}):"object"==typeof exports?module.exports=b(a,require("doc-ready"),require("desandro-matches-selector")):a.fizzyUIUtils=b(a,a.docReady,a.matchesSelector)}(window,function(a,b,c){var d={};d.extend=function(a,b){for(var c in b)a[c]=b[c];return a},d.modulo=function(a,b){return(a%b+b)%b};var e=Object.prototype.toString;d.isArray=function(a){return"[object Array]"==e.call(a)},d.makeArray=function(a){var b=[];if(d.isArray(a))b=a;else if(a&&"number"==typeof a.length)for(var c=0,e=a.length;e>c;c++)b.push(a[c]);else b.push(a);return b},d.indexOf=Array.prototype.indexOf?function(a,b){return a.indexOf(b)}:function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},d.removeFrom=function(a,b){var c=d.indexOf(a,b);-1!=c&&a.splice(c,1)},d.isElement="function"==typeof HTMLElement||"object"==typeof HTMLElement?function(a){return a instanceof HTMLElement}:function(a){return a&&"object"==typeof a&&1==a.nodeType&&"string"==typeof a.nodeName},d.setText=function(){function a(a,c){b=b||(void 0!==document.documentElement.textContent?"textContent":"innerText"),a[b]=c}var b;return a}(),d.getParent=function(a,b){for(;a!=document.body;)if(a=a.parentNode,c(a,b))return a},d.getQueryElement=function(a){return"string"==typeof a?document.querySelector(a):a},d.handleEvent=function(a){var b="on"+a.type;this[b]&&this[b](a)},d.filterFindElements=function(a,b){a=d.makeArray(a);for(var e=[],f=0,g=a.length;g>f;f++){var h=a[f];if(d.isElement(h))if(b){c(h,b)&&e.push(h);for(var i=h.querySelectorAll(b),j=0,k=i.length;k>j;j++)e.push(i[j])}else e.push(h)}return e},d.debounceMethod=function(a,b,c){var d=a.prototype[b],e=b+"Timeout";a.prototype[b]=function(){var a=this[e];a&&clearTimeout(a);var b=arguments,f=this;this[e]=setTimeout(function(){d.apply(f,b),delete f[e]},c||100)}},d.toDashed=function(a){return a.replace(/(.)([A-Z])/g,function(a,b,c){return b+"-"+c}).toLowerCase()};var f=a.console;return d.htmlInit=function(c,e){b(function(){for(var b=d.toDashed(e),g=document.querySelectorAll(".js-"+b),h="data-"+b+"-options",i=0,j=g.length;j>i;i++){var k,l=g[i],m=l.getAttribute(h);try{k=m&&JSON.parse(m)}catch(n){f&&f.error("Error parsing "+h+" on "+l.nodeName.toLowerCase()+(l.id?"#"+l.id:"")+": "+n);continue}var o=new c(l,k),p=a.jQuery;p&&p.data(l,e,o)}})},d}),function(a,b){"function"==typeof define&&define.amd?define("outlayer/item",["eventEmitter/EventEmitter","get-size/get-size","get-style-property/get-style-property","fizzy-ui-utils/utils"],function(c,d,e,f){return b(a,c,d,e,f)}):"object"==typeof exports?module.exports=b(a,require("wolfy87-eventemitter"),require("get-size"),require("desandro-get-style-property"),require("fizzy-ui-utils")):(a.Outlayer={},a.Outlayer.Item=b(a,a.EventEmitter,a.getSize,a.getStyleProperty,a.fizzyUIUtils))}(window,function(a,b,c,d,e){function f(a){for(var b in a)return!1;return b=null,!0}function g(a,b){a&&(this.element=a,this.layout=b,this.position={x:0,y:0},this._create())}function h(a){return a.replace(/([A-Z])/g,function(a){return"-"+a.toLowerCase()})}var i=a.getComputedStyle,j=i?function(a){return i(a,null)}:function(a){return a.currentStyle},k=d("transition"),l=d("transform"),m=k&&l,n=!!d("perspective"),o={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"otransitionend",transition:"transitionend"}[k],p=["transform","transition","transitionDuration","transitionProperty"],q=function(){for(var a={},b=0,c=p.length;c>b;b++){var e=p[b],f=d(e);f&&f!==e&&(a[e]=f)}return a}();e.extend(g.prototype,b.prototype),g.prototype._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},g.prototype.handleEvent=function(a){var b="on"+a.type;this[b]&&this[b](a)},g.prototype.getSize=function(){this.size=c(this.element)},g.prototype.css=function(a){var b=this.element.style;for(var c in a){var d=q[c]||c;b[d]=a[c]}},g.prototype.getPosition=function(){var a=j(this.element),b=this.layout.options,c=b.isOriginLeft,d=b.isOriginTop,e=a[c?"left":"right"],f=a[d?"top":"bottom"],g=this.layout.size,h=-1!=e.indexOf("%")?parseFloat(e)/100*g.width:parseInt(e,10),i=-1!=f.indexOf("%")?parseFloat(f)/100*g.height:parseInt(f,10);h=isNaN(h)?0:h,i=isNaN(i)?0:i,h-=c?g.paddingLeft:g.paddingRight,i-=d?g.paddingTop:g.paddingBottom,this.position.x=h,this.position.y=i},g.prototype.layoutPosition=function(){var a=this.layout.size,b=this.layout.options,c={},d=b.isOriginLeft?"paddingLeft":"paddingRight",e=b.isOriginLeft?"left":"right",f=b.isOriginLeft?"right":"left",g=this.position.x+a[d];c[e]=this.getXValue(g),c[f]="";var h=b.isOriginTop?"paddingTop":"paddingBottom",i=b.isOriginTop?"top":"bottom",j=b.isOriginTop?"bottom":"top",k=this.position.y+a[h];c[i]=this.getYValue(k),c[j]="",this.css(c),this.emitEvent("layout",[this])},g.prototype.getXValue=function(a){var b=this.layout.options;return b.percentPosition&&!b.isHorizontal?a/this.layout.size.width*100+"%":a+"px"},g.prototype.getYValue=function(a){var b=this.layout.options;return b.percentPosition&&b.isHorizontal?a/this.layout.size.height*100+"%":a+"px"},g.prototype._transitionTo=function(a,b){this.getPosition();var c=this.position.x,d=this.position.y,e=parseInt(a,10),f=parseInt(b,10),g=e===this.position.x&&f===this.position.y;if(this.setPosition(a,b),g&&!this.isTransitioning)return void this.layoutPosition();var h=a-c,i=b-d,j={};j.transform=this.getTranslate(h,i),this.transition({to:j,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},g.prototype.getTranslate=function(a,b){var c=this.layout.options;return a=c.isOriginLeft?a:-a,b=c.isOriginTop?b:-b,n?"translate3d("+a+"px, "+b+"px, 0)":"translate("+a+"px, "+b+"px)"},g.prototype.goTo=function(a,b){this.setPosition(a,b),this.layoutPosition()},g.prototype.moveTo=m?g.prototype._transitionTo:g.prototype.goTo,g.prototype.setPosition=function(a,b){this.position.x=parseInt(a,10),this.position.y=parseInt(b,10)},g.prototype._nonTransition=function(a){this.css(a.to),a.isCleaning&&this._removeStyles(a.to);for(var b in a.onTransitionEnd)a.onTransitionEnd[b].call(this)},g.prototype._transition=function(a){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(a);var b=this._transn;for(var c in a.onTransitionEnd)b.onEnd[c]=a.onTransitionEnd[c];for(c in a.to)b.ingProperties[c]=!0,a.isCleaning&&(b.clean[c]=!0);if(a.from){this.css(a.from);var d=this.element.offsetHeight;d=null}this.enableTransition(a.to),this.css(a.to),this.isTransitioning=!0};var r="opacity,"+h(q.transform||"transform");g.prototype.enableTransition=function(){this.isTransitioning||(this.css({transitionProperty:r,transitionDuration:this.layout.options.transitionDuration}),this.element.addEventListener(o,this,!1))},g.prototype.transition=g.prototype[k?"_transition":"_nonTransition"],g.prototype.onwebkitTransitionEnd=function(a){this.ontransitionend(a)},g.prototype.onotransitionend=function(a){this.ontransitionend(a)};var s={"-webkit-transform":"transform","-moz-transform":"transform","-o-transform":"transform"};g.prototype.ontransitionend=function(a){if(a.target===this.element){var b=this._transn,c=s[a.propertyName]||a.propertyName;if(delete b.ingProperties[c],f(b.ingProperties)&&this.disableTransition(),c in b.clean&&(this.element.style[a.propertyName]="",delete b.clean[c]),c in b.onEnd){var d=b.onEnd[c];d.call(this),delete b.onEnd[c]}this.emitEvent("transitionEnd",[this])}},g.prototype.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(o,this,!1),this.isTransitioning=!1},g.prototype._removeStyles=function(a){var b={};for(var c in a)b[c]="";this.css(b)};var t={transitionProperty:"",transitionDuration:""};return g.prototype.removeTransitionStyles=function(){this.css(t)},g.prototype.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},g.prototype.remove=function(){if(!k||!parseFloat(this.layout.options.transitionDuration))return void this.removeElem();var a=this;this.once("transitionEnd",function(){a.removeElem()}),this.hide()},g.prototype.reveal=function(){delete this.isHidden,this.css({display:""});var a=this.layout.options,b={},c=this.getHideRevealTransitionEndProperty("visibleStyle");b[c]=this.onRevealTransitionEnd,this.transition({from:a.hiddenStyle,to:a.visibleStyle,isCleaning:!0,onTransitionEnd:b})},g.prototype.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},g.prototype.getHideRevealTransitionEndProperty=function(a){var b=this.layout.options[a];if(b.opacity)return"opacity";for(var c in b)return c},g.prototype.hide=function(){this.isHidden=!0,this.css({display:""});var a=this.layout.options,b={},c=this.getHideRevealTransitionEndProperty("hiddenStyle");b[c]=this.onHideTransitionEnd,this.transition({from:a.visibleStyle,to:a.hiddenStyle,isCleaning:!0,onTransitionEnd:b})},g.prototype.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},g.prototype.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},g}),function(a,b){"function"==typeof define&&define.amd?define("outlayer/outlayer",["eventie/eventie","eventEmitter/EventEmitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(c,d,e,f,g){return b(a,c,d,e,f,g)}):"object"==typeof exports?module.exports=b(a,require("eventie"),require("wolfy87-eventemitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):a.Outlayer=b(a,a.eventie,a.EventEmitter,a.getSize,a.fizzyUIUtils,a.Outlayer.Item)}(window,function(a,b,c,d,e,f){function g(a,b){var c=e.getQueryElement(a);if(!c)return void(h&&h.error("Bad element for "+this.constructor.namespace+": "+(c||a)));this.element=c,i&&(this.$element=i(this.element)),this.options=e.extend({},this.constructor.defaults),this.option(b);var d=++k;this.element.outlayerGUID=d,l[d]=this,this._create(),this.options.isInitLayout&&this.layout()}var h=a.console,i=a.jQuery,j=function(){},k=0,l={};return g.namespace="outlayer",g.Item=f,g.defaults={containerStyle:{position:"relative"},isInitLayout:!0,isOriginLeft:!0,isOriginTop:!0,isResizeBound:!0,isResizingContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}},e.extend(g.prototype,c.prototype),g.prototype.option=function(a){e.extend(this.options,a)},g.prototype._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),e.extend(this.element.style,this.options.containerStyle),this.options.isResizeBound&&this.bindResize()},g.prototype.reloadItems=function(){this.items=this._itemize(this.element.children)},g.prototype._itemize=function(a){for(var b=this._filterFindItemElements(a),c=this.constructor.Item,d=[],e=0,f=b.length;f>e;e++){var g=b[e],h=new c(g,this);d.push(h)}return d},g.prototype._filterFindItemElements=function(a){return e.filterFindElements(a,this.options.itemSelector)},g.prototype.getItemElements=function(){for(var a=[],b=0,c=this.items.length;c>b;b++)a.push(this.items[b].element);return a},g.prototype.layout=function(){this._resetLayout(),this._manageStamps();var a=void 0!==this.options.isLayoutInstant?this.options.isLayoutInstant:!this._isLayoutInited;this.layoutItems(this.items,a),this._isLayoutInited=!0},g.prototype._init=g.prototype.layout,g.prototype._resetLayout=function(){this.getSize()},g.prototype.getSize=function(){this.size=d(this.element)},g.prototype._getMeasurement=function(a,b){var c,f=this.options[a];f?("string"==typeof f?c=this.element.querySelector(f):e.isElement(f)&&(c=f),this[a]=c?d(c)[b]:f):this[a]=0},g.prototype.layoutItems=function(a,b){a=this._getItemsForLayout(a),this._layoutItems(a,b),this._postLayout()},g.prototype._getItemsForLayout=function(a){for(var b=[],c=0,d=a.length;d>c;c++){var e=a[c];e.isIgnored||b.push(e)}return b},g.prototype._layoutItems=function(a,b){if(this._emitCompleteOnItems("layout",a),a&&a.length){for(var c=[],d=0,e=a.length;e>d;d++){var f=a[d],g=this._getItemLayoutPosition(f);g.item=f,g.isInstant=b||f.isLayoutInstant,c.push(g)}this._processLayoutQueue(c)}},g.prototype._getItemLayoutPosition=function(){return{x:0,y:0}},g.prototype._processLayoutQueue=function(a){for(var b=0,c=a.length;c>b;b++){var d=a[b];this._positionItem(d.item,d.x,d.y,d.isInstant)}},g.prototype._positionItem=function(a,b,c,d){d?a.goTo(b,c):a.moveTo(b,c)},g.prototype._postLayout=function(){this.resizeContainer()},g.prototype.resizeContainer=function(){if(this.options.isResizingContainer){var a=this._getContainerSize();a&&(this._setContainerMeasure(a.width,!0),this._setContainerMeasure(a.height,!1))}},g.prototype._getContainerSize=j,g.prototype._setContainerMeasure=function(a,b){if(void 0!==a){var c=this.size;c.isBorderBox&&(a+=b?c.paddingLeft+c.paddingRight+c.borderLeftWidth+c.borderRightWidth:c.paddingBottom+c.paddingTop+c.borderTopWidth+c.borderBottomWidth),a=Math.max(a,0),this.element.style[b?"width":"height"]=a+"px"}},g.prototype._emitCompleteOnItems=function(a,b){function c(){e.dispatchEvent(a+"Complete",null,[b])}function d(){g++,g===f&&c()}var e=this,f=b.length;if(!b||!f)return void c();for(var g=0,h=0,i=b.length;i>h;h++){var j=b[h];j.once(a,d)}},g.prototype.dispatchEvent=function(a,b,c){var d=b?[b].concat(c):c;if(this.emitEvent(a,d),i)if(this.$element=this.$element||i(this.element),b){var e=i.Event(b);e.type=a,this.$element.trigger(e,c)}else this.$element.trigger(a,c)},g.prototype.ignore=function(a){var b=this.getItem(a);b&&(b.isIgnored=!0)},g.prototype.unignore=function(a){var b=this.getItem(a);b&&delete b.isIgnored},g.prototype.stamp=function(a){if(a=this._find(a)){this.stamps=this.stamps.concat(a);for(var b=0,c=a.length;c>b;b++){var d=a[b];this.ignore(d)}}},g.prototype.unstamp=function(a){if(a=this._find(a))for(var b=0,c=a.length;c>b;b++){var d=a[b];e.removeFrom(this.stamps,d),this.unignore(d)}},g.prototype._find=function(a){return a?("string"==typeof a&&(a=this.element.querySelectorAll(a)),a=e.makeArray(a)):void 0},g.prototype._manageStamps=function(){if(this.stamps&&this.stamps.length){this._getBoundingRect();for(var a=0,b=this.stamps.length;b>a;a++){var c=this.stamps[a];this._manageStamp(c)}}},g.prototype._getBoundingRect=function(){var a=this.element.getBoundingClientRect(),b=this.size;this._boundingRect={left:a.left+b.paddingLeft+b.borderLeftWidth,top:a.top+b.paddingTop+b.borderTopWidth,right:a.right-(b.paddingRight+b.borderRightWidth),bottom:a.bottom-(b.paddingBottom+b.borderBottomWidth)}},g.prototype._manageStamp=j,g.prototype._getElementOffset=function(a){var b=a.getBoundingClientRect(),c=this._boundingRect,e=d(a),f={left:b.left-c.left-e.marginLeft,top:b.top-c.top-e.marginTop,right:c.right-b.right-e.marginRight,bottom:c.bottom-b.bottom-e.marginBottom};return f},g.prototype.handleEvent=function(a){var b="on"+a.type;this[b]&&this[b](a)},g.prototype.bindResize=function(){this.isResizeBound||(b.bind(a,"resize",this),this.isResizeBound=!0)},g.prototype.unbindResize=function(){this.isResizeBound&&b.unbind(a,"resize",this),this.isResizeBound=!1},g.prototype.onresize=function(){function a(){b.resize(),delete b.resizeTimeout}this.resizeTimeout&&clearTimeout(this.resizeTimeout);var b=this;this.resizeTimeout=setTimeout(a,100)},g.prototype.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},g.prototype.needsResizeLayout=function(){var a=d(this.element),b=this.size&&a;return b&&a.innerWidth!==this.size.innerWidth},g.prototype.addItems=function(a){var b=this._itemize(a);return b.length&&(this.items=this.items.concat(b)),b},g.prototype.appended=function(a){var b=this.addItems(a);b.length&&(this.layoutItems(b,!0),this.reveal(b))},g.prototype.prepended=function(a){var b=this._itemize(a);if(b.length){var c=this.items.slice(0);this.items=b.concat(c),this._resetLayout(),this._manageStamps(),this.layoutItems(b,!0),this.reveal(b),this.layoutItems(c)}},g.prototype.reveal=function(a){this._emitCompleteOnItems("reveal",a);for(var b=a&&a.length,c=0;b&&b>c;c++){var d=a[c];d.reveal()}},g.prototype.hide=function(a){this._emitCompleteOnItems("hide",a);for(var b=a&&a.length,c=0;b&&b>c;c++){var d=a[c];d.hide()}},g.prototype.revealItemElements=function(a){var b=this.getItems(a);this.reveal(b)},g.prototype.hideItemElements=function(a){var b=this.getItems(a);this.hide(b)},g.prototype.getItem=function(a){for(var b=0,c=this.items.length;c>b;b++){var d=this.items[b];if(d.element===a)return d}},g.prototype.getItems=function(a){a=e.makeArray(a);for(var b=[],c=0,d=a.length;d>c;c++){var f=a[c],g=this.getItem(f);g&&b.push(g)}return b},g.prototype.remove=function(a){var b=this.getItems(a);if(this._emitCompleteOnItems("remove",b),b&&b.length)for(var c=0,d=b.length;d>c;c++){var f=b[c];f.remove(),e.removeFrom(this.items,f)}},g.prototype.destroy=function(){var a=this.element.style;a.height="",a.position="",a.width="";for(var b=0,c=this.items.length;c>b;b++){var d=this.items[b];d.destroy()}this.unbindResize();var e=this.element.outlayerGUID;delete l[e],delete this.element.outlayerGUID,i&&i.removeData(this.element,this.constructor.namespace)},g.data=function(a){a=e.getQueryElement(a);var b=a&&a.outlayerGUID;return b&&l[b]},g.create=function(a,b){function c(){g.apply(this,arguments)}return Object.create?c.prototype=Object.create(g.prototype):e.extend(c.prototype,g.prototype),c.prototype.constructor=c,c.defaults=e.extend({},g.defaults),e.extend(c.defaults,b),c.prototype.settings={},c.namespace=a,c.data=g.data,c.Item=function(){f.apply(this,arguments)},c.Item.prototype=new f,e.htmlInit(c,a),i&&i.bridget&&i.bridget(a,c),c},g.Item=f,g}),function(a,b){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","fizzy-ui-utils/utils"],b):"object"==typeof exports?module.exports=b(require("outlayer"),require("get-size"),require("fizzy-ui-utils")):a.Masonry=b(a.Outlayer,a.getSize,a.fizzyUIUtils)}(window,function(a,b,c){var d=a.create("masonry");return d.prototype._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns();var a=this.cols;for(this.colYs=[];a--;)this.colYs.push(0);this.maxY=0},d.prototype.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var a=this.items[0],c=a&&a.element;this.columnWidth=c&&b(c).outerWidth||this.containerWidth}var d=this.columnWidth+=this.gutter,e=this.containerWidth+this.gutter,f=e/d,g=d-e%d,h=g&&1>g?"round":"floor";f=Math[h](f),this.cols=Math.max(f,1)},d.prototype.getContainerWidth=function(){var a=this.options.isFitWidth?this.element.parentNode:this.element,c=b(a);this.containerWidth=c&&c.innerWidth},d.prototype._getItemLayoutPosition=function(a){a.getSize();var b=a.size.outerWidth%this.columnWidth,d=b&&1>b?"round":"ceil",e=Math[d](a.size.outerWidth/this.columnWidth);e=Math.min(e,this.cols);for(var f=this._getColGroup(e),g=Math.min.apply(Math,f),h=c.indexOf(f,g),i={x:this.columnWidth*h,y:g},j=g+a.size.outerHeight,k=this.cols+1-f.length,l=0;k>l;l++)this.colYs[h+l]=j;return i},d.prototype._getColGroup=function(a){if(2>a)return this.colYs;for(var b=[],c=this.cols+1-a,d=0;c>d;d++){var e=this.colYs.slice(d,d+a);b[d]=Math.max.apply(Math,e)}return b},d.prototype._manageStamp=function(a){var c=b(a),d=this._getElementOffset(a),e=this.options.isOriginLeft?d.left:d.right,f=e+c.outerWidth,g=Math.floor(e/this.columnWidth);g=Math.max(0,g);var h=Math.floor(f/this.columnWidth);h-=f%this.columnWidth?0:1,h=Math.min(this.cols-1,h);for(var i=(this.options.isOriginTop?d.top:d.bottom)+c.outerHeight,j=g;h>=j;j++)this.colYs[j]=Math.max(i,this.colYs[j])},d.prototype._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var a={height:this.maxY};return this.options.isFitWidth&&(a.width=this._getContainerFitWidth()),a},d.prototype._getContainerFitWidth=function(){for(var a=0,b=this.cols;--b&&0===this.colYs[b];)a++;return(this.cols-a)*this.columnWidth-this.gutter},d.prototype.needsResizeLayout=function(){var a=this.containerWidth;return this.getContainerWidth(),a!==this.containerWidth},d});
jQuery(function($) {
var mobileNav = {
widthLimit: 768,
isActive: false,
$navContainer: $('.header-nav__container'),
$navInternalPageContainer: $('#main_menu_block'),
manageMobileNav: function () {
if (window.innerWidth < mobileNav.widthLimit) {
if (!this.isActive) {
this.addMobileNav();
}
} else {
if (this.isActive) {
this.removeMobileNav();
}
}
},
addMobileNav: function () {
var selectHtml = '<div class="mobile-nav"><select class="mobile-nav__select">';
if(this.$navInternalPageContainer.length) {
$links = this.$navInternalPageContainer.find('a');
} else {
$links = this.$navContainer.find('a');
}
$links.each(function () {
var classHtml = $(this).parents('.lv_02_menu').length ? 'lvl2':'';
var href = $(this).attr('href');
selectHtml += '<option value="' + $(this).attr('href') + '" ' + (location.pathname == href ? 'selected="selected"' : '') + ' class="' + classHtml + '">' + $(this).html() + '</option>';
});
selectHtml += '</select></div>';
this.$navContainer.find('ul').addClass('hidden');
this.$navContainer.prepend(selectHtml);
this.isActive = true;
$('.mobile-nav__select').on('change', function(){
window.location.href = this.value;
});
},
removeMobileNav: function () {
this.$navContainer.find('ul').removeClass('hidden');
this.$navContainer.find('.mobile-nav').remove();
this.isActive = false;
}
};
mobileNav.manageMobileNav();
$(window).resize(function () {
mobileNav.manageMobileNav();
manageBodyClass();
manageMosaic4Col()
});
function manageBodyClass() {
var $body = jQuery('body');
var fullWidthLameLimit = 1350;
var mobileWidthLimit = 768;
var tabletWidthLimit = 1024;
var classesToAdd = [];
var classesToRemove = [];
if (window.innerWidth > fullWidthLameLimit) {
classesToAdd.push('active-full-width-lame');
} else {
classesToRemove.push('active-full-width-lame');
}
if (window.innerWidth > tabletWidthLimit) {
classesToAdd.push('desktop');
} else {
classesToRemove.push('desktop');
}
if (window.innerWidth > mobileWidthLimit && window.innerWidth < tabletWidthLimit) {
classesToAdd.push('tablet');
} else {
classesToRemove.push('tablet');
}
if (window.innerWidth < mobileWidthLimit) {
classesToAdd.push('mobile');
} else {
classesToRemove.push('mobile');
}
classesToAdd = classesToAdd.join(' ');
classesToRemove = classesToRemove.join(' ');
$body.removeClass(classesToRemove);
$body.addClass(classesToAdd);
}
manageBodyClass();
(function animate3Chiffres() {
var $elem = $('.lame-smile-3-chiffres');
if($elem.length) {
$(document).on('scroll.animation3Chiffres', function (e) {
if (isScrolledIntoView($elem)) {
$elem.addClass('animated')
.find('.lame-smile-3-chiffres__visual').addClass('animated');
$(document).off('scroll.animation3Chiffres');
}
});
}
})();
(function manageCarriereSearchCritaries() {
$('.block_with_tabs--list li').on('click', function(){
$('.block_with_tabs--list li').removeClass('active');
$(this).addClass('active');
var id = $(this).attr('data-id');
$('.block_with_tabs--content').css({'display': 'none'});
$('.block_with_tabs--content').filter('"[data-id='+id+']"').css({'display': 'block'});
});
})();
function manageMosaic4Col() {
var dimensionRules = [{min: 980, max: 100000, nbCol: 4}, {min: 480, max: 980, nbCol: 3}, {min: 0, max: 480, nbCol: 2}]
var $mosaicDalles = $('.mosaic--4-col').find('.mosaic__dalle');
var index = 0;
var windowsWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
var nbColMosaic;
var curCol = 0;
var permuteDoubleDalle = function(dalle) {
var $dalle = $(dalle);
$dalle.insertBefore($dalle.prev());
}
for (i = 0; i < dimensionRules.length; ++i) {
if(windowsWidth > dimensionRules[i].min && windowsWidth <= dimensionRules[i].max) {
nbColMosaic = dimensionRules[i].nbCol;
}
}
$mosaicDalles.each(function(){
index++;
if(index - curCol * nbColMosaic  > nbColMosaic) {
curCol++;
};
if( $(this).hasClass('mosaic__dalle--2x') ) {
index++;
if(index - curCol * nbColMosaic  > nbColMosaic) {
curCol++;
permuteDoubleDalle(this);
};
}
});
};
manageMosaic4Col();
(function manageOpeningForm() {
$('.opening-form-button').on('click', function() {
var $formToOpen = $('.' + $(this).data("form-to-open"));
$formToOpen.addClass('open');
$(this).remove();
})
})();
(function manageFilterMobile(){
$('.filter__category').on('click', function(){
if($('body').hasClass('mobile') || $('body').hasClass('tablet')) {
$(this).parent('.filter-container').toggleClass('hover');
}
});
})();
function isScrolledIntoView(elem, delta) {
delta = delta ? delta : 0;
var $elem = $(elem);
var $window = $(window);
var docViewTop = $window.scrollTop();
var docViewBottom = docViewTop + $window.height();
var elemTop = $elem.offset().top;
var elemBottom = elemTop + $elem.height();
return (docViewBottom - delta >= elemBottom);
}
window.setTimeout(function(){
$('.smile__jobs-grid').masonry({
itemSelector: '.smile__jobs-grid',
columnWidth: 185,
gutter: 10
});
}, 4000);
});
function buildSearchLink() {
var urlString = '';
var searchNodeUrl = $('#searchNodeUrl').val();
$('#sortbar').find('ul.filter__category__list').each(function(i, elSelect) {
if (elSelect.id != '') {
var pattern = '/(' + elSelect.id + ')/';
var values = $(elSelect).find('input[type="checkbox"]:checked');
var filterVal = '';
if (values.size() > 0) {
values.each(function(i, elInput){
if (elInput.value == 'all') {
return false;
}
if (i > 0) {
filterVal += ',';
}
filterVal += elInput.value;
});
}
if (filterVal != '') {
urlString += pattern + filterVal;
}
}
});
return searchNodeUrl + urlString;
}
$(document).ready(function() {
$(".filter").click(function() {
$thisForm = $(this).closest('form');
$thisObj  = $(this);
if($(this).hasClass('all') ){
$otherItems = $(this).parent().parent().find('.filter');
if($(this).is(':checked')){
$.each( $otherItems, function() {
$(this).prop('checked', true);
});
}
else{
$.each( $otherItems, function() {
$(this).prop('checked', false);
});
}
}
if($thisObj.is(':checked')){
$items = $thisForm.find('.filter');
$.each( $items, function() {
if( $thisObj.parent().parent().attr('id') != $(this).parent().parent().attr('id')){
$(this).prop('checked', false);
}
});
}else{
$thisObj.parent().parent().find('.all').prop('checked', false);
}
$("#resultFilter").block({
message: '<div class="loading"></div>',
css: { backgroundColor: 'none', border: 'none', width: '33px' },
overlayCSS:  { backgroundColor: '#fbfbfb' }
});
jQuery.ajax({
type : 'POST',
url : $thisForm.attr('action'),
data : $thisForm.serialize(),
success : function(data) {
$('#resultFilter').html(data);
}
});
});
jQuery('.ajax a').live('click', function(e){
e.preventDefault();
var _this = jQuery(this);
var _href = _this.attr('href');
jQuery.get(_href, function(data){
jQuery('.pagination-block__page-container a.active').removeClass('active');
_this.addClass('active');
jQuery('#resultFilter').html(data);
});
});
jQuery('#filter__all_r').live('click', function(e){
e.preventDefault();
var _this = jQuery(this);
var _href = _this.attr('href');
jQuery('.filter').prop('checked', false);
jQuery.get(_href, function(data){
jQuery('#resultFilter').html(data);
});
});
jQuery('#filter__allp').live('click', function(e){
e.preventDefault();
var _this = jQuery(this);
var _href = _this.attr('href');
jQuery('.filter').prop('checked', false);
jQuery.get(_href, function(data){
jQuery('#filter_results').html(data);
});
});
sortbarEvent();
function sortbarEvent() {
$('#sortbar').find('input[type="checkbox"]').bind('click', function(e) {
if (e.target.value != 'all') {
$(e.target).parents('.filter-container').find('.allValue').prop('checked', false);
}
var url = buildSearchLink();
loadAjax(url, 'filter_results');
});
}
function ajaxPager() {
$('#ajax').find('.pagination-block a').bind('click', function(e) {
e.preventDefault();
var url = $(this).attr('href');
loadAjax(url, 'filter_results');
});
}
function loadAjax(urlString, containerId) {
$('#' + containerId).block({
message: '<div class="loading"></div>',
css: { backgroundColor: 'none', border: 'none', width: '33px' },
overlayCSS:  { backgroundColor: '#fbfbfb' }
});
$.ajax({
url: urlString,
type: 'GET',
cache: false,
success: function(data) {
$('#'+ containerId).html(data);
sortbarEvent();
$('#' + containerId).unblock();
ajaxPager();
},
error: function() {}
});
}
jQuery('#filter__all_p').live('click', function(e){
});
});
