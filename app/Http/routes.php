<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'SiteController@index');
Route::get('/contact', 'SiteController@contact');
Route::post('/save', 'SiteController@save');
Route::get('/rss', 'SiteController@rss');
Route::get('/search', 'SiteController@rss');
Route::get('/politique', 'SiteController@politique');
Route::get('/coordonnees', 'SiteController@coordonnees');
Route::get('/legales', 'SiteController@legales');

Route::auth();

Route::group(['prefix'=>'smile_lab'], function()
{
    Route::get('/', 'SmileLabController@index');
    Route::get('/mongogento', ['as' => 'smile_lab.mongogento',   'uses' => 'SmileLabController@mongogento']);
    Route::get('/elasticsuite', ['as' => 'smile_lab.elasticsuite',   'uses' => 'SmileLabController@elasticsuite']);
    Route::get('/occiware', ['as' => 'smile_lab.occiware',   'uses' => 'SmileLabController@occiware']);
    Route::get('/smilecloudng', ['as' => 'smile_lab.smilecloudng',   'uses' => 'SmileLabController@smilecloudng']);
});
Route::group(['prefix'=>'ressources'], function()
{
    Route::get('/', 'RessourcesController@index');
    Route::get('/presentations', ['as' => 'ressources.presentations',   'uses' => 'RessourcesController@presentations']);
    Route::get('/videos', ['as' => 'ressources.videos',   'uses' => 'RessourcesController@videos']);
    Route::get('/livres', ['as' => 'ressources.livres',   'uses' => 'RessourcesController@livres']);
});



Route::group(['prefix'=>'offresservices'], function()
{
    Route::get('/', 'OffresServicesController@index');
    Route::get('/offres', ['as' => 'offresservices.offres',   'uses' => 'OffresServicesController@offres']);
    Route::get('/services', ['as' => 'offresservices.services',   'uses' => 'OffresServicesController@services']);
});

Route::get('/recrutement', 'RecrutementController@index');


Route::group(['prefix'=>'societe'], function()
{
    Route::get('/', 'SocieteController@index');
    Route::get('/actualites', ['as' => 'societe.actualites',   'uses' => 'SocieteController@actualites']);
    Route::get('/agences', ['as' => 'societe.agences',   'uses' => 'SocieteController@agences']);
    Route::get('/vision', ['as' => 'societe.vision',   'uses' => 'SocieteController@vision']);
    Route::get('/histoire', ['as' => 'societe.histoire',   'uses' => 'SocieteController@histoire']);
    Route::get('/dirigeants', ['as' => 'societe.dirigeants',   'uses' => 'SocieteController@dirigeants']);
    Route::get('/chiffres', ['as' => 'societe.chiffres',   'uses' => 'SocieteController@chiffres']);
    Route::get('/qualite', ['as' => 'societe.qualite',   'uses' => 'SocieteController@qualite']);
    Route::get('/communiques', ['as' => 'societe.communiques',   'uses' => 'SocieteController@communiques']);
    Route::get('/presse', ['as' => 'societe.presse',   'uses' => 'SocieteController@presse']);
    Route::get('/partenaires', ['as' => 'societe.partenaires',   'uses' => 'SocieteController@partenaires']);
    Route::get('/_one_actualites/{id}', ['as' => 'societe._one_actualites',   'uses' => 'SocieteController@actualites1']);
    Route::get('/one_presse/{id}', ['as' => 'societe.one_presse',   'uses' => 'SocieteController@presse1']);

});


Route::group(['prefix'=>'references'], function()
{
    Route::get('/', 'ReferencesController@index');
    Route::get('/clients', 'ReferencesController@clients');
    Route::get('/one_reference/{id}', ['as' => 'references.one_reference',   'uses' => 'ReferencesController@referencesOne']);
});



Route::group(['prefix'=>'produits'], function()
{
    Route::get('/', 'ProduitsController@index');
    Route::get('/produits_one/{id}', ['as' => 'produits.produits_one',   'uses' => 'ProduitsController@indexOne']);
});

Route::group(['prefix'=>'evenements'], function()
{
    Route::get('/', 'EvenementsController@index');
    Route::get('/old',      ['as' => 'evenements.old', 'uses' => 'EvenementsController@indexOld']);
    Route::get('/one_evenement/{id}', ['as' => 'evenements.one_evenement',   'uses' => 'EvenementsController@oneEvenements']);
});

Route::group(['prefix'=>'home'], function()
{
    Route::get('/index', 'HomeController@index');
	Route::get('/', 'HomeController@index');
	
    Route::get('/create',               ['as' => 'home.create', 'uses' => 'HomeController@create']);
    Route::get('/massage',              ['as' => 'home.massage', 'uses' => 'HomeController@massage']);
    Route::get('/massage_one/{id}',     ['as' => 'home.massage_one', 'uses' => 'HomeController@massage_one']);
    Route::post('/store',               ['as' => 'home.store',  'uses' => 'HomeController@store']);
    Route::post('/update',              ['as' => 'home.update',  'uses' => 'HomeController@update']);
    Route::get('/edit/{id}',            ['as' => 'home.edit',   'uses' => 'HomeController@edit']);
    Route::get('/deleted/{id}',         ['as' => 'home.deleted',   'uses' => 'HomeController@deleted']);
    Route::get('/show_message/{id}',    ['as' => 'home.deleted',   'uses' => 'HomeController@showMessage']);
    Route::get('/delete_message/{id}',  ['as' => 'home.deleted',   'uses' => 'HomeController@deleteMessage']);
});