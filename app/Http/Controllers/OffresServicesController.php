<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class OffresServicesController extends Controller
{
    public function index()
    {
        return view('offres_services.index');
    }
    public function offres()
    {
        return view('offres_services.offres');
    }
    public function offres1()
    {
        return view('offres_services.offres-1');
    }
    public function offres2()
    {
        return view('offres_services.offres-2');
    }
    public function offres3()
    {
        return view('offres_services.offres-3');
    }
    public function offres4()
    {
        return view('offres_services.offres-4');
    }
    public function offres5()
    {
        return view('offres_services.offres-5');
    }
    public function services()
    {
        return view('offres_services.services');
    }
    public function services1()
    {
        return view('offres_services.services-1');
    }
    public function services2()
    {
        return view('offres_services.services-2');
    }
    public function services3()
    {
        return view('offres_services.services-3');
    }
    public function services4()
    {
        return view('offres_services.services-4');
    }
    public function services5()
    {
        return view('offres_services.services-5');
    }
    public function services6()
    {
        return view('offres_services.services-6');
    }
    public function services7()
    {
        return view('offres_services.services-7');
    }
}
