<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Actualites;
use App\Models\Feedback;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Actualites $actualitesModel)
    {
        $actualites = $actualitesModel->getPublishedActualites();
        return view('home',['actualites' => $actualites]);
    }


    public function create()
    {
        return view('admin.create');
    }

    public function edit($id)
    {
        if (!$id){
            return redirect('home');
        }
        $post=Actualites::find($id);
        return view('admin.edit',['post'=>$post]);
    }

    public function deleted($id)
    {
        $actualites=Actualites::find($id);
        $actualites->delete();
        return redirect('home');
    }

    public function store(Actualites $postModel, Request $request)
    {
        $postModel->create($request->all());
    }

    public function update(Actualites $postModel, Request $request, $id)
    {
        $model = $postModel::find($id);
        $model->update($request->all());
        $model->save();
        return redirect('home');
    }

    public function massage(Feedback $feedbackModel)
    {
        $feedback = $feedbackModel->getFeedback();

        return view('admin.massage',[
            'feedback' => $feedback
        ]);
    }
    public function showMessage(Feedback $feedbackModel, $id)
    {
        $feedback = $feedbackModel::find($id);
        if ($feedback->new == 1){
            $feedback->new = 0;
            $feedback->save();
        }

        $massage = $feedbackModel->getOneFeedback($id);

        return view('admin.show_massage',[
            'feedback' => $massage[0]
        ]);
    }

    public function deleteMessage(Feedback $feedbackModel,$id)
    {
        $mass=$feedbackModel::find($id);
        $mass->delete();
        return redirect('/home/massage');
    }

}
