<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\References;

class ReferencesController extends Controller
{
    public function index(References $references)
    {
        $products = $references->getReferences();

        return view('references.index',['products'=>$products]);
    }
    public function referencesOne()
    {
        return view('references._one_clients');
    }
    public function clients()
    {
        return view('references.clients');
    }
}
