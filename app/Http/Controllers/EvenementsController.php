<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Evenements;

class EvenementsController extends Controller
{
    public function index(Evenements $evenements)
    {
        $evenementsAll = $evenements->getFutureEvenements();
        return view('evenements.index', ['evenementsAll'=>$evenementsAll]);
    }
    public function oneEvenements($id)
    {
        $evenement=Evenements::find($id);

        return view('evenements.one',['evenement'=>$evenement]);
    }
    public function indexOld(Evenements $evenements)
    {
        $evenementsAll = $evenements->getPastEvenements();

        return view('evenements.old_index', ['evenementsAll'=>$evenementsAll]);
    }
}
