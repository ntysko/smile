<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Product;

class ProduitsController extends Controller
{
    public function index(Product $product)
    {
        $products = $product->getProduit();
        return view('produits.index',['products'=>$products]);
    }
    public function indexOne($id)
    {
        $prod = Product::find($id);
        return view('produits.index_one',['product' =>$prod]);
    }
}
