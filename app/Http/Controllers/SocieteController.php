<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Actualites;
use App\Models\Communiques;
use App\Models\Presse;

class SocieteController extends Controller
{
    public function index()
    {
        return view('societe.index');
    }
    public function actualites(Actualites $actualites)
    {
        $articles = $actualites->getActualites();
        return view('societe.actualites',['articles'=>$articles]);
    }
    public function actualites1()
    {
        return view('societe._one_actualites');
    }
    public function presse1()
    {
        return view('societe._one_presse');
    }
    public function agences()
    {
        return view('societe.agences');
    }
    public function histoire()
    {
        return view('societe.histoire');
    }
    public function dirigeants()
    {
        return view('societe.dirigeants');
    }
    public function chiffres()
    {
        return view('societe.chiffres');
    }
    public function partenaires()
    {
        return view('societe.partenaires');
    }
    public function qualite()
    {
        return view('societe.qualite');
    }
    public function communiques(Communiques $communiques)
    {

        $articles = $communiques->getCommuniques();
        return view('societe.communiques',['articles'=>$articles]);
    }
    public function presse(Presse $presse)
    {
        $articles = $presse->getPresse();
        return view('societe.presse',['articles'=>$articles]);

    }
    public function presseOne()
    {
        return view('societe.presse');
    }
    public function vision()
    {
        return view('societe.vision');
    }

}
