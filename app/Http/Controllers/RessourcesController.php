<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class RessourcesController extends Controller
{
    public function index()
    {
        return view('ressources.index');
    }
    public function livres()
    {
        return view('ressources.livres');
    }
    public function videos()
    {
        return view('ressources.videos');
    }
    public function presentations()
    {
        return view('ressources.presentetions');
    }
}
