<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SmileLabController extends Controller
{
    public function index()
    {
        return view('smile_lab.index');
    }
    public function mongogento()
    {
        return view('smile_lab.mongogento');
    }
    public function elasticsuite()
    {
        return view('smile_lab.elasticsuite');
    }
    public function occiware()
    {
        return view('smile_lab.occiware');
    }
    public function smilecloudng()
    {
        return view('smile_lab.smilecloudng');
    }
}
