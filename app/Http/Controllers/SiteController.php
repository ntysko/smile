<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Feedback;
use App\Models\Pays;
use App\Models\Subject;

class SiteController extends Controller
{
    public function index()
    {
        return view('site.index');
    }
    public function contact(Pays $pays, Subject $subject)
    {
        return view('site.contact',
            [
                'pays' => $pays->getPays(),
                'subject' => $subject->getSubject(),
            ]);
    }
    public function save( Request $request)
    {
        $postModel = new Feedback();
        $rules = $postModel->rules();

        if (!$this->validate($request,  $rules))
        {
            $postModel->create($request->all());
            return redirect('contact');
        }

    }
    public function rss()
    {
        return view('site.rss');
    }
    public function search()
    {
        return view('site.search');
    }
    public function politique()
    {
        return view('site.politique');
    }
    public function coordonnees()
    {
        return view('site.coordonnees');
    }
    public function legales()
    {
        return view('site.legales');
    }
}
