<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Post;

class PostController extends Controller
{
    /**
     * auth
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show list posts
     *
     */
    public function index()
    {
        return view('admin.post.index');
    }
    /**
     * Create post
     *
     */
    public function create()
    {
        return view('admin.post.create');
    }
    /**
     * Show list posts
     *
     */
    public function edit()
    {
        return view('admin.post.edit');
    }
    /**
     * Show list posts
     *
     */
    public function store(Post $postModel, Request $request)
    {
        $postModel->create($request->all());
        return redirect('home');
    }
    /**
     * Show list posts
     *
     */
    public function show()
    {
        return view('admin.post.show');
    }
    /**
     * Show list posts
     *
     */
    public function update()
    {
        return view('admin.post.update');
    }
}
