<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class References extends Model
{
    protected $table = 'references';

    protected $fillable = [
        'title','title2','image','domaines','offres','services','outils','link','slang','comment','text'
    ];

    public function getReferencesForFooter()
    {
        $posts = References::latest('id')
            ->limit(3)
            ->get();

//        print_r($posts);die();
        return $posts;
    }

    public function getReferences()
    {
        $posts = References::all();

//        print_r($posts);die();
        return $posts;
    }
}
