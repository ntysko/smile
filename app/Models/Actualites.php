<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actualites extends Model
{
    protected $table = 'actualites';

    protected $fillable = [
        'title', 'text', 'date'
    ];

    public function getPublishedActualites()
    {
        $posts = Actualites::latest('date')
            ->get();
        return $posts;
    }


    public function getActualitesForRight()
    {
        $posts = Actualites::latest('date')
            ->limit(3)
            ->get();
        return $posts;
    }
    public function getActualites()
    {
        $posts = Actualites::latest('date')
            ->get();
        return $posts;
    }
}
