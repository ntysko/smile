<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'product';

    protected $fillable = [
        'title', 'image', 'quote', 'notre_offre', 'fiche_detaillee'
    ];
    public function getProduit()
    {
        $posts = Product::all();

//        print_r($posts);die();
        return $posts;
    }

}
