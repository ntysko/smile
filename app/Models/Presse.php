<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'presse';

    protected $fillable = [
        'title', 'link', 'source', 	'text', 'date'
    ];
    public function getPresseForFooter()
    {
        $posts = Presse::latest('date')
            ->limit(3)
            ->get();

//        print_r($posts);die();
        return $posts;
    }

    public function getPresseForRight()
    {
        $posts = Presse::latest('date')
            ->limit(3)
            ->get();

//        print_r($posts);die();
        return $posts;
    }

    public function getPresse()
    {
        $posts = Presse::latest('date')
            ->get();

//        print_r($posts);die();
        return $posts;
    }
}
