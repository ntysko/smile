<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subject';

    protected $fillable = [
        'subject_title'
    ];

    public function getSubject()
    {
        return Subject::lists('subject_title','id');
    }
}
