<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Feedback extends Model
{
    protected $table = 'feedback';

    protected $fillable = [
        'subject_id', 'name', 'surname', 'email', 'department', 'pay_id', 'phone', 'societe', 'fonction','massage','new'
    ];

    public function rules()
    {
        return [
            'name'      => 'required | min:2 | max:20',
            'surname'   => 'required | min:2 | max:20',
            'email'     => 'required | email',
            'pay_id'    => 'required',
            'phone'     => 'regex:/^([+]?[0-9\s-\(\)]{3,25})*$/i',
            'massage'   => 'required | min:10'
        ];
    }

    public function getFeedback()
    {
        $posts = Feedback::latest('created_at')
            ->get();
        return $posts;
    }

    public function getOneFeedback($id)
    {
        $posts = DB::table('feedback')
            ->where('feedback.id','=', $id)
            ->join('subject', 'feedback.subject_id', '=', 'subject.id')
            ->join('pays', 'feedback.pay_id', '=', 'pays.id')
            ->get();
        return $posts;
    }
}
