<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Evenements extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'evenements';

    protected $fillable = [
        'title', 'text_small', 'img_small', 'text', 'adresse',	'date'
    ];

    public function getFutureEvenements()
    {
        $posts = Evenements::latest('date')
            ->where('date', '>=', date('Y-m-d'))
            ->get();

        return $posts;
    }

    public function getPastEvenements()
    {
        $posts = Evenements::latest('date')
            ->where('date', '<=', date('Y-m-d'))
            ->get();

        return $posts;
    }
}
