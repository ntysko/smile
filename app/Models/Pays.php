<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
    protected $table = 'pays';

    protected $fillable = [
        'pay_title'
    ];
    public function getPays()
    {

        return Pays::lists('pay_title','id');
    }
}
