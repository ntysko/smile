<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['title', 'excerpt', 'content', 'published', 'created_at', 'updated_at'];

    public function getPublishedPosts()
    {
        $posts = Post::latest('id')
            ->where('published','=', 1 )
            ->get();

        return $posts;
    }
}
