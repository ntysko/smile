<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Communiques extends Model
{
    protected $table = 'communiques';

    protected $fillable = [
        'title','files',  'text', 'date'
    ];

    public function getCommuniques()
    {
        $posts = Communiques::latest('date')
            ->get();

//        print_r($posts);die();
        return $posts;
    }
}
