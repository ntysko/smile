<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Presse;

class RightPresse extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $modelPresse = new Presse();
        $presseRight =$modelPresse->getPresseForRight();
        return view("widgets.right_presse", [
            'config' => $this->config,
            'presseRight'=>$presseRight
        ]);
    }
}