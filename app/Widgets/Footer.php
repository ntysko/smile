<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Presse;
use App\Models\References;

class Footer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        $modelPresse = new Presse();
        $presse =$modelPresse->getPresseForFooter();
        $modelReferences = new References();
        $references =$modelReferences->getReferencesForFooter();

        return view("widgets.footer", [
            'config' => $this->config,
            'presses' => $presse,
            'references' => $references
        ]);
    }
}