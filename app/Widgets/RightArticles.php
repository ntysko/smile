<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Actualites;

class RightArticles extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $modelPresse = new Actualites();
        $actualitesRight =$modelPresse->getActualitesForRight();
        return view("widgets.right_articles", [
            'config' => $this->config,
            'actualitesRight'=>$actualitesRight
        ]);
    }
}