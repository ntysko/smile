<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class LeftNav extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
       $a = app('request')->route()->getAction();

        return view("widgets.left_nav", [
            'config' => $this->config, 'action' => $a['prefix']
        ]);
    }
}