@extends('layouts.frontend')

@section('content')

    <div style="background: #3c6d69;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=img/Smile-Lab’.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Smile Lab</span>
                </div>

                <h2 class="secondary-title">Smile Lab</h2>

                <div class="rte_ctnt_block">
                    SmileLab est l’entité qui a pour mission de porter l’innovation chez Smile.<br />
                    <br />
                    Entité de prospective et d’expérimentation, notre équipe pluridisciplinaire rassemble des experts de la technologie, de l’innovation et du marketing. Ensemble, nous explorons et inventons le futur au service de nos clients. <br />
                    <br />
                    Pour repousser encore et encore l’état de l’art des technologies open source, Smile investit massivement en recherche et développement. Réalisation d’extensions, créa­tion d’outils open source prometteurs, mise en œuvre d’envergure des meilleures solu­tions open source…  Smile multiplie les inves­tissements pour mieux vous servir.<br />
                    <br />
                    L’expertise de SmileLab se construit sur les technologies de demain, en poussant nos solutions plus loin par la création d’assets différentiants, ou encore en investissant de nouveaux domaines, le tout en relation avec nos clients pour une démarche de co-innovation.<br />
                    <br />
                    En 2016, SmileLab intervient sur 3 thèmes de recherche majeurs : Cloud&amp;DevOps, BigData, Search&amp;NoSql.					</div>
                <div class="smilelab__block">
                    <h3>Selection de quelques projets R&amp;D de Smile Lab</h3>
                    <div class="smilelab__projects">
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--media">
                                <img src="img/Mongogento.jpg"/>
                            </div>
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/mongogento">Mongogento</a>
                                </h2>
                                Smile a intégré Magento
                                et MongoDB
                                , base NoSQL.Conçue pour répondre aux enjeux de stockage de catalogues les plus volumineux, cette solution évolutive vous apportera des performances inégalées.De...
                                <a title='Lire la suite' href="/smile_lab/mongogento" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--media">
                                <img src="img/ElasticSuite.jpg"/>
                            </div>
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/elasticsuite">ElasticSuite</a>
                                </h2>
                                Le serchandising Magento pour la 1ère fois en Open Source&amp;nbsp;! Optimiser la pertinence de votre recherche grâce à un produit 100% développé par SmileLab’&amp;nbsp;: Elastic Suite for Magento.
                                <a title='Lire la suite' href="/smile_lab/elasticsuite" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--media">
                                <img src="img/SmileCloudNG.jpg"/>
                            </div>
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/smilecloudng">SmileCloudNG</a>
                                </h2>
                                Smile annonce une mise à jour majeure de son infrastructure cloud basée sur OpenStack. Avec le déploiement des solutions Ceph et Neutron, notre cloud permet dorénavant la virtualisation du stockage...
                                <a title='Lire la suite' href="/smile_lab/smilecloudng" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="smilelab__block">
                    <h3>Les derniers articles de blog de Smile'Lab</h3>

                    <div class="smilelab__news">
                        <div class="smilelab__news--item clearfix">
                            <div class="smilelab__news--header">
                                <div class="smilelab__news--date">06 juin 2016</div>
                                <div class="smilelab__news--share">
                                    <span class='st_sharethis' st_url="/Le-blog-des-experts/Elastic-suite-for-magento-2-sortie-de-la-version-2-1" displayText="Partager"></span>
                                </div>
                            </div>
                            <div class="smilelab__news--content">
                                <h2>
                                    <a href='http://blog.smile.fr/Elastic-suite-for-magento-2-sortie-de-la-version-2-1' target="_blank">
                                        Elastic Suite for Magento 2 : sortie de la version 2.1
                                    </a>
                                </h2>
                                SmileLab a publié la nouvelle version de Magento Elastic Suite pour Magento 2 le 6 juin 2016, dont voici le contenu. 				              	</div>
                            <a title='Lire la suite' href='http://blog.smile.fr/Elastic-suite-for-magento-2-sortie-de-la-version-2-1' class="smilelab__item--more" target="_blank">Lire la suite</a>
                        </div>
                    </div>

                    <div class="smilelab__news">
                        <div class="smilelab__news--item clearfix">
                            <div class="smilelab__news--header">
                                <div class="smilelab__news--date">01 avril 2016</div>
                                <div class="smilelab__news--share">
                                    <span class='st_sharethis' st_url="/Le-blog-des-experts/Magento-elastic-suite-sortie-de-la-version-2-0-compatible-magento-2" displayText="Partager"></span>
                                </div>
                            </div>
                            <div class="smilelab__news--content">
                                <h2>
                                    <a href='http://blog.smile.fr/Magento-elastic-suite-sortie-de-la-version-2-0-compatible-magento-2' target="_blank">
                                        Magento Elastic Suite : sortie de la version 2.0  compatible Magento 2
                                    </a>
                                </h2>
                                C’est officiel, notre entité de R&amp;D SmileLab vient de publier son très célèbre module de searchandising et de merchandising Elastic Suite en version 2.0 afin de le rendre compatible avec Magento 2. 				              	</div>
                            <a title='Lire la suite' href='http://blog.smile.fr/Magento-elastic-suite-sortie-de-la-version-2-0-compatible-magento-2' class="smilelab__item--more" target="_blank">Lire la suite</a>
                        </div>
                    </div>

                    <div class="smilelab__news">
                        <div class="smilelab__news--item clearfix">
                            <div class="smilelab__news--header">
                                <div class="smilelab__news--date">15 février 2016</div>
                                <div class="smilelab__news--share">
                                    <span class='st_sharethis' st_url="/Le-blog-des-experts/Magento-elastic-suite-sortie-de-la-version-1-3-0" displayText="Partager"></span>
                                </div>
                            </div>
                            <div class="smilelab__news--content">
                                <h2>
                                    <a href='http://blog.smile.fr/Magento-elastic-suite-sortie-de-la-version-1-3-0' target="_blank">
                                        Magento Elastic Suite : Sortie de la version 1.3.0
                                    </a>
                                </h2>
                                SmileLab a publié la nouvelle version de Magento Elastic Suite (MagentoES) le 15 février 2016, dont voici le contenu. 				              	</div>
                            <a title='Lire la suite' href='http://blog.smile.fr/Magento-elastic-suite-sortie-de-la-version-1-3-0' class="smilelab__item--more" target="_blank">Lire la suite</a>
                        </div>
                    </div>

                    <div class="smilelab__news">
                        <div class="smilelab__news--item clearfix">
                            <div class="smilelab__news--header">
                                <div class="smilelab__news--date">02 février 2016</div>
                                <div class="smilelab__news--share">
                                    <span class='st_sharethis' st_url="/Le-blog-des-experts/Magento-elastic-suite-sortie-de-la-version-1-2-0" displayText="Partager"></span>
                                </div>
                            </div>
                            <div class="smilelab__news--content">
                                <h2>
                                    <a href='http://blog.smile.fr/Magento-elastic-suite-sortie-de-la-version-1-2-0' target="_blank">
                                        Magento Elastic Suite : Sortie de la version 1.2.0
                                    </a>
                                </h2>
                                SmileLab a publié la nouvelle version de Magento Elastic Suite (MagentoES) le 2 février 2016, dont voici le contenu.				              	</div>
                            <a title='Lire la suite' href='http://blog.smile.fr/Magento-elastic-suite-sortie-de-la-version-1-2-0' class="smilelab__item--more" target="_blank">Lire la suite</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>

@stop