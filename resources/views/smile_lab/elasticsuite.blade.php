@extends('layouts.frontend')

@section('content')

    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/smile_lab" title="Smile Lab">Smile Lab</a>
                    <span class="separator"></span>
                    <span class="current_page">ElasticSuite</span>
                </div>

                <h2 class="secondary-title">ElasticSuite</h2>
                <div class="rte_ctnt_block">

                    <p class=" text-left">Le serchandising Magento pour la 1ère fois en Open Source&nbsp;! Optimiser la pertinence de votre recherche grâce à un produit 100% développé par SmileLab’&nbsp;: Elastic Suite for Magento.</p>
                    <p class=" text-left">La solution combine 4 méthodes pour optimiser la pertinence de recherche et améliorer la conversion e-commerce&nbsp;: règle de merchandising, tracking du comportement des utilisateurs, pertinence full-text, contexte clients.</p><p class=" text-left">Les fonctionnalités clés&nbsp;:</p>
                    <ul>

                        <li>Recherche Full-text</li>

                        <li>Auto-complétion</li>

                        <li>Filtres élaborés</li>

                        <li>Catégories intelligentes</li>

                        <li>Tuning des optimisations</li>

                        <li>Données comportementales</li>

                        <li>Prévisualisation</li>

                        <li>Tri des résultats de recherche</li>

                        <li>Intégration dans Magento.</li>

                    </ul>
                    <p class=" text-left"><b>Découvrez le détail des fonctionnalités sur la page dédiée&nbsp;: <a href="http://magento-elastic-suite.io/" target="_blank">http://magento-elastic-suite.io/</a>
                        </b></p><p class=" text-left">Vous n’avez pas encore ElasticSuite for Magento ? <b><a href="https://github.com/Smile-SA/smile-magento-elasticsearch" target="_blank">Téléchargez-le sur Github ! &gt;</a>
                        </b></p><p class=" text-left">Besoin de conseils ou de tout simplement en savoir plus ? <a href="http://magento-elastic-suite.io/#page6" target="_blank">Contactez-nos experts&nbsp;!</a>
                    </p>					</div>
                <div class="smilelab__block">
                    <h3>Selection de quelques projets R&amp;D de Smile Lab</h3>
                    <div class="smilelab__projects">
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/mongogento">Mongogento</a>
                                </h2>
                                Smile a intégré Magento
                                et MongoDB
                                , base NoSQL.Conçue pour répondre aux enjeux de stockage de catalogues les plus volumineux, cette solution évolutive vous apportera des performances inégalées.De...
                                <a title='Lire la suite' href="/smile_lab/mongogento" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/smilecloudng">SmileCloudNG</a>
                                </h2>
                                Smile annonce une mise à jour majeure de son infrastructure cloud basée sur OpenStack. Avec le déploiement des solutions Ceph et Neutron, notre cloud permet dorénavant la virtualisation du stockage...
                                <a title='Lire la suite' href="/smile_lab/smilecloudng" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/occiware">OCCIWARE</a>
                                </h2>
                                Le projet Investissements d'Avenir OCCIware vise un cadre formel, outillé et standardisé pour la gestion de toutes ressources en nuage afin de décloisonner couches, métiers et domaines du Cloud Com...
                                <a title='Lire la suite' href="/smile_lab/occiware" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>


@stop