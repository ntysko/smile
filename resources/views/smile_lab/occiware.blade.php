@extends('layouts.frontend')

@section('content')

    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/smile_lab" title="Smile Lab">Smile Lab</a>
                    <span class="separator"></span>
                    <span class="current_page">OCCIWARE</span>
                </div>

                <h2 class="secondary-title">OCCIWARE</h2>
                <div class="rte_ctnt_block">

                    <p>Le projet Investissements d'Avenir OCCIware vise un cadre formel, outillé et standardisé pour la gestion de toutes ressources en nuage afin de décloisonner couches, métiers et domaines du Cloud Computing : Data Center, Déploiement, Big Data, Linked Data...</p>
                    <p>Le projet Investissements d'Avenir OCCIware vise un cadre formel, outillé et standardisé pour la gestion de toutes ressources en nuage afin de décloisonner couches, métiers et domaines du Cloud Computing : Data Center, Déploiement, Big Data, Linked Data...</p><p>
                        Le projet Investissements d'Avenir (Cloud &amp; Big Data 4) <a href="http://www.occiware.org">OCCIware</a>
                        a pour but de faire tomber les barrières qui séparent les différentes couches, domaines et métiers du Cloud Computing, en apportant au standard <a href="http://occi-wg.org/">Open Cloud Computing Interface</a>
                        (OCCI) une fondation de méthodes formelles, un outillage <a href="http://www.eclipse.org">Eclipse</a>
                        , une plateforme d'exécution Models@Runtime, et un démonstrateur pour chacun des domaines Data Center, Déploiement, Big Data, Linked Data.<br />
                        Prévu sur 2015-2018 avec un effort de 72 homme mois, il est coordonné par Open Wide et scientifiquement par l' <a href="http://www.inria.fr">Inria</a>
                        et comprend 3 laboratoires (également <a href="http://www.telecom-sudparis.eu/">Telecom SudParis</a>
                        , <a href="https://www.ujf-grenoble.fr/">Université Joseph Fourrier</a>
                        ), 5 entreprises (également <a href="http://www.obeo.fr/">Obeo</a>
                        , <a href="http://activeeon.com/">ActiveEon</a>
                        , <a href="http://www.scalair.fr/">Scalair</a>
                        , <a href="https://www.linagora.com/">Linagora</a>
                        ) et 2 associations ( <a href="http://www.pole-numerique.fr/">Pôle Numérique</a>
                        , <a href="http://www.ow2.org">OW2</a>
                        ).<br />&nbsp;as a Service (LDaaS), qui met en oeuvre la plateforme Ozwillo (projet OASIS) pour faire de son Open Linked Data Cloud Datacore un véritable &quot;broker&quot; entre sources de données liées collaboratives et Big Data, régionales et réconciliées, triplestore ou plateforme Open Data, tenant compte de leurs niveaux de service variables (lecture seule, performance vs robustesse en écriture, droits...). Il sera validé dans un cas d'usage mêlant Internet des Objets (IoT) et Big Data, où des&nbsp; des données de consommation d'énergie seront récupérées de sondes domotiques et aggrégées en des tableaux de bord &quot;temps réel&quot; non seulement pour l'utilisateur et le fournisseur d'énergie mais encore les territoires.</p>					</div>
                <div class="smilelab__block">
                    <h3>Selection de quelques projets R&amp;D de Smile Lab</h3>
                    <div class="smilelab__projects">
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/mongogento">Mongogento</a>
                                </h2>
                                Smile a intégré Magento
                                et MongoDB
                                , base NoSQL.Conçue pour répondre aux enjeux de stockage de catalogues les plus volumineux, cette solution évolutive vous apportera des performances inégalées.De...
                                <a title='Lire la suite' href="/smile_lab/mongogento" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/elasticsuite">ElasticSuite</a>
                                </h2>
                                Le serchandising Magento pour la 1ère fois en Open Source&amp;nbsp;! Optimiser la pertinence de votre recherche grâce à un produit 100% développé par SmileLab’&amp;nbsp;: Elastic Suite for Magento.
                                <a title='Lire la suite' href="/smile_lab/elasticsuite" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/smilecloudng">SmileCloudNG</a>
                                </h2>
                                Smile annonce une mise à jour majeure de son infrastructure cloud basée sur OpenStack. Avec le déploiement des solutions Ceph et Neutron, notre cloud permet dorénavant la virtualisation du stockage...
                                <a title='Lire la suite' href="/smile_lab/smilecloudng" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>

@stop