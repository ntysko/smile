@extends('layouts.frontend')

@section('content')
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/smile_lab" title="Smile Lab">Smile Lab</a>
                    <span class="separator"></span>
                    <span class="current_page">SmileCloudNG</span>
                </div>

                <h2 class="secondary-title">SmileCloudNG</h2>
                <div class="rte_ctnt_block">

                    <p class=" text-left">Smile annonce une mise à jour majeure de son infrastructure cloud basée sur OpenStack. Avec le déploiement des solutions Ceph et Neutron, notre cloud permet dorénavant la virtualisation du stockage et la virtualisation du réseau, étendant ainsi les capacités de son cloud privé pour ses clients.</p>
                    <p class=" text-left">En parallèle, la migration vers la version Juno d'OpenStack apporte les derniers bugfixs et différentes améliorations dont le dataprocessing apporté par Sahara et la possibilité des solutions «&nbsp;as a Service&nbsp;», comme Hadoop ou Spark ou encore de la messagerie SaaS.</p><p class=" text-left">
                        Cette mise à jour permet tout d'abord de réaliser des migrations à chaud des serveurs, en cas de panne ou de demande de capacité de calcul ou de stockage. <br />Elle permet ensuite une extensibilité forte en terme de stockage répondant aux besoins grandissants des clients, et notamment de bénéficier d'une capacité de stockage objet distribué, très utile pour la diffusion des médias.</p><p class=" text-left">Elle permet enfin une garantie sur la persistance des données assurée par l'infrastructure et non par le logiciel, avec la capacité d'auto-réparation («&nbsp;self-healing&nbsp;») du stockage.</p><p class=" text-left">Pour en savoir plus sur nos solutions d’hébergement, rendez-vous sur&nbsp;: <a href="http://hosting.smile.eu/" target="_blank">http://hosting.smile.eu/</a>
                    </p>					</div>
                <div class="smilelab__block">
                    <h3>Selection de quelques projets R&amp;D de Smile Lab</h3>
                    <div class="smilelab__projects">
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/mongogento">Mongogento</a>
                                </h2>
                                Smile a intégré Magento
                                et MongoDB
                                , base NoSQL.Conçue pour répondre aux enjeux de stockage de catalogues les plus volumineux, cette solution évolutive vous apportera des performances inégalées.De...
                                <a title='Lire la suite' href="/smile_lab/mongogento" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/elasticsuite">ElasticSuite</a>
                                </h2>
                                Le serchandising Magento pour la 1ère fois en Open Source&amp;nbsp;! Optimiser la pertinence de votre recherche grâce à un produit 100% développé par SmileLab’&amp;nbsp;: Elastic Suite for Magento.
                                <a title='Lire la suite' href="/smile_lab/elasticsuite" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/occiware">OCCIWARE</a>
                                </h2>
                                Le projet Investissements d'Avenir OCCIware vise un cadre formel, outillé et standardisé pour la gestion de toutes ressources en nuage afin de décloisonner couches, métiers et domaines du Cloud Com...
                                <a title='Lire la suite' href="/smile_lab/occiware" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>

@stop