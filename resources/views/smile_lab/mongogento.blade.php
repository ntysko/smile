@extends('layouts.frontend')

@section('content')

    <div style="background: #3c6d69;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=../img/Smile-Lab’.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/smile_lab" title="Smile Lab">Smile Lab</a>
                    <span class="separator"></span>
                    <span class="current_page">Mongogento</span>
                </div>

                <h2 class="secondary-title">Mongogento</h2>
                <div class="rte_ctnt_block">
                    <h3>Une base de données véloce quelque soit le volume !</h3>

                    <p class=" text-left">Smile a intégré <a href="http://www.open-source-guide.com/Solutions/Applications/E-commerce/Magento" target="_blank">Magento</a>
                        et <a href="http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Bases-de-donnees/Mongodb" target="_blank">MongoDB</a>
                        , base NoSQL.Conçue pour répondre aux enjeux de stockage de catalogues les plus volumineux, cette solution évolutive vous apportera des performances inégalées.</p><p class=" text-left">Des millions de produits... en toute légèreté</p>
                    <p>Grâce à l'intégration de <a href="http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Bases-de-donnees/Mongodb" target="_blank">MongoDB</a>
                        , la taille et l'utilisation de votre base de données est considérablement réduite lui assurant une meilleure disponibilité pour traiter plus de commandes et facilitant sa maintenance.</p><p>Mongogento a été conçu pour opérer sur des catalogues hétérogènes de plusieurs millions de produits tout en réduisant l'impact du modèle EAV de <a href="http://www.open-source-guide.com/Solutions/Applications/E-commerce/Magento" target="_blank">Magento</a>
                        .</p><p>L'utilisation de la base de données NoSQL documentaire <a href="http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Bases-de-donnees/Mongodb" target="_blank">MongoDB</a>
                        pour le stockage des attributs produits permet de réduire la sollicitation de la base de données <a href="http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Bases-de-donnees/Mysql" target="_blank">MySQL</a>
                        tout en conservant la flexibilité du modèle documentaire.</p><p class=" text-left">Des performances inégalées</p><p>Combinée au moteur <a href="http://www.open-source-guide.com/Solutions/Developpement-et-couches-intermediaires/Moteurs-de-recherche/Solr" target="_blank">SolR</a>
                        et à notre système de cache statique <a href="http://www.ecommerce-performances.com/magecache.php" target="_blank">MageCache</a>
                        , l'intégration MongoDB permet d'obtenir des performances front-office exceptionnelle même avec les plus gros catalogues.</p><p>Votre back-office reste par ailleurs réactif, permettant à vos équipes d'être plus productive dans l'animation de votre site eCommerce et de pousser plus encore votre avantage concurrentiel.</p><p>&nbsp;</p><p class=" text-left">Pour en savoir plus, rendez-vous sur&nbsp;: <a href="http://www.ecommerce-performances.com/mongogento.php" target="_blank">http://www.ecommerce-performances.com/mongogento.php</a>
                        et <a href="https://github.com/Smile-SA/mongogento" target="_blank">https://github.com/Smile-SA/mongogento</a>
                    </p>					</div>
                <div class="smilelab__block">
                    <h3>Selection de quelques projets R&amp;D de Smile Lab</h3>
                    <div class="smilelab__projects">
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/elasticsuite">ElasticSuite</a>
                                </h2>
                                Le serchandising Magento pour la 1ère fois en Open Source&amp;nbsp;! Optimiser la pertinence de votre recherche grâce à un produit 100% développé par SmileLab’&amp;nbsp;: Elastic Suite for Magento.
                                <a title='Lire la suite' href="/smile_lab/elasticsuite" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/smilecloudng">SmileCloudNG</a>
                                </h2>
                                Smile annonce une mise à jour majeure de son infrastructure cloud basée sur OpenStack. Avec le déploiement des solutions Ceph et Neutron, notre cloud permet dorénavant la virtualisation du stockage...
                                <a title='Lire la suite' href="/smile_lab/smilecloudng" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                        <div class="smilelab__projects--item">
                            <div class="smilelab__project--content clearfix">
                                <h2 class="secondary-title">
                                    <a href="/smile_lab/occiware">OCCIWARE</a>
                                </h2>
                                Le projet Investissements d'Avenir OCCIware vise un cadre formel, outillé et standardisé pour la gestion de toutes ressources en nuage afin de décloisonner couches, métiers et domaines du Cloud Com...
                                <a title='Lire la suite' href="/smile_lab/occiware" class="smilelab__item--more">Lire la suite</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>

@stop