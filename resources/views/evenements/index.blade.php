@extends('layouts.frontend')

@section('content')
    <div style="background: #c3a374;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Événements.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">

        {{ Widget::RightTwitter() }}



        <div class="common_right_block">
            {{ Widget::RightPresse() }}
        </div>		            	            		    					    	                            </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>
            <div class="common_left_block">
                <div class="item-block__img">





                    <img src="/img/Smile-se-mobilise_encart_new.png"   width="84" height="35"  style="border: 0px;" alt="" title="" />




                </div>
                <div class="item-block__content">

                    <h5>Smile se mobilise</h5><p>sur la politique d'emploi handicap</p>
                    <a href="/recrutement" title="Recrutement">&gt; Découvrir</a>

                </div>
            </div>



        </div>



        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Nos Évènements</span>
                </div>
                <div class="evenements__content">

                    <div class="evenements__list">
                        <p class="section-title">Nos prochains événements</p>
                        <ul>
                            @foreach($evenementsAll as $evenement)
                            <li class="evenements__list__item">
                                <div class="evenements__list__item__date">
                                    <span class="evenements__list__item__date__nb">{{date('j',strtotime($evenement->date)) }}</span>
                                    <span class="evenements__list__item__date__month">{{date('F',strtotime($evenement->date)) }}</span>
                                </div>
                                <div class="evenements__list__item__container">
                                    <div class="evenements__list__item__infos">
                                        <span class="evenements__list__item__info-style  {{$evenement->img_small}}">{{$evenement->text_small}}</span>
                                    </div>
                                    <h3 class="evenements__list__item__title">{{$evenement->title}}</h3>
                                    <p>
                                        {{strip_tags(str_limit($evenement->text, $limit = 250, $end = '...'))}}                                    </p>
                                    <a title="{{$evenement->title}}" class="evenements__list__item__plus-infos" href="{{ url('/evenements/one_evenement', $evenement->id) }}">Plus d'infos</a>
                                </div>
                            </li>
                            @endforeach
                        </ul>

                        <div class="pagination-block">
                        </div>

                        <a title="Nos événements précédents" href="/evenements/old" class="btn--main">Nos événements précédents</a>
                    </div>
                </div>
            </div>
        </div>



    </div>



    <div class="breaker"></div>

@endsection