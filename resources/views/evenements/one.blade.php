@extends('layouts.frontend')

@section('content')
    <div style="background: #c3a374;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Événements.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">




        <div class="common_right_block">
            {{ Widget::RightPresse() }}
        </div>		            	            		    					    	                            </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>
            <div class="common_left_block">
                <div class="item-block__img">





                    <img src="/img/Smile-se-mobilise_encart_new.png"   width="84" height="35"  style="border: 0px;" alt="" title="" />




                </div>
                <div class="item-block__content">

                    <h5>Smile se mobilise</h5><p>sur la politique d'emploi handicap</p>
                    <a href="/recrutement" title="Recrutement">&gt; Découvrir</a>

                </div>
            </div>



        </div>



        <div id="center_col">
            <div id="inner_right"></div>




            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/evenements" title="Événements">Événements</a>
                </div>

                <ul class="page-pager clearfix">
                    <li class="next"><a href="{{ url('/evenements/one_evenement', $evenement->id-1) }}" title="Séminaire UX Design Lyon 2016" class="right">Suivant</a></li>
                    <li class="previous"><a href="{{ url('/evenements/one_evenement', $evenement->id+1) }}" title="Smile Gold sponsor DrupalCon 2016" class="left">Précédent</a></li>
                </ul>
                <div class="actualites-detail__content evenement-content">
                    <div class="evenements__list__item__infos">
                        <span class="evenements__list__item__info-style  {{$evenement->img_small}}">{{$evenement->text_small}}</span>
                    </div>				<h2 class="secondary-title">{{$evenement->title}}</h2>
                    <div class="actualites-detail__date__share">
                        <span class="actualites-detail__date">{{$evenement->date}}</span>


                        <span class='st_sharethis'st_url="/Evenements/Rencontres-regionales-du-logiciel-libre-nantes-2016" displayText="Partager"></span>
                    </div>
                    <div class="rte_ctnt_block clearfix">

                        {{strip_tags($evenement->text)}}
                    </div>
        </div>



    </div>
</div>

</div>
    <div class="breaker"></div>

@endsection