@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">All article</div>

                <div class="panel-body">
                    @foreach($actualites as $post)
                        <article>
                            <p>
                                {!! $post->title.' <i>('.($post->date).')</i>' !!}
                                <a href="{{ url('/home/deleted', $post->id) }}">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                                <a href="{{ url('/home/edit',$post->id) }}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </p>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
