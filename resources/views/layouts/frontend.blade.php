<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <!-- Fonts -->
    {{--<link rel="stylesheet" href="/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">--}}

    <!-- Styles -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">--}}
    {{--<link rel="stylesheet" href="css/widget64.css">--}}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>


</head>
<body id="app-layout">

<div class="page page_internal page_4cols societe   " id="societe">


    <div class="minwidth_ie">
        <div class="layout_ie">
            <div class="container_ie">

                <div class="header--fixed">
                    <header class="banniere-promo header-top">
                        <div class="header-top__container">
                            <a href="/" title="Logo Smile : Open source solutions" class="logo">
                                <img src="/img/logo_france.png" alt="Logo Smile : Open source solutions"  />
                            </a>
                            <div class="slogan slogan_fre-FR">
                                {{--<div>Smile, 1<span>er</span></div>--}}
                                {{--<p>--}}
                                    {{--intégrateur européen--}}
                                    {{--<span>de solutions open source</span>--}}
                                {{--</p>--}}

                            </div>

                            <div class="rowHeader">
                                <div class="row1">
                                    <a href="/contact" class="button">Contactez nous</a>
                                    <div class="suivez_nous">
                                        Suivez nous :
                                        <a target="_blank" title="twitter" href="{{ url('https://twitter.com/GroupeSmile') }}" class="twitter">twitter</a>
                                        <a target="_blank" title="facebook" href="{{ url('https://www.facebook.com/smileopensource') }}" class="facebook">facebook</a>
                                        <a target="_blank" title="Linkedin" href="{{ url('https://www.linkedin.com/company/smile') }}" class="Linkedin">Linkedin</a>
                                        <a target="_blank" title="youtube" href="{{ url('https://www.youtube.com/user/SmileOpenSource') }}" class="youtube">youtube</a>
                                        <a target="_blank" title="slideshare" href="{{ url('http://fr.slideshare.net/smileopensource') }}" class="slideshare">slideshare</a>
                                        <a target="_blank" title="github" href="{{ url('https://github.com/Smile-SA') }}" class="github">github</a>
                                        <a href="{{ url('/rss') }}" title="RSS" class="rss">RSS</a>
                                    </div>
                                </div>

                                <div class="row2">
                                    <div class="link_sites">
                                        <a id="sitesLink" href="#">autres sites Smile</a>
                                        <div class="contenu_site">
                                            <a target="_blanck" href="{{ url('http://www.smile-oss.com') }}" class="site_link last_item">Group</a>
                                            <a target="_blanck" href="{{ url('http://be.smile.eu') }}" class="site_link ">Belgique</a>
                                            <a target="_blanck" href="{{ url('http://smile.ci') }}" class="site_link ">Côte d'Ivoire</a>
                                            <a target="_blanck" href="{{ url('http://smile.ma') }}" class="site_link ">Maroc</a>
                                            <a target="_blanck" href="{{ url('http://nl.smile.eu') }}" class="site_link ">Pays-bas</a>
                                            <a target="_blanck" href="{{ url('http://ch.smile.eu') }}" class="site_link ">Suisse</a>
                                            <a target="_blanck" href="{{ url('http://ua.smile.eu') }}" class="site_link ">Ukraine</a>
                                            <a target="_blanck" href="{{ url('http://ru.smile.eu') }}" class="site_link ">Russie</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </header>
                    <nav class="promo_actif header-nav">

                        <div class="header-nav__container">
                            <ul>
                                <li>
                                    <a href="{{ url('/societe') }}" title="Société"  class="curren t">
                                        Société
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/offresservices') }}" title="Offres & Services" >
                                        Offres & Services
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/produits') }}" title="Produits" >
                                        Produits
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/evenements') }}" title="Événements" >
                                        Événements
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/references') }}" title="Références" >
                                        Références
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/recrutement') }}" title="Recrutement" >
                                        Recrutement
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/ressources') }}" title="Ressources" >
                                        Ressources
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/smile_lab') }}" title="Smile Lab" >
                                        Smile Lab
                                    </a>
                                </li>
                            </ul>

                            <form action="{{ url('/search') }}" id="search_form" method="post" title="Rechercher sur smile.fr">
                                <input type="search" placeholder="Rechercher" name="(SearchText)" id="search_string"    />
                                <input type="submit"  class="button" value="Go" title="Lancer la recherche"  />
                                <input type="hidden" name="DestinationURL" value="Recherche" />
                            </form>
                        </div>

                    </nav>
                </div>


                <div class="page-content">
                @yield('content')

                    {{ Widget::Footer() }}                </div>

                <footer>
                    <div class="footer__container">
                        <div class="footer-top">
                            <div class="footer-top__logo">
                                <img src="/img/logo-open-source-footer.png" class="footer-top__logo__img">
                            </div>
                            <span>open</span>
                            <span>source</span>
                        </div>
                        <div class="footer-middle">
                            <ul class="footer-middle__link-container">
                                <li>
                                    <a href="{{ url('/societe') }}" title="Société"  class="current">
                                        Société
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/offresservices') }}" title="Offres & Services" >
                                        Offres & Services
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/produits') }}" title="Produits" >
                                        Produits
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/evenements') }}" title="Événements" >
                                        Événements
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/references') }}" title="Références" >
                                        Références
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/recrutement') }}" title="Recrutement" >
                                        Recrutement
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/ressources') }}" title="Ressources" >
                                        Ressources
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/smile_lab') }}" title="Smile Lab" >
                                        Smile Lab
                                    </a>
                                </li>
                            </ul>

                            <ul class="footer-middle__link-container">
                                <li><a title="Politique de confidentialité" href="{{ url('/politique') }}">Politique de confidentialité</a></li>
                                <li><a title="Coordonnées" href="{{ url('/coordonnees') }}">Coordonnées</a></li>
                                <li><a title="Mentions Légales" href="{{ url('/legales') }}">Mentions Légales</a></li>
                            </ul>

                            <ul class="footer-middle__link-container">
                                <li>
                                    <a href="{{ url('http://smile-oss.com') }}" target="_blank">Smile Group</a>
                                </li>
                                <li>
                                    <a href="{{ url('http://be.smile.eu') }}" target="_blank">Smile Belgique</a>
                                </li>
                                <li>
                                    <a href="{{ url('http://smile.ci') }}" target="_blank">Smile Côte d'Ivoire</a>
                                </li>
                                <li>
                                    <a href="{{ url('http://ru.smile.eu/') }}" target="_blank">Smile Russie</a>
                                </li>
                                <li>
                                    <a href="{{ url('http://www.smile-benelux.com/') }}" target="_blank">Smile Nederland</a>
                                </li>
                                <li>
                                    <a href="{{ url('http://www.smile-suisse.com/') }}" target="_blank">Smile Suisse</a>
                                </li>
                                <li>
                                    <a href="{{ url('http://ua.smile.eu/') }}" target="_blank">Smile Ukraine</a>
                                </li>
                                <li>
                                    <a href="{{ url('http://www.smile-maroc.com') }}" target="_blank">Smile Maroc</a>
                                </li>
                            </ul>
                            <ul class="footer-middle__link-container--bl cf">
                                <p class="footer-middle__title">Notre offre, 5 Business Lines</p>
                                <li class="footer-middle__bl-rect footer-middle__bl-rect--ebusiness">
                                    <img src="/img/smile-logo-74-31.png" alt="Smile"/>
                                    <a href="{{ url('http://ebusiness.smile.eu/') }}" title="E-business" target="_blank">
                                        <div class="footer-middle__bl-rect__text">
                                            <span>E-business</span>
                                            <span class="italic">Business Lines</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="footer-middle__bl-rect footer-middle__bl-rect--web">
                                    <img src="/img/smile-logo-74-31.png" alt="Smile"/>
                                    <a href="{{ url('http://web.smile.eu/') }}" title="Digital" target="_blank">
                                        <div class="footer-middle__bl-rect__text">
                                            <span>Digital</span>
                                            <span class="italic">Business Lines</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="footer-middle__bl-rect footer-middle__bl-rect--ingenierie">
                                    <img src="/img/smile-logo-74-31.png" alt="Smile"/>
                                    <a href="{{ url('http://ingenierie.openwide.fr/') }}" title="E-business" target="_blank">
                                        <div class="footer-middle__bl-rect__text">
                                            <span>Ingénierie</span>
                                            <span class="italic">Business Lines</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="footer-middle__bl-rect footer-middle__bl-rect--infrastructure">
                                    <img src="/img/smile-logo-74-31.png" alt="Smile"/>
                                    <a href="{{ url('http://infrastructure.smile.eu/') }}" title="E-business" target="_blank">
                                        <div class="footer-middle__bl-rect__text">
                                            <span>Infrastructure</span>
                                            <span class="italic">Business Lines</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="footer-middle__bl-rect footer-middle__bl-rect--si">
                                    <img src="/img/smile-logo-74-31.png" alt="Smile"/>
                                    <a href="{{ url('http://information-systems.smile.eu/') }}" title="E-business" target="_blank">
                                        <div class="footer-middle__bl-rect__text">
                                            <span>SI métier</span>
                                            <span class="italic">Business Lines</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <div class="yui3-g footer-middle__galaxie cf">
                                <p class="footer-middle__title">La galaxie Smile</p>
                                <ul class="yui3-u-1-4">
                                    <li>
                                        <a href="{{ url('http://www.open-source-guide.com/') }}" target="_blank">Guide open source</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://www.ecommerce-performances.com/') }}" target="_blank">E-Commerce performances</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://www.opensource-saas.com/') }}" target="_blank">Opensource SAAS, l'open source au format SAAS</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://hosting.smile.eu') }}" target="_blank">Smile Hosting, infogéreur open source</a>
                                    </li>
                                </ul>
                                <ul class="yui3-u-1-4">
                                    <li>
                                        <a href="{{ url('http://agency.smile.eu') }}" target="_blank">Smile Digital</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://www.drupal8-guide.com/') }}" target="_blank">Guide Drupal 8</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://blog.smile.fr') }}" target="_blank">le Blog des experts</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://tribunes-libres.smile.fr/') }}" target="_blank">Tribunes Libres</a>
                                    </li>
                                </ul>
                                <ul class="yui3-u-1-4">
                                    <li>
                                        <a href="{{ url('http://training.smile.eu') }}" target="_blank">Smile Training</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://open-source.smile.fr') }}" target="_blank">Tout savoir sur l'open source</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://opensourceschool.fr') }}" target="_blank">Open Source School</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://magento.smile.eu/') }}" target="_blank">Smile &amp; Magento</a>
                                    </li>
                                </ul>

                                <ul class="yui3-u-1-4">
                                    <li>
                                        <a href="{{ url('http://drupal.smile.eu/') }}" target="_blank">Smile &amp; Drupal</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://symfony.smile.eu/') }}" target="_blank">Smile &amp; Symfony</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://www.smile.eu/liferay/fr/') }}" target="_blank">Smile &amp; Liferay</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('http://magento-elastic-suite.io/') }}" target="_blank">Magento Elastic Suite</a>
                                    </li>
                                </ul>
                                <ul class="yui3-u-1-4">
                                    <li>
                                        <a href="{{ url('http://www.support-opensource.com/') }}" target="_blank">Smile Support</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="contact">
                            <div class="footer-contact" itemtype="http://schema.org/Organization" itemscope="">
                                <span class="footer-contact__name" itemprop="name">Smile</span>
                                <p class="footer-contact__title">Siège social :</p>
                                <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address">
                                    <span itemprop="streetAddress">20 rue des Jardins</span>
                                    <span itemprop="postalCode">92600</span>
                                    <span itemprop="addressLocality">Asnières-sur-Seine, France </span>
                                </div>
                                Téléphone :
                                <span itemprop="telephone">01 41 40 11 00 </span>
                            </div>
                            <a href="{{ url('http://twitter.com/#!/GroupeSmile') }}" target="_blank" class="twitter">Suivez-nous sur Twitter</a>
                            <a href="{{ url('/rss') }}" title="RSS" class="rss">Abonnez-vous au RSS</a>
                            <a href="{{ url('/contact') }}" class="contact">Contactez nous</a>
                        </div>
                    </div>
                </footer>

            </div>
        </div>
    </div>
</div>



        <!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="/js/tracker.js"></script>
<script type="text/javascript" src="/js/buttons.js"></script>
<script type="text/javascript" src="/js/db49487f2dd266a4f76701ea5b95f6e4_1464019952.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
