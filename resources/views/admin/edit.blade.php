@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Create post</div>

                    <div class="panel-body">
                        <h1>Create</h1>

                        {!! Form::open(['action' => 'HomeController@update']) !!}

                        <div class="form-group">
                            {!! Form::hidden('id', $post->id) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('title') !!}
                            {!! Form::text('title', $post->title, ['class'=>'form-control'] ) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('text') !!}
                            {!! Form::textarea('text', $post->text, ['class'=>'form-control'] ) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('date') !!}
                            {!! Form::text('date', $post->date, ['class'=>'form-control','id'=>'datepicker'] ) !!}
                        </div>
                        {!! Form::token() !!}


                        <div class="form-group">
                            {!! Form::submit('Create', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
