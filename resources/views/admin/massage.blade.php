@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Massage</div>

                <div class="panel-body">

                    <table class="table">
                        <thead>
                        <tr>
                            <th>Email</th>
                            <th>Massage</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($feedback as $one)
                            @if($one->new=='1')
                                <tr style="background: #c8cbff">
                            @else
                                <tr>
                            @endif

                            <td>{{$one->email}}</td>
                            <td>{{str_limit($one->massage, $limit = 70, $end = '...')}}</td>
                            <td>


                                <a href="{{ url('/home/show_message',$one->id) }}">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>

                                <a href="{{ url('/home/delete_message', $one->id) }}">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>

                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection
