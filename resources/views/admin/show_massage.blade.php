@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Massage</div>

                <div class="panel-body">

                    <table class="table">
                        <tr>
                            <th>Email</th>
                            <th>{{$feedback->email}}</th>
                        </tr>
                        <tr>
                            <th>Name&Surmane</th>
                            <th>{{$feedback->name.' '.$feedback->surname}}</th>
                        </tr>
                        <tr>
                            <th>Subject</th>
                            <th>{{$feedback->subject_title}}</th>
                        </tr>
                        <tr>
                            <th>Department</th>
                            <th>{{$feedback->department}}</th>
                        </tr>
                        <tr>
                            <th>Pay</th>
                            <th>{{$feedback->pay_title}}</th>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <th>{{$feedback->phone}}</th>
                        </tr>
                        <tr>
                            <th>Societe</th>
                            <th>{{$feedback->societe}}</th>
                        </tr>
                        <tr>
                            <th>Fonction</th>
                            <th>{{$feedback->fonction}}</th>
                        </tr>
                        <tr>
                            <th>Massage</th>
                            <th>{{$feedback->massage}}</th>
                        </tr>

                    </table>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection
