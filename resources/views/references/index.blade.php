@extends('layouts.frontend')

@section('content')

    <div style="background: #897f44;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Références.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">


        {{ Widget::RightTwitter() }}


        <div class="common_right_block">
            {{ Widget::RightPresse() }}
        </div>		            	            		    					    	                            </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Références</span>
                </div>
                <div class="references__content">
                    <h2 class="secondary-title">Votre satisfaction, notre priorité !</h2>
                    <div class="rte_ctnt_block clearfix">

                        <h3>Une démarche centrée sur l’efficacité de votre solution</h3><p>Vous <b>conseiller</b>, vous <b>accompagner</b> et <b>anticiper</b> les changements qui vous permettront d'améliorer les performances de votre organisation : telles sont les principes que nous mettons en œuvre à travers nos solutions et services.</p><p>En témoignent :</p>
                        <ul>

                            <li>L’ <b>amélioration</b> continue du taux de fidélisation de nos clients, qui atteint aujourd’hui 73%</li>

                            <li>Le choix <b>croissant</b> de Smile pour réaliser des projets de <b>grande envergure</b></li>

                            <li>La <b>variété</b> des entreprises et des institutions qui utilisent nos solutions, depuis la start-up jusqu’aux grands comptes du CAC 40</li>

                        </ul>
                        <p>Quelle que soit l'envergure de votre projet, notre devise est la même : faire <b>simple</b>, <b>utile</b> et <b>performant</b>, à votre service.</p>	                				</div>
                    <div class="references__mosaic">

                        {{--<div class="produits__mosaic__filters clearfix">--}}
                            {{--<p class="produits__mosaic__filters__title">Afficher par</p>--}}
                            {{--<form action="/referencesFilter/filter">--}}
                                {{--<a title="Tous" class="filter__all" id="filter__all_r" href="/referencesFilter/filter">Tous</a>--}}

                                {{--<div class="filter-container">--}}
                                    {{--<span class="filter__category">Domaines</span>--}}
                                    {{--<ul class="filter__category__list" id="domaines">--}}
                                        {{--<li><input type="checkbox" name="filter-domaines" value="all_domaines" class="filter all"><label for="">Tous les domaines</label></li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="1273" class="filter sub"><label for="filter-domaines-ingenierie">Agro-alimentaire</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="607" class="filter sub"><label for="filter-domaines-ingenierie">Banque, Assurance et Finance</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="1268" class="filter sub"><label for="filter-domaines-ingenierie">Immobilier</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="1269" class="filter sub"><label for="filter-domaines-ingenierie">Industrie</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="1274" class="filter sub"><label for="filter-domaines-ingenierie">Informatique et Télécom</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="188" class="filter sub"><label for="filter-domaines-ingenierie">Public et Collectivités</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="1270" class="filter sub"><label for="filter-domaines-ingenierie">Santé et Scientifique</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="1216" class="filter sub"><label for="filter-domaines-ingenierie">Services et Médias</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-domaines[]" value="1272" class="filter sub"><label for="filter-domaines-ingenierie">Tourisme, Voyages et Loisirs</label>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="filter-container">--}}
                                    {{--<span class="filter__category">Offres</span>--}}
                                    {{--<ul class="filter__category__list" id="solutions">--}}
                                        {{--<li><input type="checkbox" name="filter-solutions" name="filter-bl" value="all_solutions" class="filter all"><label for="">Toutes les offres</label></li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-solutions[]" value="1287" class="filter sub"><label for="">Collaboratif</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-solutions[]" value="1290" class="filter sub"><label for="">E-business</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-solutions[]" value="1293" class="filter sub"><label for="">Infrastructure</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-solutions[]" value="1291" class="filter sub"><label for="">SI Métiers</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-solutions[]" value="1286" class="filter sub"><label for="">Web</label>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="filter-container">--}}
                                    {{--<span class="filter__category">Services</span>--}}
                                    {{--<ul class="filter__category__list" id="services">--}}
                                        {{--<li><input type="checkbox" name="filter-services" value="all_services" class="filter all"><label for="">Tous les services</label></li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-services[]" value="1299" class="filter sub"><label for="">Agence Interactive</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-services[]" value="1296" class="filter sub"><label for="">Consulting</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-services[]" value="10788" class="filter sub"><label for="">Hosting</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-services[]" value="1300" class="filter sub"><label for="">Ingénierie et Développement</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-services[]" value="1302" class="filter sub"><label for="">TMA et exploitation</label>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="filter-container">--}}
                                    {{--<span class="filter__category" id="outils">Outils</span>--}}
                                    {{--<ul class="filter__category__list">--}}
                                        {{--<li><input type="checkbox" name="filter-outils" value="all_outils" class="filter all"><label for="">Tous les outils</label></li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="1283" class="filter sub"><label for="">Alfresco</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="1706" class="filter sub"><label for="">Développement spécifique</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="3953" class="filter sub"><label for="">Drupal</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="34760" class="filter sub"><label for="">Drupal Commerce</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="1275" class="filter sub"><label for="">eZ Publish</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="34433" class="filter sub"><label for="">GLPI</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="1276" class="filter sub"><label for="">Jahia</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="32135" class="filter sub"><label for="">Jaspersoft</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="10200" class="filter sub"><label for="">Jedox</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="7593" class="filter sub"><label for="">Liferay</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="4608" class="filter sub"><label for="">Magento</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="12313" class="filter sub"><label for="">Nuxeo</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="4387" class="filter sub"><label for="">Odoo</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="1285" class="filter sub"><label for="">Pentaho</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="1284" class="filter sub"><label for="">SpagoBI</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="34369" class="filter sub"><label for="">Symfony</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="4469" class="filter sub"><label for="">Talend</label>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="checkbox" name="filter-outils[]" value="1277" class="filter sub"><label for="">TYPO3</label>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                        <div id="resultFilter">
                            <div class="mosaic--4-col clearfix">

                                @foreach($products as $product)
                                <div class="mosaic__dalle">
                                    <a href="{{ url('/references/one_reference', $product->id) }}" title="Voir le produit">
                                        <div class="flip-card">
                                            <div class="flip-card__container">
                                                <div class="flip-card__face">






                                                    <img src="{{$product->image}}"   width="60%"   style="border: 0px;" alt="" title="" />



                                                </div>
                                                <div class="flip-card__pile">
                                                    <h3>{{$product->title}}</h3>
                                                    <p class="savoir-plus">En savoir plus</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                @endforeach


                                  	                    </div>
                            {{--<div class="pagination-block">--}}


                                {{--<div class="pagination-block__page-left">--}}
                                    {{--<p class="last_off"></p>--}}
                                {{--</div>--}}
                                {{--<div class="pagination-block__page-container">--}}


                                    {{--<a class="pagination-block__page-nb active">1</a>--}}

                                    {{--<a class="pagination-block__page-nb" href="/References/(offset)/20">2</a>--}}
                                    {{--<a class="pagination-block__page-nb" href="/References/(offset)/40">3</a>--}}
                                    {{--<a class="pagination-block__page-nb" href="/References/(offset)/60">4</a>--}}

                                    {{--<span style="margin-right: 5px;">...</span>--}}
                                    {{--<a class="pagination-block__page-nb" href="/References/(offset)/340">18</a>--}}
                                {{--</div>--}}
                                {{--<div class="pagination-block__page-next">--}}
                                    {{--<a class="forward-link" href="/References/(offset)/20">Page suivante</a>--}}
                                {{--</div>--}}


                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>



    <div class="breaker"></div>


@endsection