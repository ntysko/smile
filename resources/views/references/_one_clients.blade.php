@extends('layouts.frontend')

@section('content')
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/references" title="Références">Références</a>
                    <span class="separator"></span>
                    <span class="current_page">Snowleader 2016</span>
                </div>
                <a class="back-link" href="/References">Retour aux références</a>
                <div class="reference__header clearfix">
                    <h2 class="secondary-title">Snowleader 2016</h2>
                    <img class="reference__header__logo" src="/img/Snowleader-2016_reference_logo.jpg">
                    <div class="reference__header__infos">
                        <span class="reference__header__slogan">Snowleader choisit Smile pour lancer ses sites e-commerce</span>
                			  					  				<span class="reference__header__site-link">
	                        <a href="http://www.snowleader.com">http://www.snowleader.com</a>
	                    </span>
                    </div>
                </div>
                <div class="reference__content rte_ctnt_block" style="border-top: medium none;">

                    <p>Soucieux d’accroître ses ventes sur internet et de se développer à l’international, Snowleader a souhaité refondre intégralement son site existant et confier le développement de ce projet à une structure pouvant l’accompagner de bout en bout. C’est dans ce contexte que Smile a été choisi pour son expertise technologique conjuguée à une parfaite maîtrise des problématiques liées au e-commerce.</p><p><b>Thomas Rouault, PDG de Snowleader :</b> « Smile est une société qui dispose d’une vraie dynamique d’un point de vue de l’innovation. Cela nous permet de bénéficier de nombreuses fonctionnalités avancées qui répondent parfaitement à notre besoin. »</p><p>Dans ce contexte, l'équipe de Smile a mis en place une plateforme internationale intégrant de puissantes fonctionnalités dédiées au e-commerce notamment en matière de merchandising, de gestion de catalogues, de référencement ou encore de refonte du tunnel de vente et d’intégration des avis clients.</p><p>Grâce à ce nouveau dispositif, Snowleader est en mesure de gérer de manière industrielle un catalogue produit en constante évolution et d’accélérer ses ventes en France et à l’international en s’appuyant sur un réseau de quatre sites web disponibles en plusieurs langues.</p><p><b>Thomas Rouault, PDG de Snowleader :</b></p><p>« Ce projet a été long et ambitieux. En effet, le périmètre impliquait une refonte ergonomique, graphique, technique, mais aussi fonctionnelle. Le champ d’action était donc très vaste et il a fallu jongler avec les contraintes inhérentes à notre plateforme, mais aussi celles du responsive design. Toutes les ressources nécessaires ont été mobilisées pour livrer notre projet en temps et en heure et les intervenants se sont montrés disponibles et efficaces. Enfin, nous avons pu bénéficier du savoir de développeurs experts Magento qui ont compris nos besoins. Le projet s’est donc bien déroulé grâce à un planning tenu et à un excellent suivi. »</p>            </div>
                <div class="reference__last rte_ctnt_block">
                    <h3>Nos dernières références</h3>
                    {{--<div class="reference__last__container clearfix">--}}
                        {{--<a title="Snowleader 2016" class="reference__last__item" href="/References/References-par-domaine/Tourisme-voyages-et-loisirs/Snowleader-2016">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/tourisme-voyages-et-loisirs/snowleader-2016/713217-3-fre-FR/Snowleader-2016_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                        {{--<a title="beIn Sports" class="reference__last__item" href="/References/References-par-domaine/Services-et-medias/Bein-sports">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/services-et-medias/bein-sports/711910-1-fre-FR/beIn-Sports_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                        {{--<a title="Eram" class="reference__last__item" href="/References/References-par-domaine/Industrie/Eram">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/industrie/eram/711944-1-fre-FR/Eram_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                        {{--<a title="Abitare Kids" class="reference__last__item" href="/References/References-par-domaine/Industrie/Abitare-kids">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/industrie/abitare-kids/711758-1-fre-FR/Abitare-Kids_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                        {{--<a title="Demeco-Emoovz" class="reference__last__item" href="/References/References-par-domaine/Services-et-medias/Demeco-emoovz">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/services-et-medias/demeco-emoovz/711719-1-fre-FR/Demeco-Emoovz_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                        {{--<a title="Devred" class="reference__last__item" href="/References/References-par-domaine/Industrie/Devred">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/industrie/devred/711169-2-fre-FR/Devred_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                        {{--<a title="MANUTAN" class="reference__last__item" href="/References/References-par-domaine/Public-et-collectivites/Manutan">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/public-et-collectivites/manutan/709212-1-fre-FR/MANUTAN_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                        {{--<a title="SERIS" class="reference__last__item" href="/References/References-par-domaine/Services-et-medias/Seris">--}}
                            {{--<img src="/var/site_smile/storage/images/smile-france/clients/references-par-domaine/services-et-medias/seris/708413-1-fre-FR/SERIS_reference_logo_offre2.jpg" />--}}
                        {{--</a>--}}
                    {{--</div>--}}
                </div>
                <a class="reference__last__all" href="/references">&gt; Voir toutes nos références</a>
            </div>
        </div>

    </div>



    <div class="breaker"></div>

@endsection