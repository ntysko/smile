@extends('layouts.frontend')

@section('content')
    <div style="background: #cfd4b6;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src="/img/Services.jpg">
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>
    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Offres &amp; Services</span>
                </div>
                <h2 class="secondary-title">Découvrez nos services et offres métiers</h2>

                <div class="rte_ctnt_block clearfix">

                    <p class=" text-left">Smile est votre partenaire open source global. Nos offres et services reflètent la diversité de nos métiers. Ils sont pour vous les garants d’une compréhension globale de vos besoins.</p>
                    <p class=" text-left">Vous comprendre, réaliser vos projets et vous accompagner, c’est ainsi que nos offres et services s’articulent dans la continuité de votre projet.</p>      	                      	<h3>Des solutions compétitives et durables</h3>

                    <p>Pour être au plus près de vos problématiques et vous garantir des projets réussis qui partagent vos objectifs métiers, Smile a développé 5 offres conjuguant expertise Technique et expertise Métier.</p>      	          </div>
                <div class="offers__mosaic cf">
                    <a href="/offresservices/offres" alt="E-Business">
                        <div class="offers__mosaic__e-business offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">E-Business</p>
                                        <span class="offers__mosaic__btn offers__mosaic__e-business__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">E-Business</p>
                                        <strong>
                                            Développez votre business online                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>	            	        		    <a href="/offresservices/offres" alt="Ingénierie">
                        <div class="offers__mosaic__ingenierie offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Ingénierie</p>
                                        <span class="offers__mosaic__btn offers__mosaic__ingenierie__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Ingénierie</p>
                                        <strong>
                                            Concevez les meilleurs systèmes embarqués                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>	            	        			<a href="/offresservices/offres" alt="Digital">
                        <div class="offers__mosaic__web offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Digital</p>
                                        <span class="offers__mosaic__btn offers__mosaic__web__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Digital</p>
                                        <strong>
                                            Réussir la digitalisation de votre entreprise                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>	            	        				<a href="/offresservices/offres" alt="Systèmes d’information" id="systeme_information">
                        <div class="offers__mosaic__si offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Systèmes d’information</p>
                                        <span class="offers__mosaic__btn offers__mosaic__si__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Systèmes d’information</p>
                                        <strong>
                                            Pilotez efficacement votre activité                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>	            	        			<a href="/offresservices/offres" alt="Infrastructure">
                        <div class="offers__mosaic__infrastructure offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Infrastructure</p>
                                        <span class="offers__mosaic__btn offers__mosaic__infrastructure__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Infrastructure</p>
                                        <strong>
                                            Optimisez les performances de votre infrastructure système                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>	            	        </div>
                <div class="rte_ctnt_block clearfix">
                    <h3>Des services complémentaires</h3>

                    <p>L’offre de Smile se décline avec 6 services transverses couvrant vos besoins à 360°. Nous nous appuyons sur les meilleurs consultants du marché et sur une expérience unique issue des centaines de projets réalisés chaque année.</p>			        </div>
                <div class="offers__mosaic cf services_mosaic">

                    <a href="/offresservices/services" alt="Hosting">
                        <div class="offers__mosaic__block-l">
                            <h2>Hosting</h2>

                            <p>L'infogérance de vos sites web par les experts de l'open source.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Support">
                        <div class="offers__mosaic__block-l">
                            <h2>Support open source</h2>

                            <p>Sécurisez l'open source de votre entreprise !</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="TMA">
                        <div class="offers__mosaic__block-l">
                            <h2>TMA</h2>

                            <p>Assurez la pérennité de vos applicatifs.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Consulting">
                        <div class="offers__mosaic__block-l">
                            <h2>Consulting</h2>

                            <p>Innover et créer de la valeur pour vos projets.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Formation">
                        <div class="offers__mosaic__block-l">
                            <h2>Formation</h2>

                            <p>Formez-vous avec le leader de l’open source.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Smile Digital">
                        <div class="offers__mosaic__block-l">
                            <h2>Smile Digital</h2>

                            <p>Imaginer et concevoir de nouvelles expériences digitales.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Intégration">
                        <div class="offers__mosaic__block-l">
                            <h2>Intégration</h2>

                            <p>Profitez du meilleur de l'open source</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>	            	        </div>
            </div>
        </div>

    </div>
    <div class="breaker"></div>
@stop