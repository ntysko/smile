@extends('layouts.frontend')

@section('content')
    <div style="background: #cfd4b6;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src="/img/Services.jpg">
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>
    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col" class="page_interne">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/offresservices/services" title="Offres &amp; Services">Offres &amp; Services</a>
                    <span class="separator"></span>
                    <span class="current_page">Services</span>
                </div>

                <div class="rte_ctnt_block clearfix">
                    <h2>Des services complémentaires</h2>
                    <div class="summary_small" >

                        <p>L’offre de Smile se décline avec 6 services transverses couvrant vos besoins à 360°. Nous nous appuyons sur les meilleurs consultants du marché et sur une expérience unique issue des centaines de projets réalisés chaque année.</p>					</div>

                </div>

                <div class="offers__mosaic cf services_mosaic">


                    <a href="/offresservices/services" alt="Hosting">
                        <div class="offers__mosaic__block-l">
                            <h2>Hosting</h2>

                            <p>L'infogérance de vos sites web par les experts de l'open source.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Support">
                        <div class="offers__mosaic__block-l">
                            <h2>Support open source</h2>

                            <p>Sécurisez l'open source de votre entreprise !</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="TMA">
                        <div class="offers__mosaic__block-l">
                            <h2>TMA</h2>

                            <p>Assurez la pérennité de vos applicatifs.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Consulting">
                        <div class="offers__mosaic__block-l">
                            <h2>Consulting</h2>

                            <p>Innover et créer de la valeur pour vos projets.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Formation">
                        <div class="offers__mosaic__block-l">
                            <h2>Formation</h2>

                            <p>Formez-vous avec le leader de l’open source.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Smile Digital">
                        <div class="offers__mosaic__block-l">
                            <h2>Smile Digital</h2>

                            <p>Imaginer et concevoir de nouvelles expériences digitales.</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>
                    <a href="/offresservices/services" alt="Intégration">
                        <div class="offers__mosaic__block-l">
                            <h2>Intégration</h2>

                            <p>Profitez du meilleur de l'open source</p>    	        <span class="offers__mosaic__btn">+ d'infos</span>
                        </div>
                    </a>															</div>
            </div>

        </div>

    </div>
    <div class="breaker"></div>
@stop