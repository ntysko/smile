@extends('layouts.frontend')

@section('content')
    <div style="background: #cfd4b6;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src="/img/Services.jpg">
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>
    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col" class="page_interne">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/offresservices/offres" title="Offres &amp; Services">Offres &amp; Services</a>
                    <span class="separator"></span>
                    <span class="current_page">Offres</span>
                </div>

                <div class="rte_ctnt_block clearfix">
                    <h2>Des solutions compétitives et durables</h2>
                    <div class="summary_small" >

                        <p>Pour être au plus près de vos problématiques et vous garantir des projets réussis qui partagent vos objectifs métiers, Smile a développé 5 offres conjuguant expertise Technique et expertise Métier.</p>					</div>

                </div>

                <div class="offers__mosaic cf">

                    <a href="/offresservices/offres" alt="E-Business" >
                        <div class="offers__mosaic__e-business offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">E-Business</p>
                                        <span class="offers__mosaic__btn offers__mosaic__e-business__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">E-Business</p>
                                        <strong>
                                            Développez votre business online                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="/offresservices/offres" alt="Ingénierie" >
                        <div class="offers__mosaic__ingenierie offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Ingénierie</p>
                                        <span class="offers__mosaic__btn offers__mosaic__ingenierie__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Ingénierie</p>
                                        <strong>
                                            Concevez les meilleurs systèmes embarqués                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="/offresservices/offres" alt="Digital" >
                        <div class="offers__mosaic__web offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Digital</p>
                                        <span class="offers__mosaic__btn offers__mosaic__web__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Digital</p>
                                        <strong>
                                            Réussir la digitalisation de votre entreprise                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="/offresservices/offres" alt="Systèmes d’information" id="systeme_information">
                        <div class="offers__mosaic__si offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Systèmes d’information</p>
                                        <span class="offers__mosaic__btn offers__mosaic__si__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Systèmes d’information</p>
                                        <strong>
                                            Pilotez efficacement votre activité                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="/offresservices/offres" alt="Infrastructure" >
                        <div class="offers__mosaic__infrastructure offers__mosaic__block-l">
                            <div class="flip-card">
                                <div class="flip-card__container">
                                    <div class="flip-card__face">
                                        <p class="offre-title">Infrastructure</p>
                                        <span class="offers__mosaic__btn offers__mosaic__infrastructure__btn">Découvrir</span>
                                    </div>
                                    <div class="flip-card__pile">
                                        <p class="offre-title">Infrastructure</p>
                                        <strong>
                                            Optimisez les performances de votre infrastructure système                    	</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>															</div>
            </div>

        </div>

    </div>
    <div class="breaker"></div>
@stop