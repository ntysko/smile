@extends('layouts.frontend')

@section('content')

    <div style="background: #708177;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Ressources.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Ressources</span>
                </div>
                <h2 class="secondary-title">Une collection de ressources</h2>
                <div class="rte_ctnt_block">

                    <p class=" text-left">Retrouvez dans cette rubrique le savoir-faire de Smile diffusé dans nos livres blancs, nos vidéos et supports de présentation.</p>        </div>

                <div class="ressources__container">
                    <div class="rte_ctnt_block">
                        <h3>Nos livres blancs</h3>

                        <p>Nos livres blancs, accessibles gratuitement, présentent les concepts fondamentaux, les bonnes pratiques et les meilleures solutions open source du marché, sur les domaines d'expertise de Smile. La collection de plus de 20 livres blancs vous permettra de découvrir les produits les plus prometteurs de l’open source, de les qualifier et de les évaluer !</p><p>Bonne lecture !</p>  			</div>
                    <div class="ressources__container--list clearfix">
                        <div class="ressources__container--list__lb">
                            <div class="lb__presentation">
                                <div class="lb__presentation--couv">





                                    <img src="/img/DevOps-et-industrialisation_livreBlanc_medium.jpg"   width="112" height="160"  style="border: 0px;" alt="Couverture livre blanc DevOps et industrialisation" title="Couverture livre blanc DevOps et industrialisation" />



                                </div>
                                <div class="lb__presentation--infos">
			                  				                  		<span class="lb__new">
			                  			NOUVEAU		                  			</span>
                                    <h3>DevOps et industrialisation</h3>
                                    <a class="button" href="/" title="DevOps et industrialisation">Télécharger gratuitement</a>
                                </div>
                            </div>

                            <div class="lb__presentation--content">
                                <p>
                                    15 solutions open source décryptées !						         </p>
                            </div>
                            <div class="ressource__share">
                                <span class='st_sharethis' st_url="/Ressources/Livres-blancs/Systeme-et-infrastructure/Devops-et-industrialisation" displayText="Partager"></span>
                            </div>
                        </div>
                        <div class="ressources__container--list__lb">
                            <div class="lb__presentation">
                                <div class="lb__presentation--couv">





                                    <img src="/img/Linux-pour-l'embarqué_livreBlanc_medium.png"   width="112" height="160"  style="border: 0px;" alt="Linux pour l'embarqué" title="Linux pour l'embarqué" />



                                </div>
                                <div class="lb__presentation--infos">
			                  				                  		<span class="lb__new">
			                  			NOUVEAU		                  			</span>
                                    <h3>Linux pour l'embarqué</h3>
                                    <a class="button" href="/" title="Linux pour l'embarqué">Télécharger gratuitement</a>
                                </div>
                            </div>

                            <div class="lb__presentation--content">
                                <p>
                                    L'utilisation de Linux embarqué dans les systèmes industriels.						         </p>
                            </div>
                            <div class="ressource__share">
                                <span class='st_sharethis' st_url="/Ressources/Livres-blancs/Ingenierie/Linux-pour-l-embarque" displayText="Partager"></span>
                            </div>
                        </div>
                    </div>

                    <div class="ressources__more">
                        <a class="btn--main" title="Consulter toute la collection" href="/ressources/livres">Consulter toute la collection</a>
                    </div>
                </div>

                <div class="ressources__container">
                    <div class="rte_ctnt_block">
                        <h3>Vidéos</h3>

                        <p class=" text-left">Revivez nos conférences et webinars en images&nbsp;!</p>		            				</div>
                    <div class="ressources__container--list clearfix">
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/RdMQpm1Pk4c" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Webinar SMILE : "Tout savoir sur Magento 2"</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/RdMQpm1Pk4c" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/jkZsTh6sMQM" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Webinar SMILE : "Drupal 8 Nouveautés et retours d'expérience"</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/jkZsTh6sMQM" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/LzFKn-E-USc" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Liferay 7 - Lexicon en action / Admin menu</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/LzFKn-E-USc" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ressources__more">
                        <a class="btn--main" title="Toutes les vidéos" href="/ressources/videos">Toutes les vidéos</a>
                    </div>

                </div>
                <div class="ressources__container">
                    <div class="rte_ctnt_block">
                        <h3>Support de Présentations</h3>

                        <p class=" text-left">Les présentations de nos séminaires, conférences, offres et expertises à voir et revoir&nbsp;!</p>		            		        </div>
                    <div class="ressources__container--list clearfix">
                        <div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/q7dLenuYVLaGNC" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/">Séminaire drupal8.final</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-drupal8-final" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>											<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/6hCY3awX4Q1k65" frameborder="0" allowfullscreen> </iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/">Business line COLLABORATIVE, présentation</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Business-line-collaborative-presentation" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>											<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/ict46TdErpTQFz"  frameborder="0" marginwidth="0" marginheight="0" scrolling="no"  allowfullscreen> </iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/">Meetup ElasticSearch : « Booster votre Magento avec Elasticsearch »</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Meetup-elasticsearch-booster-votre-magento-avec-elasticsearch" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>						          	</div>
                    <div class="ressources__more">
                        <a class="btn--main" title="Tous les supports" href="/ressources/presentations">Tous les supports</a>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>

@endsection