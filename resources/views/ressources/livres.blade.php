@extends('layouts.frontend')

@section('content')

    <div style="background: #708177;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Livres-blancs.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col" >

            <div id="inner_main">


                <div class="breadcrumbs_block">
                    <a href="/Ressources" title="Ressources">Ressources</a>
                    <span class="separator"></span>
                    <span class="current_page">Livres blancs</span>
                </div>



                <div class="lb__header">
                    <h1>Livres blancs</h1>
                    <h2 class="secondary-title">Une collection de plus de 20 livres blancs</h2>

                    <p>Nos livres blancs, accessibles gratuitement, présentent les concepts fondamentaux, les bonnes pratiques et les meilleures solutions open source du marché, sur les domaines d'expertise de Smile. La collection de plus de 20 livres blancs vous permettra de découvrir les produits les plus prometteurs de l’open source, de les qualifier et de les évaluer !</p><p>Bonne lecture !</p>
                    <div class="breaker"></div>
                </div>




                <!--[if IE 6]><br class="clearall" /><![endif]-->
            </div>

            <div class="common_listing_block">

                <section id="ourWhitePapers">
                    <div class="block-title">
                        <h1>
                            <a href="/Ressources/Livres-blancs/Ingenierie" title="Ingénierie">Ingénierie</a>
                        </h1>
                    </div>
                    <div class="book-item">
                        <div>





                            <img src="/img/Linux-pour-l'embarqué_livreBlanc_big.png"   width="145" height="207"  style="border: 0px;" alt="Linux pour l'embarqué" title="Linux pour l'embarqué" />



                        </div>
                        <div>
                            <strong>NOUVEAU</strong>                <h2>
                                Linux pour l'embarqué                </h2>
                        </div>
                        <div class="book-item__infos">
                            <p>L'utilisation de Linux embarqué dans les systèmes industriels.</p>
                            <a class="button" href="/Ressources/Livres-blancs/Ingenierie/Linux-pour-l-embarque" title="Linux pour l'embarqué" >Télécharger gratuitement</a>
                        </div>
                    </div>
                    <ul>
                    </ul>
                </section>

                <section id="ourWhitePapers">
                    <div class="block-title">
                        <h1>
                            <a href="/Ressources/Livres-blancs/Culture-du-web" title="Culture du web">Culture du web</a>
                        </h1>
                    </div>
                    <div class="book-item">
                        <div>





                            <img src="/img/Guide-de-l'open-source_livreBlanc_big.png"   width="145" height="207"  style="border: 0px;" alt="" title="" />



                        </div>
                        <div>
                            <strong>Best-seller - EDITION 2014</strong>                <h2>
                                Guide de l'open source                </h2>
                        </div>
                        <div class="book-item__infos">
                            <p>Plus de 350 solutions open source passées au crible dans près de 50 domaines d'applications. Le guide de l'open source vous donne toutes les clés pour bâtir...</p>
                            <a class="button" href="/Ressources/Livres-blancs/Culture-du-web/Guide-de-l-open-source" title="Guide de l'open source" >Télécharger gratuitement</a>
                        </div>
                    </div>
                    <ul>
                        <li>





                            <img src="/img/Bonnes-pratiques-du-web_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="Couverture livre blanc bonnes pratiques du web" title="Couverture livre blanc bonnes pratiques du web" />



                            <div>
                                <h2>Bonnes pratiques du web</h2>
                                <p>Toutes les clés pour con...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Bonnes-pratiques-du-web">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Conceptions-d'applications-web_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="Couverture livre blanc applications web" title="Couverture livre blanc applications web" />



                            <div>
                                <h2>Conception d'applications web</h2>
                                <p>La conception et l'ergon...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Conceptions-d-applications-web">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Comprendre-l'Open-Source_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="LB open source" title="LB open source" />



                            <div>
                                <h2>Comprendre l'open source</h2>
                                <p>L'open source en entrepr...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Comprendre-l-open-source">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Méthodes-et-Agilité_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="" title="" />



                            <div>
                                <h2>Présentation de la filière open source</h2>
                                <p>"Présentation d'une fili...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/La-filiere-open-source">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Méthodes-et-Agilité_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="visuel LB methode agile" title="visuel LB methode agile" />



                            <div>
                                <h2>Méthodes et agilité</h2>
                                <p>Une approche agile des m...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Methodes-et-agilite">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/NoSQL_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="" title="" />



                            <div>
                                <h2>NoSQL</h2>
                                <p>Les différents paradigme...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Nosql">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Points-de-vue_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="Point de vue" title="Point de vue" />



                            <div>
                                <h2>Points de vue sur l'open source</h2>
                                <p>Décryptages et analyses...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Points-de-vue">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Politique-Open-Source_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="Politique Open Source" title="Politique Open Source" />



                            <div>
                                <h2>Politique open source</h2>
                                <p>Pourquoi les DSI doivent...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Politique-open-source">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Référencement_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="Couverture référencement" title="Couverture référencement" />



                            <div>
                                <h2>Référencement</h2>
                                <p>Ce livre blanc aborde to...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Referencement">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Recensement-et-audit_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="" title="" />



                            <div>
                                <h2>Recensement et audit open source</h2>
                                <p>Cas d'usage, principes d...</p>
                                <a href="/Ressources/Livres-blancs/Culture-du-web/Recensement-et-audit">Télécharger</a>
                            </div>
                        </li>
                    </ul>
                </section>

                <section id="ourWhitePapers">
                    <div class="block-title">
                        <h1>
                            <a href="/" title="Gestion de contenu et GED">Gestion de contenu et GED</a>
                        </h1>
                    </div>
                    <div class="book-item">
                        <div>





                            <img src="/img/Guide-Drupal-8_livreBlanc_big.jpg"   width="145" height="207"  style="border: 0px;" alt="Couverture Guide Drupal 8" title="Couverture Guide Drupal 8" />



                        </div>
                        <div>
                            <strong>NOUVEAU</strong>                <h2>
                                Guide Drupal 8                </h2>
                        </div>
                        <div class="book-item__infos">
                            <p>Toutes les nouveautés de la dernière version de Drupal.</p>
                            <a class="button" href="/" title="Guide Drupal 8" >Télécharger gratuitement</a>
                        </div>
                    </div>
                    <ul>
                        <li>





                            <img src="/img/Enquête-CMS_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="Couverture livre blanc Enquete CMS 2013" title="Couverture livre blanc Enquete CMS 2013" />



                            <div>
                                <h2>Enquête CMS open source</h2>
                                <p>Découvrez les résultats...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Les-CMS-open-source_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="Couverture CMS open source" title="Couverture CMS open source" />



                            <div>
                                <h2>CMS open source</h2>
                                <p>Une analyse approfondie...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Les-portails-open-source_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="Couverture livre blanc portails d'entreprise" title="Couverture livre blanc portails d'entreprise" />



                            <div>
                                <h2>Portails open source</h2>
                                <p>Les concepts fondamentau...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Choisir-un-CMS_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="Couverture livre blanc choisir un CMS" title="Couverture livre blanc choisir un CMS" />



                            <div>
                                <h2>Choisir un CMS</h2>
                                <p>Quelles sont les bonnes...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Gestion-documentaire_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="" title="" />



                            <div>
                                <h2>Gestion documentaire open source</h2>
                                <p>Découvrir les meilleures...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                    </ul>
                </section>

                <section id="ourWhitePapers">
                    <div class="block-title">
                        <h1>
                            <a href="/" title="e-Business">e-Business</a>
                        </h1>
                    </div>
                    <div class="book-item">
                        <div>





                            <img src="/img/Recherche-et-e-merchandising_livreBlanc_big.png"   width="145" height="207"  style="border: 0px;" alt="Couverture Recherche et e-merchandising" title="Couverture Recherche et e-merchandising" />



                        </div>
                        <div>
                            <strong>NOUVEAU</strong>                <h2>
                                Recherche et e-merchandising                </h2>
                        </div>
                        <div class="book-item__infos">
                            <p>L'optimisation de la recherche et du merchandising de votre site site e-commerce est un enjeu stratégique pour améliorer l’expérience client et les revenus.</p>
                            <a class="button" href="/" title="Recherche et e-merchandising" >Télécharger gratuitement</a>
                        </div>
                    </div>
                    <ul>
                        <li>





                            <img src="/img/e-Commerce-open-source_livreBlanc_small.png"   width="75" height="107"  style="border: 0px;" alt="" title="" />



                            <div>
                                <h2>e-Commerce</h2>
                                <p></p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Réseaux-sociaux-d'entreprise_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="couverture livre blanc RSE" title="couverture livre blanc RSE" />



                            <div>
                                <h2>Réseaux sociaux d'entreprise</h2>
                                <p>Panorama des outils et d...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Mobile_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="" title="" />



                            <div>
                                <h2>Mobile</h2>
                                <p>Comment définir une stra...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                        <li>





                            <img src="/img/Choisir-sa-solution-e-commerce_livreBlanc_small.jpg"   width="75" height="107"  style="border: 0px;" alt="" title="" />



                            <div>
                                <h2>Choisir sa solution e-commerce</h2>
                                <p>400 questions incontourn...</p>
                                <a href="/">Télécharger</a>
                            </div>
                        </li>
                    </ul>
                </section>

            </div>
        </div>


    </div>



    <div class="breaker"></div>
@endsection