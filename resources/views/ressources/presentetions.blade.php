@extends('layouts.frontend')

@section('content')


    <div style="background: #708177;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Présentations.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Ressources" title="Ressources">Ressources</a>
                    <span class="separator"></span>
                    <span class="current_page">Présentations</span>
                </div>
                <div class="rte_ctnt_block">

                    <p class=" text-left">Les présentations de nos séminaires, conférences, offres et expertises à voir et revoir&nbsp;!</p>	        </div>
                <div class="ressources__container">
                    <div class="ressources__container--list clearfix">
                        <div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/JgJ12PBQcKIKI1" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Bargento-2014-conference-smile-elasticsearch-booste-la-recherche-magento">Bargento 2014 : conférence Smile « ElasticSearch booste la recherche Magento »</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Bargento-2014-conference-smile-elasticsearch-booste-la-recherche-magento" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/HYK5wdbJh1OTwY" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Seminaire-iot-eisti-du-14-avril-2016-avec-open-wide-smile">Séminaire IoT EISTI du 14 avril 2016 avec Open Wide / Smile</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-iot-eisti-du-14-avril-2016-avec-open-wide-smile" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/FurKzDpYqpQVXI" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Presentation-sur-l-accessibilite-numerique-evenement-universite-de-lille-3">Présentation sur l'accessibilité numérique / Evènement université de Lille 3</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Presentation-sur-l-accessibilite-numerique-evenement-universite-de-lille-3" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/KOhxijFsXtyeyh" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Webinar-smile-comment-industrialiser-votre-si-avec-ansible">Webinar Smile : Comment industrialiser votre SI avec Ansible ?</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Webinar-smile-comment-industrialiser-votre-si-avec-ansible" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/tpyd8jeaOZPHlI" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/A-high-profile-project-with-symfony-and-api-platform-bein-sports">A high profile project with Symfony and API Platform: beIN SPORTS</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/A-high-profile-project-with-symfony-and-api-platform-bein-sports" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/Gx1c3aswvzYnQk" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Seminaire-drupal-8-a-nantes">Seminaire Drupal 8 à Nantes</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-drupal-8-a-nantes" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/lTCdiFjWOeKNYT" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Webinar-smile-et-wso2">Webinar Smile et WSO2</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Webinar-smile-et-wso2" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/uLp3cSu8l62Fp" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Seminaire-drupal8-lille">Seminaire drupal8 Lille</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-drupal8-lille" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/E3btHrdKuik9Bs" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Offre-search">Offre Search</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Offre-search" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/yeOklA0Ixcj2VJ" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Dam-et-e-business">Dam et e-business</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Dam-et-e-business" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/MHPrYAUvnAAfKF" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Demarche-de-cadrage-big-data">Demarche de cadrage Big data</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Demarche-de-cadrage-big-data" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/ywldTbkxexl4b9" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Seminaire-drupal8-lyon">Séminaire drupal8 Lyon</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-drupal8-lyon" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//fr.slideshare.net/slideshow/embed_code/key/2sZKFFH8IUo2fo" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Digitalisez-vos-points-de-ventes-avec-smile">Digitalisez vos points de ventes avec Smile !</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Digitalisez-vos-points-de-ventes-avec-smile" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/C8bkQlQl2kftcv" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Comment-une-marque-se-developpe-par-son-approche-user-centric-conference-smile-au-shake-event-2015">Comment une marque se développe par son approche user centric ? - Conference Smile au Shake event 2015</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Comment-une-marque-se-developpe-par-son-approche-user-centric-conference-smile-au-shake-event-2015" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/7MoPcBaMcSHYTn" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Typo3-et-hautes-performances-t3uni-2015">TYPO3 et hautes performances - T3UNI 2015</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Typo3-et-hautes-performances-t3uni-2015" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/G2auTwISyY3BK9" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Seminaire-webfactory-2015">Seminaire webfactory - 2015</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-webfactory-2015" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/rsAaewu3jX3vJr" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Meet-magento-2015-utrecht-elasticsearch-smile">Meet Magento 2015 Utrecht - ElasticSearch - Smile</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Meet-magento-2015-utrecht-elasticsearch-smile" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/t7VbUFqfi6To1H" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Webinar-smile-comment-drupal-8-peut-booster-votre-strategie-digitale">Webinar Smile - Comment Drupal 8 peut booster votre stratégie digitale ?</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Webinar-smile-comment-drupal-8-peut-booster-votre-strategie-digitale" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/kIe4jNkuPRieg9" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Seminaire-smile-sur-drupal-8-juin-2015">Seminaire Smile sur Drupal 8 - Juin 2015</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-smile-sur-drupal-8-juin-2015" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>									<div class="ressources__container--list__presentations">

                            <iframe width="245" height="180" src="//www.slideshare.net/slideshow/embed_code/key/tQJ4b2HhYr5coe" frameborder="0" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3><a href="/Ressources/Presentations/Seminaire-communication-unifiee">Seminaire communication unifiee</a></h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="/Ressources/Presentations/Seminaire-communication-unifiee" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>							</div>
                    <div style="text-align: center;">
                        <a title="Voir toutes les présentations" href="http://fr.slideshare.net/SmileOpenSource/presentations" target="_blank" class="btn--main">Voir toutes les présentations</a>
                    </div>

                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>

@endsection