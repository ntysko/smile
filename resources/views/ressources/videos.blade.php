@extends('layouts.frontend')

@section('content')

    <div style="background: #708177;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Vidéos.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Ressources" title="Ressources">Ressources</a>
                    <span class="separator"></span>
                    <span class="current_page">Vidéos</span>
                </div>
                <div class="rte_ctnt_block">

                    <p class=" text-left">Revivez nos conférences et webinars en images&nbsp;!</p>	        </div>
                <div class="ressources__container">
                    <div class="ressources__container--list clearfix">
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/RdMQpm1Pk4c" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Webinar SMILE : "Tout savoir sur Magento 2"</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/RdMQpm1Pk4c" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/jkZsTh6sMQM" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Webinar SMILE : "Drupal 8 Nouveautés et retours d'expérience"</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/jkZsTh6sMQM" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/LzFKn-E-USc" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Liferay 7 - Lexicon en action / Admin menu</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/LzFKn-E-USc" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/106o2XfU-oA" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Liferay 7 - Lexicon en action / Mobile First</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/106o2XfU-oA" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/fo-q3tPA2Xs" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Liferay 7 - Lexicon en action / Add Menu</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/fo-q3tPA2Xs" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/Tm2IoDvvGJU" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Smile - We Are Open Source Specialists // ENGLISH VERSION</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/Tm2IoDvvGJU" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/87xIQQQawag" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Vidéo de présentation 2016 de Smile</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/87xIQQQawag" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/K3QIQVcWXKo" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Présentation Smile 2015</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/K3QIQVcWXKo" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/txvQAaHf0TQ" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Webinar Smile et Talend  : "Faites communiquer vos applications en temps réel"</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/txvQAaHf0TQ" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/A0DeUoPn5vk" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>Webinar Smile  Découvrez OpenStack : solution de cloud computing</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/A0DeUoPn5vk" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/i8DdBHuN07I" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>CMSday 2014 - CMSday Awards : Cérémonie de remise des prix</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/i8DdBHuN07I" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ressources__container--list__presentations">
                            <iframe src="https://www.youtube.com/embed/9R7-JA8fX-U" allowfullscreen></iframe>
                            <div class="presentation__inner clearfix">
                                <h3>CMSday 2014 - Quels leviers pour favoriser sa production ?</h3>
                                <div class="ressource__share">
                                    <span class='st_sharethis' st_url="https://www.youtube.com/embed/9R7-JA8fX-U" displayText="Partager"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;">
                        <a title="Voir toutes les vidéos" href="https://www.youtube.com/user/SmileOpenSource/featured" target="_blank" class="btn--main">Voir toutes les vidéos</a>
                    </div>

                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>


@endsection