@extends('layouts.frontend')

@section('content')
    <div style="background: #a88f3f;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Notre-histoire.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col" class="page_interne">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Notre histoire</span>
                </div>

                <div class="rte_ctnt_block clearfix">
                    <h2>Notre histoire</h2>
                    <div class="summary_small" >

                        <p><b>Depuis ses débuts, Smile s'est positionné en avance de phase sur les mouvements profonds des technologies de l'information. Précurseur sur les outils de développement à forte productivité, précurseur sur les technologies de l'Internet, Smile a été, depuis 2001, aux avants-postes de la révolution open source, permettant aux entreprises les plus ambitieuses de bénéficier sans risque des gains de compétitivité apportés par ces nouvelles solutions.</b></p>					</div>



                    <h3>La génèse</h3><p>
                        Smile est fondée en <b>1991</b> par quatre amis issus de l’Ecole Centrale de Paris et de l’Ecole Supérieur d’Electricité, ayant chacun environ cinq années d’expérience dans de grands groupes français (Shell, Thomson, Renault, Cap Gemini). <br />&nbsp;Le premier positionnement de Smile est celui des applications de gestion sur micro-ordinateurs, utilisant des outils de développement dits &quot;RAD&quot; (Rapid Application Development), qui commencent à apparaître.</p><h3>L'Internet</h3><p>En <b>1995</b>, Smile ne compte encore que sept collaborateurs lorsqu’apparaît le World Wide Web. Smile se tourne très tôt vers ce nouvel univers et construit une expertise spécifique, mêlant sa culture des applications et des bases de données aux technologies du web. Durant ces premières années, beaucoup se contentent du web statique, c'est-à-dire de simples pages d’information, tandis que Smile se tourne immédiatement vers les applications web.</p><p>Dès <b>1996</b>, cette expertise est reconnue par de grands clients. Cette même année, la Société Générale demande à l’un de nos associés de créer et de diriger une équipe d’expertise web, tandis que le Crédit Lyonnais nous confie la réalisation du premier site de banque sur Internet. En 1997, c’est au tour de Renault de commander à Smile l’une des toutes premières applications métier web, qui sera suivie de nombreuses autres. Dès ses débuts, Smile sait servir les grands comptes.</p><p><b>1996-2001</b> est une première période de forte croissance, avec un chiffre d’affaires multiplié par 5 en cinq ans ; c’est la première vague Internet. Vers la fin 2000, Smile compte 47 collaborateurs et réalise un chiffre d’affaires de 3,5 M€.</p><p><b>2001-2003</b> constituent des années de crise de ce marché, le fameux éclatement de la bulle Internet. Smile résiste bien à la crise – qui emportera bon nombre de Web agencies – en préservant son volume d’affaires et sa rentabilité.</p><h3>L'open source</h3><p>Dès <b>2001</b>, Smile commence à construire son expertise des solutions open source : un choix d’avenir que beaucoup de ses concurrents n’osent pas alors entreprendre.</p><p>A partir de <b>2004</b>, les grandes entreprises adoptent de plus en plus souvent les solutions open source. Smile occupe une position solide de leader sur ce marché qui décolle, élargissant progressivement son offre vers de nouveaux domaines – CMS, Portails, e-Commerce, Décisionnel, Infrastructure, ERP - et créant des services associés : agence média, tierce maintenance, exploitation, système. En <b>2007</b>, Smile affiche plus de 50% de croissance et inaugure trois nouvelles agences à Lyon, Nantes et Bordeaux.</p><p>En <b>2008</b>, Smile compte plus de 270 collaborateurs et poursuit sa croissance en se développant à l'international, notamment à Kiev en Ukraine.</p><p>En <b>2009</b>, Smile compte 320 collaborateurs dans le monde et poursuit son extension internationale. En juin 2009, Smile intègre Cometa Technologies S.L., basée à Barcelone et compte alors 8 agences dans le monde.</p><p>En <b>2011</b>, Smile regroupe 500 collaborateurs et ouvre ses agences à Bruxelles, Utrecht et Amsterdam.</p><p>En <b>2013</b>, Smile compte 700 collaborateurs et 17 agences.&nbsp;</p><p>En <b>2014</b>, une année réussie pour Smile, avec plus de 20% de croissance sur ces cinq dernières années et près de 700 collaborateurs dans le monde. Au fil de ces années, Smile est devenu un acteur incontournable de l’open source, leader en France et en Europe.&nbsp;</p><p>En <b>2015</b>, Smile&nbsp;annonce être en négociations exclusives avec la société Open Wide en vue d’un rapprochement. Ceci afin d'élargir son offre et renforcer son leadership.</p>							</div>

            </div>

        </div>

    </div>



    <div class="breaker"></div>

@endsection