@extends('layouts.frontend')

@section('content')
    <div style="background: #adcdce;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Nos-communiqués.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Communiqués</span>
                </div>
                <div class="evenements__content presse">


                    <div class="evenements__list">
                        <p class="section-title">Nos communiqués</p>
                        <ul>
                            @foreach($articles as $article)
                            <li class="evenements__list__item">
                                <div class="evenements__list__item__date">
                                    <span class="evenements__list__item__date__nb">{{date('j',strtotime($article->date)) }}</span>
                                    <span class="evenements__list__item__date__month">{{date('F',strtotime($article->date)) }}</span>

                                </div>
                                <div class="evenements__list__item__container">
                                    <h3 class="evenements__list__item__title">{{$article->title}}</h3>
                                    <p>
                                        {{$article->text}}
                                    </p>
                                    <a href="{{ url($article->files) }}" target="_blank" class="evenements__list__item__plus-infos" title='"Acceder a Smile ouvre 300 nouveaux postes"'>Plus d'infos</a>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>



                    {{--<div class="pagination-block">--}}


                        {{--<div class="pagination-block__page-left">--}}
                            {{--<p class="last_off"></p>--}}
                        {{--</div>--}}
                        {{--<div class="pagination-block__page-container">--}}


                            {{--<a class="pagination-block__page-nb active">1</a>--}}

                            {{--<a class="pagination-block__page-nb" href="/Societe/Nos-communiques/(offset)/10">2</a>--}}
                            {{--<a class="pagination-block__page-nb" href="/Societe/Nos-communiques/(offset)/20">3</a>--}}
                            {{--<a class="pagination-block__page-nb" href="/Societe/Nos-communiques/(offset)/30">4</a>--}}

                            {{--<span style="margin-right: 5px;">...</span>--}}
                            {{--<a class="pagination-block__page-nb" href="/Societe/Nos-communiques/(offset)/140">15</a>--}}
                        {{--</div>--}}
                        {{--<div class="pagination-block__page-next">--}}
                            {{--<a class="forward-link" href="/Societe/Nos-communiques/(offset)/10">Page suivante</a>--}}
                        {{--</div>--}}


                    {{--</div>--}}

                    <!--[if IE 6]><br class="clearall" /><![endif]-->
                </div>
            </div>
        </div>


    </div>



    <div class="breaker"></div>

@endsection