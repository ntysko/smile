@extends('layouts.frontend')

@section('content')

    <div style="background: #b2d1d3;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Nos-chiffres-clés.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col" class="page_interne">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Nos chiffres clés</span>
                </div>

                <div class="rte_ctnt_block clearfix">
                    <h2>Nos chiffres clés</h2>
                    <div class="summary_small" >


                        <ul>

                            <li><b>1er intégrateur</b> et expert européen de solutions open source</li>

                            <li>Fondé en<b>&nbsp;1991</b>, 25 ans d'expérience</li>

                            <li><b>1000 </b>collaborateurs</li>

                            <li><b>69.5&nbsp;</b>millions d'euros de chiffre d'affaires en 2015</li>

                            <li><b>9 </b><b>agences en France :</b> Paris, Lyon, Grenoble, Nantes, Bordeaux, Montpellier, Marseille, Toulouse et Lille</li>

                            <li><b>9</b> <b>agences à l'international :</b> à Bruxelles en Belgique ; à Maarssen aux Pays-Bas ; à Genève et Lausanne en Suisse ; au Luxembourg ; à Kiev en Ukraine ; à Moscou en Russie ; à Abidjan en Côte d'Ivoire et à Casablanca au Maroc.</li>

                        </ul>
                    </div>



                    <h3>25% de croissance par an ces 5 dernières années !</h3><p>Smile a terminé une année 2015 en <b>croissance de 30%</b> au niveau européen, réalisant un <b>chiffre d'affaires de 69.5 M€.</b></p><p>La croissance se traduit également en terme de recrutement de nouveaux talents, <b>300 nouveaux collaborateurs</b> rejoindront Smile cette année.&nbsp;</p>




                    <img src="/img/Picto-croissance-2015-FR_large.png"   width="300" height="255"  style="border: 0px;" alt="" title="" />



                    <p>Autre facteur du succès de Smile, la collaboration avec nos clients et nos partenaires. En quelques chiffres :</p>
                    <ul>

                        <li><b>300</b> <b>nouveaux projets</b> réalisés par an</li>

                        <li><b>70% des entreprises du CAC 40</b> nous font confiance</li>

                        <li>et plus de <b>40 partenaires</b> open source collaborent avec Smile.</li>

                    </ul>
                </div>

            </div>

        </div>

    </div>



    <div class="breaker"></div>
@endsection