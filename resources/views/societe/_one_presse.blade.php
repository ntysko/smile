@extends('layouts.frontend')

@section('content')



    <div style="background: #c3a374;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Smile-dans-la-presse.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>




        <div id="center_col">


            <div id="inner_right"></div>


            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Smile dans la presse</span>
                </div>

                <ul class="page-pager clearfix">
                    <li class="next"><a href="/" title="Nantes : une école du logiciel libre ouvrira à la rentrée" class="right">Suivant</a></li>
                    <li class="previous"><a href="/" title="Eram booste ses ventes en ligne avec Smile" class="left">Précédent</a></li>
                </ul>
                <h1>Les nouveaux outils pour « Linux embarqué »  Pierre Ficheux, Smile</h1>


                <div class="rte_ctnt_block">
                    <h3>informatiquenews.fr - Guy Hervier</h3>
                    <div class="actualites-detail__date__share">
                        <span class="actualites-detail__date">06 juin 2016</span>


                        <span class='st_sharethis' st_url="/Societe/Smile-dans-la-presse/Les-nouveaux-outils-pour-linux-embarque" displayText="Partager"></span>
                    </div>
                    <div class="summary_small">
                        <p>Autrefois présents mais inconnus du grand public, les systèmes embarqués ont aujourd’hui envahi notre quotidien et les logiciels libres y ont une place prépondérante, en particulier le système d’exploitation Linux. Le noyau Linux est présent dans le système Android de Google utilisé dans plus d’un milliard de téléphones mobiles. Le système d’exploitation Linux (dans sa version « embarquée ») est omniprésent sur les boîtiers d’accès à Internet et à la télévision numérique (la fameuse « set-top box »).</p><p>Dernièrement, ce même Linux a envahi l’industrie automobile et anime les systèmes dénommés IVI (In Vehicle Infotainment) incluant entre-autres la navigation, la diffusion multimédia dans l’habitacle ou l’assistance à la conduite.</p><p>La construction de tels systèmes est très éloignée de l’utilisation habituelle de Linux dans l’industrie qui est basée sur des « distributions » fournies par des éditeurs commerciaux (Red Hat, SuSE, etc.) ou des communautés (Debian, Ubuntu, etc.). Sur un PC classique la mémoire – et le disque dur – ont des capacités quasiment illimitées et même si l’on a la volonté de maîtriser les consommations d’espace et d’énergie, les contraintes sont bien moindres que dans le domaine des systèmes embarqués.</p></div>
                    <a href="http://www.informatiquenews.fr/nouveaux-outils-linux-embarque-pierre-ficheux-smile-47383" class="read_more" title="Lire l'article complet" target="_blank">Lire l'article complet</a>			<div class="breaker"></div>
                </div>








                <div class="breaker"></div>

                <!--[if IE 6]><br class="clearall"  /><![endif]-->
            </div>



            <a id="article_presse" title="Retour à la liste" href="/" class="btn--main">Retour à la liste</a>


            <!--[if IE 6]><br class="clearall"  /><![endif]-->
        </div>


    </div>



    <div class="breaker"></div>

@endsection