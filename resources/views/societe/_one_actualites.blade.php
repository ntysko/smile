@extends('layouts.frontend')

@section('content')


    <div style="background: #adcdce;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Nos-actualités.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}

            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">

            <div id="inner_right"></div>


            <div id="inner_main">
                <h1 class="hiden">Eram booste ses ventes en ligne avec Smile</h1>
                <div class="breadcrumbs_block">
                    <a href="/Societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Nos actualités</span>
                </div>

                <ul class="page-pager clearfix">
                    <li class="next"><a href="/Societe/Nos-actualites/Nouveau-livre-blanc-smile-linux-pour-l-embarque" title="Nouveau livre blanc Smile : Linux pour l’embarqué" class="right">Suivant</a></li>
                </ul>
                <div class="rte_ctnt_block clearfix actualites-detail__content">

                    <h2 class="secondary-title">Eram booste ses ventes en ligne avec Smile</h2>
                    <div class="actualites-detail__date__share">
                        <span class="actualites-detail__date">09 juin 2016</span>


                        <span class='st_sharethis' st_url="/Societe/Nos-actualites/Eram-booste-ses-ventes-en-ligne-avec-smile" displayText="Partager"></span>
                    </div>
                    <div class="">

                        <p>Smile accompagne l’enseigne Eram (leader de la chaussure en centre-ville et centres commerciaux) dans la mise en œuvre d’un site responsive design de nouvelle génération lui permettant de déployer à grande échelle sa stratégie de vente en ligne. <b>Découvrez le témoignage !</b></p>				</div>

                    <p>Dès 2009, Eram a pris le virage du digital pour lancer ses activités e-commerce. Fort du succès de cette première expérimentation et au regard de l’évolution de ses ventes, l’enseigne a souhaité en 2015 donner une nouvelle impulsion à son projet et déployer un<b> <a href="http://www.eram.fr/" target="_blank">nouveau site</a>
                        </b> reposant sur un socle technique performant et évolutif.</p><p>Dans ce contexte, après avoir lancé un appel d’offres, Eram a confié ce développement au groupe Smile. <b>La sélection de Smile s’explique par son expertise unique autour des technologies Magento, mais également par ses fortes compétences en matière de design et d’ergonomie</b>. Au-delà de ces éléments, la proximité des équipes de Smile a également représenté un facteur déterminant.</p><p>Très rapidement, les équipes d’Eram et de Smile (Paris et Nantes) ont défini une méthodologie de travail agile pour valider pas à pas les développements réalisés. Cela a permis de faire travailler en parallèle les départements Intégration et Design, de tester les développements et de mettre en ligne le nouveau site en moins de huit mois.</p><p>Grâce à ce dispositif, le parcours client a gagné en fluidité ce qui a notamment permis d’enrichir considérablement l’expérience utilisateur. Cela s’est traduit par une amélioration globale des performances : amélioration du taux de rebond de 40 %, temps moyen passé sur le site en augmentation de 80 %, croissance de 35 % du trafic&nbsp; entrant depuis les mobiles…</p><p><b>Renaud MONTIN, Directeur Marketing et digital d’Eram</b> « <i>Smile nous a permis d’accélérer la digitalisation de notre marque. L’expertise, la proximité et la réactivité de leurs équipes sont des points clés qui expliquent le succès de notre projet. Nous avons bénéficié d’un accompagnement et pouvons désormais nous appuyer sur une plateforme spécialement paramétrée pour répondre à nos attentes. Forts de ces éléments, nous sommes en mesure de renforcer notre avantage concurrentiel et de nous imposer comme le leader de la vente en ligne sur les click &amp; mortar chaussures.</i> »</p><p>Grâce à son nouveau site, Eram a également pu offrir des services cross-canal à forte valeur ajoutée tels que la visibilité des stocks magasin et un store locator. Au final, cela se traduit par une expérience de shopping en ligne adaptée aux nouveaux parcours d’achat. Smile a su également concevoir une architecture ouverte à d’autres environnements. Ainsi, à titre d’exemple, les équipes de l'enseigne enrichissent les nombreuses références produits de la marque via la solution de PIM Akeneo qui est étroitement connectée au site. Le service de réservation en boutique est également intégré au site et se fait par l'intermédiaire de la solution packagée Socloz.</p><p>Au-delà du développement et de l’intégration du site-commerce, Smile a enfin réalisé le<b> <a href="http://www.eram.fr/blog/" target="_blank">blog d’Eram</a>
                        </b> sous Wordpress. Engageant et convivial, il fait partie de la stratégie digitale d’Eram et contribue à développer l'image de l’enseigne sur Internet. A noter également l'intégration d'Olapic, outil de curation de contenu sur Instagram, qui permet d'enrichir les fiches produit du contenu généré par la communauté Eram autour du hashtag #myeramtouch.</p><p>Une nouvelle fois, <b>Smile a donc démontré sa capacité à accompagner les plus grandes marques dans leurs projets e-commerce</b> : du conseil à l’intégration en passant par l’ergonomie, le design et la formation des utilisateurs.</p>
                    <div class="breaker"></div>
                    <div class="summary_small"></div>

                </div>
                <div class="news-container__list clearfix last_actus">
                    <h3>Dernières actualités</h3>
                    <div class="news-container__item">
                        <div class="news-container__item__date">09 juin 2016</div>
                        <p class="news-container__item__title">
                            <a href="/">Eram booste ses ventes en ligne avec Smile</a>
                        </p>
                        Smile accompagne l’enseigne Eram (leader de la chaussure en centre-ville et centres commerciaux) dans la mise en œuvre d’un site responsive design de nouvelle génération lui permettant de déployer...
                        <div class="news-container__item__actions">
                            <a class="more_about" href="/">Lire la suite</a>
                            <div class="news-container__item__actions__follow">
                                <span class='st_sharethis' st_url="/Societe/Nos-actualites/Eram-booste-ses-ventes-en-ligne-avec-smile" displayText="Partager">	</span>
                            </div>
                        </div>
                    </div>                                    <div class="news-container__item">
                        <div class="news-container__item__date">02 juin 2016</div>
                        <p class="news-container__item__title">
                            <a href="/">Nouveau livre blanc Smile : Linux pour l’embarqué</a>
                        </p>
                        Smile inaugure une nouvelle collection de livre blanc sur le thème de l'embarqué. Le premier de la série est consacré à l'utilisation de Linux embarqué dans les systèmes industriels.
                        <div class="news-container__item__actions">
                            <a class="more_about" href="">Lire la suite</a>
                            <div class="news-container__item__actions__follow">
                                <span class='st_sharethis' st_url="/Societe/Nos-actualites/Nouveau-livre-blanc-smile-linux-pour-l-embarque" displayText="Partager">	</span>
                            </div>
                        </div>
                    </div>                                    <div class="news-container__item">
                        <div class="news-container__item__date">31 mai 2016</div>
                        <p class="news-container__item__title">
                            <a href="">Webinars Smile : Notre expertise au coeur de votre succès</a>
                        </p>
                        Le printemps de Smile c'est aussi le temps des webinars. Pour tout savoir sur les solutions Open Source, retrouvez notre programme complet en mai &amp; juin 2016.
                        <div class="news-container__item__actions">
                            <a class="more_about" href="">Lire la suite</a>
                            <div class="news-container__item__actions__follow">
                                <span class='st_sharethis' st_url="/Societe/Nos-actualites/Webinars-smile-printemps-2016" displayText="Partager">	</span>
                            </div>
                        </div>
                    </div>                                    </div>
                <div class="breaker"></div>
            </div>

        </div>
        <script type="text/javascript">

            jQuery(document).ready(function(){
                $('#eZHumanCAPTCHACode').bind("click", function(e)
                {
                    imageGenerate('/redirectextension/imagegenerete');
                });
            });
            function imageGenerate(uri){
                empty = '';
                $.ajax({
                    url: uri,
                    success: function(result){

                        $('#captchaimg').attr('src',result) ;
                        $('#captchaimg').show();
                    },
                    error: function(){
                        /* $('#loading').hide();*/
                        $('#captchaimg').text();
                    }
                }) ;
            }

        </script>

    </div>



    <div class="breaker"></div>

@endsection