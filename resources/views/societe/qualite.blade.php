@extends('layouts.frontend')

@section('content')

    <div style="background: #dadee0;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Démarche-qualité.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col" class="page_interne">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Démarche qualité</span>
                </div>

                <div class="rte_ctnt_block clearfix">
                    <h2>Démarche qualité</h2>
                    <div class="summary_small" >

                        <h3>Notre démarche qualité</h3><p>Depuis 2005, Smile a défini comme axe stratégique de développement l’industrialisation de ses savoir-faire. Cette démarche constitue un véritable outil de travail au service de nos équipes et de nos clients, qui contribue à construire la marque de fabrique Smile.</p><p>En définissant des standards, elle garantit au client que le déroulement de la conduite du projet sera identique d’un projet à l’autre, quels que soient le chef de projet, le contexte, la taille du projet ou les difficultés techniques.</p>					</div>



                    <h4><b>Zoom sur la Démarche Qualité Technique</b></h4><p>
                        <b>Qu’est ce qu’une Démarche Qualité appliquée à la technique ?</b> <br />&nbsp;L’objectif de notre démarche Qualité Technique est de mettre en place des méthodes de travail optimales et communes à tous. Ainsi, pour chaque technologie que nous utilisons, nous mettons à disposition l’ensemble des informations sur les process et bonnes pratiques à suivre. Ce partage d’informations nous incite à réutiliser les bonnes idées et éviter les mêmes erreurs. Cela nous permet bien sûr de gagner du temps, de capitaliser le savoir et de rendre des livrables de meilleure qualité. </p><p>
                        <b>Et pour les clients de Smile, quel est l’intérêt ?</b> <br />&nbsp;La qualité de la solution web que nous livrons au client est directement liée à la Démarche Qualité appliquée en interne. En effet, un projet mené dans les règles de la Démarche Qualité garantit le respect de la période de recettes par exemple ou réduit le nombre de bugs. Autre point, la Qualité élimine un certain nombre de problèmes techniques, nos équipes peuvent donc se concentrer davantage sur des aspects fonctionnels du projet. </p><p>
                        <b>Quels outils utilise-t-on pour mettre en œuvre une Démarche Qualité efficace ?</b> <br />&nbsp;Chez Smile, la démarche Qualité se matérialise par des outils concrets que nous mettons à disposition des débutants mais aussi des chefs de projet ou des expérimentés. Ainsi, nous avons créé un Intranet Qualité dans lequel sont détaillés les process Qualité à appliquer à la production. Ici sont décrits pour chaque technologie les éléments essentiels pour les équipes de développements :</p>
                    <ul>

                        <li>Process</li>

                        <li>Outils</li>

                        <li>Méthodes et bonnes pratiques</li>

                    </ul>
                    <p>
                        Un Twiki interne à Smile fournit également une plate-forme de travail collaboratif à laquelle les collaborateurs peuvent facilement contribuer.<br />
                        &nbsp;Enfin, il existe également des listes de diffusion par technologie. C’est un moyen simple et réactif pour faire partager à l’instant T une information à l’ensemble des personnes qui travaillent sur une technologie en question.<br />&nbsp;Bref, quel que soit l’outil, les objectifs sont les mêmes : partage des connaissances et mise en place de standards.</p><p>
                        <b>Quelle place tient la formation dans le projet Qualité d’une entreprise ?</b> <br />&nbsp;Smile est très impliqué dans la stratégie de formation, pierre angulaire d’un secteur aussi dynamique et innovant que les nouvelles technologies. Aujourd’hui plus 150 heures de formation sont délivrées chaque année chez Smile. La formation est également un élément indispensable de notre projet Qualité. En facilitant l’acquisition de nouvelles compétences de façon homogène, la formation contribue à la capitalisation du savoir chez Smile, un axe essentiel de notre projet Qualité.</p>							</div>

            </div>

        </div>

    </div>



    <div class="breaker"></div>

@endsection