@extends('layouts.frontend')

@section('content')


<div style="background: #d5b687;" class="lame-full-width lame-smile-carriere-banner">
    <div class="lame-full-width__container">
        <div class="lame-full-width__content cf">
            <img alt="Smile recrute 250 super experts" src=/img/Société.jpg>
        </div>
    </div>
</div>
<div id="right_col">


    {{ Widget::RightTwitter() }}


    <div class="common_right_block">
        {{ Widget::RightPresse() }}
    </div>		            	            		    					    	                            </div>


<div id="main_col">

    <div id="left_col">



        <div id="main_menu_block">

            {{ Widget::LeftNav() }}
        </div>

        <div class="breaker"></div>
        <div class="common_left_block">
            <div class="item-block__img">





                <img src="/img/Smile-se-mobilise_encart_new.png"   width="84" height="35"  style="border: 0px;" alt="" title="" />




            </div>
            <div class="item-block__content">

                <h5>Smile se mobilise</h5><p>sur la politique d'emploi handicap</p>            	            <a href="/Recrutement" title="Recrutement">&gt; Découvrir</a>

            </div>
        </div>



    </div>


    <div id="center_col" class="page_interne">

        <div id="inner_main">
            <div class="breadcrumbs_block">
                <span class="current_page">Société</span>
            </div>

            <div class="rte_ctnt_block clearfix">
                <h2>Smile, premier intégrateur de solutions open source</h2>
                <div class="summary_small" style="border-bottom: medium none;">

                    <p>Smile est une société d'experts des architectures web et des solutions open source.</p><p>Avec plus de 1000 collaborateurs en Europe et une expertise open source reconnue dans une variété de domaines, Smile est le premier intégrateur européen de logiciel libre. Avec une devise : faire simple, utile, performant.</p><p>Acteur engagé dans les progrès de l’Internet depuis 1995, Smile a réalisé quelques-uns des plus grands sites de l'Internet français, des sites à forte valeur ajoutée et à forte audience. Smile a également été choisie par les plus grandes entreprises françaises pour concevoir, réaliser et maintenir des applicatifs Intranet stratégiques, servant des centaines d'utilisateurs sur des milliers de transactions.</p><p>Grâce à des investissements continus en veille technologique, Smile anticipe les tendances, choisit et maîtrise les produits le plus en amont possible pour apporter à ses clients des solutions open source performantes et sûres.</p>					</div>

            </div>

            <div class="common_listing_block products_listing_block">

                <div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/actualites" title="Nos actualités">
                                Nos actualités            </a>
                        </h2>

                        <p>Ne manquez rien sur l'actualité de Smile ! Suivez nos news et nos actions.</p>
                        <a class="more_about" href="/societe/actualites" title="Nos actualités">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/agences" title="Nos agences">
                                Nos agences	  		</a>
                        </h2>

                        <p>Smile, c'est 1000 collaborateurs répartis sur 18 agences dans le monde. Découvrez l'agence Smile près de chez vous !</p>
                        <a class="more_about" href="/societe/agences" title="Nos agences">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/vision" title="Notre vision de l'Open Source">
                                Notre vision de l'Open Source	  		</a>
                        </h2>

                        <p>Notre première ambition est de livrer à nos clients des systèmes simples, utiles et performants, qui leur permettent d’atteindre leurs objectifs et d’obtenir un avantage compétitif.</p>
                        <a class="more_about" href="/societe/vision" title="Notre vision de l'Open Source">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/histoire" title="Notre histoire">
                                Notre histoire	  		</a>
                        </h2>

                        <p>Depuis nos débuts en 1995, nous avons contribué à des progrès significatifs de l'Internet en France, à la fois en nous engageant au côté de grandes entreprises et en développant des technologies de pointe.</p>
                        <a class="more_about" href="/societe/histoire" title="Notre histoire">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/dirigeants" title="Nos dirigeants">
                                Nos dirigeants	  		</a>
                        </h2>

                        <p>Grâce à une structure collégiale, notre direction bénéfice d'expériences multiples où chacun joue un rôle de relais pour transmettre notre identité, développer le sens du service et favoriser une culture de convivialité.</p>
                        <a class="more_about" href="/societe/dirigeants" title="Nos dirigeants">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/chiffres" title="Nos chiffres clés">
                                Nos chiffres clés	  		</a>
                        </h2>

                        <p>Faits marquants, évolution du chiffre d'affaire, répartition de nos activités, développement de nos effectifs... Retrouvez les chiffres clés de Smile avec toutes les explications nécessaires.</p>
                        <a class="more_about" href="/societe/chiffres" title="Nos chiffres clés">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/partenaires" title="Nos partenaires">
                                Nos partenaires	  		</a>
                        </h2>

                        <p>Smile construit depuis plusieurs années des partenariats technologiques et commerciaux : des synergies gagnantes pour apporter à nos clients le meilleur service sur les meilleurs produits.</p>
                        <a class="more_about" href="/societe/partenaires" title="Nos partenaires">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/qualite" title="Démarche qualité">
                                Démarche qualité	  		</a>
                        </h2>

                        <p>Smile a choisi de placer l’industrialisation de son savoir-faire au cœur de sa stratégie de développement. Découvrez pourquoi et comment, avec l'interview de François-Xavier Bonnet, directeur technique.</p>
                        <a class="more_about" href="/societe/qualite" title="Démarche qualité">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/communiques" title="Communiqués de presse">
                                Communiqués de presse            </a>
                        </h2>

                        <p>Retrouvez les communiqués de presse de notre Groupe.</p>
                        <a class="more_about" href="/societe/communiques" title="Communiqués de presse">Lire la suite</a>
                    </div>
                </div>																								<div class="same_lv_pg_presentation_block">
                    <div class="content">
                        <h2>
                            <a href="/societe/presse" title="Smile dans la presse">
                                Smile dans la presse            </a>
                        </h2>

                        <p>Dans la presse ou sur la toile, retrouvez un aperçu de nos interviews et nos annonces.</p>
                        <a class="more_about" href="/societe/presse" title="Smile dans la presse">Lire la suite</a>
                    </div>
                </div>															</div>
        </div>

    </div>

</div>



<div class="breaker"></div>



@stop