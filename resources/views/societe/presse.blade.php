@extends('layouts.frontend')

@section('content')
    <div style="background: #c3a374;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Smile-dans-la-presse.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Smile dans la presse</span>
                </div>
                <h2 class="secondary-title">Smile dans la presse</h2>
                <div class="news-container__list clearfix">
                    @foreach($articles as $article)


                    <div class="news-container__item">
                        <div class="news-container__item__date">{{$article->date}}</div>
                        <h3 class="news-container__item__title">
                            <a href="{{ url('/societe/one_presse', $article->id) }}">{{$article->title }}</a>
                        </h3>
                        <p>
                            {{strip_tags(str_limit($article->text, $limit = 250, $end = '...'))}}
                        </p>
                        <div class="news-container__item__actions">
                            <a class="more_about" href="{{ url('/societe/one_presse', $article->id) }}">Lire la suite</a>
                            <div class="news-container__item__actions__follow">
                                <span class='st_sharethis' st_url="{{ url('/societe/one_presse', $article->id) }}" displayText="Partager"></span>
                            </div>
                        </div>
                    </div>

                    @endforeach
                  						<!--[if IE 6]><br class="clearall" /><![endif]-->
                </div>

                {{--<div class="pagination-block">--}}


                    {{--<div class="pagination-block__page-left">--}}
                        {{--<p class="last_off"></p>--}}
                    {{--</div>--}}
                    {{--<div class="pagination-block__page-container">--}}


                        {{--<a class="pagination-block__page-nb active">1</a>--}}

                        {{--<a class="pagination-block__page-nb" href="/Societe/Smile-dans-la-presse/(offset)/12">2</a>--}}
                        {{--<a class="pagination-block__page-nb" href="/Societe/Smile-dans-la-presse/(offset)/24">3</a>--}}
                        {{--<a class="pagination-block__page-nb" href="/Societe/Smile-dans-la-presse/(offset)/36">4</a>--}}

                        {{--<span style="margin-right: 5px;">...</span>--}}
                        {{--<a class="pagination-block__page-nb" href="/Societe/Smile-dans-la-presse/(offset)/168">15</a>--}}
                    {{--</div>--}}
                    {{--<div class="pagination-block__page-next">--}}
                        {{--<a class="forward-link" href="/Societe/Smile-dans-la-presse/(offset)/12">Page suivante</a>--}}
                    {{--</div>--}}


                {{--</div>--}}

            </div>
        </div>


    </div>



    <div class="breaker"></div>

@endsection