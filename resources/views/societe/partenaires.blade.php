@extends('layouts.frontend')

@section('content')
    <div style="background: #d2b281;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Nos-partenaires.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <h1>Partenaires</h1>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/Societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Nos partenaires</span>
                </div>
                <a href="#" name="page_top" id="page_top"></a>

                <!--[if IE 6]><br class="clearall" /><![endif]-->


                <div class="rte_ctnt_block">

                    <h2>Nos partenaires</h2>


                    <div class="summary">
                        <p>Depuis plusieurs années, Smile construit des partenariats technologiques et commerciaux afin d’offrir à ses clients le meilleur service sur les meilleurs produits, en développant des synergies gagnantes.</p><p>Pour Smile, ces partenariats ne prennent leur sens que dans le contexte d'un investissement massif, permettant de construire et d'approfondir une vraie expertise, démontrée par des projets réussis.</p></div>

                </div>



                <div class="common_listing_block products_listing_block">


                    <div class="item_block">





                        <img src="/img/Acquia-Drupal_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Acquia - Drupal</h3>

                            <p>
                                Fin 2008, après de nombreux tests et analyses, nous avons décidé d’intégrer <b>Drupal</b> aux solutions de gestion de contenu que nous proposons chaque jour à nos clients. Il s’en est suivi un plan stratégique de montée en compétence et d’investissements.<br />
                                Pour accélérer notre développement et notre reconnaissance sur l’expertise Drupal, Smile a signé début 2010 un contrat de partenariat avec la société Acquia (USA, fondée par Dries Buytaert, le créateur de Drupal). Ce contrat nous permet d’accéder à de nombreuses ressources de qualité et d’être au plus prêt de l'évolution du produit.<br />Plus d'information sur: <a href="http://www.acquia.com" target="_blank">www.acquia.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/GLPI_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="logo GLPI" title="logo GLPI" />



                        <div class="datas_block">
                            <h3>GLPI</h3>

                            <p>Smile est aujourd'hui partenaire GOLD de GLPI, une&nbsp;solution d'assistance et de gestion d'inventaire de parc informatique open source. Initialement GLPI était axé sur la gestion du parc informatique d'une entreprise mais au fil des versions celui-ci a évolué vers la&nbsp;gestion des contrats associées aux équipements, la gestion des licences, l'assistance aux utilisateurs associée à une base de connaissances et d'autres fonctions annexes.</p><p>Plus d'information sur : <a href="http://glpi-project.org/" target="_blank">http://glpi-project.org/</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Akeneo_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="logo Akeneo" title="logo Akeneo" />



                        <div class="datas_block">
                            <h3>Akeneo</h3>

                            <p>Crée en 2012 par une équipe franco-américaine (dont Yoav Kutner - co-fondateur et ancien CTO de Magento), Akeneo est un outil de PIM (Product Information Management) open source destiné aux marchands souhaitant centraliser l'ensemble de leurs informations produits en un seul et même point. Akeneo s'appuie sur le framework Symfony2 et la plateforme applicative Oro Platorm. Diffusée sous licence OSL 3.0, la première beta publique d'Akeneo est sortie fin septembre 2013.</p><p>Plus d'informations sur : <a href="http://www.akeneo.com/" target="_blank">http://www.akeneo.com/</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Ansible_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Ansible</h3>

                            <p>
                                Ansible, société basée en Caroline du Nord, Etats-Unis, a développé une solution innovante et open source d'automatisation . La solution Ansible est très puissante, permettant le management des configurations complexes et industrialisant les déploiements, pour un résultat &quot;DevOps made simple&quot;. <br />
                                &nbsp;Ansible a déjà conquis de nombreuses organisations, y compris les plus exigeantes : &quot;AgentLess&quot;, &quot;Human readable automation&quot;, &quot;workflow orchestration&quot;, &quot;security&quot;, &quot;control delegation&quot; ... et est compatible avec tout l'écosystème Cloud, dont Docker, OpenStack ou Amazon...<br />Plus d'information sur : <a href="http://www.ansible.com/home" target="_blank">www.ansible.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/WSO2_dirigeant.jpg"   width="100" height="73"  style="border: 0px;" alt="WSO2" title="WSO2" />



                        <div class="datas_block">
                            <h3>WSO2</h3>

                            <p>WSO2 est développé depuis 2005 par le Dr. Sanjiva Weerawarana et Paul Fremantle, anciens de chez IBM, créateurs notamment de Apache SOAP et WSIF (Web Services Invocation Framework).</p><p>WSO2 ESB repose sur la plateforme Carbon, implémentant les spécifications OSGi, commune à tous les produits de WSO2, modulaire, extensible et pouvant nativement être mise à l'échelle. Il utilise les projets Apache Synsapse, pour la composante de médiation, et Apache Axis2, pour les web services, dont l'éditeur est le principal contributeur.</p><p>Contrairement à ses principaux concurrents, WSO2 ESB est 100% Open Source et ne possède pas de version premium payante. Son code source est librement accessible sur un dépôt SVN. La société se rémunère sur le support et les services de développement et production.</p><p>Plus d'information ici :&nbsp;<a href="http://wso2.com/">http://wso2.com/</a>
                                &nbsp;</p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/eZ-Publish_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="Logo eZ Publish Gold Partner" title="Logo eZ Publish Gold Partner" />



                        <div class="datas_block">
                            <h3>eZ Publish</h3>

                            <p>
                                eZ Publish est une solution de <b>gestion de contenu open source haut de gamme</b>, certainement l'une <b>des plus complètes</b> disponibles actuellement, particulièrement sur les fonctionnalités centrales de la gestion de contenu, telles que la structuration des contenus, la gestion des workflows, des versions, ou la catégorisation.<br />
                                Smile met en oeuvre eZ Publish depuis 2003, et a pu en apprécier la <b>grande robustesse</b>, et la <b>qualité de finition</b> irréprochable.<br />
                                « Smile est l’un de nos partenaires historique en France. Avec une parfaite maîtrise de la gestion de contenus, Smile exploite au maximum les capacités d’eZ Publish et a développé certains des plus grands sites de l’internet français. <i>&nbsp;</i>»<br /> Bertrand Maugain, Global Partner Manager eZ Systems.</p><p>Smile est aujourd'hui Platinum Partner et donc le <b>premier partenaire mondial</b> d'eZ Publish.</p><p>Plus d'information sur : <a href="http://ez.no/fr/" target="_blank">http://ez.no/fr/</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Jahia_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="Logo Jahia" title="Logo Jahia" />



                        <div class="datas_block">
                            <h3>Jahia</h3>

                            <p><b>Partenaire historique</b> de Jahia, Smile était, dès 2003, l’un des tout <b>premiers intégrateurs</b> du produit en France. Depuis, Smile est resté <b>à la pointe</b> dans les déploiements Jahia, en nombre de références, en envergure, mais surtout en <b>maîtrise technique</b>. Ce sont quelques-uns des points forts qui ont amené de très grandes sociétés à choisir Jahia en collaboration avec Smile : Bouygues Immobilier, Valorissimo, Arjowiggins, Eutelsat, Beauté Prestige International, Groupe COLAS, Académie de Rennes, Conseil Régional d’Ile de France...</p><p>Plus d'information sur : <a href="http://www.jahia.com/jahia/Jahia" target="_blank">www.jahia.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/TYPO3_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="logo Typo3" title="logo Typo3" />



                        <div class="datas_block">
                            <h3>TYPO3</h3>

                            <p>TYPO3 est l'un des tous premiers CMS open source, et l'un des plus actifs. Son succès tient en bonne partie à son système d'extensions, et à la <b>richesse du catalogue d'extensions</b>, qui compte quelques contributions majeures de Smile.</p><p>Typo3.Association est une association à but non lucratif qui finance et coordonne les développements du produit TYPO3, organise les événements qui fédèrent la communauté, et offre des certifications.</p><p>En 2003, Smile a été parmi les tous premiers intégrateurs de TYPO3 en France, et depuis cette date, ce sont plus de <b>130 sites et portails </b>qui ont été réalisés avec ce superbe outil. En devenant membre de l'association Typo3, Smile confirme son attachement à la communauté créée par Kasper Skårhøj, et son <b>implication dans le développement</b> du produit.</p><p>Plus d'information sur : <a href="http://www.typo3.fr/" target="_blank">www.typo3.fr</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/alfresco_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="Logo Alfresco" title="Logo Alfresco" />



                        <div class="datas_block">
                            <h3>Alfresco</h3>

                            <p>Alfresco est une solution open source de référence pour la gestion de contenu d'entreprise. Alfresco combine l'innovation du monde open source avec la stabilité et les performances d'une plateforme dédiée à l'entreprise.</p> <p>« Logiciel ouvert, Alfresco s’enrichit en permanence grâce aux contributions de la communauté. Il permet ainsi à Smile de pouvoir proposer à ses clients une solution de plus grande qualité, plus rapidement, et à moindre coût », Denis Dorval, Vice-Président d’Alfresco en charge de la zone sud EMEA.</p> <p>Pour en savoir plus, découvrez une sélection de <a href="/References/References-par-outil/Alfresco">nos références avec Alfresco</a>
                                .</p> <p>Plus d'information sur : <a href="http://www.alfresco.com/fr/" target="_blank">www.alfresco.com </a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/nuxeo_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Nuxeo</h3>

                            <p>La suite de solutions d'ECM de Nuxeo se compose de solutions open source modulaires, flexibles à un coût très avantageux pour gérer l'information. </p> <p> <b>Pionnier du marché de l'ECM open source</b>, Nuxeo est utilisé par des centaines de clients dans le monde, tels The Press Association, L'Agence France-Presse, EllisDon ou encore Cengage. Des entreprises de toute taille, dans tous les secteurs utilisent les solutions d'Enterprise Content Management de Nuxeo pour répondre à leurs besoins spécifiques de gestion de contenu. Le support, la formation et les services de consulting sont disponibles dans le monde entiers pour nos clients et partenaires. </p> <p>Basé à Paris, Nuxeo est également présent en Amérique du Nord (Boston), en Europe de l'Est (Moscou), en Europe de l'Ouest (Paris, Madrid, Rome) et au Moyen Orient (Dubaï).</p> <p>« Pour répondre au développement de la demande des entreprises pour les solutions open source, il est essentiel d'enrichir notre réseau de partenaires de spécialistes reconnus, » ajoute Arnaud Lefèvre, DGA en charge des ventes de Nuxeo.</p> <p>Plus d'information sur : <a href="http://www.nuxeo.com/fr" target="_blank">www.nuxeo.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img//liferay_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Liferay</h3>

                            <p>Liferay Enterprise Portal est un <b>portail J2EE open source</b> américain avec un aspect fort de personnalisation possible par l'utilisateur final. Il permet des opérations de construction de sa page par assemblage de blocs, le tout en actions de glisser/déposer, simples et ergonomiques.</p> <br/> <p>Liferay est une excellente solution pour un portail d'entreprise, permettant <b>l'intégration standardisée </b>de tous les applicatifs existants, proposant des fonctionnalités d'animation du portail et de paramétrage des pages et des modules, avec une ergonomie toujours travaillée, séduisant aussi bien les utilisateurs que les gestionnaires d'Intranets.</p> <br/> <p>Enfin, Liferay a su aussi créer des <b>partenariats technologiques </b>intéressants pour proposer des solutions packagées Portail+GED ou Portail+Décisionnel ; et des <b>partenariats commerciaux </b>comme avec Smile afin d'accroître sa présence sur le marché européen.</p> <p>Plus d'information sur : <a href="http://www.liferay.com/" target="_blank">www.liferay.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Hippo_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Hippo</h3>

                            <p>
                                Hippo définit les standards pour les entreprises qui veulent accroître l'intérêt de leur public pour leurs contenus web.<br />
                                Hippo alimente les plateformes des plus grands et plus populaires acteurs du web dans le monde. C'est le premier outil de gestion de contenu web à proposer des contenus contextuels à ses clients.<br />
                                Que ce soit pour un site web, une application mobile, un portail intranet, du e-commerce, un réseau social ou même du print, la solution Hippo est un outil puissant pour les entreprises qui veulent créer, proposer et optimiser leurs contenus afin d'en dégager plus de valeur ajoutée.<br />Plus d'information sur : <a href="http://www.onehippo.com/" target="_blank">http://www.onehippo.com/</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/magento_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Magento</h3>

                            <p>Smile et Magento ont signé un partenariat stratégique permettant de proposer et de déployer une solution d'e-Commerce <b>performante et </b> <b>complète</b> à leurs clients communs. En effet, dans le monde open source, Magento est la solution e-Commerce la plus aboutie, disposant d'une très large couverture fonctionnelle. Par ailleurs, grâce aux technologies PHP5 et Zend Framework, Magento dispose d'une forte extensibilité. </p> <p>Plus d'information sur : <a href="http://www.magentocommerce.com/" target="_blank">www.magentocommerce.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Talend_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="Logo Talend" title="Logo Talend" />



                        <div class="datas_block">
                            <h3>Talend</h3>

                            <p>Talend est un <b>éditeur logiciel open source</b> intervenant sur le marché de <b>l’intégration de données</b>. Les solutions de Talend sont utilisées pour l’ETL (Extraction, Chargement, Transformation) décisionnel, et pour l’intégration entre systèmes opérationnels. Avec ce partenariat, Smile garantit ainsi une <b>expertise approfondie</b> et un <b>suivi de qualité</b> à ses clients.</p><p>« Smile est un des acteurs majeurs de l’intégration de solutions open source, et nous sommes fiers de les voir rejoindre le Talend Alliance Program. En offrant à nos partenaires support et accompagnement technique, commercial et marketing, nous avons pour ambition de les aider à optimiser leurs projets, à rationaliser leurs choix technologiques, et à participer à une plus grande satisfaction de leurs clients.» François Méro, Directeur Europe de Talend.</p><p><a href="/Produits/Talend">Vous pouvez découvrir la présentation de Talend.</a>
                            </p><p>Plus d'information sur : <a href="http://fr.talend.com/index.php" target="_blank">http://fr.talend.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Pentaho_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="logo Pentaho" title="logo Pentaho" />



                        <div class="datas_block">
                            <h3>Pentaho</h3>

                            <p>Smile et Pentaho ont signé un partenariat stratégique permettant de proposer et de déployer des <b>plateformes décisionnelles open source complètes</b> et garanties à leurs clients communs. En effet, Pentaho est une suite décisionnelle extrêmement complète. Elle comprend en propre toutes les briques décisionnelles, de <b>l’ETL </b>à la solution de <b>reporting</b>, en passant par <b>l’analyse multidimensionnelle OLAP</b>, le <b>portail </b>personnalisé et le <b>datamining</b>. Pentaho propose de plus une solution de modélisation de metamodel, permettant aux utilisateurs métier d’interroger les bases de données opérationnelles sans aucune connaissance informatique.</p><p>« Nous sommes certain que Smile, 1er intégrateur de solutions open source, peut participer pleinement au développement de Pentaho et ensemble nous espérons transformer de nombreux prospects en clients satisfaits », explique Davy Nys, Responsable commercial EMEA.</p><p>Plus d'information sur : <a href="http://www.pentaho.com/" target="_blank">www.pentaho.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Hortonworks_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Hortonworks</h3>

                            <p>Smile est partenaire &quot;System Integrateur&quot; de Hortonworks, l'éditeur d'une distribution autour de Hadoop, dédiée au bigdata. Hortonworks est leader dans la contribution à Hadoop ainsi qu'aux projets de l'écosysteme Apache, tout en restant complètement open source.</p><p>Ce partenariat permet à Smile de disposer de savoir-faire et de compétences fortes sur Hadoop et la distribution Hortonworks, avec une équipe de consultants dédiés et certifiés.</p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/jaspersoft_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Jaspersoft</h3>

                            <p>
                                Jaspersoft propose la <b>suite décisionnelle open source la plus utilisée au monde</b>, avec plus de 10 millions de téléchargements et plus de 12 000 clients professionnels dans 100 pays. La suite décisionnelle de Jaspersoft via une interface web propose une approche ouverte et modulable, qui adresse la prochaine génération de besoin décisionnel pour tous types d’entreprises. <br />Le logiciel est rapidement mis à jour par une communauté de plus de 120 000 membres enregistrés, qui travaillent sur plus de 350 projets, ce qui représente la communauté décisionnelle la plus large au monde. </p> <p>Plus d'information sur : <a href="http://www.jaspersoft.com" target="_blank">www.jaspersoft.com</a>
                                et <a href="http://jasperforge.org/" target="_blank">www.jasperforge.org</a>
                                .</p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/spagobi_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="logo SpagoBI" title="logo SpagoBI" />



                        <div class="datas_block">
                            <h3>SpagoBI</h3>

                            <p>Smile est partenaire intégrateur de la solution décisionnelle open source <a href="http://www.spagobi.org/" target="_blank">SpagoBI</a>
                                , la solution qui permet la plus grande interopérabilité avec les solutions décisionnelles, open source comme propriétaires.</p> <br/> <p>SpagoBI; plateforme web autonome ou intégrée au portail eXo, permet d'exécuter les rapport BIRT, JasperReport, Business Objects et Microsoft SqlServer Reporting Service, d'exécuter les cubes OLAP Mondrian et Palo, ... et propose en plus une fonctionnalité de requêtage simplifié pour les utilisateurs métier, ne nécessitant aucune connaissance technique, et basée sur la solution open source Hibernate.</p> <p>Pour en savoir plus, découvrez une sélection de <a href="/References/References-par-outil/Spagobi">nos références avec SpagoBI</a>
                            </p> <p>Plus d'information sur : <a href="http://www.spagoworld.org/xwiki/bin/view/SpagoBI/" target="_blank">www.spagoworld.org</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/jedox-palo_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="Logo Jedox" title="Logo Jedox" />



                        <div class="datas_block">
                            <h3>Jedox - Palo</h3>

                            <p>Jedox AG été fondé en 2002 par Kristian Raue et est un éditeur leader de solutions Open Source pour la Business Intelligence. Smile est partenaire de Jedox et intègre à ce titre l'outil de BI open source Palo, produit édité par Jedox qui couvre tout le domaine de la BI à savoir la planification, l’édition de rapports et l'analyse.</p> <p>Plus d'information sur : <a href="http:///www.jedox.com/fr/apercu-jedox/accueil.html" target="_blank">www.jedox.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Odoo_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Odoo</h3>

                            <p>Odoo (ex-OpenERP) est un outil très complet qui tire parti d'une redoutable avance technologique pour proposer des fonctionnalités toujours plus poussées à un rythme très soutenu en terme de développement. C’est un outil très riche fonctionnellement, sans doute l’ERP libre le plus riche, il possède environ 5 ans d’avance technologique sur ses concurrents libres. La modularité de la plateforme a permis l'émergence de tout un éco-système d'acteurs qui contribuent des modules toujours plus avancés.</p><p>Pour en savoir plus sur <a href="/Ressources/Livres-blancs/Livres-blancs/Erp-open-source">les ERP open source</a>
                            </p><p>Plus d'information sur : <a href="https://www.odoo.com/">www.odoo.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Zimbra_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="logo Zimbra" title="logo Zimbra" />



                        <div class="datas_block">
                            <h3>Zimbra</h3>

                            <p>Zimbra Collaborative Suite est une solution open source de travail collaboratif ou groupware.</p><p>
                                Depuis sa création en 2003, à l’initiative de trois anciens employés de SUN, la solution est soutenue par une communauté active de plus de 22 000 membres. Zimbra a été rachetée par le géant américain du web Yahoo en 2007 puis, en 2010, VMware a conclu un accord définitif avec Yahoo! pour acquérir Zimbra.<br /> Aujourd’hui, cette solution est utilisée dans plus de 80 000 organisations soit environ 44 millions de boîtes aux lettres dans plus de 80 pays.</p><p>Plus d'information sur : <a href="http://www.zimbra.com/" target="_blank">http://www.zimbra.com/</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/CDC-Arkhineo_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="logo CDC Arkineo" title="logo CDC Arkineo" />



                        <div class="datas_block">
                            <h3>CDC Arkhinéo</h3>

                            <p>Créée en 2001, CDC Arkhinéo est une filiale détenue à 100% par la Caisse des Dépôts. CDC Arkhinéo, tiers de confiance archiveur, est la première société française d’archivage à valeur probante des données électroniques (90 millions de documents archivés en 2010, avec une croissance record de 40 % par rapport à 2009). Grâce à sa solution unique de Coffre-fort électronique® en mode ASP, CDC Arkhinéo assure la conservation intègre et à long terme de tous types de documents électroniques (factures, bulletins de paie, contrats de prêt, contrats de travail, courriers électroniques, jetons d’horodatage, fichiers de preuve, etc.). CDC Arkhinéo s’engage en effet à tous les stades du processus : garantie de conservation des données dans des centres d’archivage hautement sécurisés (site de sauvegarde distant de plus de 450 kilomètres des deux principaux situés en région parisienne), garantie d’intégrité des données déposées, accès en ligne permanent à tous les documents (taux de disponibilité contractuels).</p> <p>CDC Arkhinéo est membre de l'ADIJ, du GS1, a participé au groupe de travail en charge de la révision de la norme AFNOR Z 42-013 et est régulièrement consultée comme expert. En mai 2010, le service Coffre-fort électronique® de CDC Arkhinéo a été attesté par un tiers en conformité avec la norme Z42-013 révisée en 2009 au niveau de sécurisation « Renforcé ». CDC Arkhinéo, s’est vu délivrer en septembre 2010, par le Ministère de la Culture, son agrément pour la conservation des archives publiques des collectivités et administrations.</p> <p>Pour plus d’informations, vous pouvez consulter le site et vous abonner au flux RSS : <a href="http://www.cdcarkhineo.fr/">www.cdcarkhineo.fr</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/SugarCRM_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>SugarCRM</h3>

                            <p>
                                SugarCRM est le premier éditeur mondial de solutions commerciales Open Source de gestion de la relation client (GRC).<br />
                                L'architecture Open Source de SugarCRM permet aux entreprises de faire évoluer et d'intégrer plus aisément leurs processus, l'objectif associé étant d'entretenir et de développer des relations clients encore plus profitables.<br />
                                Sugar est une solution de CRM souple d'utilisation, permettant d'obtenir une vision globale des interactions entre les clients et les équipes commerciales, le marketing et le support client.<br />Plus d'information sur : <a href="http://www.sugarcrm.com/crm/" target="_blank">www.sugarcrm.com</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Blue-Mind_dirigeant.jpg"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Blue Mind</h3>

                            <p>Blue Mind est une solution complète de <b>messagerie</b> d’entreprise, d’agendas et de travail collaboratif. Disponible depuis 2012, cette solution est avant tout pensée pour ne proposer à l’écran que les fonctions essentielles tout en gardant un aspect épuré et moderne.</p><p>Plus d'information sur : <a href="http://www.blue-mind.net/" target="_blank">www.blue-mind.net</a>
                            </p>	</div>
                    </div>

                    <div class="item_block">





                        <img src="/img/Symfony-SensioLabs_dirigeant.png"   width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <div class="datas_block">
                            <h3>Symfony - SensioLabs</h3>

                            <p>Symfony est un framework PHP5 Open-Source, créé en 2005 par l’agence interactive française SensioLabs. Dédié au développement d’applications web d’entreprises, il rencontre un succès international retentissant depuis son lancement au point de séduire de nombreux acteurs de référence (Yahoo!, Dailymotion,…). Symfony est soutenu par une communauté mondiale impliquée et active, en croissance constante.</p><p>Plus d'information sur : <a href="http://sensiolabs.com/" target="_blank">http://sensiolabs.com/</a>
                            </p>	</div>
                    </div>
                </div>

            </div>
        </div>


    </div>



    <div class="breaker"></div>

@endsection