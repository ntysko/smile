@extends('layouts.frontend')

@section('content')
    <div style="background: #8eaca0;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Notre-vision.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col" class="page_interne">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Notre vision</span>
                </div>

                <div class="rte_ctnt_block clearfix">
                    <h2>Notre vision de l'Open Source</h2>
                    <div class="summary_small" >

                        <h3><b>L'Open Source, facteur de compétitivité pour l'entreprise</b></h3>					</div>



                    <p>L’open source est avant tout <b>un mouvement de pensée</b> fondé sur des valeurs de <b>liberté</b>, de respect, de responsabilité sociale et d’ouverture. En tant que citoyens, nous sommes attachés à ces valeurs. Auxquelles nous associons la <b>transparence</b>, la fiabilité et la fidélité, un goût pour <b>le service rendu</b> autant que pour <b>l’expertise technique</b>, et la vision de systèmes d’informations <b>simples, utiles et performants</b>.</p><p>Dans la sphère économique, la diffusion de l’open source est un atout pour <b>&nbsp;la compétitivité de notre économie</b> en général, apportant à nos entreprises <b>des gains de productivité majeurs</b>, qui contribuent à les rendre plus performantes et plus rentables.</p><p>Nous ne pensons pas pour autant que le logiciel propriétaire soit moralement condamnable. Il peut avoir sa raison d’être, et a amené quelques produits de qualité. Mais dans l’informatique on constate que la prime au premier est si forte qu’elle conduit très vite à <b>des situations de monopoles</b> ou d’oligopoles, qui freinent la compétition et l’innovation, et au final <b>coûtent cher aux entreprises</b>.</p><p>Au contraire, l’open source apporte des solutions ouvertes, robustes et toujours <b>respectueuses des standards</b> d’interopérabilité. Et cette offre est <b>chaque année plus étendue et plus mature à la fois</b>, couvrant aujourd’hui l’ensemble des domaines, depuis l’infrastructure, les frameworks et outils de développement, la gestion de contenus, les portails, la Business Intelligence, le CRM, la GED, le e-Commerce, et jusqu’aux progiciels de gestion intégrés (ERP).</p><p>Dans chacun de ces domaines, <b>Smile investit dans une action de veille soutenue</b>, afin de construire une large expertise, couvrant une diversité de solutions. <a href="/Ressources/Livres-blancs">Un aperçu de cette expertise est offert au travers de nos Livres Blancs</a>
                        , disponibles gratuitement et déjà <b>téléchargés à plus de 80 000 exemplaires</b>.</p><p>Pour ceux qui veulent mieux connaître l’open source, nous vous invitons en particulier à lire l’ouvrage intitulé <a href="/Ressources/Livres-blancs/Culture-du-web/Comprendre-l-open-source">&nbsp;<b>« Introduction à l’Open Source et au Logiciel Libre »</b>, qui présente ce mouvement sous ses différents aspects, historiques et philosophiques, licences, modèles économiques, modèles de support et de développement. </a>
                    </p><p>Certes, les concepteurs, les développeurs ou intégrateurs doivent bien gagner leur vie et l’open source n’est en général pas gratuit. Mais les solutions open source apportent souvent <b>un rapport qualité / prix très favorable</b>. C’est une des raisons de leur très rapide progression en entreprise. Ce n’est toutefois pas la seule. Les solutions open source, parce qu’elles sont diffusées à une très grande échelle, acquièrent rapidement une <b>grande robustesse</b>. Parce qu’elles fédèrent de larges communautés d’utilisateurs, elles disposent d’une <b>dynamique de développement</b> incomparable. Et parce qu’elles sont moins sujettes à des logiques purement commerciales, elles présentent <b>de meilleures assurances de pérennité</b>.</p><p>L’open source n’est pas uniquement de nature communautaire. On peut distinguer trois grandes familles de solutions open source : les produits de grandes fondations (Apache, Linux, Eclipse, Mozilla, …), les produits purement communautaires, et les produits d’éditeurs. L’open source communautaire est toujours aussi vivace, mais ces dernières années ont vu l’arrivée de nombreux nouveaux acteurs éditeurs open source, avec un business model le plus souvent basé sur le support. <a href="/Societe/Nos-partenaires">&nbsp;<b>Smile est partenaire d’un grand nombre de ces éditeurs</b> </a>
                        .</p><p>Chez Smile, <b>nous reversons régulièrement nos contributions</b> aux produits que nous intégrons et les développements financés par nos clients pour leurs besoins sont intégrés aux produits chaque fois qu’ils ont une utilité plus générale. Ainsi, le <a href="http://smile-ez-plugin.sourceforge.net/">plugin Eclipse réalisé par Smile est utilisé par les développeurs eZ Publish</a>
                        dans le monde entier. En 2008 notre client INA a financé d’importants enrichissements de l’outil de Business Intelligence Spago BI. Smile a apporté <b>de nombreuses extensions</b> précieuses aux outils Typo3 et eZ Publish. Et <b>Smile pilote également des projets open source spécifiques</b>, par exemple le <a href="http://code.google.com/p/magento-openerp-smile-synchro/">connecteur Magento-OpenERP</a>
                        , qui permet de coupler le leader des outil e-commerce open source avec l’un des meilleurs ERP, ou encore le <a href="http://sourceforge.net/projects/webassembletool">Web Assemble Tool, un outil performant à la fois agrégateur de contenus et gestionnaire de cache</a>
                        .</p><p>Smile est membre de l’<a href="http://www.april.org">APRIL, l'association pour la promotion et la défense du logiciel libre. </a>
                    </p><p>Smile est également membre de <a href="http://ploss.fr">PLOSS, l'association qui fédère les entreprises du Libre en Ile de France</a>
                        .</p>							</div>

            </div>

        </div>

    </div>



    <div class="breaker"></div>

@endsection