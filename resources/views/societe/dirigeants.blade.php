@extends('layouts.frontend')

@section('content')

    <div style="background: #aba291;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Nos-dirigeants.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <h1>Nos dirigeants</h1>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/societe" title="Société">Société</a>
                    <span class="separator"></span>
                    <span class="current_page">Nos dirigeants</span>
                </div>
                <a href="#" name="page_top" id="page_top"></a>

                <!--[if IE 6]><br class="clearall" /><![endif]-->

                <h2 class="secondary-title">Nos dirigeants</h2>

                <div class="rte_ctnt_block">

                    <p>Créé en 1991, Smile est détenu par ses Managers et les fonds d'investissement Keensight (majoritaire) et Edmond de Rothschild Investment Partners. Depuis janvier 2007, Marc Palazon dirige la société en tant que Président du directoire épaulé en cela par des managers talentueux spécialisés par offre, par&nbsp; région ou par pays. Tout le management de la société est tourné vers la croissance forte au service de ses clients et de l'Open Source.</p><h3>Comité Exécutif</h3>		</div>



                <div class="company_head">

                    <div class="company_head--item">





                        <img src="/img/Marc-Palazon_dirigeant_new.jpg"   width="196" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Marc Palazon</p>
                        <p class="company_head--item_job">Président du Directoire Groupe</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Badr-Chentouf_dirigeant_new.jpg"   width="196" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Badr Chentouf</p>
                        <p class="company_head--item_job">Directeur Général Innovation et Infrastructure</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Patrick-Bénichou_dirigeant_new.jpg"   width="195" height="264"  style="border: 0px;" alt="Patrick Bénichou - Directeur Général Adjoint - Smile" title="Patrick Bénichou - Directeur Général Adjoint - Smile" />



                        <p class="company_head--item_name">Patrick Bénichou</p>
                        <p class="company_head--item_job">Directeur Général Adjoint</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Emmanuel-Tornini_dirigeant_new.jpg"   width="196" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Emmanuel Tornini</p>
                        <p class="company_head--item_job">Directeur des Opérations</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Jérôme-Rideau_dirigeant_new.jpg"   width="195" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Jérôme Rideau</p>
                        <p class="company_head--item_job">Directeur Agence Smile Paris</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Jean-François-Bossard_dirigeant_new.jpg"   width="196" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Jean François Bossard</p>
                        <p class="company_head--item_job">Directeur Commercial Groupe</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Grégory-Becue_dirigeant_new.jpg"   width="196" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Grégory Becue</p>
                        <p class="company_head--item_job">Directeur Marketing &amp; Développement</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Lionel-Faugué_dirigeant_new.jpg"   width="195" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Lionel Faugué</p>
                        <p class="company_head--item_job">Directeur Agences Régionales</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Erwan-Kerzreho_dirigeant_new.jpg"   width="195" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Erwan Kerzreho</p>
                        <p class="company_head--item_job">Directeur des Centres d’Expertise France</p>
                    </div>

                    <div class="company_head--item">





                        <img src="/img/Luc-Fayet_dirigeant_new.jpg"   width="195" height="264"  style="border: 0px;" alt="" title="" />



                        <p class="company_head--item_name">Luc Fayet</p>
                        <p class="company_head--item_job">Directeur des Ressources Humaines</p>
                    </div>

                </div>

            </div>
        </div>


    </div>



    <div class="breaker"></div>
@endsection