@extends('layouts.frontend')

@section('content')

    <div style="background: #accbcc;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Produits.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">


        {{ Widget::RightTwitter() }}


        <div class="common_right_block">
            {{ Widget::RightPresse() }}

        </div>		            	            		    					    	                            </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>
            <div class="common_left_block">
                <div class="item-block__img">





                    <img src="/img/Smile-se-mobilise_encart_new.png"   width="84" height="35"  style="border: 0px;" alt="" title="" />




                </div>
                <div class="item-block__content">

                    <h5>Smile se mobilise</h5><p>sur la politique d'emploi handicap</p>            	            <a href="/Recrutement" title="Recrutement">&gt; Découvrir</a>

                </div>
            </div>



        </div>




        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <a href="/produits" title="Produits">Produits</a>
                    <span class="separator"></span>
                    <span class="current_page">{{$product->title}}</span>
                </div>
                <h2 class="secondary-title">{{$product->title}}</h2>

                <div class="product__presentation">
                    <div class="product__presentation--image">





                        <img src="{{$product->image}}"     style="border: 0px;" alt="HippoCMS" title="HippoCMS" />



                    </div>
                    <div class="product__presentation--quote">






                        <img src="{{$product->image}}"  class="float_left" width="100" height="100"  style="border: 0px;" alt="" title="" />



                        <p>{{$product->quote}}</p>	          	</div>
                </div>

                <form class="block_with_tabs">
                    <ul class="block_with_tabs--list clearfix">
                        <li class="active" data-id="offre">Notre offre</li>
                        <li data-id="fiche" class="">Fiche détaillée</li>
                        <li data-id="actus" class="">Actualités</li>
                        <li data-id="formations" class="">Formations</li>
                    </ul>
                    <div class="block_with_tabs--container">
                        <div class="active block_with_tabs--content with-border" data-id="offre" style="display: block;">
                            <div class="rte_ctnt_block">

                                <p>{{$product->notre_offre}}</p>
                            </div>
                        </div>
                        <div class="block_with_tabs--content with-border" data-id="fiche" style="display: none;">
                            <div class="rte_ctnt_block">
                                <p>{{$product->fiche_detaillee}}</p>
                            </div>
                        </div>
                        <div class="block_with_tabs--content with-border" data-id="actus" style="display: none;">
                            <div class="news-container__list clearfix">
                                <div class="news-container__item">
                                    <div class="news-container__item__date">
                                        20 mai 2015
                                    </div>
                                    <h3 class="news-container__item__title">
                                        <a title="Hippo CMS 10 en mode plateforme de performance des contenus" href="http://www.open-source-guide.com/Actualites/Hippo-cms-10-en-mode-plateforme-de-performance-des-contenus" target="_blank">Hippo CMS 10 en mode plateforme de performance des contenus</a>
                                    </h3>
                                    Hippo, l’éditeur du gestionnaire de contenu éponyme a annoncé hier le démarrage de sa solution qualifiée de plateforme de performance des contenus avec la sortie du produit Hippo CMS 10, fournissant des fonctionnalités avancées pour les services marketing e...
                                    <div class="news-container__item__actions">
                                        <a class="more_about" href="http://www.open-source-guide.com/Actualites/Hippo-cms-10-en-mode-plateforme-de-performance-des-contenus" target="_blank">Lire la suite</a>
                                        <div class="news-container__item__actions__follow">
                                            <span class='st_sharethis' st_url=http://www.open-source-guide.com/Actualites/Hippo-cms-10-en-mode-plateforme-de-performance-des-contenus displayText="Partager"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="block_with_tabs--content with-border" data-id="formations" style="display: none;">
                            <div class="news-container__list formations-container__list clearfix">
                                Il n’y a pas de formation disponible pour le moment. Intéressé par une formation ?
                                <a target="_blank" href="/contact" class="more_about">Contactez smile training</a>
                            </div>
                        </div>
                    </div>
                </form>

                <a class="reference__last__all" href="/references">> Voir toutes nos références</a>
            </div>
        </div>



    </div>



    <div class="breaker"></div>


@endsection