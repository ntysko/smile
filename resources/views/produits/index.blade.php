@extends('layouts.frontend')

@section('content')

    <div style="background: #accbcc;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Produits.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">


        {{ Widget::RightTwitter() }}


        <div class="common_right_block">
            {{ Widget::RightPresse() }}

        </div>		            	            		    					    	                            </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>
            <div class="common_left_block">
                <div class="item-block__img">





                    <img src="/img/Smile-se-mobilise_encart_new.png"   width="84" height="35"  style="border: 0px;" alt="" title="" />




                </div>
                <div class="item-block__content">

                    <h5>Smile se mobilise</h5><p>sur la politique d'emploi handicap</p>            	            <a href="/Recrutement" title="Recrutement">&gt; Découvrir</a>

                </div>
            </div>



        </div>




        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Produits</span>
                </div>
                <div class="produits__content">
                    <h2 class="secondary-title">Le choix des meilleurs outils</h2>

                    <p>Quel que soit le domaine d'application, Smile s'attache à maîtriser non pas un outil unique, mais une sélection des meilleurs outils offerts par l'open source. Une puissante action de veille technologique permet de découvrir, puis de valider les outils qui s'imposeront demain.</p>            <div class="produits__mosaic" id="filter_results">


                        <div class="produits__mosaic__filters clearfix" id="sortbar">
                            <p class="produits__mosaic__filters__title">Afficher par :</p>
                            <a title="Tous" class="filter__all" id="filter__all_p" href="/Produits">Tous</a>

                            <div class="filter-container">
                                <span class="filter__category">Business Lines</span>
                                <ul class="filter__category__list" id="bl">
                                    <li>
                                        <input type="checkbox" checked="checked" class="allValue" name="filter-bl" value="all">
                                        <label>Toutes les Business Lines</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="filter-bl" value="31010"
                                        >
                                        <label>E-business</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="filter-bl" value="31215"
                                        >
                                        <label>Web</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="filter-bl" value="31330"
                                        >
                                        <label>SI-Métier</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="filter-bl" value="31445"
                                        >
                                        <label>Infrastructure</label>
                                    </li>
                                </ul>
                            </div>


                        </div>

                        <input type="hidden" id="searchNodeUrl" value="/Produits/Recherche-avance" />
                        <div class="mosaic--4-col clearfix">
                            <div class="produits-row">
                                @foreach($products as $product)
                                    <div class="mosaic__dalle">
                                        <a href="{{ url('/produits/produits_one', $product->id) }}" title="Voir le produit">
                                            <div class="flip-card">
                                                <div class="flip-card__container">
                                                    <div class="flip-card__face">
                                                        <img src="{{$product->image}}"   width="40%"  style="border: 0px;" alt="" title="" />
                                                    </div>
                                                    <div class="flip-card__pile">
                                                        <h3>{{$product->title}}</h3>
                                                        <p class="savoir-plus">En savoir plus</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>


@endsection