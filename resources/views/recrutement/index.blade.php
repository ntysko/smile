@extends('layouts.frontend')

@section('content')

    <div style="background: #d8bd92;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Recrutement.jpg>
            </div>
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">
                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Recrutement</span>
                </div>
                <div class="rte_ctnt_block carriere-home__content">
                    <h2 class="secondary-title">Devenez  un super-smilien !</h2>

                    <p>Vous recherchez un job passionnant et vous aimez relever des défis ?</p><p><b>Rejoignez-nous !</b></p><p>Découvrez toutes <b><a href="http://jobs.smile.eu/offres" target="_blank">nos offres d'emploi et de stages</a>
                        </b> ! Et entrez dans l'univers de <b><a href="http://jobs.smile.eu/notre-equipe" target="_blank">nos 18 agences</a>
                        </b> Smile !</p>




                    <a href="http://jobs.smile.eu/" target="_blank">
                        <img src="/img/btn-recrutement-smile-3.png"   width="320" height="130"  style="border: 0px;" alt="" title="" /></a>



                </div>
            </div>
        </div>

    </div>


    <div class="breaker"></div>
    <div class="lame-full-width lame-smile-3-chiffres animated">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <h2 class="lame-smile-3-chiffres__title">
                    <span class="lame-smile-3-chiffres__title__primary">Smile</span>
                    <span class="lame-smile-3-chiffres__title__secondary">en 3 chiffres</span>
                </h2>
                <div class="lame-smile-3-chiffres__item lame-smile-3-chiffres__item--cac">
                    <div class="lame-smile-3-chiffres__visual animated">
                        <div class="cake-container">
                            <div class="cake-no-progress">

                            </div>
                            <div class="cake-progress">
                                <div class="cake-progress__fill__1"></div>
                                <div class="cake-progress__fill__2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="lame-smile-3-chiffres__caption">
	                    <span class="lame-smile-3-chiffres__caption__number">
	                        70<span class="exposant">%</span>
	                    </span>
	                    <span class="lame-smile-3-chiffres__caption__text">
	                       du cac 40 nous fait confiance
	                    </span>
                    </div>
                </div>
                <div class="lame-smile-3-chiffres__item lame-smile-3-chiffres__item--croissance">
                    <div class="lame-smile-3-chiffres__visual animated">
                        <div class="bar bar--1"></div>
                        <div class="bar bar--2"></div>
                        <div class="bar bar--3"></div>
                    </div>
                    <div class="lame-smile-3-chiffres__caption">
	                    <span class="lame-smile-3-chiffres__caption__number">
	                        22<span class="exposant">%</span>
	                    </span>
	                    <span class="lame-smile-3-chiffres__caption__text">
	                        de croissance par an depuis 5 ans
	                    </span>
                    </div>
                </div>
                <div class="lame-smile-3-chiffres__item lame-smile-3-chiffres__item--agence">
                    <div class="lame-smile-3-chiffres__visual animated">

                    </div>
                    <div class="lame-smile-3-chiffres__caption">
	                    <span class="lame-smile-3-chiffres__caption__number">
	                        18
	                    </span>
	                    <span class="lame-smile-3-chiffres__caption__text">
	                        <span class="highlight">agences</span> en Europe
	                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breaker"></div>


@endsection