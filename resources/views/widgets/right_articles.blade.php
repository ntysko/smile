<span class="title-news-block">Nos actualités</span>

@foreach($actualitesRight as $actualite)

    <a class="item_block" href="{{url('/societe/one_presse', $actualite->id)}}" title="{{$actualite->title}}">
        <span class="item_date">{{date('d/m/y',strtotime($actualite->date))}}</span>
        <span class="item_title">{{$actualite->title}}</span>
        <p class="item_summary">p {{strip_tags(str_limit($actualite->text, $limit = 350, $end = '...'))}}</p>
    </a>

@endforeach