

<ul class="lv_01_menu">

    <li class="first_item"><a href="/societe" title="Société">Société</a>
        @if ($action=='/societe')
        <ul class="lv_02_menu societe-ul" >

            <li>

                <a href="/societe/actualites" title="Nos actualités">Nos actualités</a>

            </li>

            <li>

                <a href="/societe/agences" title="Nos agences">Nos agences</a>

            </li>

            <li>

                <a href="/societe/vision" title="Notre vision">Notre vision</a>

            </li>

            <li>

                <a href="/societe/histoire" title="Notre histoire">Notre histoire</a>

            </li>

            <li>

                <a href="/societe/dirigeants" title="Nos dirigeants">Nos dirigeants</a>

            </li>

            <li>

                <a href="/societe/chiffres" title="Nos chiffres clés">Nos chiffres clés</a>

            </li>

            <li>

                <a href="/societe/partenaires" title="Nos partenaires">Nos partenaires</a>

            </li>

            <li>

                <a href="/societe/qualite" title="Démarche qualité">Démarche qualité</a>

            </li>

            <li>

                <a href="/societe/communiques" title="Nos communiqués">Nos communiqués</a>

            </li>

            <li>

                <a href="/societe/presse" title="Smile dans la presse">Smile dans la presse</a>

            </li>
        </ul>
        @endif

    </li>

    <li><a href="/offresservices" title="Offres &amp; Services">Offres &amp; Services</a>

        @if ($action=='/offresservices')
        <ul class="lv_02_menu offresservices-ul" >

            <li>

                <a href="/offresservices/offres" title="Offres">Offres</a>

            </li>

            <li>

                <a href="/offresservices/services" title="Services">Services</a>

            </li>
        </ul>
        @endif
    </li>

    <li><a href="/produits" title="Produits">Produits</a>

    </li>

    <li><a href="/evenements" title="Événements">Événements</a>

    </li>

    <li><a href="/references" title="Références">Références</a>
        @if ($action=='/references')
        <ul class="lv_02_menu references-ul" >


            <li>

                <a href="/references/clients" title="Témoignages clients">Témoignages clients</a>

            </li>
        </ul>
        @endif
    </li>

    <li><a href="/recrutement" title="Recrutement">Recrutement</a>

    </li>

    <li><a href="/ressources" title="Ressources">Ressources</a>
        @if ($action=='/ressources')
        <ul class="lv_02_menu ressources-ul" >

            <li>

                <a href="/ressources/livres" title="Livres blancs">Livres blancs</a>

            </li>

            <li>

                <a href="/ressources/videos" title="Vidéos">Vidéos</a>

            </li>

            <li>

                <a href="/ressources/presentations" title="Présentations">Présentations</a>

            </li>
        </ul>
        @endif
    </li>

    <li><a href="/smile_lab" title="Smile Lab">Smile Lab</a>
        @if ($action=='/smile_lab')
        <ul class="lv_02_menu smile_lab-ul" >

            <li>

                <a href="/smile_lab/mongogento" title="Mongogento">Mongogento</a>

            </li>

            <li>

                <a href="/smile_lab/elasticsuite" title="ElasticSuite">ElasticSuite</a>

            </li>

            <li>

                <a href="/smile_lab/smilecloudng" title="SmileCloudNG">SmileCloudNG</a>

            </li>

            <li>

                <a href="/smile_lab/occiware" title="OCCIWARE">OCCIWARE</a>

            </li>
        </ul>
            @endif
    </li>
</ul>