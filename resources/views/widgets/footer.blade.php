<section class="homeContentLayer lame-home-bottom lame-full-width">


    <div class="lame-full-width__container">
        <div class="lame-full-width__content">
            <div class="yui3-g">
                <div class="yui3-u-1-4">
                    <section id="pressReleases">
                        <div class="block-title">
                            <h2>Smile dans la presse</h2>

                <span>
                    <h3>
                        <a href="/societe/presse" title="Voir tous les articles de presse">Tous nos articles</a>
                    </h3>
                            </span>
                        </div>

                        <ul class="newsList">
                            @foreach($presses as $presse)
                            <li>
                                <p class="newsList-title">
                                    <a href="{{ url('/societe/one_presse', $presse->id) }}" title="{{$presse->title}}">
                                        <time datetime="16/06/06">{{$presse->date}}</time>&nbsp; {{$presse->title}} </a>
        <span class="item_date">
                    Article paru dans {{$presse->source}} {{$presse->date}}
          </span>
                                </p>
                                <p>
                                   {{strip_tags(str_limit($presse->text, $limit = 250, $end = '...'))}}
                                    <a class="moreInfo" title="{{$presse->title}}" href="{{ url('/societe/one_presse', $presse->id) }}"></a>
                                </p>
                            </li>
                            @endforeach
                       </ul>

                    </section>										    			    </div>
                <div class="yui3-u-1-4">
                    <section id="recentReferences">

                        <div class="block-title">
                            <h2>Références récentes</h2>

                            <h3><a href="/references" title="/References">TOUTES NOS RÉFÉRENCES</a></h3>
                        </div>
                        <ul class="newsList">
                            @foreach($references as $reference)
                            <li>
                                <p class="newsList-title">
                                    <a href="{{ url('/references/one_reference', $reference->id) }}">
                                       {{$reference->title}}		                </a>
                                </p>





                                <img src="{{$reference->image}}"   width="62" height="62"  style="border: 0px;" alt="" title="" />



                                <p>
                                    {{strip_tags(str_limit($reference->comment, $limit = 250, $end = '...'))}}
                                    <a class="moreInfo" title="{{$reference->title}}'" href="{{ url('/references/one_reference', $reference->id) }}"></a>
                                </p>
                            </li>
                            @endforeach
                        </ul>
                    </section>				        									</div>
                <div class="yui3-u-1-4">
                    <section id="expertBlog">
                        <div class="block-title">
                            <h2>Le blog des experts</h2>
                            <h3>
                                <a href="http://blog.smile.fr/" title="Accéder au blog Smile" target="_blank">TOUS NOS BILLETS</a>
                            </h3>
                        </div>
                        <ul class="newsList">
                            <li>
                                <p class="newsList-title">
                                    <a href="http://blog.smile.fr/Integrer-des-donnees-d-iot-en-temps-reel-avec-talend-real-time-big-data" title="Intégrer des données d'IoT en temps réel avec Talend Real-Time Big Data" target="_blank">
                                        <time datetime=16-07-20>20/07/2016</time>
                                        Intégrer des données d'IoT en temps réel avec Talend Real-Time Big Data			                </a>
                                </p>
                                <p>
                                    Une solution pour vous faciliter l'intégration de données IoT.
                                    <a href="http://blog.smile.fr/Integrer-des-donnees-d-iot-en-temps-reel-avec-talend-real-time-big-data" title="En savoir plus sur 'Intégrer des données d'IoT en temps réel avec Talend Real-Time Big Data'" class="moreInfo" target="_blank"></a>
                                </p>
                            </li>
                            <li>
                                <p class="newsList-title">
                                    <a href="http://blog.smile.fr/Webmecanik-met-l-inbound-marketing-au-service-du-marketing-automation" title="Webmecanik met l’inbound marketing au service du Marketing automation" target="_blank">
                                        <time datetime=16-07-18>18/07/2016</time>
                                        Webmecanik met l’inbound marketing au service du Marketing automation			                </a>
                                </p>
                                <p>
                                    Focus sur la solution Webmecanik pour automatiser vos process marketing.
                                    <a href="http://blog.smile.fr/Webmecanik-met-l-inbound-marketing-au-service-du-marketing-automation" title="En savoir plus sur 'Webmecanik met l’inbound marketing au service du Marketing automation'" class="moreInfo" target="_blank"></a>
                                </p>
                            </li>
                            <li>
                                <p class="newsList-title">
                                    <a href="http://blog.smile.fr/Fevad-les-enjeux-du-e-commerce-2016" title="FEVAD : Les enjeux du e-commerce 2016" target="_blank">
                                        <time datetime=16-07-07>07/07/2016</time>
                                        FEVAD : Les enjeux du e-commerce 2016			                </a>
                                </p>
                                <p>
                                    Le 14 juin dernier, Smile était présent à la conférence organisée par la FEVAD « Les enjeux du e-commerce 2016 ».
                                    <a href="http://blog.smile.fr/Fevad-les-enjeux-du-e-commerce-2016" title="En savoir plus sur 'FEVAD : Les enjeux du e-commerce 2016'" class="moreInfo" target="_blank"></a>
                                </p>
                            </li>
                        </ul>
                    </section>				        				    			    </div>
                <aside class="yui3-u-1-4">
                    <section id="technologyWatch">
                        <div class="block-title">
                            <h2>Actualités open-source-guide.com</h2>
                            <h3>
                                <a href="http://www.open-source-guide.com/Actualites" title="Voir tout" target="_blank">Voir tout</a>
                            </h3>
                        </div>
                        <ul class="newsList">
                            <li>
                                <p class="newsList-title">
                                    <a href="http://www.open-source-guide.com/Actualites/Talend-s-offre-une-belle-introduction-au-nasdaq" target="_blank">
                                        <time datetime="16-08-02">02/08/2016</time>
                                        Talend s’offre une belle introduction au Nasdaq
                                    </a>
                                </p>
                                <p>
                                    Le titre de l’éditeur français de la solution open source éponyme flambe dans les premiers échanges.		            			                <a href="http://www.open-source-guide.com/Actualites/Talend-s-offre-une-belle-introduction-au-nasdaq" title="En savoir plus sur Talend s’offre une belle introduction au Nasdaq" class="moreInfo" target="_blank"></a>
                                </p>
                            </li>
                            <li>
                                <p class="newsList-title">
                                    <a href="http://www.open-source-guide.com/Actualites/Spark-2-0-pour-une-approche-tout-en-un-du-big-data" target="_blank">
                                        <time datetime="16-07-29">29/07/2016</time>
                                        Spark 2.0 pour une approche tout-en-un du Big Data
                                    </a>
                                </p>
                                <p>
                                    Avec un nouveau système de streaming, des améliorations de performance et le perfectionnement des interfaces de programmation, la solution libre de calcul « en mémoire » Apache Spark 2.0 offre de nouvelles perspectives aux utilisateurs de Big Data.		            			                <a href="http://www.open-source-guide.com/Actualites/Spark-2-0-pour-une-approche-tout-en-un-du-big-data" title="En savoir plus sur Spark 2.0 pour une approche tout-en-un du Big Data" class="moreInfo" target="_blank"></a>
                                </p>
                            </li>
                            <li>
                                <p class="newsList-title">
                                    <a href="http://www.open-source-guide.com/Actualites/Un-nouveau-convertisseur-pour-beaglebone" target="_blank">
                                        <time datetime="16-07-28">28/07/2016</time>
                                        Un nouveau convertisseur pour BeagleBone
                                    </a>
                                </p>
                                <p>
                                    Les amateurs d’électroniques embarqués qui souhaitent réaliser des projets avec les cartes de développement BeagleBone accueilleront avec plaisir la nouvelle carte open source permettant la conversion analogique/numérique.		            			                <a href="http://www.open-source-guide.com/Actualites/Un-nouveau-convertisseur-pour-beaglebone" title="En savoir plus sur Un nouveau convertisseur pour BeagleBone" class="moreInfo" target="_blank"></a>
                                </p>
                            </li>
                        </ul>
                    </section>					    									</aside>
            </div>
        </div>
    </div>
</section>