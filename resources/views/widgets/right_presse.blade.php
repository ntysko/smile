<h2>Dans la presse</h2>


@foreach($presseRight as $press)

<a class="item_block" href="{{url('/societe/one_presse', $press->id)}}" title="{{$press->title}}">
    <span class="item_date">{{date('d/m/y',strtotime($press->date))}}</span>
    <span class="item_title">{{$press->title}}</span>
    <p class="item_summary">p {{strip_tags(str_limit($press->text, $limit = 350, $end = '...'))}}</p>
</a>

@endforeach