@extends('layouts.frontend')

@section('content')
  

    <section id="promotions">
        <div class="active lame-full-width" style="background-color: #00367c">
            <div class="lame-full-width__container">
                <div class="lame-full-width__content"
                     style="background-image:url('img/Livre-Blanc-Linux-pour-lembarque.jpg')">
                    <div class="promo-title" style="color:#ffffff">
                        Linux pour l'embarqué
                    </div>
                    <p style="color:#ffffff">
                        Spécial systèmes industriels
                    </p>
                    <a href="/Ressources/Livres-blancs/Ingenierie/Linux-pour-l-embarque" class="button">Télécharger
                        gratuitement !</a>
                </div>
            </div>
        </div>
        <div class="active lame-full-width" style="background-color: #204987">
            <div class="lame-full-width__container">
                <div class="lame-full-width__content"
                     style="background-image:url('img/Smile-décroche-le-label-HappyAtWork-for-Starters-2016.jpg')">
                    <div class="promo-title" style="color:#ffffff">
                        Smile décroche le label HappyAtWork
                    </div>
                    <p style="color:#ffffff">
                        Décerné par les Echos Start & meilleures-entreprises.com
                    </p>
                    <a href="/" class="button">En savoir +</a>
                </div>
            </div>
        </div>
        <div class="active lame-full-width" style="background-color: #4394a2">
            <div class="lame-full-width__container">
                <div class="lame-full-width__content"
                     style="background-image:url('img/Vidéo-de-présentation-de-Smile-2016.jpg')">
                    <div class="promo-title" style="color:#ffffff">
                        Notre univers en vidéo
                    </div>
                    <p style="color:#ffffff">
                        Notre ADN, nos convictions, et plus encore...
                    </p>
                    <a href="https://www.youtube.com/watch?v=87xIQQQawag" class="button">Voir la vidéo</a>
                </div>
            </div>
        </div>
        <div class="active lame-full-width" style="background-color: #64feca">
            <div class="lame-full-width__container">
                <div class="lame-full-width__content" style="background-image:url('img/OSS.jpg')">
                    <div class="promo-title" style="color:#213b56">
                        Smile et l'EPSI vous présentent
                    </div>
                    <p style="color:#213b56">
                        La 1ère École du logiciel libre et des solutions open source
                    </p>
                    <a href="" class="button">Découvrir
                        l'OSS</a>
                </div>
            </div>
        </div>
        <div class="active lame-full-width" style="background-color: #093041">
            <div class="lame-full-width__container">
                <div class="lame-full-width__content" style="background-image:url('img/Livre-Blanc-Drupal-8.jpg')">
                    <div class="promo-title" style="color:#ffffff">
                        Livre Blanc Drupal 8
                    </div>
                    <p style="color:#ffffff">
                        Décryptage de la nouvelle version
                    </p>
                    <a href="" class="button">Téléchargez
                        le guide !</a>
                </div>
            </div>
        </div>
    </section>


    <section class="yui3-g homeContentLayer homeContentLayer--services">

        <div class="yui3-u-1-2 homeContentLayer--services__main">
            <section id="ourOffersServices">
                <div class="block-title offers__block-title">
                    <a href="/offresservices" title="Offres &amp; Services">
                        <h2 class="main-title">
                            Offres et Services </h2>
                    </a>

                    <p class=" text-left">Smile est votre partenaire open source global. Nos offres et services
                        reflètent la diversité de nos métiers. Ils sont pour vous les garants d’une compréhension
                        globale de vos besoins.</p>
                    <div class="more">
                        <h3>
                            <a href="/offresservices" title="Offres &amp; Services">
                                > Toutes nos offres et services
                            </a>
                        </h3>
                    </div>
                </div>
                <div class="offers__mosaic cf">
            	            		                		<a href="/offresservices" alt="E-Business">
    <div class="offers__mosaic__e-business offers__mosaic__block-l">
        <div class="flip-card">
            <div class="flip-card__container">
                <div class="flip-card__face">
                    <p class="offre-title">E-Business</p>
                    <span class="offers__mosaic__btn offers__mosaic__e-business__btn">Découvrir</span>
                </div>
                <div class="flip-card__pile">
                    <p class="offre-title">E-Business</p>
                                            <strong>
                        	Développez votre business online                    	</strong>
                	                	                		<p class="italic">Magento, Drupal Commerce ...</p>
            		                </div>
            </div>
        </div>
    </div>
</a>            	            		                	    <a href="/offresservices/" alt="Ingénierie">
    <div class="offers__mosaic__ingenierie offers__mosaic__block-l">
        <div class="flip-card">
            <div class="flip-card__container">
                <div class="flip-card__face">
                    <p class="offre-title">Ingénierie</p>
                    <span class="offers__mosaic__btn offers__mosaic__ingenierie__btn">Découvrir</span>
                </div>
                <div class="flip-card__pile">
                    <p class="offre-title">Ingénierie</p>
                                            <strong>
                        	Concevez les meilleurs systèmes embarqués                    	</strong>
                	                	                		<p class="italic">Linux embarqué, Buildroot, Yocto...</p>
            		                </div>
            </div>
        </div>
    </div>
</a>            	            		                		<a href="/offresservices" alt="Digital">
    <div class="offers__mosaic__web offers__mosaic__block-l">
        <div class="flip-card">
            <div class="flip-card__container">
                <div class="flip-card__face">
                    <p class="offre-title">Digital</p>
                    <span class="offers__mosaic__btn offers__mosaic__web__btn">Découvrir</span>
                </div>
                <div class="flip-card__pile">
                    <p class="offre-title">Digital</p>
                                            <strong>
                        	Réussir la digitalisation de votre entreprise                    	</strong>
                	                	                		<p class="italic">Drupal, eZ publish, TYPO3 ...</p>
            		                </div>
            </div>
        </div>
    </div>
</a>            	            		<a href="/offresservices" alt="Systèmes d’information" id="systeme_information">
    <div class="offers__mosaic__si offers__mosaic__block-s">
        <div class="flip-card">
            <div class="flip-card__container">
                <div class="flip-card__face">
                    <p class="offre-title">Systèmes d’information</p>
                </div>
                <div class="flip-card__pile">
                    <p class="offre-title">Systèmes d’information</p>
                                            <strong>
                        	Pilotez efficacement votre activité                    	</strong>
                	                	                </div>
            </div>
        </div>
    </div>
</a>            	                <div class="offers__mosaic__hosting-support offers__mosaic__block-s">
                	        				        			<a href="/offresservices" alt="Hosting">
    <div class="offers__mosaic__hosting">
        <p class="service-title">Hosting</p>
              	        	<span class="offers__mosaic__btn">+ d'infos</span>
    	    </div>
</a>                    	            			            				        				        	
		<a href="/offresservices" alt="Support">
    <div class="offers__mosaic__support">
        <p class="service-title">Support</p>
              	        	<span class="offers__mosaic__btn">+ d'infos</span>
    	    </div>
</a>                                    </div>
                            		
	<a href="/offresservices" alt="Infrastructure">
    <div class="offers__mosaic__infrastructure offers__mosaic__block-s">
        <div class="flip-card">
            <div class="flip-card__container">
                <div class="flip-card__face">
                    <p class="offre-title">Infrastructure</p>
                </div>
                <div class="flip-card__pile">
                    <p class="offre-title">Infrastructure</p>
                                            <strong>
                        	Optimisez les performances de votre infrastructure système                    	</strong>
                	                	                </div>
            </div>
        </div>
    </div>
</a>            	            	            		            			        			        			<a href="/offresservices/Services/Tma" alt="TMA">
    <div class="offers__mosaic__tma offers__mosaic__block-l">
        <p class="service-title">TMA</p>
                  	
<p>Assurez la pérennité de vos applicatifs.</p>      	      	        	<span class="offers__mosaic__btn">+ d'infos</span>
    	    </div>
</a>                                <div class="offers__mosaic__consulting-formation offers__mosaic__block-l">
	                	            			            				            			        			<a href="/offresservices/Services/Consulting" alt="Consulting">
    <div class="offers__mosaic__consulting">
        <p class="service-title">Consulting</p>
                  	
<p>Innover et créer de la valeur pour vos projets.</p>      	      	    </div>
</a>	                	            			            				        				        			<a href="/offresservices/Services/Formation" alt="Formation">
    <div class="offers__mosaic__formation">
        <p class="service-title">Formation</p>
                  	
<p>Formez-vous avec le leader de l’open source.</p>      	      	    </div>
</a>	                                </div>
                            		        				        			        			
                            		        				        			        			
                            		        		<a href="/offresservices" alt="Smile Digital">
    <div class="offers__mosaic__digital offers__mosaic__block-l">
        <p class="service-title">Smile Digital</p>
                  	
<p>Imaginer et concevoir de nouvelles expériences digitales.</p>      	      	        	<span class="offers__mosaic__btn">+ d'infos</span>
    	    </div>
</a>                            </div>
            </section>
        </div>

        <aside class="yui3-u-1-2 homeContentLayer--services__aside">
            <div class="yui3-g">

                <div class="yui3-u-1-2 home-rdv-block">
                    <section id="rendezVous">
                        <h2>
                            <a href="/evenements" title="Événements">
                                <span>Les rendez-vous</span>
                                <span>de l'open source</span>
                            </a>
                        </h2>
                        <ul>
                            <li itemscope href="/Blocs-communs/Evenements-home/Smile-sponsor-gold-de-jahiax">
                                <time datetime=16-11-07>
                                    <a href="/evenements" class="date_event">
                                        <span>07</span>
                                        nov
                                    </a>
                                </time>
                                <div class="infos_event">
                                    <span class="evenements__list__item__info-style salon">Salon</span>	    		    <span
                                            itemprop="location" itemscope itemtype="http://schema.org/PostalAddress">
		        <span itemprop="addressLocality">Paris</span>
		    </span>
                                    <a class="event_desc" href="/evenements"
                                       itemprop=description>
                                        Smile est partenaire Gold de la JahiaX </a>
                                </div>
                            </li>
                            <li itemscope href="/Blocs-communs/Evenements-home/Evenement-blendwebmix-2016">
                                <time datetime=16-11-02>
                                    <a href="/evenements" class="date_event">
                                        <span>02</span>
                                        nov
                                    </a>
                                </time>
                                <div class="infos_event">
                                    <span class="evenements__list__item__info-style evenement">Evénement</span>	    		    <span
                                            itemprop="location" itemscope itemtype="http://schema.org/PostalAddress">
		        <span itemprop="addressLocality">Lyon</span>
		    </span>
                                    <a class="event_desc" href="/evenements"
                                       itemprop=description>
                                        Smile partenaire du BlendWebMix 2016 </a>
                                </div>
                            </li>
                            <li itemscope href="/Blocs-communs/Evenements-home/Smile-gold-sponsor-drupalcon-2016">
                                <time datetime=16-09-26>
                                    <a href="/evenements" class="date_event">
                                        <span>26</span>
                                        sept
                                    </a>
                                </time>
                                <div class="infos_event">
                                    <span class="evenements__list__item__info-style salon">Salon</span>	    		    <span
                                            itemprop="location" itemscope itemtype="http://schema.org/PostalAddress">
		        <span itemprop="addressLocality">Dublin</span>
		    </span>
                                    <a class="event_desc" href="/evenements"
                                       itemprop=description>
                                        Smile est Gold sponsor de la DrupalCon 2016 </a>
                                </div>
                            </li>
                            <li itemscope
                                href="/Blocs-communs/Evenements-home/Rencontres-regionales-du-logiciel-libre-nantes-2016">
                                <time datetime=16-09-20>
                                    <a href="/evenements"
                                       class="date_event">
                                        <span>20</span>
                                        sept
                                    </a>
                                </time>
                                <div class="infos_event">
                                    <span class="evenements__list__item__info-style salon">Salon</span>	    		    <span
                                            itemprop="location" itemscope itemtype="http://schema.org/PostalAddress">
		        <span itemprop="addressLocality">Nantes</span>
		    </span>
                                    <a class="event_desc"
                                       href="/evenements"
                                       itemprop=description>
                                        Smile est sponsor des Rencontres Régionales du Logiciel Libre. </a>
                                </div>
                            </li>
                        </ul>
                        <div>
                            <h3>
                                <a href="/evenements" title="Événements">Tous nos événéments</a>
                            </h3>
                        </div>
                    </section>
                </div>


                <div class="yui3-u-1-2 home-news-block">
                    <section id="ourNews">
                        <div class="block-title news__block-title">
                            <a title="Nos actualités" href="/Societe/Nos-actualites">
                                <h2 class="main-title">Nos actualités</h2>
                            </a>
                        </div>
                        <ul class="newsList">
                            <li>
                                <div class="ourNews-title">
                                    <time datetime=16-06-09>09/06/2016</time>
                                    <a class="news_title"
                                       href="/Societe/Nos-actualites/Eram-booste-ses-ventes-en-ligne-avec-smile">
                                        Eram booste ses ventes en ligne avec Smile </a>
                                </div>
                                <p>
                                    <a href="/Societe/Nos-actualites/Eram-booste-ses-ventes-en-ligne-avec-smile"
                                       title="Eram booste ses ventes en ligne avec Smile">
                                        Smile accompagne l’enseigne Eram (leader de la chaussure en centre-ville et
                                        centres commerciaux) dans la mise en œuvre d’un site responsive design de
                                        nouvelle génération lui permettant de déployer à grande échelle sa stratégie de
                                        vente en ligne. Découvrez le témoignage !
                                    </a>
                                </p>
                            </li>
                            <li>
                                <div class="ourNews-title">
                                    <time datetime=16-06-02>02/06/2016</time>
                                    <a class="news_title"
                                       href="/Societe/Nos-actualites/Nouveau-livre-blanc-smile-linux-pour-l-embarque">
                                        Nouveau livre blanc Smile : Linux pour l’embarqué </a>
                                </div>
                                <p>
                                    <a href="/Societe/Nos-actualites/Nouveau-livre-blanc-smile-linux-pour-l-embarque"
                                       title="Nouveau livre blanc Smile : Linux pour l’embarqué">
                                        Smile inaugure une nouvelle collection de livre blanc sur le thème de
                                        l'embarqué. Le premier de la série est consacré à l'utilisation de Linux
                                        embarqué dans les systèmes industriels.
                                    </a>
                                </p>
                            </li>
                            <li>
                                <div class="ourNews-title">
                                    <time datetime=16-05-31>31/05/2016</time>
                                    <a class="news_title" href="/Societe/Nos-actualites/Webinars-smile-printemps-2016">
                                        Webinars Smile Printemps 2016 </a>
                                </div>
                                <p>
                                    <a href="/Societe/Nos-actualites/Webinars-smile-printemps-2016"
                                       title="Webinars Smile Printemps 2016">
                                        Le printemps de Smile c'est aussi le temps des webinars. Pour tout savoir sur
                                        les solutions Open Source, retrouvez notre programme complet en mai &amp; juin
                                        2016.
                                    </a>
                                </p>
                            </li>
                        </ul>
                        <div>
                            <h3>
                                <a href="/Societe/Nos-actualites" title="Nos actualités">TOUTES NOS ACTUALITÉS </a>
                            </h3>
                        </div>
                    </section>
                </div>

            </div>
            <div class="home-find-agency clearfix">
                <a href="http://www.smile.fr/Societe/Nos-agences">
                    <h2><img src="img/find-agencies-text.png" alt="17 Agences Smile dans 8 pays"
                             class="home-find-agency__text"/></h2>
                    <img src="img/find-agencies-map.jpg" alt="17 Agences Smile dans 8 pays"
                         class="home-find-agency__map"/>
                </a>
            </div>
        </aside>
    </section>
    <section class="yui3-g homeContentLayer homeContentLayer--papers">

        <div class="yui3-u-3-4">
            <section id="ourWhitePapers">
                <div class="block-title papers__block-title">
                    <a href="/Ressources/Livres-blancs">
                        <h2 class="main-title">Nos livres blancs</h2>
                    </a>
                </div>
                <div class="book-item">
                    <div>


                        <img src="img/DevOps-et-industrialisation_livreBlanc_big.jpg" width="145" height="207"
                             style="border: 0px;" alt="Couverture livre blanc DevOps et industrialisation"
                             title="Couverture livre blanc DevOps et industrialisation"/>


                    </div>
                    <div>
                        <strong>Nouveau</strong>
                        <p class="book-item-title">
                            DevOps et industrialisation </p>
                        <p>
                            15 solutions open source décryptées ! </p>
                        <a href="/Ressources/Livres-blancs/Systeme-et-infrastructure/Devops-et-industrialisation"
                           class="button">Télécharger gratuitement</a>
                    </div>
                </div>
                <ul>
                    <li>


                        <img src="img/ERP-e-commerce_livreBlanc_small.jpg" width="75" height="107" style="border: 0px;"
                             alt="" title=""/>


                        <div>
                            <p class="ourWhitePapers-title">ERP e-commerce</p>
                            <a href="/Ressources/Livres-blancs/Erp-et-decisionnel/Erp-e-commerce">Télécharger</a>
                        </div>
                    </li>
                    <li>


                        <img src="img/Guide-de-l'open-source_livreBlanc_small.png" width="75" height="107"
                             style="border: 0px;" alt="" title=""/>


                        <div>
                            <p class="ourWhitePapers-title">Guide de l'open source</p>
                            <a href="/Ressources/Livres-blancs/Culture-du-web/Guide-de-l-open-source">Télécharger</a>
                        </div>
                    </li>
                    <li>


                        <img src="img/Gestion-documentaire_livreBlanc_small.png" width="75" height="107"
                             style="border: 0px;" alt="" title=""/>


                        <div>
                            <p class="ourWhitePapers-title">Gestion documentaire</p>
                            <a href="/Ressources/Livres-blancs/Gestion-de-contenu-et-ged/Gestion-documentaire">Télécharger</a>
                        </div>
                    </li>
                    <li>


                        <img src="img/Les-CMS-open-source_livreBlanc_small.png" width="75" height="107"
                             style="border: 0px;" alt="Couverture CMS open source" title="Couverture CMS open source"/>


                        <div>
                            <p class="ourWhitePapers-title">Les CMS open source</p>
                            <a href="/Ressources/Livres-blancs/Gestion-de-contenu-et-ged/Les-cms-open-source">Télécharger</a>
                        </div>
                    </li>
                    <li>


                        <img src="img/Mobile_livreBlanc_small.jpg" width="75" height="107" style="border: 0px;" alt=""
                             title=""/>


                        <div>
                            <p class="ourWhitePapers-title">Mobile</p>
                            <a href="/Ressources/Livres-blancs/e-Business/Mobile">Télécharger</a>
                        </div>
                    </li>
                    <li>


                        <img src="img/Choisir-sa-solution-e-commerce_livreBlanc_small.jpg" width="75" height="107"
                             style="border: 0px;" alt="" title=""/>


                        <div>
                            <p class="ourWhitePapers-title">Choisir sa solution e-commerce</p>
                            <a href="/Ressources/Livres-blancs/e-Business/Choisir-sa-solution-e-commerce">Télécharger</a>
                        </div>
                    </li>
                </ul>
            </section>

        </div>


        <aside class="yui3-u-1-4">
            <section id="customerInterview">
                <div class="block-title interview__block-title">
                    <a href="/References/Temoignages-clients" title="Témoignages clients">
                        <h2 class="main-title">Témoignages</h2>
                    </a>
                </div>
                <div class="interview-item">
                    <div class="interview-item__header clearfix">


                        <img src="img/Hélène-BARTOLUCCI_temoignage_home.png" width="100" height="100"
                             style="border: 0px;" alt="" title=""/>


                        <p class="interview-item-title">
                            <a href="/References/References-par-domaine/Sante-et-scientifique/Krys2">
                                Hélène BARTOLUCCI, </a>
                            <span>Responsable Marketing Digital pour Krys</span>
                        </p>
                    </div>
                    <p class="interview-item__content">
                        <a href="/References/References-par-domaine/Sante-et-scientifique/Krys2">
                            Smile s'est occupé de la mise en place du site mobile de Krys. </a>
                    </p>
                </div>
                <div>
                    <h3>
                        <a href="/References/Temoignages-clients" title="Témoignages clients">
                            TOUS NOS TÉMOIGNAGES
                        </a>
                    </h3>
                </div>
            </section>
        </aside>

    </section>
    <div class="breaker"></div>
    <section class="lame-pub-recrutement lame-full-width">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content">
                <a href="/Recrutement" title="Rejoingnez-nous">
                    <img src="img/Smile-France.jpg"/>
                </a>
            </div>
        </div>
    </section>

@stop