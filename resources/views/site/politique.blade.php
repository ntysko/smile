@extends('layouts.frontend')

@section('content')

    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">
            <a href="#" name="page_top" id="page_top" title=""></a>
            <!--[if IE 6]><br class="clearall" /><![endif]-->

            <div class="rte_ctnt_block">

                <h3>Désolé, mais cette page n'existe plus.</h3><p>Pour poursuivre votre navigation :</p>
                <ul>

                    <li>Retournez en <a href="/" title="Smile">page d'accueil</a>
                    </li>

                </ul>

                <ul>

                    <li>Explorez nos <a href="/References">références</a>
                    </li>

                </ul>

                <ul>

                    <li>Ou découvrez nos <a href="/Ressources/Livres-blancs">livres blancs</a>
                    </li>

                </ul>
            </div>

        </div>

    </div>



    <div class="breaker"></div>


@endsection