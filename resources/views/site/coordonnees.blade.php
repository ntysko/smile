@extends('layouts.frontend')

@section('content')

    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>



        <div id="center_col">
            <div id="inner_right"></div>
            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Nos coordonnées</span>
                </div>
                <div class="coordonnees__header">
                    <h2 class="secondary-title">Nos agences</h2>

                    <p>Smile, c'est 800 collaborateurs répartis sur 16 agences dans le monde. Chaque agence vous propose toute l'expertise de Smile près de chez vous !</p><h3>Découvrez nos agences de :</h3><p><a href="/Societe/Nos-agences/Paris">Paris</a>
                        , <a href="/Societe/Nos-agences/Lyon">Lyon</a>
                        , <a href="/Societe/Nos-agences/Nantes">Nantes</a>
                        , <a href="/Societe/Nos-agences/Bordeaux">Bordeaux</a>
                        , <a href="/Societe/Nos-agences/Montpellier">Montpellier</a>
                        , <a href="/Societe/Nos-agences/Marseille">Marseille</a>
                        , <a href="/Societe/Nos-agences/Lille">Lille</a>
                        , <a href="/Societe/Nos-agences/Grenoble">Grenoble</a>
                        , <a href="/Societe/Nos-agences/Toulouse">Toulouse</a>
                        , <a href="/Societe/Nos-agences/Pays-bas">Pays bas</a>
                        , <a href="/Societe/Nos-agences/Belgique">Belgique</a>
                        , <a href="/Societe/Nos-agences/Suisse">Suisse</a>
                        , <a href="/Societe/Nos-agences/Maroc">Maroc</a>
                        , <a href="/Societe/Nos-agences/Ukraine">Ukraine</a>
                        , <a href="/Societe/Nos-agences/Russie">Russie</a>
                        , <a href="/Societe/Nos-agences/Luxembourg">Luxembourg</a>
                        et <a href="/Societe/Nos-agences/Cote-d-ivoire">Côte d’Ivoire</a>
                    </p>        </div>
                <div class="coordonnees__content">

                    <div class="coordonnees__content__map"><h3>Nos Implantations</h3>




                        <img src="/img/Carte-Smile-2015-Fond-Blanc-828x487.jpg"   width="828" height="487"  style="border: 0px;" alt="" title="" />



                    </div><div class="coordonnees__content__agences-list"><div class="coordonnees__content__agences-list__france clearfix"><h3>En France</h3><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Paris</b><br />
                                    20 rue des Jardins<br />
                                    92600 Asnières-sur-Seine<br />
                                    Téléphone : 01 41 40 11 00<br /><a href="/Societe/Nos-agences/Paris">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Lyon</b><br />
                                    107 Boulevard de Stalingrad<br />
                                    69100 Lyon Villeurbanne<br />
                                    Téléphone : 04 37 23 56 69<br /><a href="/Societe/Nos-agences/Lyon">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Grenoble</b><br />
                                    Immeuble &quot;Pulsar&quot;<br />
                                    4 Avenue du Doyen Louis Weil<br />
                                    38000 Grenoble<br />
                                    Téléphone : 04 37 23 56 69<br /><a href="/Societe/Nos-agences/Grenoble">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Toulouse</b><br />
                                    36 rue Jacques Babinet <br />
                                    31100 Toulouse<br />
                                    Téléphone : 05 33 00 51 27<br /><a href="/Societe/Nos-agences/Toulouse">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Nantes</b><br />
                                    LE COLOMBIA 32 boulevard Vincent Gache<br />
                                    44000 Nantes<br />
                                    Téléphone : 02 40 35 75 01<br /><a href="/Societe/Nos-agences/Nantes">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Bordeaux</b><br />
                                    2, boulevard Jean Jacques Bosc<br />
                                    33130 Bègles<br />
                                    Téléphone : +33 (0)5 56 87 51 18<br /><a href="/Societe/Nos-agences/Bordeaux">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Montpellier</b><br />
                                    26 Cours Gambetta<br />
                                    34000 Montpellier<br />
                                    Téléphone : 04 99 77 20 10<br /><a href="/Societe/Nos-agences/Montpellier">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Marseille</b><br />
                                    Pôle Media de la Belle de Mai<br />
                                    37/41 rue Guibal<br />
                                    13003 Marseille<br />
                                    Téléphone : 04 84 25 40 16<br /><a href="/Societe/Nos-agences/Marseille">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Agence de Lille</b><br />
                                    7 Boulevard Louis XIV<br />
                                    59800 Lille<br />
                                    Téléphone : 03 66 72 82 23<br /><a href="/Societe/Nos-agences/Lille">Détails de l'agence</a>
                                </p></div></div><div class="coordonnees__content__agences-list__europe clearfix"><h3>Dans le monde</h3><div class="coordonnees__content__agences-list__item"><p>
                                    <b><b>Smile Suisse à Genève</b> </b><br />
                                    10 avenue de Sécheron<br />
                                    1202 Genève<br />
                                    Téléphone : +41 78 603 46 66<br /><a href="/Societe/Nos-agences/Suisse">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Smile Suisse à Zurich</b> <br />
                                    98/100 Bahnhofstrasse<br />
                                    Zurich<br />
                                    Téléphone : +41 78 603 46 66<br /><a href="/Societe/Nos-agences/Suisse">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Smile Maroc à Casablanca</b> <br />
                                    7 rue Assilan (2ème étage)<br />
                                    CP 20 040 Casablanca Maroc<br />
                                    Téléphone : +212 (0)5 22 27 13 74<br /><a href="/Societe/Nos-agences/Maroc">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Smile Ukraine à Kiev</b> <br />
                                    45/2 Pushkinska str, office.32,<br />
                                    01004, Kyiv, (KIEV)<br />
                                    Ukraine<br />
                                    Téléphone : + 38 (044) 498-30-28<br /><a href="/Societe/Nos-agences/Ukraine">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Smile Russie à Moscou</b> <br />
                                    of.1-329, 5 Tkatskaya Str.<br />
                                    Moscow 105318<br />
                                    Russia<br />
                                    Téléphone : +7 968 056 33 53<br /><a href="/Societe/Nos-agences/Russie">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Neopixl à Luxembourg</b><br />
                                    115a, rue Emile Mark 92600<br />
                                    L-4620 Differdange <br />
                                    Luxembourg<br />
                                    Téléphone : (+352) 26 58 06 03<br /><a href="/Societe/Nos-agences/Luxembourg">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Smile </b><b>Pays-Bas</b> <b>à Maarsen</b><br />
                                    Bisonspoor 5003<br />
                                    3605 LV Maarssen<br />
                                    Nederland<br />
                                    Téléphone : +31 (0)20 58 16 211<br /><a href="/Societe/Nos-agences/Pays-bas">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Smile Belgique à Bruxelles</b><br />
                                    Clos Lucien Outers 11/21<br />
                                    1160 Brussels<br />
                                    Téléphone : +32 (0)2 318 20 02<br /><a href="/Societe/Nos-agences/Belgique">Détails de l'agence</a>
                                </p></div><div class="coordonnees__content__agences-list__item"><p>
                                    <b>Smile Côte d'Ivoire à Abidjan</b><br />
                                    Cocody - 2 Plateaux Vallon <br />
                                    Rue J79 Villa 159 <br />
                                    06 BP 2730 Abidjan 06<br />
                                    Téléphone : 00 225 22 51 60 10<br /><a href="/Societe/Nos-agences/Cote-d-ivoire">Détails de l'agence</a>
                                </p></div></div></div>        </div>
            </div>
        </div>

    </div>



    <div class="breaker"></div>
@endsection