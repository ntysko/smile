@extends('layouts.frontend')

@section('content')
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col" class="page_interne">

            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Mentions Légales</span>
                </div>

                <div class="rte_ctnt_block clearfix">
                    <h2>Mentions Légales</h2>
                    <div class="summary_small" >

                        <p>
                            <b>Smile - 1er intégrateur Français et Européen de solutions open source</b><br />
                            20 rue des Jardins<br />
                            92600 Asnières-sur-Seine<br />
                            Téléphone : 01 41 40 11 00<br /><a href="{{ url('http://www.smile.fr') }}">www.smile.fr</a>
                        </p><p>
                            Président du Directoire : Marc Palazon<br />
                            Le directeur de la publication : Grégory Bécue<br />Capital social : 814 314,88€</p><p>
                            <b>Coordonnées de l'hébergeur</b><br />
                            Smile Hosting - Premier hébergeur français spécialisé en infogérance de solutions open source<br />
                            20 rue des Jardins<br />
                            92600 Asnières-sur-Seine<br /><a href="{{ url('http://hosting.smile.eu') }}" target="_blank">hosting.smile.eu</a>
                        </p>					</div>

                </div>

            </div>

        </div>

    </div>



    <div class="breaker"></div>

@endsection