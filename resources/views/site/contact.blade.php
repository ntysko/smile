@extends('layouts.frontend')

@section('content')
    <div style="background: #b9d7d9;" class="lame-full-width lame-smile-carriere-banner">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <img alt="Smile recrute 250 super experts" src=/img/Contacts.jpg>
            </div>
        </div>
    </div>
    <div id="right_col">
        <div class="common_right_block" id="r_latest_news_block">
            {{ Widget::RightArticles() }}
        </div>
    </div>


    <div id="main_col">

        <div id="left_col">



            <div id="main_menu_block">

                {{ Widget::LeftNav() }}
            </div>

            <div class="breaker"></div>


        </div>


        <div id="center_col">

            <div id="inner_right"></div>


            <div id="inner_main">
                <div class="breadcrumbs_block">
                    <span class="current_page">Contacts</span>
                </div>
                <div class="contact__header">
                    <h2 class="secondary-title">Contactez-nous</h2>

                    <p>Vous souhaitez nous soumettre un projet? Déposer une candidature spontanée? Nous proposer un partenariat?</p><p><a href="/Pied-de-page/Nos-coordonnees">Vous cherchez nos coordonnées ?</a>
                    </p><p>Choisissez le sujet adapté dans le formulaire ci-dessous...</p>        </div>

                <div style="color: rgb(228, 86, 2);" class="bloc_erreur">
                    @if (count($errors)>0)
                        <div id="errorjs">
                            <ul>
                                @foreach( $errors->all() as $er)
                                    <li>{{$er}}</li>
                                @endforeach
                            </ul>
                        </div>

                    @endif
                </div>



                    {!! Form::open(['action' => 'SiteController@save','class'=>'contact__form']) !!}

                    <fieldset title="Formulaire de contact">

                        <legend>Formulaire de contact</legend>

                        <div class="input-container">
                            {!! Form::label('Subject') !!}<br>
                            {!! Form::select('subject_id', $subject, ['class'=>'box','id'=>'subject_id'] ) !!}
                        </div>
                        <div class="input-container">

                        </div>

                        <div class="input-container">
                            {!! Form::label('Name*') !!}
                            {!! Form::text('name', null, ['class'=>'box','id'=>'name'] ) !!}
                        </div>
                        <div class="input-container">
                            {!! Form::label('Surname*') !!}
                            {!! Form::text('surname', null, ['class'=>'box','id'=>'surname'] ) !!}
                        </div>
                        <div class="input-container">
                            {!! Form::label('Email*') !!}
                            {!! Form::text('email', null, ['class'=>'box','id'=>'email'] ) !!}
                        </div>

                        <div class="input-container">
                            {!! Form::label('Department (ex: 75,92)') !!}
                            {!! Form::text('department', null, ['class'=>'box','id'=>'department'] ) !!}
                        </div>

                        <div class="input-container">
                            {!! Form::label('Pays*') !!}<br>
                            {!! Form::select('pay_id', $pays, ['class'=>'box','id'=>'pay_id'] ) !!}
                        </div>

                        <div class="input-container">
                            {!! Form::label('Phone') !!}
                            {!! Form::text('phone', null, ['class'=>'box','id'=>'phone'] ) !!}
                        </div>

                        <div class="input-container">
                            {!! Form::label('Societe') !!}
                            {!! Form::text('societe', null, ['class'=>'box','id'=>'societe'] ) !!}
                        </div>
                        <div class="input-container">
                            {!! Form::label('Fonction') !!}
                            {!! Form::text('fonction', null, ['class'=>'box','id'=>'fonction'] ) !!}
                        </div>


                        <div class="form-group">
                            {!! Form::label('Massage*') !!}
                            {!! Form::textarea('massage', null, ['class'=>'box','id'=>'massage'] ) !!}
                        </div>


                        <div class="breaker"></div>
                        <div class="submit-container">
                            {!! Form::token() !!}
                            {!! Form::hidden('new',1) !!}
                            <input type="submit" name="ActionCollectInformation" class="btn--main" value="Validez" title="Validez" />
                        </div>


                    </fieldset>
                {!! Form::close()!!}




                <div class="obligatory_field_block common_listing_block">
                    <em>(*) Champ obligatoire</em>

                    <p>Conformément à l'article 27 de la loi &quot;Informatique et Libertés&quot; du 6 janvier 1978, les champs d'information que vous remplissez sont nécessaires à l'enregistrement et au traitement de votre demande. Nous ne les transmettrons pas à des tiers. Par ailleurs, vous disposez d'un droit d’accès, de modification, de rectification et de suppression des données qui vous concernent. Pour l'exercer, contactez-nous par courrier (Smile,&nbsp;&nbsp;20 rue des Jardins - 92600 Asnières-sur-Seine) ou en <a href="/Pied-de-page/Contacts">cliquant ici</a>
                        .</p><p>Merci de votre confiance.</p>
                </div>

            </div>
            <!--[if IE 6]><br class="clearall" /><![endif]-->
        </div>
        <script type="text/javascript">

            jQuery(document).ready(function(){
                $('#eZHumanCAPTCHACode').bind("click", function(e)
                {
                    imageGenerate('/redirectextension/imagegenerete');
                });
            });
            function imageGenerate(uri){
                empty = '';
                $.ajax({
                    url: uri,
                    success: function(result){

                        $('#captchaimg').attr('src',result) ;
                        $('#captchaimg').show();
                    },
                    error: function(){
                        /* $('#loading').hide();*/
                        $('#captchaimg').text();
                    }
                }) ;
            }

        </script>

    </div>


    <div class="breaker"></div>
    <div class="lame-full-width lame-smile-3-chiffres animated">
        <div class="lame-full-width__container">
            <div class="lame-full-width__content cf">
                <h2 class="lame-smile-3-chiffres__title">
                    <span class="lame-smile-3-chiffres__title__primary">Smile</span>
                    <span class="lame-smile-3-chiffres__title__secondary">en 3 chiffres</span>
                </h2>
                <div class="lame-smile-3-chiffres__item lame-smile-3-chiffres__item--cac">
                    <div class="lame-smile-3-chiffres__visual animated">
                        <div class="cake-container">
                            <div class="cake-no-progress">

                            </div>
                            <div class="cake-progress">
                                <div class="cake-progress__fill__1"></div>
                                <div class="cake-progress__fill__2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="lame-smile-3-chiffres__caption">
	                    <span class="lame-smile-3-chiffres__caption__number">
	                        70<span class="exposant">%</span>
	                    </span>
	                    <span class="lame-smile-3-chiffres__caption__text">
	                       du cac 40 nous fait confiance
	                    </span>
                    </div>
                </div>
                <div class="lame-smile-3-chiffres__item lame-smile-3-chiffres__item--croissance">
                    <div class="lame-smile-3-chiffres__visual animated">
                        <div class="bar bar--1"></div>
                        <div class="bar bar--2"></div>
                        <div class="bar bar--3"></div>
                    </div>
                    <div class="lame-smile-3-chiffres__caption">
	                    <span class="lame-smile-3-chiffres__caption__number">
	                        22<span class="exposant">%</span>
	                    </span>
	                    <span class="lame-smile-3-chiffres__caption__text">
	                        de croissance par an depuis 5 ans
	                    </span>
                    </div>
                </div>
                <div class="lame-smile-3-chiffres__item lame-smile-3-chiffres__item--agence">
                    <div class="lame-smile-3-chiffres__visual animated">

                    </div>
                    <div class="lame-smile-3-chiffres__caption">
	                    <span class="lame-smile-3-chiffres__caption__number">
	                        18
	                    </span>
	                    <span class="lame-smile-3-chiffres__caption__text">
	                        <span class="highlight">agences</span> en Europe
	                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breaker"></div>

@endsection